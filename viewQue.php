<html>
<head>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<?php
        // @file connect to the database.
        include('classConnectQA.php');       
        if($mysqli->errno)
		{
			echo "Data Base Error";
		}
		else
		{						
			$iTestid=Null;			
			if(isset($_GET['id']))
			{
				$iTestid=$_GET['id'];
			}	

			$sTimerQuery = "select test_duration,test_name, test_neg_status from test_detail where test_id= $iTestid";
			$iResTimerQuery = $mysqli->query($sTimerQuery);
			
			$aRowForTimer = $iResTimerQuery->fetch_row();
			
			$sQuery = "select a.que_question, a.que_id , a.test_id , a.que_type , a.que_marks ,b.test_name, a.que_neg_marks, a.que_ans_desc from question_table as a , test_detail as b where b.test_id = a.test_id AND b.test_id = '$iTestid'";
			$iResult = $mysqli->query($sQuery); 
			$iQue_Count = $iResult->num_rows;
			
			
			?>
			<div class="class_form_div_quiz" id="id_form_div_addTest">							
				<div id="idDivFname" class="classDivTabForDescrAns">	
					<div class="classDivBodyFormat">
			<?php //! @brif this function figure out the total pages in the database
			if($iQue_Count >= 1)
			{
				echo "<span class='classNoDecoration'>Question <span class='classSpanTabLink'><a href='addQuestionHTML.php?id=$iTestid'><i class='icon-plus-sign'></i> Add </a></span>								                      							
					   </span><br/>";		
				$ii=1;
				while($aRow = $iResult->fetch_row())
				{
					echo "<div class='classLineHeight'>";
					//! display data in table
					$iQue_id = $aRow[1];
					$iTest_id = $aRow[2];				
					$sQueType = $aRow[3];
					$iQueMarks = $aRow[4];
					
					if($aRow[6]==0 || $aRow[6]==null)
					{
						$iNegMeksPerQue = 0;
					}
					else
					{
						$iNegMeksPerQue = $aRow[6];						
					}

			          
			            /* end of - 1 */	
			            if($aRowForTimer[2]==1)
			            {			            	
			            	echo "<span class='classNoDecoration'>{$ii} ] </span> <span class='classSpanDisplayQue'>{$aRow[0]}&nbsp;&nbsp;&nbsp;&nbsp;</span><span id='idSpanDisplayMarks' class='classSpanDiaplayMarks'>[ Marks : {$iQueMarks} | Negative Mark : {$iNegMeksPerQue} ]</span></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class=classSpanSmallWhite><a rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Edit Question' href='editQuestion.php?id={$iTest_id}&qid={$iQue_id}'><i class='icon-pencil'></i> Edit</a></span>";
			            	 
			            	
			            }			
			            else
			            {
			            	echo "<span class='classNoDecoration'>{$ii} ] </span> <span class='classSpanDisplayQue'>{$aRow[0]}&nbsp;&nbsp;&nbsp;&nbsp;</span><span id='idSpanDisplayMarks' class='classSpanDiaplayMarks'>[ Marks : {$iQueMarks} ]</span></span>&nbsp;&nbsp;&nbsp;&nbsp;<span class=classSpanSmallWhite><a rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Edit Question' href='editQuestion.php?id={$iTest_id}&qid={$iQue_id}'><i class='icon-pencil'></i> Edit</a></span>";			            	
			            	
			            }
						echo "&nbsp;&nbsp;&nbsp;&nbsp;<span class=classSpanSmallWhite><a rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Delete Question' href='deleteQue.php?id={$iTest_id}&qid={$iQue_id}'><i class='icon-trash'></i> Delete</a></span>";
						if($aRow[7] != ''){
							echo "<span class=''><pre>{$aRow[7]}</pre></span>";
						}
					$sQueryQtpFromImg = "select * from question_attachment as a where a.que_id = {$iQue_id} AND file_status = 1";
			                                        /* 1] this query return af question have any attachment */     
			            $iResultAttachment = $mysqli->query($sQueryQtpFromImg);
			            if($iResultAttachment == true)
			            {
				            $aFetchRowAttachment = $iResultAttachment->fetch_row();
				             $sQtpInAttachment = $aFetchRowAttachment[2];
				            $sAttachLocation = $aFetchRowAttachment[3];
				            $sAttachMentName = $aFetchRowAttachment[4];
				            if($iQue_id == $aFetchRowAttachment[1])
				            {
				            	echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'><a href='$sAttachLocation$sAttachMentName'><img src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></a></div>";
	                      	}
			            }
					echo "<table class='classPreviewQuestionTable'><tr></tr>";
					$ij='A';
					if($sQueType=="Objective")
					{
						
						$sQuerySel = "select a.op_id as op_id, a.op_option as opt, a.op_correct_ans as cra from option_table as a ,question_table as b , test_detail as c 
						where c.test_id = b.test_id And b.que_id = a.que_id AND c.test_id ='$iTestid' AND b.que_id ='$iQue_id' order by a.op_id";
									
						$iResult2 = $mysqli->query($sQuerySel);
						while($aRowOpt = $iResult2->fetch_row())
						{
							$iOpidAns = $aRowOpt[0];					  
							echo "<tr>";
							echo "<td width=10px> </td><td width=20px> $ij. </td>";	
							if($aRowOpt[2]==1)
							{
								echo "<td>" . $aRowOpt[1] . "&nbsp;&nbsp;&nbsp;&nbsp;<img src='images/right.jpg' width='18px' height='18px'></td>";

							}	
							else
							{
								echo "<td>" . $aRowOpt[1] . "</td>";	
							}				  
							
							echo "</tr>";	
							$ij++;
						}
					}


					if($sQueType=="Multiple")
					{

						$sQuerySel = "select a.multi_op_id as op_id, a.multi_option as opt, a.multi_op_ans as cra from multi_option_table as a ,question_table as b , test_detail as c 
						where c.test_id = b.test_id And b.que_id = a.que_id AND c.test_id ={$iTestid} AND b.que_id ={$iQue_id} order by a.multi_op_id";											
						$iResult2 = $mysqli->query($sQuerySel);

						while($aRowOpt = $iResult2->fetch_row())
						{
							$iOpidAns = $aRowOpt[0];					  
							echo "<tr>";
							echo "<td width=10px> </td><td width=20px> $ij. </td>";	
							if($aRowOpt[2]==1)
							{
								echo "<td>" . $aRowOpt[1] . "&nbsp;&nbsp;&nbsp;&nbsp;<img src='images/right.jpg' width='18px' height='18px'></td>";

							}	
							else
							{
								echo "<td>" . $aRowOpt[1] . "</td>";	
							}				  
							
							echo "</tr>";	
							$ij++;
						}	
					}
					
				/*
					$sQueryAns = "select op_id from option_table where op_correct_ans = 1 and que_id = $iQue_id";
					$iResult3 = $mysqli->query($sQueryAns);
					$aRowForAns = $iResult3->fetch_row();					*/
					echo "</table><br/><br/>";
					echo "</div>";					
					$ii++;
				}
				echo "<div class='classDivAddTestResponce classNoDecoration' id='idTestAQue'><a href='addQuestionHTML.php?id={$iTestid}'><input id='idAddQuestion' class='btn btn-primary classAddTestButtonSize' type='button' value='Add Question' name='ViewTest' /></a></div>";
			}
			else
			{
				echo "<div class='classDivAddTestResponce classNoDecoration' id='idStartTestAQue'><a href='addQuestionHTML.php?id={$iTestid}'><input id='idStartAddQuestion' class='btn btn-primary classAddTestButtonSize' type='button' value='Add Question' name='ViewTest' /></a></div>";
			}
		        //  !close database connection
	        $mysqli->close();
		}
?>

<input type="hidden" name="totalQue" value="<?php echo $iQue_Count; ?>">
</div>
</div>
</div>
</body>
</html>