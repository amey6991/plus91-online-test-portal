<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Thank You!</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<?php 

    include('classConnectQA.php');
    
        $iTestId=$_GET['id'];
        if (isset($_GET['msg']))
            {
                 $iMsg = $_GET['msg'];//! This Receive value from url.
            }
            else
            {
                 $iMsg = "";
            }
        if (isset($_GET['qun']))
            {
                 $iTotalQue = $_GET['qun'];//! This Receive value from url.
            }
            else
            {
                 $iTotalQue = "";
            }
        session_start();
        $iUserId = $_SESSION['user_id'];
        if(isset($_SESSION['lid']))     // This is Use to check a Session
        {

        }
        else
        {
            header("location:index.php");
        }
        //var_dump($_POST);exit;
        
        $sQueryForResultType = "select test_result_type, test_msg from test_detail where test_id = $iTestId";
        $iResultForResultType = $mysqli->query($sQueryForResultType);
        $aRowForResultType = $iResultForResultType->fetch_row();
        
            $ut=$_SESSION['ut'];
            $iLoginId = $_SESSION['lid'];
            $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                from login as a , user_details as b
                                where a.login_id = b.login_id 
                                AND a.login_id  = '$iLoginId' limit 1";
            $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
            $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        
 ?>
<div id="id_header_wrapper">
  <div id="id_header">
    
    <div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
    </div>
        

        <div id="id_menu">
            <div id="id_menu_left">
                <div id="idDivUserNameTop" class="classDivTopMenuUser">
                    <?php 
                    echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                            <ul id='menu'>
                            <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                <ul>
                                    <li>
                                        <a href='profile.php'>Profile</a>       
                                    </li>
                                    <li>
                                        <a href='profileedit.php'>Update Profile</a>            
                                    </li>
                                    <li>
                                        <a href='changePassword.php'>Change Password</a>            
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href='manageTest.php'>Home</a>       
                            </li>
                            <li>
                                <a href='showOpportunity.php'>Opportunity</a>
                            </li>
                            <li>
                                <a href='displayStudentResult.php'>Result</a>       
                            </li>
                            <li>
                                <a href='logout.php'>Logout </a>    
                            </li>
                            </ul>
                            </div>";
                    ?>
                </div>
            </div>      
        </div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
    <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
    <div id="id_banner">
        <!--<div id="id_banner_content">
            <div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
    </div> <!-- end of banner -->

</div> <!-- end of banner wrapper -->
<div id="id_content_wrapper">
    <div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
                <div id="idDivSignUp" class="header_0345"></div>
            </div>
            <?php 

                    if($iMsg = 123)
                    {
                        $sResMsg = "";                      
                    }
                    if($aRowForResultType[1] != Null)            // If Message is set by the admin.
                    {
                        $sMsgForResult=$aRowForResultType[1];
                    }
                    else                                                      // If Message not set by the admin then show default. 
                    {
                        $sMsgForResult="Test Submitted Successfully. Thank You !";
                    }
                    
                    echo"<div id='idDivDesg7' class='classDivDisplayPendingResult'>$sMsgForResult
                    <strong></strong><br/><br/><br/>$sResMsg</div>";

            ?>
                <br/>
                <div id="idDivMob" class="classDivAddTest">
                    <span id="idSpanTabMobIP" class="classSpanTabText">
                    
                    </span>
                </div>
        </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>