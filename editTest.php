<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!--Calender script -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="js/jquery-1.9.1.js"></script>
<!--Calender script -->
<!-- script for text counter in text area -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
      
    var characterLimit = 500;  
      
    google.setOnLoadCallback(function(){  
          
        $('#remainingCharacters').html(characterLimit);  
          
        $('#idclassTADis').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
              
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            }  
              
            var charactersRemaining = characterLimit - charactersUsed;  
              
            $('#remainingCharacters').html(charactersRemaining);  
        });  
    });  
</script>
<!-- end of script for text counter in text area -->

<!-- Script start to POST For data using simple button -- >
<script>  
 function submitform()
    {
      document.QuizDisplay.submit();
    }
</script>
<!-- Script end to POST For data using simple button -- >

<!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
<script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idFormEdit").validationEngine();
        });
			/*end of javascript to validate the textbox*/
		
		/* start of Script For datepicker */
		
		$(document).ready(function() {
		// get the current date
		var date = new Date();
		var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
		// Disable all dates till today
		$('#idDatePicFrom').datepicker({
		minDate: new Date(y, m, d),
		dateFormat: "yy-mm-dd",
		});
		$('#idDatePickTill').datepicker({
		minDate: new Date(y, m, d),
		dateFormat: "yy-mm-dd",
		});
		});
		/* end of Script For datepicker */
</script>
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body>
	<?php 

		session_start();
		include ('classConnectQA.php');	
		
		if(isset($_SESSION['user_id']))
		{
			$iUid=$_SESSION['user_id'];
		}
		else
		{
			$iUid="";
		}
		if(isset($_SESSION['lid']))		// This is Use to check a Session
		{
			$iLoginId = $_SESSION['lid'];
		}
		else
		{
			header("location:index.php");
		}
			
		if(isset($_SESSION['ut']))  
            {
                $ut=$_SESSION['ut'];
            }
            else
            {
            	$ut=null;	
            } 
		$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
							from login as a , user_details as b
							where a.login_id = b.login_id 
							AND a.login_id  = '$iLoginId' limit 1";
		$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
		$aRowForUserInfo = $iResultForUserInfo->fetch_row();

	?>
<div id="id_header_wrapper">
  <div id="id_header">
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
				<?php

					if($ut==0||$ut==2)
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
									<ul id='menu'>
									<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
									</li>
									<li>
										<a href='manageTest.php'>Home</a>		
									</li>";
									
					
						if($ut==2)
						{
							echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
						}
						else
						{
							echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
								<ul>
									<li>
									<a href='groupHTML.php'>Create Group</a>		
									</li>
									<li>
										<a href='addTestHTML.php'>Create Test</a>		
									</li>
									<li>
										<a href='addUserHTML.php'>Create User</a>			
									</li>
									<li>
										<a href='excelReader/index.php'>Bulk Upload</a>			
									</li>
								</ul>
							</li>";
						} 
						
						echo "<li>
									<a>Manage </a>  
									  <ul>
									  		<li>
												<a href='manageGroup.php'>Manage Group</a>			
											</li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
									</ul>	
								</li>
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";
					}	
					else
					{
						if($ut==2)
						{

						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
								<ul id='menu'>

								</li>
								<li>
									<a href='manageTest.php'>Home</a>		
								</li>								
								<li>
									<a href='manageUser.php'>Manage User</a>	
								</li>
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";

						}
						else
						{
							echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
								<ul id='menu'>
								<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
								</li>
								<li>
									<a href='manageTest.php'>Home</a>		
								</li>
								<li>
									<a href='showOpportunity.php'>Opportunity</a>
	                            </li>
								<li>
	                            	<a href='displayStudentResult.php'>Result</a>       
	                        	</li>
								
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";	
						}
						
					}
				?>		
				
				</div>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
			
        </div>-->  
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php
				
// @file connect to the database.
include('classConnectQA.php');
if($mysqli->errno)
{
	//echo "DB Error";
	header('location: manageTest.php?msg=-1');
}
else
{				
	$iTestid=Null;
	$iMsg=Null;
	$iGid=Null;
	if(isset($_GET['id']))	
    {
    	$iTestid=$_GET['id'];
    }
	if(isset($_GET['gid']))	
    {
    	$iGid=$_GET['gid'];
    }
    if(isset($_GET['msg']))
    {
    	$iMsg=$_GET['msg'];
    	echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
    	if(isset($_GET['msg']))
		{
			if($_GET['msg']==1)
			{
				echo "<div class=classMsg>Test Updated Successfully</div>";
			}
			if($_GET['msg']==0)
			{
				echo "<div class=classMsg >Try again: Test not Updated</div>";
			}
			if($_GET['msg']==-1)
			{
				echo "<div class=classMsg >DB Error in Update Test</div>";
			}
			if($_GET['msg']==-2)
			{
				echo "<div class=classMsg >Mendatory fields must be filled</div>";
			}

			if($_GET['msg']==-123)
			{
				echo "<div class=classMsg >Try again, Question not deleted</div>";
			}
			if($_GET['msg']==-12)
			{
				echo "<div class=classMsg >DB Error in deleting question</div>";
			}
			if($_GET['msg']==123)
			{
				echo "<div class=classMsg >Question deleted successfully</div>";
			}
		}
    	echo "</div>";
    }
	
	

	$sTestQuery = "select test_duration,test_name,test_code,test_from,test_to,test_subject,test_desc, group_id, test_neg_status, test_Msg, test_scope from test_detail where test_id= {$iTestid}";
	$iResTestQuery = $mysqli->query($sTestQuery);
	$aRowTest = $iResTestQuery->fetch_row();
	$sQuery = "select * from question_table where test_id = {$iTestid}";
	$iResult = $mysqli->query($sQuery); 
	$iQue_Count = $iResult->num_rows;
}	
?>
<form action="updateTest.php?id=<?php echo $iTestid; ?>" id="idFormEdit" method="POST" name="QuizDisplay" >
  <div id="id_content_wrapper">
	<div id="id_content">
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">
				<div id="idDivSignUp" class="header_0345">

					<div id="idDivEditTest" class="classDivEditTest">Test Name<span class="classRed">*</span> :-<input type="text" class = "classUserDtInput classFont validate[required] text-input"name="testName" value="<?php echo $aRowTest[1]; ?>"/>

					</div>
				</div>
				<div class="classHorizHRSubHead"></div>
			</div>
	
				<div class="class_form_div_quiz" id="id_form_div_addTest">

					<div id="idDivTabForDescrAns1" class="classDivTabForDescrAns classDivBodyFormat">	
						
							<input type="hidden" name="testId" value="<?php echo $iTestid; ?>"/>
							
							<div id="idDivNat" class="classDivAddTest">
			                    <span id="" class="classSpanAddTestDesc">Test Subject <span class="classRed">*</span></span>
			                    <span id=":" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP">
			                      <select id="" class="validate[required] classUserDtInputSelect" name="testSubject">
									<option value="">Subject</option>			                              
			                              <?php	
			                               subject($aRowTest[5]);                           
				                            function subject($subChecked)	// Function is use to display the Test Subject
				                            {
				                                $aSub=array("English", "Maths", "Reasoning", "Aptitude", "Computer", "Common");  
				                                foreach ($aSub as $sValue) {
				                                	if($subChecked==$sValue)
				                                	{
				                                		echo "<option value=$sValue selected=selected>$sValue</option>";
				                                	}
				                                	else
				                                	{
				                                		echo "<option value=$sValue>$sValue</option>";	
				                                	}
				                                    
				                                }
				                            }
				                           
			                            ?>
										 
			                        </select>  
			                    </span>
			                     <span id="" class="classSpanAddTestDesc">Test Code</span>
			                    <span id="" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP">
			                      <input type="text" name="testCode" value="<?php echo $aRowTest[2]; ?>" class="classUserDtInput classInputBackColor" disabled="disabled"/>
			                    </span>
 								
 							</div>

 							<?php
			                    $sGQuery="select * from quiz_online.group_table where group_status=1";                                             
			                    $bGResult=$mysqli->query($sGQuery);                   
			                    echo "<div id='idDivGroup' class='classDivAddTest'>
			                            <span id='idSpanTabGroup' class='classSpanAddTestDesc'>Group <span class='classRed'>*</span></span>
			                            <span id='idSpanTabGroup:' class='classSpanTabCol'>:</span>
			                            <span id='idSpanTabGroupIP' class='classSpanTabIP'>
			                               <select id='id_TestAddGroup' class='classUserDtInputSelect validate[required]' name='testGroup'>";   
			                                   
			                                   $iGFlag=0;
			                                   if($bGResult==True)
			                                    {
			                                        $iGFlag=1;
			                                        while($aGRow=$bGResult->fetch_row())
			                                        {
			                                            $iGFlag=2;
			                                            if($aRowTest[7]===$aGRow[0])
			                                            {
			                                            	echo "<option value={$aGRow[0]} selected=selected>{$aGRow[1]}</option>";	
			                                            }
			                                            else
			                                            {
			                                            	echo "<option value={$aGRow[0]} >{$aGRow[1]}</option>";		
			                                            }
			                                        }			                                           
			                                    }
			                                    else
			                                    {
			                                        echo "<option value=''>Error in Group</option>";
			                                    }
			                    echo "</select></span>";


			                 ?> 

			                 	<span id="idSpanTabMob" class="classSpanAddTestDesc">Time <span class="classRed">*</span></span> 
 								<span id="idSpanTabMob:" class="classSpanTabCol">:</span>
			                    <span id="idSpanTabMobIP" class="classSpanTabIP">
		                        <span class="input-prepend">
		                        <span class="add-on">
		                        <i class="icon-time"></i>
		                        </span>
 								<input type="text" class="classContactNoUpProf classFont validate[required,custom[integer]] text-input" name="testDuration" value="<?php echo $aRowTest[0]; ?>" /> 						
			                    </span>&nbsp;min
			                    </div>
 							<div id="idDivTotQue" class="classDivAddTest">
 								<span id="" class="classSpanAddTestDesc">Total Questions</span>
 								<span id=":" class="classSpanTabCol">:</span>
 								<span id="" class="classSpanTabIP"><?php echo $iQue_Count; ?></span>
	 							
	 							<span id="" class="classSpanAddTestDesc">Test Scope</span>
	 							<span id=":" class="classSpanTabCol">:</span>
	 							<span id="" class="classSpanTabIP">
 							<?php

 								if($aRowTest[10]==2)

 								{ 	 									
 									echo "<input type='checkbox' name='testScope' id='idTestScope' class='classTestScope' value='2' checked />&nbsp;Public";	
 								}
 								else
 								{
 									echo "<input type='checkbox' name='testScope' id = 'idTestScope' class='classTestScope' value='2' />&nbsp;Public";	
 								}

 								echo "<span id='idSpanTabCommonGroup' class='classSmallFontForOpp'>( This make test common,for all user. )</span>";
 							?>	 								 
 								</span>
 							</div>
 							<div id="idDivNat" class="classDivAddTest">
								<span id="" class="classSpanAddTestDesc">Test Valid From <span class="classRed">*</span></span>
			                    <span id=":" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP">
		                    	<span class="input-prepend">
		                        <span class="add-on">
		                        <i class="icon-calendar"></i>
		                        </span>
			                      <input class="classContactNoUpProf validate[custom[date]] text-input" type="text" name="testValidFrom"  id = "idDatePicFrom" value="<?php echo $aRowTest[3]; ?>"/>
			                    </span>
			                    </span>

 								<span id="idSpanTabNeg" class="classSpanAddTestDesc">Negative Marks </span> 
 								<span id="idSpanTabNeg:" class="classSpanTabCol">:</span>
			                    <span id="idSpanTabNegIP" class="classSpanTabIP">
			                    	<?php 
			                    		if($aRowTest[8]== 1)
			                    		{
			                    			echo "<input type='checkbox' name='testNegMarksOff' id = 'idNegMarksOff' class='classNegMarks' value='1' checked='checked'>&nbsp;Applicable";	
			                    		}
			                    		else
			                    		{
			                    			echo "<input type='checkbox' name='testNegMarksOff' id = 'idNegMarksOff' class='classNegMarks' value='1'>&nbsp;Applicable";		
			                    		}
			                    	?>
 								
 								 </span>

			                     
			                </div>

			                <div id="idDivNat" class="classDivAddTest">
			                     <span id="" class="classSpanAddTestDesc">Test Valid Till <span class="classRed">*</span></span>
			                    <span id=":" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP">
		                    	<span class="input-prepend">
		                        <span class="add-on">
		                        <i class="icon-calendar"></i>
		                        </span>
			                      <input type="text" name="testValidTo" class="classContactNoUpProf validate[custom[date]] text-input" id = "idDatePickTill" value="<?php echo $aRowTest[4]; ?>"/>
			                    </span>
			                    </span>	
						        <span id="idSpanTabMob" class="classSpanAddTestDesc">Test Description</span>
			                    <span id="idSpanTabMob:" class="classSpanTabCol">:</span>
			                    <span id="idSpanTabMobIP" class="classSpanTabIP">
			                        <textarea id="idclassTADis" class="classSpanUserAddress classTADis classInputBackColor" name="testDescription"><?php echo $aRowTest[6];?></textarea>
			                    </span>
			                </div>
			                <div id="idDivTMsg" class="classDivAddTest">
			                    <span id="idSpanTabTMsg" class="classSpanAddTestDesc">Message for User</span>
			                    <span id="idSpanTabTMsg:" class="classSpanTabCol">:</span>
			                    <span id="idSpanTabTMsgIP" class="classSpanTabIP">
			                        <input type="text" id="idTAMsg" class="classUserDtInput" name="testMsg" value="<?php echo $aRowTest[9];?>"/>
			                    </span>
			                </div>
			                <div id="idDivNat" class="classDivAddTest">
			                	<br/>
			                     <br/>
			                     <span class="classRed">*</span> Fields are Mandatory.
			                    <span id="" class="classSpanTabIP classNoDecoration">
			                      <input type="submit" name="SubmitTest" value="Update Test"  class="btn btn-primary classAddTestButtonSize"/>
								
									<a href="manageTest.php?id=<?php echo $iTestid; ?>"><input id="idTestEditCancel" class="btn btn-primary classAddTestButtonSize" type="button" value="Back" name="backBut"/></a>
								</span>
			                </div>
			                </br>
               		</div>
				</div>
				<hr>
				 <?php include('viewQue.php'); ?>	
			 <div id="idDivNat" class="classDivTime">
                    <div id="idDivTq" class="classDivRight"></div>	
					<div id="idTT" class="classDivRight"></div>
					
             </div>
			</div>
		</div> <!-- end of content wrapper -->
	</div> <!-- end of content wrapper -->
  </form>
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About Us</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>