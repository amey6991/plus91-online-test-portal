<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Test Question's</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- script for text counter in text area -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
      
    var characterLimit = 500;  
      
    google.setOnLoadCallback(function(){  
          
        $('#remainingCharacters').html(characterLimit);  
          
        $('#idAddQuestion').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
              
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            }      
        });  
    });  
</script>
<!-- end of script for text counter in text area -->
<!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#id_AddQuestion_form").validationEngine();
        });
    </script>               
<!--end of javascript to validate the textbox -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body onload="setOption()">
<div id="id_header_wrapper">
  <div id="id_header">
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<?php
			/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@gid: is use for Group Id.
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status 

			session_start();
			*/
			session_start();
			include ('classConnectQA.php');	
			
			if(isset($_SESSION['lid']))		// This is Use to check a Session
			{

			}
			else
			{
				header("location:index.php");
			}
			$iUid=null;
			$iLoginId=null;
			if(isset($_SESSION['user_id']))
			{
				$iUid=$_SESSION['user_id'];	
			}
			if(isset($_SESSION['lid']))
			{
				$iLoginId = $_SESSION['lid'];
			}
			 if(isset($_SESSION['ut']))  
            {
                $ut=$_SESSION['ut'];
            }
            else
            {
            	$ut=null;	
            } 
			$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();
			$tid=$_GET['id'];
			$iMsg=Null;
			$iGid=Null;
			if(isset($_GET['id']))	
			{
				$iTestid=$_GET['id'];
			}
			echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
					<ul id='menu'>
					<li><a href='profile.php'>$aRowForUserInfo[2]</a>
						<ul>
                        <li>
                            <a href='profile.php'>Profile</a>       
                        </li>
                        <li>
                            <a href='profileedit.php'>Update Profile</a>            
                        </li>
                        <li>
                            <a href='changePassword.php'>Change Password</a>            
                        </li>
                    </ul>
					</li>
					<li>
						<a href='manageTest.php'>Home</a>		
					</li>";
					
					if($ut==2)
					{
						echo " <li>
                                    <a href='showOpportunity.php'>Opportunity</a>            
                                </li>";
					}
					else
					{
						echo "<li><a href='opportunityHTML.php'>Opportunity</a>
                                <ul>
                                <li>
                                    <a href='opportunityHTML.php'>Create</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Manage</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
							<a>Create</a>
							<ul>
								<li>
									<a href='groupHTML.php'>Create Group</a>		
								</li>
								<li>
									<a href='addTestHTML.php'>Create Test</a>				
								</li>
								<li>
									<a href='addUserHTML.php'>Create User</a>			
								</li>
								<li>
									<a href='excelReader/index.php'>Bulk Upload</a>			
								</li>
							</ul>
						</li>";
					} 
					
					echo "<li>
							<a>Manage </a>  
							  <ul>	
						  		<li>
										<a href='manageGroup.php'>Manage Group</a>			
								</li>
								<li>
									<a href='manageUser.php'>Manage User</a>			
								</li>
							  <li>
								<a href='viewAllotedTestHTML.php'>Assign Test</a>     
							  </li>
							</ul>	
						</li>
						<li>
							<a href='logout.php'>Logout </a>	
						</li>
						</ul>
						</div>";
			?>		
				
			</div>  	
		</div> <!-- end of menu -->
    </div>
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
if(isset($_GET['msg']))
	{
		$iMsg=$_GET['msg'];
		echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
		if($_GET['msg']==1)
		{
			echo "<div class=classMsg >Question added successfully</div>";
		}
		if($_GET['msg']==0)
		{
			echo "<div class=classMsg >Try again: Question not added</div>";
		}
		if($_GET['msg']==-1)
		{
			echo "<div class=classMsg >DB Error in adding question</div>";
		}
		if($_GET['msg']==-2)
		{
			echo "<div class=classMsg >Mendatory fields must be filled</div>";
		}
		if($_GET['msg']==-3)
		{
			echo "<div class=classMsg >Question not added, Because blank option is chosen as a answer.</div>";
		}
		if($_GET['msg']==4512)
		{
			echo "<div class=classMsg >Attachment added.</div>";
		}
		if($_GET['msg']==4511)
		{
			echo "<div class=classMsg >Attachment removed.</div>";
		}
		echo "</div>";
	}
	else
	{
		$iMsg ="";
	}

?>		
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
			<form action="addQuestion.php" id="id_AddQuestion_form" method="POST" name="addQuestionForm" onsubmit="return validateForm()">

				<input type="hidden" name="hiddenTestId" value="<?php echo $iTestid; ?>">				
				<div id="idDivSignUp" class="header_0345">Add Question
					<?php
									
						$iQueNo=null;
						$sQueryGetQuesID = "select max(que_id) from question_table";
						$iResultGetQueID = $mysqli->query($sQueryGetQuesID);
						$aFetcgQueId = $iResultGetQueID->fetch_row();
						$iQueID = $aFetcgQueId[0];
						$iQueID++;
						$sQueryForQueNo="select count(que_id) from question_table where test_id={$iTestid}";
						if($sCountQue=$mysqli->query($sQueryForQueNo))
						{
							$iQueRow=$sCountQue->fetch_row();							
							$iQueNo=$iQueRow[0];
							$iQueNo++;
						}
						echo " {$iQueNo} ";
						$sTestName=$mysqli->query("select test_name, test_neg_status from test_detail where test_id={$iTestid}");
						if($sTestName!=null)
						{
							$aTestRow=$sTestName->fetch_row();
							echo " to :  {$aTestRow[0]}";	
							$negMarksOff = 	$aTestRow[1];						
						}
						//To Display the msg send by the called page.
						
					?>	
				</div>
				<div class="classHorizHRSubHead"></div>
				
				<div id="idDivAddQuestion" class="classDivAddQuestionOnly">
                <div id="idDivQue" class="classDivAddTest">
                    <span id="idSpanTabQue" class="classSpanAddTestDesc">Question <span class="classRed">*</span></span>
                    <span id="idSpanTabQue:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQueIP" class="classSpanTabIP">
                    	<textarea name="queName" id = "idQuestionDetel" class="classTADescription classInputBackColor validate[required] text-input"></textarea>                        
                    </span>
                </div>
               <script type='text/javascript' language='javascript'>  
				    google.load('jquery', '1.4.2');  
				      
				    var characterLimit = 500;  
				      
				    google.setOnLoadCallback(function(){  
				          
				        $('#remainingCharacters').html(characterLimit);  
				          
				        $('#idQuestionDetel').bind('keyup', function(){  
				            var charactersUsed = $(this).val().length;  
				              
				            if(charactersUsed > characterLimit){  
				                charactersUsed = characterLimit;  
				                $(this).val($(this).val().substr(0, characterLimit));  
				                $(this).scrollTop($(this)[0].scrollHeight);  
				            }  
  
				        });  
				    });  
				</script>
			   <div id="idDivQueMark" class="classDivAddTest">
                    <span id="idSpanTabQueMark" class="classSpanAddTestDesc">Question Marks <span class="classRed">*</span></span>
                    <span id="idSpanTabQueMark:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQueMarkIP" class="classSpanTabIP">
                        <input type="text" name="queMarks" id = "" size="35" class="classUserDtInput classInputBackColor class_login_txt validate[required,custom[integer]] text-input" value="1">
                    </span>
                </div>
				
				<div id="idDivNegMark" class="classDivAddTest">
                    <span id="idSpanTabNegMark" class="classSpanAddTestDesc">Negative Marks <span class="classRed">*</span></span>
                    <span id="idSpanTabNegMark:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabNegMarkIP" class="classSpanTabIP">
                        <input type="text" name="negMarks" id = "idNegMarks" size="35" class="classUserDtInput classInputBackColor validate[required,custom[integer]] text-input" value="0">
                   		<input type="hidden" name="negMarksOff" id="idNegMarksOff" class="classNegMarksOff" value="<?php echo $negMarksOff; ?>" />
                    </span>
                </div>
				
				
                <div id="idDivQtype" class="classDivAddTest">
                    <span id="idSpanTabQtype" class="classSpanAddTestDesc">Question Type<span class="classRed">*</span></span>
                    <span id="idSpanTabQtype:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQtypeIP" class="classSpanTabIP">
					<select id="id_TestQtype" onchange="setOption(this)" class="classUserDtInputSelect class_light_black classFont" name="queType">
					
						<option value="Objective">
                              Objective
                         </option><option value="Descriptive">
                              Descriptive
                            </option>
                            <option value="Multiple">Multiple Choice</option>
                        </select> </span> 
				</div>
                        
				<script type="text/javascript"> 
				
					function setOption()
					{	
						var e = document.getElementById("id_TestQtype");
						var neg = document.getElementById("idNegMarksOff").value;
						var temp = e.options[e.selectedIndex].value;											
						if(temp == "Objective" && neg == 1)
						{
							document.getElementById('idNegMarks').disabled=false;
							document.getElementById('idNegMarks').value=0;
							
						}					
						else
						{
							document.getElementById('idNegMarks').disabled=true;
							document.getElementById('idNegMarks').value='';
						}						
					
						if(e.options[e.selectedIndex].value  == "Descriptive")	// When Question Type is of Descriptive.
						{	
							document.getElementById('idDivAddOp').style.visibility = 'hidden';							
							//following code is use to disable the option when question type is descriptive.
							var el = document.getElementById('idDivAddOp'),
					        all = el.getElementsByTagName('input'),ii;					        
						    for (ii = 0; ii < all.length; ii++) 
						    {
						        all[ii].disabled = true;
						         all[ii].style.visibility='hidden';
						    }	

						}
						else 
						{	
							if(e.options[e.selectedIndex].value  == "Multiple")				// When Question Type is of Multiple Choice.
							{								
								//document.getElementById('idNegMarks').disabled=true;
								//document.getElementById('idNegMarks').value='';
								document.getElementById('idDivAddOp').style.visibility = 'visible';
								var el = document.getElementById('idDivAddOp'),
						        all = el.getElementsByTagName('input'),ii;					    
							    for (ii = 0; ii < all.length; ii++) 
							    {	
							    	if(all[ii].name=='op')
							    	{
							    		all[ii].disabled = true;
							    		all[ii].style.visibility='hidden';
							    	}	
							    	else
							    	{

							    		all[ii].disabled = false;	
							    		all[ii].style.visibility='visible';						        	
							    	}						    	
							        
							    }								     
							}
							else 														// When Question Type is of Objective.
							{
								if(e.options[e.selectedIndex].value  == "Objective")
								{									
									//document.getElementById('idNegMarks').disabled=false;
									//document.getElementById('idNegMarks').value=0;
									document.getElementById('idDivAddOp').style.visibility = 'visible';
									var el = document.getElementById('idDivAddOp'),
							        all = el.getElementsByTagName('input'),ii;		
							        var ij=1;			    
								    for (ii = 0 ; ii < all.length; ii++) 
								    {		
								    	
								    	if(all[ii].name=='checkOpp'+ij)
								    	{									    		
								    		all[ii].disabled = true;
								    		all[ii].style.visibility='hidden';
								    		ij++;
								    	}	
								    	else
								    	{					    		
								    		all[ii].disabled = false;	
								    		all[ii].style.visibility='visible';						        	
								    	}	
								    }
								}	
								else
								{	//document.getElementById('idNegMarks').disabled=true;
									//document.getElementById('idNegMarks').value='';
									document.getElementById('idDivAddOp').style.visibility = 'hidden';
									all = el.getElementsByTagName('input'),ii;					    
								    for (ii = 0; ii < all.length; ii++) 
								    {
								        all[ii].disabled = true;
								        all[ii].style.visibility='visible';
								    }
								}

							}
						}
						
					}
					
				</script>
				
				 <div id="idDiv" class="classDivAddTest">
                    <span id="idSpanTabQDesc" class="classSpanAddTestDesc">Question<br/><br/>Description</span>
                    <span id="idSpanTabQDesc:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQDescIP" class="classSpanTabIP">
                        <textarea name="queDescription" id = "idAddQuestion" class="classTADescription classInputBackColor" value=""></textarea>						                    	
                    </span>
                </div>
                <div id="idDivAttachment" class="classDivAddTest">
                    <span id="idSpanTabQDesc" class="classSpanAddTestDesc">Attach image</span>
                    <span id="idSpanTabQDesc:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQDescIP" class="classSpanTabIP">
                    	<!--Start Style for using formatted File tag -->	
						  <style>
                            .idiv{display:inline-block;width:230px;height:30px;}
                            .iidiv1{opacity:0.8;width:230;height:30px;position:absolute;}
                            .iidiv2{opacity:0.0;width:230;height:30px;margin-top: 0px;}
                            .iinputFile{height:40px;width: 50px;margin-top: 0px;}
                            .iinputBut{height:30px;width: 50px;margin-top: -10px;}
                            .iinputTxt{overflow:hidden;font-size:14px;height:28px;width: 150px;border: 1px solid rgb(192, 192, 192);display:inline-block;margin-top: 0px;}
                            
                        </style>
						<!--END of Style for using formatted File tag -->	
                    	<!--Start HTML Code for using formatted File tag -->
							<!-- START0 Outer Container div which holds textbox, button and file tag. -->
							<div class="idiv"> 
								<!-- START1 Inner Container1 div which holds textbox and button. -->
								<div class=iidiv1>	
									<input type="text" class="iinputTxt classRounded_Radius_5" id="idBrowseFileName"/> 
									<input type="button"class="iinputBut but btn btn-primary" value="Browse">
									</div>							<div class=iidiv2>	
									<input type="file"class="iinputFile" id="fileToUpload" name="fileToUpload" onchange="return ajaxFileUpload(this.value);">			
								</div>

								
								<!-- END1 Inner Container1 div which holds textbox and button. -->
								<!-- START2 Inner Container2 div which holds File. -->
								
								<!-- END2 Inner Container2 div which holds File. -->
							</div>
							<!-- START0 Outer Container div which holds textbox, button and file tag. -->
						<!--END HTML Code for using formatted File tag -->
          <!-- <input id="fileToUpload" type="file" size="26" name="fileToUpload" class="input" onchange="return ajaxFileUpload();"> --> 						
                    </span>
                    <div id="idDivDisplayUploadProceessImg" class="classDivDisplayUploadProceessImg">
                    	
                    	<span id="idShowImage"><img id="loading" src="images/loading.gif" style="display:none;">
                    	<?php
                    		$sAttachmentQuery="select * from question_attachment where que_id={$iQueID} and file_status=1";
                    		$bAttachmentResult=$mysqli->query($sAttachmentQuery);
                    		
                    			$aAttachmentRow=$bAttachmentResult->fetch_row();
                    			$sAttachmentPath=$aAttachmentRow[3];
                    			$sAttachmentName=$aAttachmentRow[4];
                    		
	                    	$sQueryCheckAttachment = "select count(que_id) from question_attachment where que_id = {$iQueID} AND file_status = 1";
	                    	$iCheckAttach = $mysqli->query($sQueryCheckAttachment);
				            $aCheckRowFileAttach = $iCheckAttach->fetch_row();
				            if($aCheckRowFileAttach[0] >=1)
				            {
				            	echo "<span class='classLikeAnchor' onclick='removeImage();'><u>Remove Attachment</u></span>";				            	
				            	echo "<div><img id='idAttachment' class='classRounded_Radius_5 classDispAttachmentEdit' src='$sAttachmentPath$sAttachmentName'></div>";
				            } 
				            else
				            {
				            	// Do  Nothing.
				            }
	                    	

                    	?></span>
                    </div>
                </div>
                <br/>
               
                   <span id="idSpanCountry" class="classSpanUserInfoTxt"><span class="classRed">*</span> Fields are Mandatory.</span>
                   <span id="idSpanCountryDb" class="classSpanUserDetailInput">
                    
                   </span>
                  <br/>
                	<!-- Below Script are use to Fetch a image when Question is added. -->
                <script type="text/javascript" language="javascript">

                	function showImage(val)
                	{
                		
                	if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					    {
					      $("#idShowImage").html(xmlhttp.responseText);
					    }
					  }
					xmlhttp.open("GET","fetchImage.php?tid=<?php echo $iTestid; ?>&qid=<?php echo $iQueID; ?>",true);
					xmlhttp.send();
					document.getElementById('idBrowseFileName').value=val;
					}

					function removeImage()
                	{
                		
                	if (window.XMLHttpRequest)
					  {// code for IE7+, Firefox, Chrome, Opera, Safari
					  xmlhttp=new XMLHttpRequest();
					  }
					else
					  {// code for IE6, IE5
					  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
					  }
					xmlhttp.onreadystatechange=function()
					  {
					  if (xmlhttp.readyState==4 && xmlhttp.status==200)
					    {
					   	  $("#idShowImage").html(xmlhttp.responseText);
					    }
					  }
					xmlhttp.open("GET","removeQuestionAttachment.php?tid=<?php echo $iTestid; ?>&qid=<?php echo $iQueID; ?>",true);
					xmlhttp.send();
					document.getElementById('idBrowseFileName').value="";
					}
				</script>	
				<br/>
				
				  <div id="idDivQDesc" class="classDivAddTest">
                    <span id="idSpanTabQDesc" class="classSpanAddTestDesc"></span>
                    <span id="idSpanTabQDesc:" class="classSpanTabCol">   </span>
                    <div id="idDivAddQue" class="classAddQuestionButten">
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<span><input id="idsubmitQue" class="btn btn-primary classAddTestButtonSize" type="submit" value="Add Question" name="submitQue" /></span>						
						<span class="classNoDecoration"><a href="viewTest.php?id=<?php echo $iTestid; ?>"><input id="id_add_test_cancel" class="btn btn-primary classAddTestButtonSize" type="button" value="View Test" name="cancelBut"/></a></span>                   
                    </div>
                </div>
				 <div id="idDivHidden" class="classDivAddTest classHidden">
                    <span id="idSpanTabManda" class="classSpanTabIP">
										                       
                    </span>
                </div>
            </div>            
				<div class="classDivAddOp classRounded_Radius_5" id="idDivAddOp">
					
					<div class="classDivOptions classRounded_Radius_5" >
						<div class="classOpLeft classFont16 " id="idOptionHeading">Option's</div>
						<div class="classOpRight classFont16" id="idAnswerHeading">Answer</div>
					</div>
					
					
					<?php
						// this loop use to display the text boxes and radio button for options.
						for($ii=1,$cAlfabet='A';$ii<=7;$ii++,$cAlfabet++)
						{
							echo "<div class='classDivOptions classRounded_Radius_5'>";	
															
							echo "{$cAlfabet} : <input type='text' name='optext{$ii}' class='classInputRadioText classFont'>";
							echo "<span class='classRadioCheck'>&nbsp;<input type='radio' name='op' value='optext{$ii}' class='validate[required] radio' id='idOp{$ii}'></span>";
							echo "<span class='classRadioCheck'><input type='checkbox' name='checkOpp{$ii}' value='optext{$ii}' class='classCheckBox' id='idCheckOpp{$ii}'></span>";
							echo "</div>";

						}
					?>
				</div>
					<!--<input type="hidden" name="ans" ></input>-->
				



			</form>
            </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->
<!-- Upload file using ajax -->
 
  <script type="text/javascript" src="js/ajaxfileupload.js"></script>
  <script type="text/javascript">
  function ajaxFileUpload(val)
  {
  	var e = document.getElementById("id_TestQtype");
  	var strQtp = e.options[e.selectedIndex].value;
  	//alert(strQtp);
    $("#loading")
    .ajaxStart(function(){
      $(this).show();
    })
    .ajaxComplete(function(){
      $(this).hide();
    });

    $.ajaxFileUpload
    (
      {
        url:"questionAttachment.php?quid=<?php echo $iQueID; ?>&qtp=" + strQtp,
        secureuri:false,
        type: "POST",
        fileElementId:'fileToUpload',
        dataType: 'json',
        data: strQtp,
        success: function (data, status)
        {
          if(typeof(data.error) != 'undefined')
          {
            if(data.error != '')
            {
              alert(data.error);
            }else
            {
              alert("File Uploaded Successfully.");              
              showImage(val);
               //window.location="addQuestionHTML.php?id=<?php echo $iTestid;?>";              
            }
          }
        },
        error: function (data, status, e)
        {
          alert(e);
        }
      }
    )
    return false;
  }
  </script>
 <!-- end of ajax-->
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>