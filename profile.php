<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Profile</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<style type="text/css" title="currentStyle">
  @import "media/css/demo_page.css"; 
  @import "media/css/header.ccss";
  @import "media/css/demo_table_jui.css";
  @import "media/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#idDisplayAllTest').dataTable({
          "bPaginate": true,
          "bFilter": false,
          "bJQueryUI": true
          //"sPaginationType": "full_numbers"
        });
    } );
</script>
                <!--end of Datatable js and css -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>
</head>
<body>
<?php 

 	include('classConnectQA.php');
 	
		session_start();
		    if(isset($_SESSION['user_id']))
        {
          $iUserId = $_SESSION['user_id'];
        }
        else
        {
          $iUserId ="";
        }
        if(isset($_SESSION['ut']))
        {
          $ut=$_SESSION['ut'];
        }
        else
        {
          $ut="";
        }
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }
        if(isset($_GET['usid']))
        {
          $iUser=$_GET['usid'];
        }
        else
        {
            $iUser=null;
        }
        if($iUser==null) // only for user
        {
          $sQueryUserInfo = "select * from user_details where login_id = {$iLoginId}";    
        }
        else
        {
          $sQueryUserInfo ="select * from user_details where user_id = {$iUser} limit 1";
        }       
       
       
        $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
        $aRowForUserInfo = $iResultForUserInfo->fetch_row();
       

 ?>
<div id="id_header_wrapper">
  <div id="id_header">
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
		<div id="id_menu">
      		<div id="id_menu_left">
				    <div id="idDivUserNameTop" class="classDivTopMenuUser">
					   <?php                      
        if($iUser==Null)  // start :for admin and tester and editing their own profile.
                        { 
                          echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                        <ul id='menu'>
                                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                  <ul>
                                      <li>
                                        <a href='profile.php'>Profile</a>   
                                      </li>
                                      <li>
                                        <a href='profileedit.php'>Update Profile</a>      
                                      </li>
                                      <li>
                                        <a href='changePassword.php'>Change Password</a>      
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                  <a href='manageTest.php'>Home</a>   
                                </li>";
                                 if($ut ==2)
                                {
                                  echo "<li><a href='showOpportunity.php'>Opportunity</a></li>";
                                } 

                                  if($ut==0)
                                {
                                  echo "
                                      <li><a >Opportunity</a>
                                        <ul>
                                          <li>
                                              <a href='opportunityHTML.php'>Create</a>       
                                          </li>
                                          <li>
                                              <a href='showOpportunity.php'>Manage</a>            
                                          </li>
                                        </ul>
                                    </li>
                                    <li>
                                    <a>Create</a>
                                    <ul>
                                          <li>
                                            <a href='groupHTML.php'>Create Group</a>    
                                          </li>
                                      <li>
                                        <a href='addTestHTML.php'>Create Test</a>       
                                      </li>
                                      <li>
                                        <a href='addUserHTML.php'>Create User</a>      
                                      </li>
                                      <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>      
                                      </li>
                                      
                                    </ul>
                                  </li>";
                                }

                               if($ut==1)
                               {
                                  
                                echo "<li>
                                  <a href='showOpportunity.php'>Opportunity</a>
                                </li>
                                <li>
                                    <a href='displayStudentResult.php'>Result</a>       
                                </li>";
                               }
                               else
                               {
                                  
                                  echo "<li>
                                      <a>Manage </a>  
                                          <ul>
                                           <li>
                                              <a href='manageGroup.php'>Manage Group</a>      
                                                </li>
                                            <li>
                                              <a href='manageUser.php'>Manage User</a>      
                                            </li>

                                            <li>
                                            <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                            </li>
                                        </ul> 
                                    </li>";
                               }                 
                                
                                echo "<li>

                                      <a href='logout.php'>Logout </a>  

                                    </li>
                                    </ul>
                                    </div>"; 
                                  
                        }  // End :for admin and tester and editing their own profile.
                        else
                        {    // Start : For AAdmin and Tester user but for editing user profile not self profile.            
                          $sSUserQuery="select a.user_full_name from user_details as a, login as b where b.login_id={$iLoginId} and a.login_id={$iLoginId}";

                          $bSUserResult=$mysqli->query($sSUserQuery);
                          $aSURow=$bSUserResult->fetch_row();                      
                          echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                       <ul id='menu'>
                                <li><a href='profile.php'>$aSURow[0]</a>
                                  <ul>
                                    <li>
                                      <a href='profile.php'>Profile</a>   
                                    </li>
                                    <li>
                                      <a href='profileedit.php'>Update Profile</a>      
                                    </li>
                                    <li>
                                      <a href='changePassword.php'>Change Password</a>      
                                    </li>
                                  </ul>
                                
                                </li>
                                <li>
                                  <a href='manageTest.php'>Home</a>   
                                </li>";
                                
                                if($ut==0)
                                {
                                  echo "
                                  <li><a >Opportunity</a>
                                        <ul>
                                          <li>
                                              <a href='opportunityHTML.php'>Create</a>       
                                          </li>
                                          <li>
                                              <a href='showOpportunity.php'>Manage</a>            
                                          </li>
                                        </ul>
                                    </li>
                                    <li>
                                    <li>
                                    <a>Create</a>
                                    <ul>
                                      <li>
                                        <a href='groupHTML.php'>Create Group</a>    
                                      </li>
                                      <li>
                                        <a href='addTestHTML.php'>Create Test</a>       
                                      </li>
                                      <li>
                                        <a href='addUserHTML.php'>Create User</a>      
                                      </li>
                                      <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>      
                                      </li>
                                    </ul>
                                  </li>";
                                }
                                if($ut ==2)
                                {
                                  echo "<li><a href='showOpportunity.php'>Opportunity</a></li>";
                                } 
                               if($ut==1)
                               {

                               }
                               else{
                                  echo "<li>
                                      <a>Manage </a>  
                                          <ul>
                                             <li>
                                              <a href='manageGroup.php'>Manage Group</a>      
                                                </li>
                                            <li>
                                              <a href='manageUser.php'>Manage User</a>      
                                            </li>
                                           <li>
                                            <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                            </li>
                                        </ul> 
                                    </li>";
                               }                 
                                
                                echo "<li>

                                      <a href='logout.php'>Logout </a>  

                                    </li>
                                    </ul>
                                    </div>"; 
                        }
                  ?>
				</div>
			</div>   	
		</div> <!-- end of menu -->
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
 if(isset($_GET['msg']))
    {
        $iMsg = $_GET['msg'];
        echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
       if($iMsg == 256244578 || $iMsg == 256248)    // when update complete.
              {
                  echo "<div id='idDivWarMsg' class='classDivTestDur classDivResumeText classWarningMsg classGreen'>
                      Profile Updated Successfully.
                      </div>";
              }
        if($iMsg === 0) // when update not completed.
        {

                  echo "<div id='idDivWarMsg' class='classDivTestDur classDivResumeText classWarningMsg classGreen'>
                      Profile Not Updated.
                      </div>";
        }
        echo "</div>";
    }
    else
    {
    $iMsg ="";
    }
?>
<div id="id_content_wrapper">
	<div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				<div id="idDivSignUp" class="header_0345"><?php echo $aRowForUserInfo[2]; ?> Profile </div>
        <div class="classHorizHRSubHead"></div>
            </div>
            <br/>

				<div id="idDivUserfullName" class="classDisplayProfile">
	               <span id="idSpanFullName" class="classPreInfo"> Full Name </span>
                   <span id="idSpanFNDb" class="classUserAddressDesc">:&nbsp; <?php echo $aRowForUserInfo[2]; ?></span>
				</div>
                <div id="idDivDob" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[6] == 0000-00-00)
                        {

                        }
                        else
                        {
                            echo "<span id='idSpanDOB' class='classPreInfo'> DOB </span>
                            <span id='idSpanDOBDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[6] </span>";
                        }
                   
                   ?>
                </div>
                <div id="idDivContactNo" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[5] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanConNo' class='classPreInfo'> Mobile No. </span>
                           <span id='idSpanConNoDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[5] </span>";
                        }
                    ?>
                </div>
                <div id="idDivUserEmail" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[3] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanEml' class='classPreInfo'> Email </span>
                           <span id='idSpanEmlDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[3] </span>";
                        }
                    ?>
                </div>
                <div id="idDivUserAddress" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[7] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanAddress' class='classPreInfo'> Address </span>
                           <span id='idSpanAddressDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[7] </span>";
                        }
                    ?>
                </div>
                <div id="idDivUserCity" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[8] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanCity' class='classPreInfo'> City </span>
                           <span id='idSpanCityDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[8] </span>";
                        }
                    ?>
                </div>
                <div id="idDivUserDist" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[9] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanDist' class='classPreInfo'> District </span>
                           <span id='idSpanCityDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[9] </span>";
                        }
                    ?>
                </div>
                <div id="idDivUserState" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[10] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanState' class='classPreInfo'> State </span>
                           <span id='idSpanStateDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[10]</span>";
                        }
                    ?>
                </div>
                <div id="idDivUserCountry" class="classDisplayProfile">
                     <?php 
                        if($aRowForUserInfo[11] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanCountry' class='classPreInfo'> Country </span>
                           <span id='idSpanCountryDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[11]</span>";
                        }
                    ?>
                </div>
                <div id="idDivUserTwitlink" class="classDisplayProfile">
                     <?php 
                        if($aRowForUserInfo[15] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanTwtlink' class='classPreInfo'> Twiter profile link </span>
                           <span id='idSpanTwitlink' class='classUserAddressDesc'>:&nbsp; <a href='$aRowForUserInfo[15]' target='_blank'>$aRowForUserInfo[15]</a></span>";
                        }
                    ?>
                </div>
                <div id="idDivUserLinkdnlink" class="classDisplayProfile">
                     <?php 
                        if($aRowForUserInfo[16] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanLinklink' class='classPreInfo'> Linkdin profile link </span>
                           <span id='idSpanLinklink' class='classUserAddressDesc'>:&nbsp; <a href='$aRowForUserInfo[16]' target='_blank'>$aRowForUserInfo[16]</a></span>";
                        }
                    ?>
                </div>
                <div id="idDivUsergitLink" class="classDisplayProfile">
                     <?php 
                        if($aRowForUserInfo[17] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpangitLink' class='classPreInfo'> Github profile link </span>
                           <span id='idSpanGitlink' class='classUserAddressDesc'>:&nbsp; <a href='$aRowForUserInfo[17]' target='_blank'>$aRowForUserInfo[17]</a></span>";
                        }
                    ?>
                </div>
                <div id="idDivUserOwnWeb" class="classDisplayProfile">
                     <?php 
                        if($aRowForUserInfo[18] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanOwnWeb' class='classPreInfo'> Website </span>
                           <span id='idSpanOwnWeb' class='classUserAddressDesc'>:&nbsp; <a href='$aRowForUserInfo[18]' target='_blank'>$aRowForUserInfo[18]</a></span>";
                        }
                    ?>
                </div>
                <div id="idDivUserAbout" class="classDisplayProfile">
                   <?php 
                        if($aRowForUserInfo[14] == Null)
                        {

                        }
                        else
                        {
                           echo "<span id='idSpanAbout' class='classPreInfo'> Address </span>
                           <span id='idSpanAboutDb' class='classUserAddressDesc'>:&nbsp; $aRowForUserInfo[14] </span>";
                        }
                    ?>
                </div>
                <div id="idDivUserDownloadRes" class="classDisplayProfile">
                   <?php 
                      
                        $iTestId = 0;
                        if($ut == 1)
                           { 
                            $sResumeCheckQuery = "select * from user_resume where user_id = {$iUserId}";
                            $iCheckResume = $mysqli->query($sResumeCheckQuery);
                            $iCheckRowResume = $iCheckResume->num_rows;
                              if($iCheckRowResume >=1)
                              {
                                 echo "<span id='idSpanAbout' class='classPreInfo'> Download Resume </span>";
                                 echo "<span id='idSpanAboutDb' class='classUserAddressDesc'>
                                      <a href='downloadResume.php?id={$iTestId}&uid={$iUserId}'>
                                      <img src='images/DownloadIcon.jpg' id='idDownloadIcon' class='classDownloadResIcon' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Download Resume'>
                                      </a> </span>";
                              }
                            }
                            else
                            { 

                                $sResumeCheckQuery = "select count(user_id) from user_resume where user_id = {$iUser}";
                                $iCheckResume = $mysqli->query($sResumeCheckQuery);
                                if($iCheckResume == true)
                                {
                                $aFetchResume = $iCheckResume->fetch_row();
                                if($aFetchResume[0] == 0)
                                {

                                }
                                else
                                {
                                    echo "<span id='idSpanAbout' class='classPreInfo'> Download Resume </span>";
                                     echo "<span id='idSpanAboutDb' class='classUserAddressDesc'>
                                          <a href='downloadResume.php?id={$iTestId}&uid={$iUser}'>
                                          <img src='images/DownloadIcon.jpg' id='idDownloadIcon' class='classDownloadResIcon' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Download Resume'>
                                          </a> </span>";
                                  
                                }
                              }
                              
                            }
                               
                    ?>
                </div><br/><br/>
                <div id="idDivUserCountry" class="classDisplayProfile">
                  <?php if($ut==2 && $iUser !=null)
                  {
                        // Tester user cant update the profile.
                  }
                  else
                  {                    
                    echo "<span class='classNoDecoration'><a href='profileedit.php?usid={$iUser}'><input id='idUpdateProfile' class='btn btn-primary classAddTestButtonSize' type='button' value='Edit Profile' name='editprofile'/></a></span>"; 
                  }
                   
                  echo "</div>";
                                  
                    if($ut==0 || $ut ==2)
                    {

                    }
                    else
                    {
                      $sUserQuery="select user_id, user_full_name from user_details where user_id={$iUserId}";

                      $aResult=$mysqli->query($sUserQuery);
                      $Urow=$aResult->fetch_row();
                      // loop through result of database query, displaying them in the table.
                      $sTestCode=Null;
                      $sScore=Null;
                      $icounter=0;
                     /*         Query Without Score.
                      $sQuery="select distinct b.sb_doa, a.test_name, c.test_id, d.share_status 
                                from test_detail as a,score_board_table as b , stud_ans_table as c, share_result as d 
                                  where b.user_id = {$Urow[0]}  AND c.test_id = b.test_id  and a.test_id = b.test_id  and a.test_id=c.test_id";

                      */


                      // Query with Score Status 
                      $sQuery="select distinct b.sb_doa, a.test_name, c.test_id, d.share_status, b.sb_marks_obt, b.sb_marks_out_of
                                from test_detail as a,score_board_table as b , stud_ans_table as c, share_result as d 
                                  where b.user_id = {$Urow[0]}  AND c.test_id = b.test_id  and a.test_id = b.test_id  and a.test_id=c.test_id and b.test_id=d.test_id and b.user_id = d.user_id";
                   
                      $bResult=$mysqli->query($sQuery);
                      $iCountTestRow = $bResult->num_rows;
                      if($iCountTestRow == 0)
                      {

                      }
                      else
                      {
                        
                        $sQuerySelect = "select * from share_result where user_id = {$iUserId} AND share_status = 1 ORDER BY share_id DESC";
                        $iResultForResultStatus = $mysqli->query($sQuerySelect);

                        echo "<div id='idDivDisplayAttemptTest' class='classDivDisplayAttemptTest'>
                            <div id='idDivSignUp' class='headerForAttemptedTestUser'>Attempted Test</div>
                            <div class='classHorizHRSubHead'></div>
                            <div id='idDivTestTxt' class='classDivTestTxt'>";
                            
                            
                              echo "<table cellpadding='0' cellspacing='0' border='0' class='display classDisplayAttemptedTest' id='idDisplayAllTest'>";
                                      
                              echo "<thead><tr><th class='classDTabdWidthSrno'>Sr. No.</th> 
                                              <th class='classDTabdWidthTName'>Test</th>
                                              <th>Date Of Attempt</th>
                                              <th>Score</th>";   
                              echo "</tr></thead>";
                              
                              
                              if($bResult!=Null)
                              {
                                
                                while($aRow=$bResult->fetch_row())
                                {   
                                  $icounter++;                        
                                  if($aRow[3] == 0 || $aRow[3] == Null)
                                  {
                                    $sScore="Pending";
                                  }
                                  else
                                  {
                                    $sScore=$aRow[4]."/".$aRow[5];
                                  }
                                  echo "<tr>";
                                  echo "<td class='classDTabdWidthUserAt'>" . $icounter . "</td>";
                                  echo "<td class='classDTabdWidthTName'>" . $aRow[1] . "</td>"; // Test Name
                                  echo "<td class='classDTabdWidthTName'>" . $aRow[0] . "</td>"; // Test Attempt Date                                  
                                  echo "<td class='classDTabdWidthTName'>" . $sScore . "</td>"; // Test Score.
                                  echo "</tr>";
                                } 
                                
                              }
                                 
                            echo "</table></div>
                                          </div>
                                          </div>";
                            }
                          $mysqli->close();
                  }
                  ?>
                </div>
              
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>