<?php 

  $sctype="application/xls";

  $sFilePath = "excelReader/";  //! the folder of the file that is downloaded , you can put the file in a folder on the server just for more order

  $sfilename = "sample.xls";

  if($sfilename == '')
  {
    exit();
  }
  else
  {
    //! required for IE, otherwise Content-disposition is ignored
    if(ini_get('zlib.output_compression'))
    ini_set('zlib.output_compression', 'Off');
    header("Pragma: public");           //!Send a raw HTTP header
    header("Expires: 0");
    header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
    header("Cache-Control: private",false); // required for certain browsers
    header("Content-Type: $sctype");
    header("Content-Disposition: attachment; filename=\"".basename($sfilename)."\";" );
    header("Content-Transfer-Encoding: binary");
    header("Content-Length: ".filesize($sfilename));
    readfile("$sfilename");
    exit();
}

?>