<html>
<body style="margin: 5px;">
<div style="width: 600px; font-family: Arial, Helvetica, sans-serif; font-size: 11px; margin-bottom : 50px;">
	<div id="idDisplayHeading" 
			style="font-size: 11px; 
			color: #000000;
			margin-left:0px; 
			margin-top:10px;
			width: 540px;
			-moz-border-radius: 10px;
			-webkit-border-radius: 10px;
			border-radius: 10px;
			border: 1px solid #C0C0C0;
			margin-bottom:10px;
			padding-left:30px;
			padding-top:10px;
			padding-bottom:10px;
			padding-right:30px;">
<?php 
		
		include('config/config.php');
		include ('classConnectQA.php');

		if(isset($_GET['opid']))
			{
				$iOppId = $_GET['opid'];
			}
			else
			{
				$iOppId = "";
			}
			if(isset($_GET['usid']))
			{
				$iUserId = $_GET['usid'];
			}
			else
			{
				$iUserId = "";
			}

			/* This query returns opportunity name */
			$sQueryFetchOppName = "select a.opp_name
									from
									opportunity_table as a
									where
									a.opp_id = {$iOppId}";
			$bFetchOpportunityName = $mysqli->query($sQueryFetchOppName);
            $aOppName = $bFetchOpportunityName->fetch_row();
            $sOpportunityName = $aOppName[0];
            /* This query returns user information */
            $sQueryForUserDetail = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
            		,b.user_contact 
                  from login as a , user_details as b
                  where a.login_id = b.login_id 
                  AND b.user_id  = $iUserId limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryForUserDetail);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();
			$sUserEmail = $aRowForUserInfo[3];
			$sUserFullName = $aRowForUserInfo[2];
			$sUserMob = $aRowForUserInfo[4];

	?>
			<div id="idDivText" style="font-size: 20px;
										color: rgb(112, 112, 112); ">
				Job Application for - <?php echo $sOpportunityName; ?><br/><br/>

			</div>
			<div id="idDivText2" style="font-size: 15px;
										color: rgb(38, 114, 236); ">
			Applicant Details <br/> 
			</div>
			<div id="idDivText4" style="font-size: 14px;
										color: #000000; ">
			Name :- <?php echo $sUserFullName; ?> <br/>
			Email :- <?php echo $sUserEmail; ?> <br/>
			Contact No :- <?php echo $sUserMob; ?>
			</div>
			<div id="idDivText4" style="font-size: 14px;
										color: #000000; ">
				Test Details <br/>
			</div>
			<div>
			<table border="1">
			<thead>
			<tr><th>Test Name</th><th>Date of Attempt</th><th>Score</th><th>Out of</th></tr>
			</thead>
			
	<?php
			$sQuerySelectTestDetails = "select a.user_id,a.test_id,a.sb_doa,a.sb_marks_obt,a.sb_marks_out_of
										from
										score_board_table as a , opportunity_test as b
										where
										a.user_id = {$iUserId} AND 
										a.test_id = b.test_id AND
										b.opp_id = {$iOppId} AND
										b.opp_test_status = 1";
			$bResultFetchTestDetail = $mysqli->query($sQuerySelectTestDetails);
			
			while($aRowFetchTestDetails = $bResultFetchTestDetail->fetch_row())
			{				
				$iTestId = $aRowFetchTestDetails[1];
				$dTestAttemptDate = $aRowFetchTestDetails[2];
				$sMarkObt = $aRowFetchTestDetails[3];
				$sMarkOutOf = $aRowFetchTestDetails[4];
				$sQuerySelectTestName = "select test_name,test_scope from test_detail where test_id = {$iTestId}";
				$bResultFetchTestName = $mysqli->query($sQuerySelectTestName);
				$aRowFetchTestName = $bResultFetchTestName->fetch_row();
				$sTestName = $aRowFetchTestName[0];
			
				echo "<tr>";
				echo "<td align='middle'> $sTestName";
				if($aRowFetchTestName[1]==2)
				{
					echo "<span style='font-size:10px;'> ( Public )</span>";
				}
				else
				{

				}
				echo"</td>";
				echo "<td align='middle'> $dTestAttemptDate </td>";				
				if($sMarkObt==Null)
				{
					echo "<td align='middle'> Pending </td>";
				}
				else
				{
					echo "<td align='middle'> $sMarkObt </td>";	
				}				
				echo "<td align='middle'> $sMarkOutOf </td>";
				echo "</tr>";

			}
				
	?>
			
			</table>
			</div>
			
	</div>
</div>
</body>
</html>