<?php 
			/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status 
			
			*/
			session_start();
			
			$sUserName = addslashes($_POST['UserName']);
			$sPassword = addslashes($_POST['password']);	
			
			/* encrypt password using base 64 encode */
			function fnEncrypt($sValue, $sSecretKey)
			{
			    return rtrim(
			        base64_encode(			/* base64_encode — Encodes data with MIME base64 */				
			            mcrypt_encrypt(		/* mcrypt_decrypt — Decrypts crypttext with given parameters */
			                MCRYPT_RIJNDAEL_256,
			                $sSecretKey, $sValue, 
			                MCRYPT_MODE_ECB, 
			                mcrypt_create_iv(	/* mcrypt_create_iv — Creates an initialization vector (IV) from a random source */
			                    mcrypt_get_iv_size(	/* mcrypt_get_iv_size — Returns the size of the IV belonging to a specific cipher/mode combination */
			                        MCRYPT_RIJNDAEL_256, 
			                        MCRYPT_MODE_ECB		/* One of the MCRYPT_MODE_modename constants, or one of the following strings: "ecb", "cbc", "cfb", "ofb", "nofb" or "stream". */
			                    ), 
			                    MCRYPT_RAND)
			                )
			            )
			        );
			}
			
			$sKey = "quizapplication";
			
			$sPasswordEncrypted = fnEncrypt($sPassword, $sKey);

			
			/* end of encrypt */

			include ('classConnectQA.php');
			if ($mysqli->errno) 									
			{	
					$_SESSION['msg']=-2;							// code -1 : Technical Error Try again.
					header("location:index.php");
				
			}
			/*@ when connetion stablished.*/
			else
			{
				$sQuery = "select * from login where user_name='{$sUserName}' and password='{$sPasswordEncrypted}' limit 1" ;
			
				//var_dump($_POST);exit; <this to chek value>
				/*@where sql query executed.*/
				if($result=$mysqli->query($sQuery))
				{
					$row=$result->fetch_row();
					if($row[1]!=Null&&$row[2]!=Null)
					{
						
						$_SESSION['ut']=$row[3];

						if(isset($_SESSION['lid']))
						{
							$iLoginId = $_SESSION['lid'];
						}
						else
						{
							$iLoginId = Null;
						}

						
						
						if($row[3]==1)
						{
							$_SESSION['lid']=$row[0];
							$sQuery = "select * from user_details where login_id='{$row[0]}'";
							
							$Uresult=$mysqli->query($sQuery);							
							$Urow=$Uresult->fetch_row();
							$_SESSION['user_id'] = $Urow[0];
							if($Urow[12]==1)
							{
								$_SESSION['uid']=$Urow[0];
								
								//$_SESSION['msg']=123;			//Code 123 : Welcome Admin								
							header("location:manageTest.php");
							
							}
							else
							{
								$_SESSION['msg']=123;			//Code 123 : You are Deactivated
								header("location:index.php");
								
							}
						}
						else
						{
							
							$_SESSION['lid']=$row[0];
							//$_SESSION['msg']=234;						//Code 234 : Welcome User
							header("location:manageTest.php");
							
						}
					}
					else
					{
							$_SESSION['msg']=234;						//Code 234 : Fill all Fields
							header("location:index.php");
					}
					
				}
				/*@	when sql query to fail to execute.*/
				else
				{	
					$_SESSION['msg']=-1;							// code -1 : Technical Error Try again.
					header("location:index.php");
				}
			}	
	
?>