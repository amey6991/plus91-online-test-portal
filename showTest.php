<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
                <?php
                session_start();
                include ('classConnectQA.php'); 
                $ut=NUll;
                $iLoginId=Null;
                $iTestId=Null;
                if(isset($_GET['id']))
                {
                    $iTestId=$_GET['id'];
                }
                if(isset($_SESSION['ut']))  
                {
                    $ut=$_SESSION['ut'];
                }   
               if(isset($_SESSION['lid']))      // This is Use to check a Session
                {
                    $iLoginId = $_SESSION['lid'];
                }
                else
                {
                    header("location:index.php");
                }
                
                $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                    from login as a , user_details as b
                                    where a.login_id = b.login_id 
                                    AND a.login_id  = '$iLoginId' limit 1";
                $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                $aRowForUserInfo = $iResultForUserInfo->fetch_row();
                    
                    echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                            <ul id='menu'>
                            <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                            </li>
                            <li>
                                <a href='manageTest.php'>Home</a>       
                            </li>
                            <li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                            <li>
                                <a>Create</a>
                                <ul>
									<li>
										<a href='groupHTML.php'>Create Group</a>		
									</li>
                                    <li>
                                        <a href='addTestHTML.php'>Create Test</a>               
                                    </li>
                                    <li>
                                        <a href=''>User</a>         
                                    </li>
                                    <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>          
                                    </li>
                                </ul>
                            </li>
                            <li>

                                <a>Manage </a>  
									  <ul>                                      
                                            <li>
                                                <a href='manageGroup.php'>Manage Group</a>      
                                            </li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
											<a href='allotTesthtml.php'>Allocate Test</a>   
										  </li>
										  <li>
											<a href='viewAllotedTestHTML.php'>Deallocate Test</a>     
										  </li>
									</ul>    
                            </li>
                            <li>
                                <a href='logout.php'>Logout </a>    
                            </li>
                            </ul>
                            </div>";
                ?>      
                
			</div>  	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
 </div><!-- end of header wrapper -->
 <div id="idDivHorizBar" class="classDivHorizBar radial-center"></div>
</div> 
    
<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
if(isset($_GET['msg']))
    {
        echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
        if($_GET['msg']==1)
        {
            echo "<div class=classMsg >Test Added Successfully</div>";
        }
        if($_GET['msg']==0)
        {
            echo "<div class=classMsg >Try again: Test not Added</div>";
        }
        if($_GET['msg']==-1)
        {
            echo "<div class=classMsg >DB Error in Adding Test</div>";
        }
        if($_GET['msg']==-2)
        {
            echo "<div class=classMsg >Mendatory fields must be filled</div>";
        }
        echo "</div>";
    }
?>
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				    
                <?php  
                    
                     
                            $sTestQuery="select * from test_detail where test_id={$iTestId}";
                            $bResult=$mysqli->query($sTestQuery);
                            if($bResult==True)
                            {
                                $aRow=$bResult->fetch_row();
                                $_SESSION['identifier']=$aRow[2];
                                echo "<div class='header_0345' id='idTestDetail'>Test Detail &nbsp;&nbsp;&nbsp;<a href=editTest.php?id={$aRow[0]}><img src='images/Edit.png' class='classAddIcon' alt='Click to Edit Test'/><span class='classSmall'>[ Edit ]</span></a>";
                                
                                        $iGid=Null;
                                        if(isset($_GET['gid']))
                                        {
                                            $iGid=$_GET['gid'];
                                        }
                                        
                                        
                                echo "</div><br/>";   
                                echo "<div class='classHorizHRSubHead'></div>";
                                echo "<div class='classSmallFont ' id='idEditFloatRight'></div></div><br/>"; 
                                echo "<div class='classDivAddTestResponce' id='idTestName'>Test Name </div>:<div class='classDivAddTestResponce' id='idTestNameAns'>{$aRow[2]}</div><br/>";
                                echo "<div class='classDivAddTestResponce' id='idTestCode'>Test Code</div>:<div class='classDivAddTestResponce' id='idTestCodeAns'>{$aRow[12]}</div><br/>";
                                echo "<div class='classDivAddTestResponce' id='idTestSub'>Test Subject</div>:<div class='classDivAddTestResponce' id='idTestSubAns'>{$aRow[3]}</div><br/>";

                                $sGroupQuery="select group_name from group_table where group_id={$aRow[1]}";
                                $bGroupResult=$mysqli->query($sGroupQuery);
                                if($bGroupResult==true)
                                {
                                    $aGRow=$bGroupResult->fetch_row();                                        
                                    $groupName=$aGRow[0];
                                }
                                else
                                {
                                    $groupName=null;
                                }                                
                                echo "<div class='classDivAddTestResponce' id='idTestGroup'>Test Group</div>:<div class='classDivAddTestResponce' id='idTestGroupAns'>{$groupName}</div><br/>";                               

                                echo "<div class='classDivAddTestResponce' id='idTestVF'>Test Valid From</div>:<div class='classDivAddTestResponce' id='idTestVFAns'>{$aRow[6]}</div><br/>";
                                echo "<div class='classDivAddTestResponce' id='idTestVT'>Test Vaild Till</div>:<div class='classDivAddTestResponce' id='idTestVTAns'>{$aRow[7]}</div><br/>";
                                echo "<div class='classDivAddTestResponce' id='idTestD'>Test Duration</div>:<div class='classDivAddTestResponce' id='idTestDAns'>{$aRow[8]} Minutes</div><br/>";  
                                echo "<div class='classDivAddTestResponce classNoDecoration' id='idTestAQue'><a href='addQuestionHTML.php?id={$aRow[0]}'><input id='idAddQue' class='btn btn-primary classAddTestButtonSize' type='button' value='Add Question' name='addQueButton'/></div>";                                
                                echo "<div class='classDivAddTestResponce classNoDecoration' id='idTestHome'><a href='manageTest.php?id={$aRow[0]}&msg=T1'><input id='idAddQue' class='btn btn-primary classAddTestButtonSize' type='button' value='Home' name='addQueButton'/></div>";
                                
                            }
                ?>
         </div>
        </div> 

    </div> <!-- end of content wrapper -->
 </div>
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>