<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Manage Result</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<style type="text/css" title="currentStyle">
	@import "media/css/demo_page.css"; 
	@import "media/css/header.ccss";
	@import "media/css/demo_table_jui.css";
	@import "media/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#idDisplayAllTest').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
            } );
        </script>
			<!-- datatable js -->
</head>
<body>
	<?php
		/*
			SESSION Variable info.
			@lid: is use for login id
			@uid: is for user id
			@id: is use for any other id such as group, test qustion
			@ut:  is use for the user type;
			@st:  use for status 
		 */
			session_start();
			include ('classConnectQA.php');	
			$iTestid=Null;
			if(isset($_SESSION['lid']))		// This is Use to check a Session
			{
				$iLoginId = $_SESSION['lid'];
			}
			else
			{
				header("location:index.php");
			}
			if(isset($_SESSION['ut']))  
            {
                $ut=$_SESSION['ut'];
            }
            else
            {
            	$ut=null;	
            } 
			if(isset($_GET['id']))	
			{
				$iTestid=$_GET['id'];
			}
			if(isset($_SESSION['user_id']))
			{
				$iUid=$_SESSION['user_id'];
			}
			$iLoginId = $_SESSION['lid'];
			$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();

				
	?>
<div id="id_header_wrapper">
  <div id="id_header">
   	<div id="site_logo">
		<div id="idDivHeadTxt" class="classDivHeadTxtInner">
	      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"/></span>
	      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
	    </div>
	</div>
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
				<?php
					echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
					<ul id='menu'>
					<li><a href='profile.php'>$aRowForUserInfo[2]</a>
					<ul>
                        <li>
                            <a href='profile.php'>Profile</a>       
                        </li>
                        <li>
                            <a href='profileedit.php'>Update Profile</a>            
                        </li>
                        <li>
                            <a href='changePassword.php'>Change Password</a>            
                        </li>
                    </ul>
					</li>
					<li>
						<a href='manageTest.php'>Home</a>		
					</li>";
					
					if($ut==2)
					{
						echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
					}
					else
					{
						echo "
						<li><a >Opportunity</a>
                                <ul>
                                <li>
                                    <a href='opportunityHTML.php'>Create</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Manage</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
							<a>Create</a>
							<ul>
								<li>
									<a href='groupHTML.php'>Create Group</a>		
								</li>
								<li>
									<a href='addTestHTML.php'>Create Test</a>				
								</li>
								<li>
									<a href='addUserHTML.php'>Create User</a>			
								</li>
								<li>
									<a href='excelReader/index.php'>Bulk Upload</a>			
								</li>
							</ul>
						</li>";
					} 
					
					echo "<li>
								<a>Manage </a>  
								  <ul>
							  		<li>
							          <a href='manageGroup.php'>Manage Group</a>      
							        </li>
									<li>
										<a href='manageUser.php'>Manage User</a>			
									</li>
								  <li>
									<a href='viewAllotedTestHTML.php'>Assign Test</a>     
								  </li>
								</ul>	
							</li>
							<li>
								<a href='logout.php'>Logout </a>	
							</li>
							</ul>
							</div>";
				?>
				</div>  	
			</div> <!-- end of menu -->
    	</div>  <!-- end of header -->
</div> 
<div id="idDivHorizBar" class="classDivHorizBar radial-center"></div>
</div><!-- end of header wrapper -->
<div id="id_banner_wrapper">
	<div id="id_banner">
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				<div id="idDivSignUp" class="header_0345">Test Result's : 
				  <?php 
					/*
						To view the user name.
					*/
					$sUserQuery="select test_name, test_code, test_scope from test_detail where test_id=$iTestid";
					$aResult=$mysqli->query($sUserQuery);
					$row=$aResult->fetch_row();
					echo "$row[0]";
					if($row[2]==2)
					{
						echo "<span class='classSmallFontForOpp'>(PUBLIC)</span>";
					}
					
				  ?>
				</div>
				<div class="classHorizHRSubHead"></div>
				<div id="idDivFname" class="classDivAddTest">
			<?php
								
			/*@ when connetion failed.*/
			if ($mysqli->errno) 									
			{	
				$this->ExceptionSet("Connect failed");
				
			}
			/*@ when connetion Established.*/
			else
			{	
				$icounter=0;
				$msg="user";				
				echo "<div id='idDivDispAllTest'>";
				echo "<table cellpadding='0' cellspacing='0' border='0' class='display' id='idDisplayAllTest' width='100%'>";
				echo "<thead><tr><th class='classWidthForDataTableColumn' width='20px'>Sr. No.</th>
								<th class='classWidthForDataTableColumn' width='200px'>Name</th>
								<th class='classWidthForDataTableColumn' width='200px'>User Email</th>
								<th class='classWidthForDataTableColumn' width='150px'>User Contact</th>
								<th class='classWidthForDataTableColumn' width='150px'>Date Of Attempt</th>
								<th class='classWidthForDataTableColumn40' width='150px'>Score</th>
								<th class='classWidthForDataTableColumn20' width='20px'>Result</th>
								</tr></thead>";
				// loop through result of database query, displaying them in the table
				echo "<tbody>";
				$sAQuery="select * from score_board_table where test_id={$iTestid}";
				$Aresult=$mysqli->query($sAQuery);
				if($Aresult!=Null)
				{
					
					while($Arow=$Aresult->fetch_row())
					{		
						
						$sUQuery="select * from user_details where user_id=(select user_id from score_board_table where score_board_id={$Arow[0]})";
						if($Uresult=$mysqli->query($sUQuery))
						{	
							$icounter++;
							$Urow=$Uresult->fetch_row();
													
							echo "<tr>";
							echo "<td align='middle' class='classWidthForDataTableColumn' width='20px'>$icounter</td>";
							echo "<td align='middle'>$Urow[2]</td>";
							echo "<td align='middle'>$Urow[3]</td>";
							echo "<td align='middle'>$Urow[5]</td>";
							echo "<td align='middle'>$Arow[3]</td>";
							if($Arow[10]==null)			
								{
									$sScore="Pending";	
								}
								else
								{
									$sScore=$Arow[10];		
								}
							echo "<td align='center'>$sScore</td>";	// This show the Score 				
							echo "<td align='middle'><a href=testResultPreview.php?usid={$Urow[0]}&id={$iTestid}&msg=tw><img src='images/showTest.png' class='classAddIcon' id='idAddTestIcon'/></td>"; 
													
							echo '</tr>';				
									
						}
					}
					echo "</tbody>";
					echo "</table>";
					$mysqli->close();				
					
				}
				else
				{
					$mysqli->close();
					echo "Data Base Error.";
				}
				echo "</div>";
			}?>
				</div>
			</div>
            </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>