<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
<html xmlns='http://www.w3.org/1999/xhtml'>
<head>
<meta http-equiv='Content-Type' content='text/html; charset=iso-8859-1'>
    <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
<title>Upload Resume</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name='description' content='online quiz application' />
<link href='css/style.css' rel='stylesheet' type='text/css' />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script src='js/fileSecur.js'></script>     <!-- javascript for file validation -->
<!-- Upload file using ajax -->
<?php 
 session_start();
 $iUserId = $_SESSION['user_id'];
 $iTestid= $_GET['id']; 
?>
 <script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
  <script type="text/javascript" src="js/ajaxfileupload.js"></script>
  <script type="text/javascript">
  function ajaxFileUpload(val)
  {   
    
    $("#loading")
    .ajaxStart(function(){
      $(this).show();
    })
    .ajaxComplete(function(){
      $(this).hide();
    });

    $.ajaxFileUpload
    (
      {        
        url:"uploadResume.php?usid=<?php echo $iUserId; ?>",
        secureuri:false,
        fileElementId:'fileToUpload',
        dataType: 'json',
        data:{name:'logan', id:'id'},
        success: function (data, status)
        {
          if(typeof(data.error) != 'undefined')
          {
            if(data.error != '')
            {
              alert(data.error);
            }else
            {             
              //window.location="uploadCV-StartTest.php?msg=45678&id=<?php echo $iTestid; ?>";
              document.getElementById('idResponceMsg').innerHTML="Resume uploaded successfully";
              document.getElementById('idBrowseFileName').value=val;
            }
          }
        },
        error: function (data, status, e)
        {
          alert(e);
        }
      }
    )
    
    return false;

  }
  </script>
 <!-- end of ajax-->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body>
    <?php
            include ('classConnectQA.php');

            $ut=$_SESSION['ut'];
            if(isset($_SESSION['lid']))     // This is Use to check a Session
            {
                $iLoginId = $_SESSION['lid'];
            }
            else
            {
                header("location:index.php");
            }
            
            $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                from login as a , user_details as b
                                where a.login_id = b.login_id 
                                AND a.login_id  = '$iLoginId' limit 1";
            $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
            $aRowForUserInfo = $iResultForUserInfo->fetch_row();
     ?>
<div id='id_header_wrapper'>
  <div id='id_header'>
   	<div id='site_logo'>
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
		<div id='id_menu'>
      		<div id='id_menu_left'>
				<div id='idDivUserNameTop' class='classDivTopMenuUser'>
					<?php 
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                <ul id='menu'>
                                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href='manageTest.php'>Home</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Opportunity</a>
                                </li>
                                <li>
                                    <a href='displayStudentResult.php'>Result</a>       
                                </li>
                                <li>
                                    <a href='logout.php'>Logout </a>    
                                </li>
                                </ul>
                                </div>";

                    ?>
				</div>
			</div>   	
		</div> <!-- end of menu -->
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id='id_banner_wrapper'>
	<div id='id_banner'>
        <!--<div id='id_banner_content'>
        	<div id='idDivWelcomMsg' class='header_01'>Welcome to e-Quiz! </div>
        </div>-->
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
    if (isset($_GET['msg']))    /* if msg variable not get from url then this function assign blank to msg variable */
        {
             $iMsg = $_GET['msg'];
             echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
             if($iMsg == 123)    
                {
                    echo "<div id='idDivWarMsg' class='classMsg'>
                        Selected file is too big , You can upload file up to 2 MB.
                        </div>";
                }
                if($iMsg == 123748)
                {
                    echo "<div id='idDivWarMsg' class='classMsg'>
                        Upload resume first, You can't upload file more than 2 MB.
                        </div>";
                }
                if($iMsg == 45678)
                {
                     echo "<div id='idDivWarMsg' class='classMsg'>
                        Resume uploaded successfully. 
                        </div>";
                }
                echo"</div>";
        }
        else
        {
             $iMsg = "";
        }
?>
<div id='id_content_wrapper'>
	<div id='id_content'>  <!-- start of body -->
        <div id='idDivMiddleBody' class='classDivMiddleBody'>
            <div id='idDiv' class='classDiv'>
				<div id='idDivSignUp' class='header_0345'>Upload Resume </div>
                <div class="classHorizHRSubHead"></div>
            </div>
                <br/>
                <?php 
                    
                    // @file connect to the database.
                    
                    
                    /* session variable for user id  */
                          /* it receives test id from url */            
                    $sQueryForMarks = '';
                   
                    $sQueryCheckTestAttempt = "select distinct user_id , test_id from user_resume where resume_status = 1 AND user_id = {$iUserId} limit 1";
                    $iResCheckTestAttempt = $mysqli->query($sQueryCheckTestAttempt);    /* This query check user id and test id present in database or not */
                    $aTestAttempt = $iResCheckTestAttempt->fetch_row();
                   
                    $sTestAttemptMsg = "";
                    if($aTestAttempt[1] == $iTestid) /* This condition check if user attempt this test first time or he/she already submit this test */
                    {
                        $sTestAttemptMsg = "This Test is Already Submitted by You.Try any other";
                    }
                    else
                    {
                        /* If user firs time upload his/her resume then this else part executes */
                        $sQueryForMarks = "select test_id,que_id,que_question,que_marks,que_neg_marks from question_table where test_id = $iTestid";
                            /* This query gives number of marks for each question and negative marks for each question */
                        $iQueForMrkResult = $mysqli->query($sQueryForMarks);
                        $iQueCount = $iQueForMrkResult->num_rows;
                        $iTotalMks ='';
                        
                        while($aRowTotalMks = $iQueForMrkResult->fetch_row())
                        {
                            $iTotalMks += $aRowTotalMks[3];
                        }

                        $sQueryTestNameDur = "select test_name , test_duration from test_detail where test_id= $iTestid limit 1";
                        $iResCheckTestNameDur = $mysqli->query($sQueryTestNameDur);
                        $aTestNameDur = $iResCheckTestNameDur->fetch_row();
                    }
                ?>
				<div id="idDivMob" class="classDivAddTest">
				<?php 
                if($sTestAttemptMsg != "")
                {
                    header("location:manageTest.php");
                }
                else
                {

                    
                echo "<div id='idDivUploadCV' class='classDivUploadCV'>
                        <div id='idDivCenter' class= 'header_054'>
                            <form id='idFormResumeUpload' class='classFormResumeUpload' name='UploadResume' action='UploadRes-StartTest.php?id=$iTestid' enctype='multipart/form-data' method='POST'>";
                                if($aTestAttempt[0] == $iUserId)
                                {
                                    echo "<div id='idDivResumeText' class='classDivResumeText'>
                                     Update your resume here(Maximum file size 2 MB), 
                                     <br/>or start test.
                                    </div>
                                    <div id='idDivForFileUpload' class='classDivForFileUpload classDivResumeText header_054'>";
                                }
                                else
                                {
                                    echo "<div id='idDivResumeText' class='classDivResumeText'>
                                     Upload your resume here(Maximum file size 2 MB), <br/>After resume upload test will start.
                                    </div>
                                    <div id='idDivForFileUpload' class='classDivForFileUpload classDivResumeText header_054'>";
                                }    

                                ?>   
                                        
                                    <!--START of Style for using formatted File tag --> 
                                      <style>
                                            .idiv{display:inline-block;width:230px;height:30px;}
                                            .iidiv1{opacity:0.8;width:230;height:30px;position:absolute;}
                                            .iidiv2{opacity:0.0;width:230;height:30px;margin-top: 0px;}
                                            .iinputFile{height:40px;width: 50px;margin-top: 0px;}
                                            .iinputBut{height:30px;width: 50px;margin-top: -10px;}
                                            .iinputTxt{overflow:hidden;font-size:14px;height:28px;width: 150px;border: 1px solid rgb(192, 192, 192);display:inline-block;margin-top: 0px;}
                                            
                                        </style>
                                        <!--END of Style for using formatted File tag --> 
                                                  <!--Start HTML Code for using formatted File tag -->
                                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                                          <div class="idiv"> 
                                            <!-- START1 Inner Container1 div which holds textbox and button. -->
                                            <div class=iidiv1>  
                                              <input type="text" class="iinputTxt classRounded_Radius_5" id="idBrowseFileName"> 
                                              <input type="button"class="iinputBut but btn btn-primary" value="Browse">
                                              </div>              <div class=iidiv2>  
                                              <input type="file"class="iinputFile" id="fileToUpload" name="fileToUpload" onchange="return ajaxFileUpload(this.value);">              
                                            </div>

                                            
                                            <!-- END1 Inner Container1 div which holds textbox and button. -->
                                            <!-- START2 Inner Container2 div which holds File. -->
                                            
                                            <!-- END2 Inner Container2 div which holds File. -->
                                          </div>
                                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                                        <!--END HTML Code for using formatted File tag -->   
                                    
                                                               <!-- <span><input id='fileToUpload' type='file' size='45' name='fileToUpload' class='input' onchange='return ajaxFileUpload();'></span> -->
                            <?php  
                                    echo"<span id='' class=''><img id='loading' src='images/loading.gif' style='display:none;'></span>";
                                    echo "<br/><span id='idResponceMsg' class='classSmallFontForOpp' ></span>
                                    </div>";
                                
                                echo"<div id='idDivStartTestButten' class='classDivStartTestButten classDivResumeText header_054'>
                                    <div id='idDivTestName' class='classDivTestDur classDivResumeText header_054'> 
                                        Test Name : - $aTestNameDur[0]
                                    </div>
                                    <div id='idDivTestQue' class='classDivTestDur classDivResumeText header_054'> 
                                        Test Question : - $iQueCount
                                    </div>
                                    <div id='idDivTestTotalMark' class='classDivTestDur classDivResumeText header_054'> 
                                        Test Total Mark :- $iTotalMks
                                    </div>
                                    <div id='idDivTestDur' class='classDivTestDur classDivResumeText header_054'> 
                                        Time : - $aTestNameDur[1] &nbsp;min.
                                    </div>";

                                    $sQueryToDisNegMark ="select test_neg_status from test_detail where test_id={$iTestid}";
                                    $iResultDispNegMark = $mysqli->query($sQueryToDisNegMark);
                                    if($iResultDispNegMark == true)
                                    {
                                        $aFetchDispNegMark = $iResultDispNegMark ->fetch_row();
                                        if($aFetchDispNegMark[0] == 1)
                                        {
                                            echo "<div id='idDivTestDur' class='classDivTestDur classDivResumeText header_054'>
                                                        Negative Marks : Applicable
                                                  </div>";
                                        }
                                        else
                                        {
                                            echo "<div id='idDivTestDur' class='classDivTestDur classDivResumeText header_054'>
                                                      Negative Marks : Not Applicable
                                                  </div>";
                                        }
                                    }
                                    echo "<div id='idDivStartTest' class='classDivStartTest classDivResumeText header_054'>
                                        <input type='hidden' name='test_id' value='$iTestid'>
                                        <input type = 'submit' id = 'idSubmitButten' class='classSubmitButten btn btn-primary classSignUpButtenSize' value='Start Test'/>";
                }
                ?>
                                    </div>
                                    </div>
                                </div>
                            </form>
                        </div>
					</div>
				</div>
        </div>
		</div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>