<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Alloted Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script>
/* start of use of ajax to fetch Select alloted student in a selected test */
function showAllotedUser(tid)
{  
  var e = document.getElementById("id_TestGroupSelect");  
  var strUser = e.options[e.selectedIndex].value;
if (tid=="" || tid == 0 || tid==null)
  {
    alert("Select Test");   
    document.getElementById("idAllotTest").style.visibility="hidden";  
    //document.getElementById("idDivDelocateTab").innerHTML="";
    return;
  }
document.getElementById("idAllotTest").style.visibility="visible";
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)

    {      
    $("#idDivDispUserAllotTest").html(xmlhttp.responseText);
     oDataTable = $('#idDisplayAllTest').dataTable({
                      //"sScrollY": "200px",
                      "bPaginate": false,
                      "bFilter": true,
                      //"bDestroy": false,
                      "bJQueryUI": true,
                      "sPaginationType": "full_numbers"
                    });
     $(function() {  
        $('#select_all').click(function() {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if($(this).is(':checked')) {
                checkboxes.attr('checked', 'checked');
            } else {
                checkboxes.removeAttr('checked');
            }
        });
      });
    }
  }
xmlhttp.open("GET","ajxFetchTestAllotted.php?gid=" + strUser + "&tid=" + tid,true);
xmlhttp.send();
}
/* end of use of ajax to fetch Select alloted student in a selected test */
</script>
<script>
/* start of use of ajax to fetch test in a selected group */
function showTest(str)
{
if (str=="" || str==0 || str==null)
  {

  document.getElementById("idDivDispTestSelectBox").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      $("#idDivDispTestSelectBox").html(xmlhttp.responseText);
    }
  }
xmlhttp.open("GET","ajaxFetchTest.php?gid="+str,true);
xmlhttp.send();
}
/* end of use of ajax to fetch test in a selected group */
</script>
<!-- Datatable link library -->
<style type="text/css" title="currentStyle">
   @import "media/css/demo_page.css"; 
    @import "media/css/header.ccss";
    @import "media/css/demo_table_jui.css";
    @import "media/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {

                $('#idDisplayAllTest').dataTable({
                      //"sScrollY": "200px",
                      "bPaginate": false,
                      "bFilter": true,
                      //"bDestroy": false,
                      "bJQueryUI": true,
                      "sPaginationType": "full_numbers"
                    });

            } );

/* start of select all checkbox script */
           $(function() {  
              $('#select_all').click(function() {
                  var checkboxes = $(this).closest('form').find(':checkbox');
                  if($(this).is(':checked')) {
                      checkboxes.attr('checked', 'checked');
                  } else {
                      checkboxes.removeAttr('checked');
                  }
              });
            });
 /* end of select all checkbox script */
        </script>
<!--end of Datatable link library -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body>
<?php 

 	include('classConnectQA.php');
 	include('config/config.php');
		session_start();
		    if(isset($_SESSION['user_id']))
        {
          $iUserId = $_SESSION['user_id'];
        }
        else
        {
          $iUserId ="";
        }
        if(isset($_SESSION['ut']))
        {
          $ut=$_SESSION['ut'];
        }
        else
        {
          $ut="";
        }
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }
       
        $sQueryUserInfo = "select * from user_details as a where a.login_id = {$iLoginId} limit 1";
        $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
        $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        
 ?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<?php 
          if($ut==0||$ut==2)
          {
            echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                  <ul id='menu'>
                  <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                  <ul>
                    <li>
                      <a href='profile.php'>Profile</a>   
                    </li>
                    <li>
                      <a href='profileedit.php'>Update Profile</a>      
                    </li>
                    <li>
                      <a href='changePassword.php'>Change Password</a>      
                    </li>
                  </ul>
                  </li>
                  <li>
                    <a href='manageTest.php'>Home</a>   
                  </li>";
                  
          
            if($ut==2)
            {
              echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
            }
            else
            {
              echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
                <ul>
                  <li>
                  <a href='groupHTML.php'>Create Group</a>    
                  </li>
                  <li>
                    <a href='addTestHTML.php'>Create Test</a>   
                  </li>
                  <li>
                    <a href='addUserHTML.php'>Create User</a>     
                  </li>
                  <li>
                    <a href='excelReader/index.php'>Bulk Upload</a>     
                  </li>
                </ul>
              </li>";
            } 
            
            echo "<li>
                  <a>Manage </a>  
                    <ul>
                        <li>
                        <a href='manageGroup.php'>Manage Group</a>      
                      </li>
                      <li>
                        <a href='manageUser.php'>Manage User</a>      
                      </li>
                      <li>
                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                        </li>
                  </ul> 
                </li>
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";
          } 
          else
          {
           echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                <ul id='menu'>
                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                  <ul>
                    <li>
                      <a href='profile.php'>Profile</a>   
                    </li>
                    <li>
                      <a href='profileedit.php'>Update Profile</a>      
                    </li>
                    <li>
                      <a href='changePassword.php'>Change Password</a>      
                    </li>
                  </ul>
                </li>
                <li>
                  <a href='manageTest.php'>Home</a>   
                </li>
                <li>
                  <a href='showOpportunity.php'>Opportunity</a>
                              </li>
                <li>
                                <a href='displayStudentResult.php'>Result</a>       
                            </li>
                
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";  
            }
          ?>
				</div>
			</div>   	
		</div> <!-- end of menu -->
    </div>  <!-- end of header -->
  <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
if(isset($_GET['msg']))
    {
      echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
        $iMsg = $_GET['msg'];
        if($iMsg == 1)    
          {
             echo "<div class=classMsg >Test assigned. </div>";
          }
          if($iMsg == 10)    
          {
             echo "<div class=classMsg >Test not assigned, try again.</div>";
          }
          if($iMsg == 0)    
          {
             echo "<div class=classMsg >Test not assigned, try again.</div>";
          }
          if($iMsg == -1)    
          {
             echo "<div class=classMsg >Technical Error : Query not executing.</div>";
          }
          echo "</div>";
    }
    else
    {
    $iMsg =null;
    }
?>
<div id="id_content_wrapper">
	<div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
             
				<div id="idDivSignUp" class="header_0345">Assign Test

        </div>
        <div class="classHorizHRSubHead"></div>
            </div>
                       
          <form action="testAllotRemove.php" method="post" id="idAllotTestForm" name="AllotTestForm">
    				<div id="idDivDispUser" class="classDivAllotGPTS">
              <?php 
                $sQueryToSelectGroup ="SELECT group_id,group_name FROM group_table where group_status = 1";                
                $iResultFetchGroup = $mysqli->query($sQueryToSelectGroup);                
                echo "Select Group : <select id='id_TestGroupSelect' class='classAllotTstInputSelect' name='testGroup' onchange='showTest(this.value)'>";
                    echo"<option value='0' >Select group</option>";
                while($aRowFetchGroup = $iResultFetchGroup->fetch_row())
                  {
                     echo"<option value='{$aRowFetchGroup[0]}'>{$aRowFetchGroup[1]}</option>";                   
                  }
                echo "</select>";
              ?>   
              
    				</div>
            <div id="idDivDispTestSelectBox" class="classDivTstSelectBox">
              <?php              
              echo "Select Test : <select id='id_TestAddSub' class='classAllotTstInputSelect' name='test' onchange='showAllotedUser(this.value)'>";
               echo"<option value='0'>Select test</option>";
              echo "</select>";
              ?>
            </div>
            <script type="text/javascript">
              function submitAllotRemove()
              {
                document.AllotTestForm.submit();
              }

            </script>
            <div id="idDivDispSubmitAllot" class="classDivTstSubmitAllot">
              <input type="button" value="Save" id="idAllotTest" class="btn btn-primary classAddTestButtonSize" onclick="submitAllotRemove()"/>
            </div>

				    <div class="classDivDispUserAllotTest" id="idDivDispUserAllotTest">

            <?php 
                $iactive=0;
                $ideactive=0;
                $icounter=0;
                $msg="user";
                 $sUserQuery="select a.* from user_details as a, login as b where a.login_id=b.login_id and b.type=1 and a.user_status=1 order by user_id Desc";
                $bUserResult=$mysqli->query($sUserQuery);
                $ii=1;
                echo "<table cellpadding='0' cellspacing='0' border='0' class='display classForTable' id='idDisplayAllTest' width='100%'>";
                echo "<thead><tr>
                  <th class='classDTabdWidthSrno'>Select All<input type='checkbox' id='select_all' name='chkAllChk' value=''></th>
                  <th class='classDTabdWidthTName'>Name</th>                  
                  <th class='classDTabdWidthTName'>Email</th>                  
                  <th class='classDTabdWidthTName'>Contact No.</th>
                  </tr></thead>";
                  echo "<tbody >";                                         
                    while($aUserRow=$bUserResult->fetch_row())
                    {

                      echo "<tr>";
                     /*
                      $sAllotedQuery ="SELECT user_id, allot_status FROM allot_test where user_id={$aUserRow[0]}";
                      $aAllotedResult = $mysqli->query($sAllotedQuery);
                      $sAllotedQuery=$aAllotedResult->fetch_row();   
                      
                      if($sAllotedQuery[1]==1)
                      {
                        echo "<td><input name='chkAllotTest$ii' type='checkbox' id='idchkAllotTest$ii' value='{$aUserRow[0]}'' checked='checked'></td>";   
                      }
                      else
                      {
                        echo "<td><input name='chkAllotTest$ii' type='checkbox' id='idchkAllotTest$ii' value='{$aUserRow[0]}'></td>";      
                      }*/   
                        echo "<td class='classDTabdWidthSrno'><input type='checkbox' class='child'></td>";       //user name                        
                        echo "<td class='classDTabdWidthTName'>$aUserRow[2]</td>";       //user name
                        echo "<td class='classDTabdWidthTName'>$aUserRow[3]</td>";       // User Contact
                        echo "<td class='classDTabdWidthTName'>$aUserRow[5]</td>";       // User Email
                        echo "</tr>";
                        $ii++;
                    }                       
                  echo "</tbody>";                                    
                  echo "</table>";
            ?>
            </div>
            </form>
          </div>     
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>