<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Group</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<style type="text/css" title="currentStyle">
    @import "media/css/demo_page.css"; 
	@import "media/css/header.ccs";
	@import "media/css/demo_table_jui.css";
	@import "media/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#idDisplayAllTest').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers"
		});
    } );
</script>
<!--end of Datatable js and css -->
<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>
</head>
<body>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
				<?php
					/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status 
			
			*/
			include ('classConnectQA.php');
			session_start();
			$ut=$_SESSION['ut'];
			if(isset($_SESSION['lid']))		// This is Use to check a Session
			{
				$iLoginId = $_SESSION['lid'];
			}
			else
			{
				header("location:index.php");
			}
			
			$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();
			$_SESSION['user_id'] = $aRowForUserInfo[1];	
?>
			<div id="idSpanTopMenu" class="classSpanTopMenu header_044">
					<?php 
					if($ut==0||$ut==2)
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
									<ul id='menu'>
									<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
									</li>
									<li>
										<a href='manageTest.php'>Home</a>		
									</li>";
									
					
						if($ut==2)
						{
							echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
						}
						else
						{
							echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
								<ul>
									<li>
									<a href='groupHTML.php'>Create Group</a>		
									</li>
									<li>
										<a href='addTestHTML.php'>Create Test</a>		
									</li>
									<li>
										<a href='addUserHTML.php'>Create User</a>			
									</li>
									<li>
										<a href='excelReader/index.php'>Bulk Upload</a>			
									</li>
								</ul>
							</li>";
						} 
						
						echo "<li>
									<a>Manage </a>  
									  <ul>
									  		<li>
												<a href='manageGroup.php'>Manage Group</a>			
											</li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
									</ul>	
								</li>
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";
					}	
					else
					{
					
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
							<ul id='menu'>
							<li><a href='profile.php'>$aRowForUserInfo[2]</a>
								<ul>
									<li>
										<a href='profile.php'>Profile</a>		
									</li>
									<li>
										<a href='profileedit.php'>Update Profile</a>			
									</li>
									<li>
										<a href='changePassword.php'>Change Password</a>			
									</li>
								</ul>
							</li>
							<li>
								<a href='manageTest.php'>Home</a>		
							</li>
							<li>
								<a href='showOpportunity.php'>Opportunity</a>
                            </li>
							<li>
                            	<a href='displayStudentResult.php'>Result</a>       
                        	</li>
							
							<li>
								<a href='logout.php'>Logout </a>	
							</li>
							</ul>
							</div>";	
					}
					?>
						</div>
</div>
			</div>  	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
        <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">   
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				<div id="idDivSignUp" class="header_0345">Group</div>
				<div class="classHorizHRSubHead"></div>
				<div id="idDivFname" class="classDivAddTest">
					<?php
						
			
			/*@ when connetion failed.*/
			if ($mysqli->errno) 									
			{	
				echo '<a href="javascript:window.location.reload(history.go(-2));">DataBase Error: click here to Go Back<a>';
				
			}
			/*@ when connetion Established.*/
			else
			{	
				$iactive=0;
				$ideactive=0;
				$icounter=0;
				$id=Null;
				$msg="group";
				
				echo "<div id='idDivDispAllTest'>";
				echo "<table cellpadding='0' cellspacing='0' border='0' class='display classForTable' id='idDisplayAllTest' width='100%'>";
				
				echo "<thead><tr>";
				echo "<th>Sr. No.</th> <th>Group</th><th>Description</th><th>Group Added On</th><th>Group Status</th><th>No of Test</th><th>show Test</th>";
				

				//if(!$rowT[0])
				if(!$ut)
				{
					echo "<th>Edit</th>";

				}
				echo "</tr></thead>";

				/*if(!$ut)
				{
					echo "<th>Add Test</th>";
				}*/
				echo "<tbody>";


				// loop through result of database query, displaying them in the table
				
				$sQuery="SELECT * FROM quiz_online.group_table";
				if($result=$mysqli->query($sQuery))
				{
					
					while($row=$result->fetch_row())
					{		
							$icounter++;
													
							echo "<tr align='middle'>";
							echo "<td align='middle'> $icounter  </td>"; //Group No
							echo "<td align='middle'> $row[1] </td>";	//group Name
							echo "<td align='middle'> $row[2] </td>";    // group description
							echo "<td align='middle'> $row[3] </td>";	//group date													
							if($row[4]==1)
							{
								echo "<td align='middle'> Active </td>";	//group Status	
							}
							else
							{
								echo "<td align='middle'> Inactive </td>";	//group Status	
							}
							
							// to find the number of test available in group
							
							$sTestResult=$mysqli->query("select count(test_id) from test_detail where group_id={$row[0]}");
							$testrow=$sTestResult->fetch_row();
							echo "<td align='middle'> $testrow[0] </td>";  // No of Test Available.

							echo "<td align='middle'><a href=manageTest.php?gid={$row[0]} rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Show Test'><img src='images/showTest.png' class='classAddIcon' id='idAddTestIcon'/></td>"; 
									
							if(!$ut)
							{
								echo "<td class='classDTabdWidth6'><a href='groupHTML.php?gid={$row[0]}' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Edit Test'><img src='images/Edit.png' class='classAddIcon'/></a></td>";	
								//echo "<td align='middle'><a href=groupHTML.php?gid={$row[0]} rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Edit Test'><i class='icon-pencil'></i></td>"; 
							}	
						
							echo "</tr>";
					}
				}
					echo "</tbody></table>";
					$mysqli->close();
					if(!$ut)
					{
						echo '<br/><br/><br/><br/>total Group : '.$icounter;
						echo '<br/><br/>Active Group : '.$iactive;
						echo '<br/><br/>Deactive Group : '.$ideactive;
					
					}
			}
?>					</div>
				</div>
            </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>