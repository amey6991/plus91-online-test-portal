<?php

	/*
		SESSION Variable info.
		@lid: is use for login id
		@uid: is for user id
		@gid: is use for GroupId
		@id: is use for any other id such as group, test qustion
		@ut:  is use for the user type;
		@st:  use for status 
	
	*/
session_start();
$iMsg=null;
if(isset($_POST['oppid']))
{
	$iOppId=$_POST['oppid'];
}
else
{
	$iOppId= null;
}
$ij=0;
if(isset($_POST['noofopptest']))
{
	$iNoOfTest=$_POST['noofopptest'];
	$aTestForOpp = array();
	for($ii=0 ; $ii<$iNoOfTest ; $ii++)
	{
		if(isset($_POST['oppTest'.$ii]))
		{
			$aTestForOpp[$ij]= $_POST['oppTest'.$ii];
			$ij++;
		}	
		
	}
}
else
{
	$iNoOfTest=null;
	$aTestForOpp = array(null);
}
include ('classConnectQA.php');
if(isset($_SESSION['lid']))		// This is Use to check a Session
{
	$iLoginId = $_SESSION['lid'];
}
else
{
	header("location:index.php");
}

if ($mysqli->errno) 									
{	
	$iMsg=-1;
}
/*@ when connetion Established.*/

else
{	
	
	if(isset($_POST) && $_POST['oppName'] && $_POST['oppDescription'])
	{
		$aOpportunity= array();
		$aOpportunity=$_POST;
		$iFlag=0;
		if($iOppId==null)
		{
			$aOpportunityDesc = ($_POST['oppDescription']);
 			
 			$textDesc = htmlspecialchars($aOpportunityDesc, ENT_QUOTES);

			$sOppQuery="INSERT INTO opportunity_table(opp_name, opp_code, opp_des, opp_status)
						 values('{$aOpportunity['oppName']}','{$aOpportunity['hiddenOppCode']}','{$textDesc}','{$aOpportunity['oppStatus']}')";
				$iFlag=1;		 
		}
		else
		{
			$aOpportunityDesc = ($_POST['oppDescription']);
 			
 			$textDesc = htmlspecialchars($aOpportunityDesc, ENT_QUOTES);
			$sOppQuery="UPDATE opportunity_table SET opp_name='{$aOpportunity['oppName']}',
													 opp_code='{$aOpportunity['hiddenOppCode']}',
													 opp_des='{$textDesc}',
													 opp_status='{$aOpportunity['oppStatus']}'
													 where opp_id='{$iOppId}'";	
				$iFlag=2;
		}		
		$bOppResult=$mysqli->query($sOppQuery);
		if($bOppResult==true)
		{
			if($iFlag==1)
			{
				$iMsg=1;	
			}
			else
			{
				$iMsg=11;
			}			
		}
		else
		{
			if($iFlag==1)
			{
				$iMsg=2;	
			}
			else
			{
				$iMsg=22;
			}
		}
	}
	else
	{
		$iMsg=-2;		
	}
	
}

$ii=0;
if($iOppId==null)
{
	$sNewOppQuery="select max(opp_id) from opportunity_table where opp_status=1";
	$bMaxOppResult=$mysqli->query("$sNewOppQuery");
	$aRowMax=$bMaxOppResult->fetch_row();		
	for($ii=0 ; $ii<$ij ; $ii++)
	{
		$sAddOppTestQuery="insert into opportunity_test(opp_test_id,opp_id,test_id,opp_test_status) values (null,{$aRowMax[0]},{$aTestForOpp[$ii]},1)";			
		$bAddOppTestResult=$mysqli->query($sAddOppTestQuery);

	}
	
}
else
{	

	$sDeleteOppTestQuery="update opportunity_test set opp_test_status = 0 where opp_id={$iOppId}";
	$bDelOppTestResult=$mysqli->query($sDeleteOppTestQuery);	
	for($ii=0 ; $ii<$ij ; $ii++)
	{
		$sChekOppTest="select * from opportunity_test where opp_id={$iOppId} and test_id={$aTestForOpp[$ii]}";		
		$bCheckOppTestResult=$mysqli->query($sChekOppTest);		
		$aCheckOppTestRow=$bCheckOppTestResult->fetch_row();
		if($aCheckOppTestRow != null)
		{
			$sOppTestQuery="update opportunity_test set opp_test_status = 1 where opp_id={$iOppId} and test_id = {$aTestForOpp[$ii]}";
		}
		else
		{
			$sOppTestQuery="insert into opportunity_test(opp_test_id,opp_id,test_id,opp_test_status) values (null,{$iOppId},{$aTestForOpp[$ii]},1)";				
		}
		$bAddOppTestResult=$mysqli->query($sOppTestQuery);
	}
	
}
if($ii>0)
{
	$sUpdateApplyStatus="update apply_opportunity set ap_opp_status=0 where opp_id={$iOppId}";
	$bUpdateApplyStatusResult=$mysqli->query($sUpdateApplyStatus);
}
header("Location: opportunityHTML.php?oppid={$iOppId}&msg={$iMsg}");
?>