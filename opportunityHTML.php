<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Opportunity</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/> 
    <link rel="stylesheet" href="js/prettyCheckable/prettyCheckable.css">  
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/prettyCheckable/prettyCheckable.js"></script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idFormOpportunity").validationEngine();
        });
    </script>               
<!--end of javascript to validate the textbox -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
<!-- Script for checkbox style -->
<script>
$(document).ready(function(){
  $('input.myClass').prettyCheckable();
});
</script>
<!-- end of Script checkbox style -->

<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>

</head>
<body>
<div id="id_header_wrapper">
  <div id="id_header">    
   	<div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>        
        <div id="id_menu">
            <div id="id_menu_left">
                <div id="idDivUserNameTop" class="classDivTopMenuUser">
                <?php
                session_start();
                include ('classConnectQA.php'); 

                if(isset($_SESSION['lid']))     // This is Use to check a Session
                {
                     $iLoginId = $_SESSION['lid'];
                }
                else
                {
                    header("location:index.php");
                }                                 

                if(isset($_SESSION['ut']))  
                {
                    $ut=$_SESSION['ut'];
                }
                else
                {
                     $ut="";
                }   
                /*
                    Below Code is use to edit the Opportunity
                */
                $iOppId=null;
                $sOppName=Null;                    
                $sOppDes=Null;
                $sOppStatus=1;
                // code to genrate the unique code for each test.                        
                $sMaxOppQuery="select max(opp_id) from opportunity_table";                
                $bResult=$mysqli->query($sMaxOppQuery);                
                if($bResult==true)
                {
                    $aRow=$bResult->fetch_row();
                    $sOppCode=getCode($aRow[0]);                    
                }
                else
                {
                    $sOppCode=Null;
                }
                function getCode($iKey)
                {
                    $codeString="OPPORTUNITYopportunity";                                    
                    $codeArray=str_split($codeString);
                    $iKey++;
                    $iCount=count($codeArray);
                    $sCode=null;
                    $ans=Null;                                   
                    for($ij=0;$ij<6;$ij++)
                    {
                        $sCode=$sCode.$codeArray[rand(0,21)];   
                    }
                    $ans="{$sCode}{$iKey}";
                    return $ans;                             
                }
                if(isset($_GET['oppid']))
                {
                    $iOppId=$_GET['oppid'];
                    $sOppQuery="select * from opportunity_table where opp_id={$iOppId}";
                    $bOppResult=$mysqli->query($sOppQuery);
                    if($bOppResult==true)
                    {
                        $aOppRow=$bOppResult->fetch_row();
                        $sOppName=$aOppRow[1];
                        $sOppCode=$aOppRow[2];
                        $sOppDes=$aOppRow[3];
                        $sOppStatus=$aOppRow[5];
                    }                     
                    else
                    {
                        // Using the Null value for all variables.
                    }
                }   
                else
                {
                     // Using the Null value for all variables.
                } 
                               
                    
            $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                from login as a , user_details as b
                                where a.login_id = b.login_id 
                                AND a.login_id  = '$iLoginId' limit 1";
            $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
            $aRowForUserInfo = $iResultForUserInfo->fetch_row();
            
       echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                    <ul id='menu'>
                    <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                        <ul>
                            <li>
                                <a href='profile.php'>Profile</a>       
                            </li>
                            <li>
                                <a href='profileedit.php'>Update Profile</a>            
                            </li>
                            <li>
                                <a href='changePassword.php'>Change Password</a>            
                            </li>
                        </ul>
                    </li>                            
                    <li>
                        <a href='manageTest.php'>Home</a>       
                    </li>
                     <li><a >Opportunity</a>
                        <ul>
                            <li>
                                <a href='opportunityHTML.php'>Create</a>       
                            </li>
                            <li>
                                <a href='showOpportunity.php'>Manage</a>            
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a>Create</a>
                        <ul>
							<li>
							<a href='groupHTML.php'>Create Group</a>		
							</li>
                            <li>
                                <a href='addTestHTML.php'>Create Test</a>               
                            </li>
                            <li>
                            <a href='addUserHTML.php'>Create User</a>          
                        </li>
                        <li>
                            <a href='excelReader/index.php'>Bulk Upload</a>            
                        </li>
                        </ul>
                    </li>
                    <li>
                        <a>Manage </a>  
						  <ul>
                                <li>
                                        <a href='manageGroup.php'>Manage Group</a>          
                                </li>
								<li>
									<a href='manageUser.php'>Manage User</a>			
								</li>
							 <li>
                                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                      </li>
						</ul>    
                    </li>
                    <li>
                        <a href='logout.php'>Logout </a>    
                    </li>
                    </ul>
                    </div>";

                ?>      
                </div>
				</div>  	
			</div> <!-- end of menu -->
		</div>
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div>  <!-- end of header -->
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
 if(isset($_GET['msg']))
                {
                    echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
                    if($_GET['msg']==1)
                    {
                        echo "<div class=classMsg >Opportunity created successfully</div>";
                    }
                    if($_GET['msg']== 10)
                    {
                        echo "<div class=classMsg >Try again: Opportunity not created</div>";
                    }
                    if($_GET['msg']==-1)
                    {
                        echo "<div class=classMsg >DB Error in creating  cpportunity</div>";
                    }
                    if($_GET['msg']==-2)
                    {
                        echo "<div class=classMsg >Mendatory fields must be filled</div>";
                    }
                        if($_GET['msg']==11)
                    {
                        echo "<div class=classMsg >Opportunity updated successfully</div>";
                    }
                    if($_GET['msg']== 100)
                    {
                        echo "<div class=classMsg >Try again: Opportunity not updated</div>";
                    }
                    if($_GET['msg']==-11)
                    {
                        echo "<div class=classMsg >DB Error in updating opportunity</div>";
                    }
                    if($_GET['msg']==-22)
                    {
                        echo "<div class=classMsg >Mendatory fields must be filled</div>";
                    }
                    echo"</div>";
                }

?>
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
        <div id="idDiv" class="classDivBodyFormat">					 
			
		<div id="idDivSignUp" class="header_0345">Opportunity</div>
        <div class="classHorizHRSubHead"></div>
          <form class="formular" onsubmit="return validateForm();" action="opportunity.php" method="POST" name="AddTest" id="idFormOpportunity">
              <input type="hidden" name="oppid" value="<?php echo $iOppId; ?>" />
           <div id="idDivOpportunity" class="classTopMarginForChangePass classChangePassword">
                <div id="idDivOppname" class="classDivAddTest">
                    <div id="idSpanTabOppName" class="classSpan250">Opportunity<span class="classRed">*</span></div>                
                    <div id="idSpanTabOppNameIP" class="classUserAddressDesc">
                        : &nbsp;&nbsp;&nbsp;<input type="text" name="oppName" id = "idOppName" value="<?php echo $sOppName; ?>" class="classTextOpportunity classInputBackColor validate[required] text-input">
                    </div>
                </div>
               <br/>
			   <div id="idDivOppCode" class="classDivAddTest">
                    <div id="idSpanTabOppCode" class="classSpan250">Opportunity Code<span class="classRed">*</span></div>                    
                    <div id="idSpanTabOppCodeIP" class="classUserAddressDesc">
                        : &nbsp;&nbsp;&nbsp;<input type="text" name="OppCode" id = "idOppCode" size="35" class="classUserDtInput classInputBackColor" value='<?php echo $sOppCode; ?>' disabled="disabled"/>
                         <input type="hidden" name="hiddenOppCode" id = "idHiddenOppCode" class="classHiddenOppCode" value="<?php echo $sOppCode; ?>" />
                    </div>
                </div>
			     <br/>              

                  <br/>
                <div id="idDivOppStauts" class="classDivAddTest">
                    <span id="idSpanTabOppStauts" class="classSpan250">Opportunity Status<span class="classRed">*</span></span>                    
                    <span id="idSpanTabOppStautsIP" class="classUserAddressDesc">
                        : &nbsp;&nbsp;&nbsp;
                        <?php
                            if($sOppStatus==1)
                            {
                               echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>Active</span>
                                <span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>
                                    <input type='radio' id='idOppStauts1' class='myClass' name='oppStatus' value='1' checked='checked'/></span>";  
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>Inactive </span>
                                    <span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'><input type='radio' class='myClass' id='idOppStauts0' name='oppStatus' value='0'/></span>";      
                            }
                            else
                            {
                                echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>Active</span>
                                <span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'><input type='radio' class='myClass' id='idOppStauts1' name='oppStatus' value='1'/></span>";
                                echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                                echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>Inactive</span>
                                <span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'><input type='radio' class='myClass' id='idOppStauts0' name='oppStatus' value='0' checked='checked'/></span>";
                            }
                        ?>                       
                        
                         
                    </span>
                </div>
			     <br/>
                <div id="idDivOppDes" class="classDivAddTest">
                    <span id="idSpanTabOppDes" class="classSpan250">Opportunity Description<span class="classRed">*</span></span>:&nbsp;&nbsp;&nbsp;                    

                    <span id="idSpanTabOppDesIP" class=" classSpanAddTestDesc">
                        
                        <textarea id="idOppADis" class="classTAOpportunity validate[required] text-input" name="oppDescription"><?php echo $sOppDes; ?></textarea>
                    </span>
                </div>
                  <br/>


                    <fieldset id='idFrameTabOppTestIP'>
                        <legend> Mandatory test </legend>
                         <?php
                            $sTestQuery="select test_id, test_name, test_scope from test_detail where test_status=1";                                                     
                            $bTestResult=$mysqli->query("$sTestQuery");
                            if($bTestResult==true)
                            {   
                                $ij=0;
                                $ik=1;
                                while($aTestRow=$bTestResult->fetch_row())
                                {    
                                    if($iOppId != Null)           // Execute when existing Opportunity is editing.
                                    {
                                        $sOppTestQuery="select * from opportunity_test where Opp_id={$iOppId} and test_id={$aTestRow[0]}";                                      
                                        $bOppTestResult=$mysqli->query($sOppTestQuery);                                        
                                        $aOppTestRow=$bOppTestResult->fetch_row();
                                        if($aOppTestRow[3]==1)
                                        {                                                

                                            echo "<fieldset id='idSpanOppEachTest{$ik}' class='classSpanOppEachTest'>";  
                                            echo "<legend>Test {$ik}</legend>";
                                            echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'><input type='checkbox' class='myClass' name='oppTest{$ij}' class='' id='idoppTest{$ij}' value='{$aTestRow[0]}' checked='checked'/></span>"; 
                                            echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'><lable for='oppTest{$ij}'>";
                                            if($aTestRow[2]==2)
                                            {
                                               echo $aTestRow[1]."<span class='classSmallFontForOpp'>(PUBLIC)</span>";
                                            }
                                            else
                                            {
                                                echo $aTestRow[1];
                                            }
                                            
                                             echo "</lable></span></fieldset>";   
                                        }
                                        else
                                        {

                                            echo "<fieldset id='idSpanOppEachTest{$ik}' class='classSpanOppEachTest'>";  
                                            echo "<legend>Test {$ik}</legend>";
                                            echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'><input type='checkbox' class='myClass' name='oppTest{$ij}' class='' id='idoppTest{$ij}' value='{$aTestRow[0]}'/></span>"; 
                                            echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'><lable for='oppTest{$ij}'>";
                                            if($aTestRow[2]==2)
                                            {
                                               echo $aTestRow[1]."<span class='classSmallFontForOpp'>(PUBLIC)</span>";
                                            }
                                            else
                                            {
                                                echo $aTestRow[1];
                                            }
                                            
                                             echo "</lable></span></fieldset>";       
                                        }                                                                                
                                    }
                                    else                        // Execute when Adding New Opportunity.
                                    {                                            
                                        echo "<fieldset id='idSpanOppEachTest{$ij}' class='classSpanOppEachTest'>";  
                                        echo "<legend>Test {$ik}</legend>";
                                        echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'><input type='checkbox' class='myClass' name='oppTest{$ij}' class='' id='idoppTest{$ij}' value='{$aTestRow[0]}'/></span>"; 
                                        echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'><lable for='oppTest{$ij}'>";
                                        if($aTestRow[2]==2)
                                        {
                                           echo $aTestRow[1]."<span class='classSmallFontForOpp'>(PUBLIC)</span>";
                                        }
                                        else
                                        {
                                            echo $aTestRow[1];
                                        }                                            
                                        echo "</lable></span></fieldset>";
                                                                                  
                                    }
                                    $ij++;
                                    $ik++;
                                }

                                if($ij == 0 && $ik == 1)
                                {
                                     echo "<fieldset id='idSpanTestNotAvailable' class='classSpanOppEachTest'>";  
                                    echo "<legend>Test not available.</legend>";                                   
                                    echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";                                    
                                    echo "<a href='manageTest.php' style='cursor:pointer;' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title=''>All test are deactivated so Activate them.</a></span>";
                                    echo "</fieldset>"; 
                                }
                                echo "<input type='hidden' name='noofopptest' value='$ij'/>";                                
                            }
                            else
                            {
                                echo "<fieldset id='idSpanTestNotAvailable' class='classSpanOppEachTest'>";  
                                    echo "<legend>Test Not Available</legend>";                                   
                                    echo "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";                                    
                                    echo "<a href='addTestHTML.php' style='cursor:pointer;' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title=''>Create Test</a></span>";
                                echo "</fieldset>";
                            }                                   

                        ?>
                    </fieldset>
                <br/><br/><br/>

                <div id="idDivNote" class="classDivAddTest">
                    <span id="idSpanTabNoteIP" class="classSpanTabIP">
                        <span class="classRed">*</span> Fields are Mandatory.
                    </span>
                </div>
                <br/>
                <div id="idDivOppSub" class="classDivAddTest">
                    <div id="idSpanTabOpp" class="classSpan250"></div>                    
                       <div id="idDivAddOpp" class="classSpan250">
						<input id="idOppSubmit" class="btn btn-primary classAddTestButtonSize" type="submit" value="Save" />	

                     </div>
                </div>
				 
            </div>
		   </form>
            
        </div>            
   
        </div> 
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>