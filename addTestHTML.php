<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- script for text counter in text area -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
    var characterLimit = 500;  
    google.setOnLoadCallback(function(){  
        $('#remainingCharacters').html(characterLimit);  
        $('#idTADispr').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            } 
        });  
    });  
</script>
<!-- end of script for text counter in text area -->
<!--Calender script -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="js/jquery-1.9.1.js"></script>
<script>
$(document).ready(function() {
// get the current date
var date = new Date();
var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
// Disable all dates till today
$('#idDatePicFrom').datepicker({
minDate: new Date(y, m, d),
dateFormat: "yy-mm-dd",
});
$('#idDatePickTill').datepicker({
minDate: new Date(y, m, d),
dateFormat: "yy-mm-dd",
});
}); 
</script>
<!--Calender script -->
 <!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idFormAddTest").validationEngine();
        });

    </script>               
<!--end of javascript to validate the textbox -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
                <?php
                session_start();
                include ('classConnectQA.php'); 

                if(isset($_SESSION['lid']))     // This is Use to check a Session
                {
                     $iLoginId = $_SESSION['lid'];
                }
                else
                {
                    header("location:index.php");
                }                                 

                if(isset($_SESSION['ut']))  
                {
                    $ut=$_SESSION['ut'];
                }
                else
                {
                     $ut="";
                }   
               
                    $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                        from login as a , user_details as b
                                        where a.login_id = b.login_id 
                                        AND a.login_id  = '$iLoginId' limit 1";
                    $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                    $aRowForUserInfo = $iResultForUserInfo->fetch_row();
                    
                echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                    <ul id='menu'>
                            <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                <ul>
                                    <li>
                                        <a href='profile.php'>Profile</a>       
                                    </li>
                                    <li>
                                        <a href='profileedit.php'>Update Profile</a>            
                                    </li>
                                    <li>
                                        <a href='changePassword.php'>Change Password</a>            
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href='manageTest.php'>Home</a>       
                            </li>
                            <li><a >Opportunity</a>
                                        <ul>
                                        <li>
                                            <a href='opportunityHTML.php'>Create</a>       
                                        </li>
                                        <li>
                                            <a href='showOpportunity.php'>Manage</a>            
                                        </li>
                                        </ul>
                                    </li>
                            <li>
                                <a>Create</a>
                                <ul>
									<li>
									<a href='groupHTML.php'>Create Group</a>		
									</li>
                                    <li>
                                        <a href='addTestHTML.php'>Create Test</a>               
                                    </li>
                                    <li>
                                    <a href='addUserHTML.php'>Create User</a>          
                                </li>
                                <li>
                                    <a href='excelReader/index.php'>Bulk Upload</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
                                <a>Manage </a>  
								  <ul>
                                        <li>
                                                <a href='manageGroup.php'>Manage Group</a>          
                                        </li>
										<li>
											<a href='manageUser.php'>Manage User</a>			
										</li>
									 <li>
                                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                      </li>
								</ul>    
                            </li>
                            <li>
                                <a href='logout.php'>Logout </a>    
                            </li>
                            </ul>
                            </div>";
                        ?>  
                        </div>    
				</div>  	
			</div> <!-- end of menu -->
		</div>
  <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div>  <!-- end of header -->
</div> <!-- end of header wrapper -->
<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php
                $iGid=Null;
                if(isset($_GET['gid']))
                {
                    $iGid=$_GET['gid'];
                }
                if(isset($_GET['msg']))
                {
                    echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
                    if($_GET['msg']==1)
                    {
                        echo "<div class=classMsg >Test Added Successfully</div>";
                    }
                    if($_GET['msg']==0)
                    {
                        echo "<div class=classMsg >Try again: Test not Added</div>";
                    }
                    if($_GET['msg']==-1)
                    {
                        echo "<div class=classMsg >DB Error in Adding Test</div>";
                    }
                    if($_GET['msg']==-2)
                    {
                        echo "<div class=classMsg >Mendatory fields must be filled</div>";
                    }
                    echo "</div>";
                }
                $iTestId=Null;
                if(isset($_GET['id']))
                {
                    $iTestId=$_GET['id'];
                }
            ?>
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">

			<form action="addTest.php" method="post" name="AddTest" id="idFormAddTest">
			
				<div id="idDivSignUp" class="header_0345">Create Test</div>
                <div class="classHorizHRSubHead"></div>
				     <?php               // code to genrate the unique code for each test.
                                
                                $sTestQuery="select max(test_id) from test_detail";
                                $bResult=$mysqli->query($sTestQuery);
                                if($bResult!=null)
                                {
                                    $aRow=$bResult->fetch_row();
                                    $sCode=getCode($aRow[0]);
                                }
                                else
                                {
                                    $sCode=Null;
                                }
                                function getCode($iKey)
                                {
                                    
                                    $codeArray=array('A','B','C','D','E','F','a','b','c','d','e','f');                              
                                    $iKey++;
                                    $iCount=count($codeArray);
                                    $sCode=null;
                                    $ans="a0";                                   
                                        for($ij=0;$ij<5;$ij++)
                                        {
                                            $sCode=$sCode.$codeArray[rand(0,11)];   
                                        }
                                    $ans="{$sCode}{$iKey}";
                                    return $ans;
                                         
                                }


                        ?>
                <input type="hidden" class="classHiddenCode" id="idHiddenCode" name="hiddenCode" value="<?php echo $sCode; ?>">



				<input type="hidden" class="classGroupId" id="idGroupId" name="hiddenGroupId" value="<?php echo $iGid; ?>">
                   <div id="idDivAddQuestion" class="classDivAddQuestion">
                <div id="idDivTname" class="classDivAddTest">
                    <span id="idSpanTabTName" class="classSpanAddTestDesc">Test Name <span class="classRed">*</span></span>
                    <span id="idSpanTabTName:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabTNameIP" class="classSpanTabIP">
                        <input type="text" name="testName" id = "idMname" size="35" class="classTextOpportunity classInputBackColor validate[required] text-input">
                    </span>
                </div>
               
			   <div id="idDivSub" class="classDivAddTest">
                    <span id="idSpanTabSub" class="classSpanAddTestDesc">Test Subject <span class="classRed">*</span></span>
                    <span id="idSpanTabSub:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabSubIP" class="classSpanTabIP">
                        <select id="id_TestAddSub" class="classUserDtInputSelect class_light_black classFont validate[required]" name="testSubject"  style='width:263px;'>
						<option value="">Subject</option>
                        <?php                           
                            subject();                           
                            function subject()
                            {
                                $aSub=array("English", "Maths", "Reasoning", "Aptitude", "Computer", "Common", "Technical");  
                                foreach ($aSub as $sValue) {
                                    echo "<option value=$sValue>$sValue</option>";
                                }
                            }
                        ?>
                        </select>                        
                    </span>
                </div>
				<?php
                    $sGQuery="select * from quiz_online.group_table where group_status='1'";                                             
                    $bGResult=$mysqli->query($sGQuery);                   
                    echo "<div id='idDivGroup' class='classDivAddTest'>
                            <span id='idSpanTabGroup' class='classSpanAddTestDesc'>Group <span class='classRed'>*</span></span>
                            <span id='idSpanTabGroup:' class='classSpanTabCol'>:</span>
                            <span id='idSpanTabGroupIP' class='classSpanTabIP'>
                               <select id='id_TestAddGroup' class='classUserDtInputSelect class_light_black classFont validate[required]' name='testGroup' onchange='testForAllUser(this);' style='width:263px;'>";   
                                  echo "<option value=''>Group</option>";
                                    
                                   $iGFlag=0;
                                   if($bGResult==True)
                                    {
                                        $iGFlag=1;
                                        while($aGRow=$bGResult->fetch_row())
                                        {
                                            $iGFlag=2;
                                            if($aGRow[4]==2)
                                            {
                                                echo "<option value={$aGRow[0]}>{$aGRow[1]}</option>";
                                            }
                                            else
                                            {
                                                echo "<option value={$aGRow[0]}>{$aGRow[1]}</option>";    
                                            }
                                        }                                        
                                    }
                                    else
                                    {
                                        echo "<option value=''>Error in Group</option>";
                                    }
                    echo "</select></span>";
               
                    echo "</div>";
                 ?>   
				 <div id="idDivTV" class="classDivAddTest">
                    <span id="idSpanTabTV" class="classSpanAddTestDesc">Test Valid From <span class="classRed">*</span></span>
                    <span id="idSpanTabTV:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabTVIP" class="classSpanTabIP">
                        <span class="input-prepend">
                        <span class="add-on">
                        <i class="icon-calendar"></i>
                        </span>
                        <input type="text" name="testValidFrom" id = "idDatePicFrom" size="35" class="classContactNoUpProf validate[custom[date]] text-input ">
                        </span>
                    </span>
                </div>
				
				<div id="idDivVT" class="classDivAddTest">
                    <span id="idSpanTabVT" class="classSpanAddTestDesc">Test Valid Till <span class="classRed">*</span></span>
                    <span id="idSpanTabVT:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabVTIP" class="classSpanTabIP">
                        <span class="input-prepend">
                        <span class="add-on">
                        <i class="icon-calendar"></i>
                        </span>
                        <input type="text" name="testValidTill" id = "idDatePickTill" size="35" class="classContactNoUpProf validate[custom[date]] text-input" >
                        </span>
                    </span>
                </div>
				
				<div id="idDivTD" class="classDivAddTest">
                    <span id="idSpanTabTD" class="classSpanAddTestDesc">Test Duration <span class="classRed">*</span></span>
                    <span id="idSpanTabTD:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabTDIP" class="classSpanTabIP">
                        <span class="input-prepend">
                        <span class="add-on">
                        <i class="icon-time"></i>
                        </span>
                        <input type="text" name="testDuration" id = "idMin" size="35" class="classContactNoUpProf validate[required,custom[integer]] text-input">
						</span>
                        &nbsp;Minutes
                    </span>
                    <span></span>
                </div>

                   <div id="idDivTScope" class="classDivAddTest">
                    <span id="idSpanTabTScope" class="classSpanAddTestDesc">Test Scope</span>
                    <span id="idSpanTabTScope:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabTScopeIP" class="classSpanTabIP">
                        <input type="checkbox" name="testScope" id = "idTScope" size="35" class="classTScope" value="2">&nbsp;Public.
                        <span id='idSpanTabCommonGroup' class='classSmallFontForOpp'>This make test common,for all user. </span>
                    </span>
                    
                </div>
                
                <div id="idDivTNegOff" class="classDivAddTest">
                    <span id="idSpanTabTNegOff" class="classSpanAddTestDesc">Negative Marks</span>
                    <span id="idSpanTabTNegOff:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabTNegOffIP" class="classSpanTabIP">
                        <input type="checkbox" name="testNegMarksOff" id = "idNegMarksOff" size="35" class="classNegMarks" value="1">&nbsp;Applicable
                        
                    </span>
                    
                </div>

                <div id="idDivTDes" class="classDivAddTest">
                    <span id="idSpanTabTDes" class="classSpanAddTestDesc">Test Description</span>
                    <span id="idSpanTabTDes:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabTDesIP" class="classSpanTabIP">
                        <textarea id="idTADispr" class="classTADescription" name="testDescription"></textarea>
                    </span>
                </div>

                 <div id="idDivTMsg" class="classDivAddTest">
                    <span id="idSpanTabTMsg" class="classSpanAddTestDesc">Message for User</span>
                    <span id="idSpanTabTMsg:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabTMsgIP" class="classSpanTabIP">
                        <input type="text" id="idTAMsg" class="classTextOpportunity" name="testMsg" value=""/>
                    </span>
                </div>

                
                <div id="idDivNote" class="classDivAddTest">
                    <span id="idSpanTabNoteIP" class="classSpanTabIP">
                        <span class="classRed">*</span> Fields are Mandatory.
                    </span>
                </div>
                <div id="idDivSub" class="classDivAddTest">
                    <span id="idSpanTabSub:" class="classSpanTabCol">   </span>
                    <span id="idSpanTabSub:" class="classSpanTabCol">   </span>
                       <div id="idDivAddQue" class="classAddQuestionButton">
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						<input id="idSubmit" class="btn btn-primary classAddTestButtonSize" type="submit" value="Create Test" />
						<input type="Reset" value="Reset" class="btn btn-primary classAddTestButtonSize">
                     </div>
                </div>
				 
            </div>
				</form>
            
            </div>
            
                <?php  
                    if($iTestId!=Null)
                    {
                        echo "<div class='classAddTestResponce' id='idAddTestResponce'>";
                            $sTestQuery="select * from test_detail where test_id={$iTestId}";
                            $bResult=$mysqli->query($sTestQuery);
                            if($bResult==True)
                            {
                                $aRow=$bResult->fetch_row();
                                $_SESSION['identifier']=$aRow[2];
                                echo "<div class='classDivAddTestResponceHead' id='idTestDetail'>Test Detail";
                                echo "<div class='classSmallFont ' id='idEditFloatRight'><a href=editTest.php?id={$aRow[0]}><img src='images/Edit.png' class='classAddIcon' alt='Click to Edit Test'/>[ Edit ]</a></div></div><br/>";
                                echo "<div class='classDivAddTestResponce' id='idTestName'>Test Name </div>:<div class='classDivAddTestResponce' id='idTestNameAns'>{$aRow[2]}</div>";
                                 echo "<div class='classDivAddTestResponce' id='idTestCode'>Test Code</div>:<div class='classDivAddTestResponce' id='idTestCodeAns'>{$aRow[12]}</div>";
                                echo "<div class='classDivAddTestResponce' id='idTestSub'>Test Subject</div>:<div class='classDivAddTestResponce' id='idTestSubAns'>{$aRow[3]}</div>";
                                echo "<div class='classDivAddTestResponce' id='idTestVF'>Test Valid From</div>:<div class='classDivAddTestResponce' id='idTestVFAns'>{$aRow[6]}</div>";
                                echo "<div class='classDivAddTestResponce' id='idTestVT'>Test Vaild Till</div>:<div class='classDivAddTestResponce' id='idTestVTAns'>{$aRow[7]}</div>";
                                echo "<div class='classDivAddTestResponce' id='idTestD'>Test Duration</div>:<div class='classDivAddTestResponce' id='idTestDAns'>{$aRow[8]}</div>";
                               
                                echo "<div class='classDivAddTestResponce' id='idTestAQue'><a href='addQuestionHTML.php?id={$aRow[0]}'><input id='idTestAddQue' class='classTestAddQue' name='AddQuestion' type='image' src='images/f.jpg' value='Add Question' alt='Click to Add Question'/></div>";                                
                                echo "<div class='classDivAddTestResponce' id='idTestHome'><a href='manageTest.php?id={$aRow[0]}&msg=T1'><input id='idTestHome'  class='classTestHome' name='GoToHome' type='image' src='images/f.jpg' value='Add Question' alt='Click for Home'/></div>";
                                echo "<div class='classDivAddTestResponce' id='idTestHome'><a href='viewTest.php?id={$aRow[0]}'><input id='idTestHome'  class='classTestHome' name='GoToHome' type='image' src='images/f.jpg' value='Add Question' alt='Click for Home'/></div>";
                            }
                            else
                            {
                                header("location : manageTest.php");
                            }
                        echo "</div>";     
                    }
                   
                ?>
            
        </div> 
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>