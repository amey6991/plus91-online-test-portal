<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Add User</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- script for text counter in text area -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
    var characterLimit = 500;  
    google.setOnLoadCallback(function(){  
        $('#remainingCharacters').html(characterLimit);  
        $('#idInputAddress').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            }
        });  
    });  
</script>
<!-- end of script for text counter in text area -->
<!--Calender script -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="js/jquery-1.9.1.js"></script>
<script>
$(document).ready(function() {
// get the current date
var date = new Date();
var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
// Disable all dates till today
$('#idInputDob').datepicker({
changeMonth: true, changeYear: true, yearRange: '-50:+0',
dateFormat: "yy-mm-dd",
});
$('#idDatePickTill').datepicker({
minDate: new Date(y, m, d),
dateFormat: "yy-mm-dd",
});
}); 
</script>
<!--Calender script -->
 <!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idFormAddUser").validationEngine();
        });
    </script>               
<!--end of javascript to validate the textbox -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){
    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
});
</script>
<!-- end of error message script -->
<script>
$(function()
{
  $('.username').change(function()
  {
    var username=$(this).val();
    username=trim(username);
    if(username!='')
    {
      $('.check').show();
      $('.check').fadeIn(400).html('<img src="images/ajax-loading.gif" /> ');

      var dataString = 'username='+ username;
      $.ajax
      ({
            type: "POST",
            url: "checkuser.php",
            data: dataString,
            cache: false,
            success: function(result)
            {
               var result=trim(result);
               if(result=='')
               {

                  /*Below code is use to show Available status only when Valid email id is entered and Email does not exists */
                  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;              
                  if (!filter.test(document.getElementById("id_login_txt3").value))
                  {                
                     $('.check').html('Invalid Email Id');
                     $('#idCreateSubmitBtn').attr('disabled', 'disabled');
                     $(".username").removeClass("white");
                     $(".username").addClass("red");
                     $("#idDivDisplayAvailability").addClass("classNotAvailable");             
                  }
                  else
                  {
                     $('.check').html("Available");
                     $("#idDivDisplayAvailability").addClass("classAvailableText");
                     $("#idDivDisplayAvailability").removeClass("classNotAvailable");
                     document.getElementById("idCreateSubmitBtn").disabled = false;
                     $(".username").removeClass("red");            
                  }                         
               }
               else
               {  
                 $('.check').html('This email is already registered');
                 $('#idCreateSubmitBtn').attr('disabled', 'disabled');
                 $(".username").removeClass("white");
                 $(".username").addClass("red");
                 $("#idDivDisplayAvailability").addClass("classNotAvailable");
               }
            }
      });

   }
   else
   {
       $('.check').html('');
   }
  });
});
$(function()
{
  $('.Contact').change(function()
  {
    var Contact=$(this).val();
    Contact=trim(Contact);
    if(Contact!='')
    {
      $('.checkContact').show();
      $('.checkContact').fadeIn(400).html('<img src="images/ajax-loading.gif" /> ');

      var dataString = 'Contact='+ Contact;
      $.ajax
      ({
            type: "POST",
            url: "checkContact.php",
            data: dataString,
            cache: false,
            success: function(resultContact)
            {
               var resultContact=trim(resultContact);
               if(resultContact=='')
               {
                  /*Below code is use to show Available status only when Valid email id is entered and Email does not exists */
                  var filter =  /^[0-9]{10,13}$/; 
                  if (!filter.test(document.getElementById("idInputConno").value))
                  {                
                     $('.checkContact').html('Invalid Contact No.');
                     $('#idCreateSubmitBtn').attr('disabled', 'disabled');
                     $(".Contact").removeClass("white");
                     $(".Contact").addClass("red");
                     $("#idDivDisplayContactAvailability").addClass("classNotAvailable");             
                  }
                  else
                  {
                     $('.checkContact').html("Available");
                     $("#idDivDisplayContactAvailability").addClass("classAvailableText");
                     $("#idDivDisplayContactAvailability").removeClass("classNotAvailable");
                     document.getElementById("idCreateSubmitBtn").disabled = false;
                     $(".Contact").removeClass("red");            
                  }                         
               }
               else
               {  
                 $('.checkContact').html('This Contact is already registered');
                 $('#idCreateSubmitBtn').attr('disabled', 'disabled');
                 $(".Contact").removeClass("white");
                 $(".Contact").addClass("red");
                 $("#idDivDisplayContactAvailability").addClass("classNotAvailable");
               }
            }
      });

   }
   else
   {
       $('.checkContact').html('');
   }
  });
});
function trim(str){
     var str=str.replace(/^\s+|\s+$/,'');
     return str;
}
</script>
</head>
<body>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
                <?php
                session_start();
                include ('classConnectQA.php'); 

                if(isset($_SESSION['lid']))     // This is Use to check a Session
                {
                     $iLoginId = $_SESSION['lid'];
                }
                else
                {
                    header("location:index.php");
                }
                if(isset($_SESSION['ut']))  
                {
                    $ut=$_SESSION['ut'];
                }
                else
                {
                     $ut="";
                }   
                
                    $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                        from login as a , user_details as b
                                        where a.login_id = b.login_id 
                                        AND a.login_id  = '$iLoginId' limit 1";
                    $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                    $aRowForUserInfo = $iResultForUserInfo->fetch_row();
                    
                  echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                    <ul id='menu'>
                            <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                <ul>
                                    <li>
                                        <a href='profile.php'>Profile</a>       
                                    </li>
                                    <li>
                                        <a href='profileedit.php'>Update Profile</a>            
                                    </li>
                                    <li>
                                        <a href='changePassword.php'>Change Password</a>            
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href='manageTest.php'>Home</a>       
                            </li>
                            <li><a >Opportunity</a>
                                <ul>
                                <li>
                                    <a href='opportunityHTML.php'>Create</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Manage</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
                            <a>Create</a>
                            <ul>
      								<li>
      								<a href='groupHTML.php'>Create Group</a>		
      							</li>
                        <li>
                            <a href='addTestHTML.php'>Create Test</a>               
                        </li>
                        <li>
                        <a href='addUserHTML.php'>Create User</a>          
                    </li>
                    <li>
                        <a href='excelReader/index.php'>Bulk Upload</a>            
                    </li>
                    </ul>
                </li>
                <li>
                      <a>Manage </a>  
								  <ul>
                      <li>
                              <a href='manageGroup.php'>Manage Group</a>          
                      </li>
										<li>
											<a href='manageUser.php'>Manage User</a>			
										</li>
									  <li>
                    <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                  </li>
								</ul>    
                    </li>
                    <li>
                        <a href='logout.php'>Logout </a>    
                    </li>
                    </ul>
                    </div>";
                ?> 
              </div>
				</div>  	
			</div> <!-- end of menu -->
		</div>
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
		</div>  <!-- end of header -->

</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php
                if(isset($_GET['msg']))
                {
                  echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
                    if($_GET['msg']==2)
                    {
                        echo "<div class=classMsg >Try again: User already exists</div>";
                    }
                    if($_GET['msg']==1)
                    {
                        echo "<div class=classMsg >User Created Successfully</div>";
                    }
                    if($_GET['msg']==0)
                    {
                        echo "<div class=classMsg >Try again: User not Created</div>";
                    }
                    if($_GET['msg']==-1)
                    {
                        echo "<div class=classMsg >DB Error in Creating User</div>";
                    }
                    if($_GET['msg']==-2)
                    {
                        echo "<div class=classMsg >Mendatory fields must be filled</div>";
                    }
                    echo "</div>";
                }
                
            ?>
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">		
			
            <div id="idDivSignUp" class="header_0345">Create User
                
            </div>
            <div class="classHorizHRSubHead"></div>				                 
            <br/><br/> 
          
                <form action="addUser.php" method="POST" name="AddUserForm" id="idFormAddUser" enctype="multipart/form-data">                          
                 <!-- To Check User Type Tester or User -->
                <div id="idDivUserType" class="classDisplayProfile">
                    <span id="idSpanFullName" class="classSpanUserInfoTxt">User Type  <span class='classRed'>*</span></span>                    
                    <span id="idSpanFullName" class="class">:
                        <lable for="idRadioUser" >User</lable><input type="radio" name="UType" id = "idRadioUser" value="1" checked="checked">
                        &nbsp;&nbsp;&nbsp;<lable for="idRadioTester">Tester User</lable><input type="radio" name="UType" id = "idRadioTester" value="2">                        
                    </span>
                </div> 
                
    <!-- The Below div having id=idDivForSingle is use to disable the control when user are creatd in bulk.  -->
             <div id="idDivForSingle" class="classDivForSingle">                             
				
				<div id="idDivUserfullName" class="classDisplayProfile">
                   <span id="idSpanFullName" class="classSpanUserInfoTxt"> Full Name <span class='classRed'>*</span></span>
                   <span id="idSpanFNDb" class="classSpanUserDetailInput">:
                    <input type="text" id="idInputFullName" name="Name" class="classUserDtInput validate[required,custom[onlyLetterSp]] text-input" value=""/>
                   </span>
                </div>
                
                <div id="idDivDob" class="classDisplayProfile">
                   <span id='idSpanDOB' class='classSpanUserInfoTxt'> DOB </span>
                   <span id='idSpanDOBDb' class='classSpanUserDetailInput'>:
                    <span class='input-prepend'>
                    <span class='add-on'>
                    <i class='icon-calendar'></i>
                    </span> 
                   <input type='text' id='idInputDob' name='userdob' class='classContactNoUpProf validate[custom[date]] text-input'/>
                   </span>
                   </span>                        
                </div>

                <div id="idDivContactNo" class="classDisplayProfile">
                   <span id="idSpanConNo" class="classSpanUserInfoTxt"> Mobile No. <span class='classRed'>*</span></span>
                   <span id="idSpanConNoDb" class="classSpanUserDetailInput">: 
                    <span class="input-prepend">
                    <span class="add-on">
                    +91
                    </span>
                    <input type="text" id="idInputConno" name="Contact" maxlength="10" class="Contact classContactNoUpProf validate[required,minSize[10]] text-input" value=""/>
                    </span>
                   </span>
                <span class="checkContact classDivUserNameAvailableProfile" id="idDivDisplayContactAvailability" >
                </span>
                </div>
               

                <div id="idDivUName" class="classDisplayProfile">
                   <span id="idSpanUName" class="classSpanUserInfoTxt"> Email <span class='classRed'>*</span></span>
                   <span id="idSpanIUName" class="classSpanUserDetailInput"> :
                  <span class="input-prepend">
                  <span class="add-on">
                  <i class="icon-envelope"></i>
                  </span>
                    <input type="text" class="username classContactNoUpProf class_login_txt validate[required] text-input username" id="id_login_txt3" name="username" value=""/>
                   </span>
                   </span>
                <span class="check classDivUserNameAvailableProfile" id="idDivDisplayAvailability" >
                </span>
                </div>

                <div id="idDivUPass" class="classDisplayProfile">
                  <span id="idSpanUPass" class="classSpanUserInfoTxt">Password  <span class='classRed'>*</span></span>
                  <span id="idSpanIUPass" class="classSpanUserDetailInput">: <input type="Password" class="classUserDtInput class_login_txt validate[required,minSize[6]] text-input" id="id_Pass" name="Password" value="" onkeyup="passwordStrength(this.value)"/></span>
                    <div id="idPasswordStrength" class="classRasswordStrength">
                          <div id="passwordDescription"></div>
                          <div id="passwordStrength" class="strength0"></div>
                    </div>
                </div>
                 <div id="idDivUCPass" class="classDisplayProfile">
                  <span id="idSpanUCPass" class="classSpanUserInfoTxt">Confirm Password <span class='classRed'>*</span></span>
                <span id="idSpanIUPass" class="classSpanUserDetailInput">: <input type="Password" class="classUserDtInput class_login_txt validate[required,equals[id_Pass]] text-input" id="id_login5" name="CPassword" value=""/></span>
                </div>


                <div id="idDivUserAddress" class="classDisplayProfile">
                   <span id="idSpanAddress" class="classSpanUserInfoTxt"> Address </span>&nbsp;:
                   <span id="idSpanAddressDb" class="classSpanUserDetailInput">
                    <textarea id="idInputAddress" name="userAddress" class="classSpanUserAddress" ></textarea>
                   </span>
                </div>

                <div id="idDivUserCity" class="classDisplayProfile">
                   <span id="idSpanCity" class="classSpanUserInfoTxt"> City </span>
                   <span id="idSpanCityDb" class="classSpanUserDetailInput">:
                    <input type="text" id="idInputCity" name="userCity" class="classUserDtInput validate[custom[onlyLetterSp]] text-input" value=""/>
                   </span>
                </div>
                
                <div id="idDivUserDist" class="classDisplayProfile">
                   <span id="idSpanDist" class="classSpanUserInfoTxt"> District </span>
                   <span id="idSpanCityDb" class="classSpanUserDetailInput">:
                    <input type="text" id="idInputDist" name="userDist" class="classUserDtInput validate[custom[onlyLetterSp]] text-input" value=""/>
                   </span>
                </div>
                
                <div id="idDivUserState" class="classDisplayProfile">
                   <span id="idSpanState" class="classSpanUserInfoTxt"> State </span>
                   <span id="idSpanStateDb" class="classSpanUserDetailInput"> :
                    <input type="text" id="idInputState" name="userState" class="classUserDtInput validate[custom[onlyLetterSp]] text-input" value=""/>
                   </span>
                </div>
                
                <div id="idDivUserCountry" class="classDisplayProfile">
                   <span id="idSpanCountry" class="classSpanUserInfoTxt"> Country </span>
                   <span id="idSpanCountryDb" class="classSpanUserDetailInput">:
                    <input type="text" id="idInputCountry" name="userCountry" class="classUserDtInput validate[custom[onlyLetterSp]] text-input" value=""/>
                   </span>
                </div>
                <br/>
                <div id="idDivUserCountry" class="classDisplayProfile">
                   <span id="idSpanCountry" class="classSpanUserInfoTxt"><span class="classRed">*</span> Fields are Mandatory.</span>
                   <span id="idSpanCountryDb" class="classSpanUserDetailInput">
                    
                   </span>
                </div>
                <br/>
             </div> 

                <div id="idDivUserCountry" class="classDisplayProfile">
                   <span id="idSpanAddUserSubmit" class="classSpanUserInfoTxt"> </span>
                   <span id="idSpanAddUserSubmitBut" class="classSpanUserDetailInput">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                   <input type="submit" value="Create User" id="idCreateSubmitBtn" name="createUser" class="btn btn-primary classSignUpButtenSize">
                  </span>
                </div>
            </div>   
          </form> 
          </div>                                
        </div> 
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>