<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>User Test Details</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<style type="text/css" title="currentStyle">
    @import "media/css/demo_page.css"; 
	@import "media/css/header.ccs";
	@import "media/css/demo_table_jui.css";
	@import "media/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#idDisplayAllTest').dataTable({
					"bJQueryUI": true,
					"sPaginationType": "full_numbers"
				});
            } );
        </script>
<!-- datatable js -->
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />

</head>
<body>
	<?php
			session_start();
			include ('classConnectQA.php');
			$ut=NUll;
			$iLoginId=Null;
			if(isset($_SESSION['ut']))	
			{
				$ut=$_SESSION['ut'];
			}	
			if(isset($_SESSION['lid']))		// This is Use to check a Session
			{
				$iLoginId = $_SESSION['lid'];
			}
			else
			{
				header("location:index.php");
			}
			
			
			$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();
			$_SESSION['user_id'] = $aRowForUserInfo[1];
			/*@ when connetion failed.*/
			if ($mysqli->errno) 									
			{	
				echo '<script type="text/javascript"> alert("User is Still Activated"); 
						window.location.replace("manageTest.php");</script>';
			}
			/*@ when connetion Established.*/
			else
			{						
				$iactive=0;
				$ideactive=0;
				$icounter=0;
				$iactive=0;
				$ideactive=0;
				$icounter=0;
				$id=Null;
				$ut=$_SESSION['ut'];	
				$iTestid=Null;
				$iMsg=Null;
				$iUid=Null;
				if(isset($_GET['uid']))	
				{
					$iUid=$_GET['uid'];
				}
		?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					
						<?php 

					if($ut==0||$ut==2)
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
									<ul id='menu'>
									<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
									</li>
									<li>
										<a href='manageTest.php'>Home</a>		
									</li>";
					
						if($ut==2)
						{
							echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
						}
						else
						{
							echo "
									<li><a >Opportunity</a>
		                                <ul>
		                                <li>
		                                    <a href='opportunityHTML.php'>Create</a>       
		                                </li>
		                                <li>
		                                    <a href='showOpportunity.php'>Manage</a>            
		                                </li>
		                                </ul>
		                            </li>
		                            <li>
								<a>Create</a>
								<ul>
									<li>
									<a href='groupHTML.php'>Create Group</a>		
									</li>
									<li>
										<a href='addTestHTML.php'>Create Test</a>		
									</li>
									<li>
										<a href='addUserHTML.php'>Create User</a>			
									</li>
									<li>
										<a href='excelReader/index.php'>Bulk Upload</a>			
									</li>
								</ul>
							</li>";
						} 
						
						echo "<li>
									<a>Manage </a>  
									  <ul>
											<li>
								                <a href='manageGroup.php'>Manage Group</a>      
								            </li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
									</ul>	
								</li>
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";
					}	
					else
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
								<ul id='menu'>
								<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
								</li>
								<li>
									<a href='manageTest.php'>Home</a>		
								</li>
								
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";	
						}						
					
						?>
				</div>
			</div>  	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				<div id="idDivSignUp" class="header_0345">User :
				<?php 
				/*
					To view the user name.
				*/
				$sUserQuery="select user_id, user_full_name from user_details where user_id={$iUid}";

				$aResult=$mysqli->query($sUserQuery);
				$Urow=$aResult->fetch_row();
				echo $Urow[1];
				?>
				</div>
				<div class="classHorizHRSubHead"></div>
				<div id="idDivFname" class="classDivAddTest">

			<?php	

				echo "<div id='idDivDispAllTest'>";
				echo "<table cellpadding='0' cellspacing='0' border='0' class='display classForTable' id='idDisplayAllTest' width='100%''>";
                
				echo "<thead><tr><th>Sr. No.</th> <th>Test</th><th>Test Attempt On</th><th>Code</th><th>Score</th><th>Result</th>";		
				echo "</tr></thead>";
				// loop through result of database query, displaying them in the table.
				$sTestCode=Null;
				$sScore=Null;
				$sQuery="select distinct b.sb_doa, a.test_name, c.test_id, a.test_scope from test_detail as a,score_board_table as b , stud_ans_table as c where b.user_id = {$Urow[0]}  AND c.test_id = b.test_id  and a.test_id = b.test_id  and a.test_id=c.test_id";
				$bResult=$mysqli->query($sQuery);
				if($bResult!=Null)
				{
					
					while($row=$bResult->fetch_row())
					{		
						$icounter++;												
						echo "<tr>";
						echo '<td align="middle" class="classWidthForDataTableColumn20">' . $icounter . '</td>';

						echo '<td align="middle">';
						
						echo $row[1];
						if($row[3]==2) 					// If test is common to all.
						{
							echo "<span class='classSmallFontForOperation'> (PUBLIC)</span>";
						}
						 echo '</td>'; // Test Name
						
						echo '<td align="middle">' . $row[0] . '</td>'; // Test Attempt On
						
						$bCodeResult=$mysqli->query("select test_code from test_detail where test_id={$row[2]}");
						if($bCodeResult!=Null)
						{
							$sTCRow=$bCodeResult->fetch_row();
							$sTestCode=$sTCRow[0];
						}
						echo '<td align="middle">'.$sTestCode.'</td>'; 		// Test Code.
	

						$bScoreResult=$mysqli->query("select sb_marks_obt from score_board_table where user_id={$iUid} and test_id={$row[2]}");
						if($bScoreResult==true)
						{
							$aScoreRow=$bScoreResult->fetch_row();	
							if($aScoreRow==true)
							{					
								if($aScoreRow[0]==null)			
								{
									$sScore="Pending";	
								}
								else
								{
									$sScore=$aScoreRow[0];		
								}								
							}
							else
							{
								$sScore="Pending";	
							}
						}
						
						echo "<td align='middle'>{$sScore}</td>"; 		// Test Score.
						echo "<td align='middle'><a href=testResultPreview.php?usid={$Urow[0]}&id={$row[2]}><img src='images/View.png' class='classAddIcon' /></a></td>";

					}	
					echo '</tr>';
				}
			}				
			echo '</table></div>';
			$mysqli->close();				
				
		
		?>
				</div>
            </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>