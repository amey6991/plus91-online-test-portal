<html>
<body style="margin: 10px;">
<div style="width: 500px; font-family: Arial, Helvetica, sans-serif; font-size: 11px;">
<div align="center"><img src="images/emailheader.jpg" style="height: 40px; width: 500px;-moz-border-radius: 10px;
						-webkit-border-radius: 10px;
    					border-radius: 10px;
    					border: 1px solid #C0C0C0;"></div><br>

<?php 

	include('classConnectQA.php');
 	
	
	$iTestId= $_GET['id'];
	session_start();
	$iUserId = $_GET['usid'];
/* This selects user information from database */
    $sQueryForUserDetail = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                from login as a , user_details as b
                where a.login_id = b.login_id 
                AND b.user_id  = $iUserId limit 1";
    $iResultForUserInfo = $mysqli->query($sQueryForUserDetail);
    $aRowForUserInfo = $iResultForUserInfo->fetch_row();
    
    $sUserFullName = $aRowForUserInfo[2];


/* 2) This code give test result like - total number of question in test , number of question attempted by the student,
		number of right question , number of wrong question , total mark and percentage  */
		$sQuery = "select a.que_question, a.que_id , a.test_id , a.que_type , a.que_marks ,b.test_name
					from question_table as a , test_detail as b 
					where b.test_id = a.test_id 
					AND b.test_id = {$iTestId}";
		$iResult = $mysqli->query($sQuery); 	/* This query displayes the question from database */
		$iQue_Count = $iResult->num_rows;

		$sStdResQuery = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
						b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks ,b.op_option as OptionText
						from stud_ans_table as a , option_table as b , question_table as c
						where a.user_ans_answer = b.op_id 
						AND b.op_correct_ans  = 1 
						AND b.que_id = c.que_id 
						AND a.user_id = {$iUserId} 
						AND a.test_id = {$iTestId}";
						/* this query select user test data from database.*/
		$iStdResult = $mysqli->query($sStdResQuery);		/* $iStdResult - this variable stores query result */
		
		$iRowCount = 0;
		$iObtenMarks=0;

		while($aRowStdResult = $iStdResult->fetch_row())
		{
		$iObtenMarks += $aRowStdResult[6];
		$iRowCount = $iRowCount + 1;
		}

		$sGetTestNameQuery = "select test_name from test_detail where test_id={$iTestId}";	/* this query select test name from test details */
		$iGTNResult = $mysqli->query($sGetTestNameQuery);
		$aRowGTNResult = $iGTNResult->fetch_row();

		$iTotalAttemptQueQuery = "select * from stud_ans_table where user_ans_answer <> '' AND user_id = {$iUserId} AND test_id = {$iTestId}"; 
										/* This query gives number of attempted question by the user for perticular test */
		$iAtempQueResult = $mysqli->query($iTotalAttemptQueQuery);
		$iAttemptQue_Count = $iAtempQueResult->num_rows;

		$sQueryForMarks = "select test_id,que_id,que_question,que_marks,que_neg_marks 
							from question_table where test_id = {$iTestId}";
					/* This query gives number of marks for each question and negative marks for each question */
		$iQueForMrkResult = $mysqli->query($sQueryForMarks);
		$iTotalMks = 0;
		$iTM = 1;
		while($aRowTotalMks = $iQueForMrkResult->fetch_row())
		{
		$iTotalMks += $aRowTotalMks[3];
		}

		/*$sWrongQue = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
					b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks , c.que_neg_marks as Negativemark 
					from stud_ans_table as a , option_table as b , question_table as c
					where a.user_ans_answer = b.op_id 
					AND b.op_correct_ans  != 1 
					AND b.que_id = c.que_id 
					AND a.user_id = {$iUserId} 
					AND a.test_id = {$iTestId}";	
					/* This query calculate number of wrong question and thir mark
		$iWrongQueResult = $mysqli->query($sWrongQue);
		$iTotalNegMark = 0;
		$sMsgForResult = "";
		while($aRowWrongQueResult = $iWrongQueResult->fetch_row())
		{
			$iTotalNegMark += $aRowWrongQueResult[7];
		}
		$iCountWrongQue = $iWrongQueResult->num_rows;
		*/
		$sQueryForTimeTimeTk = "select a.sb_time_taken,a.sb_doa,a.sb_attempt_que,a.sb_correct_ans,a.sb_wrong_ans,a.sb_marks_obt,a.sb_marks_out_of
							from score_board_table as a
							where a.user_id = {$iUserId}
							AND a.test_id = {$iTestId}";   
	    $iResultForTimeTimeTk = $mysqli->query($sQueryForTimeTimeTk);
	    $aRowForTimeTimeTk = $iResultForTimeTimeTk->fetch_row();  
		echo "<div id='idDisplayHeading' 
						style='font-size: 11px; 
						color: #000000;
						margin-left:0px; 
						background-color:#F8F8F8;
						-moz-border-radius: 10px;
						-webkit-border-radius: 10px;
    					border-radius: 10px;
    					border: 1px solid #C0C0C0;
    					margin-bottom:10px;
    					padding-left:30px;
    					padding-top:10px;
    					padding-bottom:10px;
    					padding-right:30px;'>";
		echo "<div class='class_form_div_quiz' id='id_form_div_addTest' style='font-size: 11px; color: #000000'>							
				<div id='idDivFname' class='classDivAddTest'>	
					<span id='idSpanTabFName11' class='classSpanTabText'>Name : {$sUserFullName}<span><br/>
					<span id='idSpanTabFName1' class='classSpanTabText'>Test Name</span>
					<span id='idSpanTabFName:1' class='classSpan'>:</span>
					<span id='idSpanTabFNameIP1' class='classSpan'>";
						echo $aRowGTNResult[0];					
			echo "</span>
				</div>
				<div id='idDivMname2' class='classDivAddTest'>
					<span id='idSpanTabMName2' class='classSpanTabText'>Total Question</span>
					<span id='idSpanTabMName:2' class='classSpan'>:</span>
					<span id='idSpanTabMNameIP2' class='classSpan'>";
						echo $iQue_Count;
			echo "</span>
				</div>
				<div id='idDivDesg7' class='classDivAddTest'>
					<span id='idSpanTabDesg7' class='classSpanTabText'>Total Marks</span>
					<span id='idSpanTabDesg:7' class='classSpan'>:</span>
					<span id='idSpanTabDesgIP7' class='classSpan'>";
						echo $aRowForTimeTimeTk[6];
			echo"</span>
					</span>
				</div>
				<div id='idDivOrgName5' class='classDivAddTest'>
					<span id='idSpanTabOName5' class='classSpanTabText'>Question Attempt</span>
					<span id='idSpanTabOName:5' class='classSpan'>:</span>
					<span id='idSpanTabONameIP5' class='classSpan'>";
						echo $aRowForTimeTimeTk[2];
			echo"</span>
				</div>
				<div id='idDivLname3' class='classDivAddTest'>
					<span id='idSpanTabLName3' class='classSpanTabText'>Answer Right</span>
					<span id='idSpanTabLName:3' class='classSpan'>:</span>
					<span id='idSpanTabLNameIP3' class='classSpan'>";
						echo $aRowForTimeTimeTk[3];
			echo"</span>
				</div>
				<div id='idDivNat4' class='classDivAddTest'>
					<span id='idSpanTabNat4' class='classSpanTabText'>Answer wrong</span>
					<span id='idSpanTabNat:4' class='classSpan'>:</span>
					<span id='idSpanTabNatIP4' class='classSpan'>";
						echo $aRowForTimeTimeTk[4];
			echo"</span>
				</div>";
			echo"<div id='idDivTMarks454' class='classDivAddTest'>
					<span id='idSpanTabF354' class='classSpanTabText'>Time Taken</span>
					<span id='idSpanTabS354' class='classSpan'>:</span>
					<span id='idSpanTabT354' class='classBlack'>";
						echo $aRowForTimeTimeTk[0];
			echo"</span>
				</div>";
				/* If there is no descriptive question then result send to admin without calculating descriptive mark */
				$sGetDescQue = "select count(a.que_type) 
					from question_table as a , test_detail as b 
					where b.test_id = a.test_id 
					AND a.que_type = 'Descriptive'
					AND b.test_id ={$iTestId} limit 1";	/* this query select test name from test details */
				$iGDescQueResult = $mysqli->query($sGetDescQue);
				$aRowGDescQueResult = $iGDescQueResult->fetch_row();
				$iCountDescQue = $aRowGDescQueResult[0];

				if($iCountDescQue >= 1)  /* This function checks if test have a descriptive question or not , and if have then calculate score including descriptive question mark */
				{
					/* This query calculates the descriptive question mark */
						$sQcCalDescMark = "select a.test_id ,a.user_id , a.des_marks
											from des_marks_table as a 
											where a.test_id ={$iTestId} AND a.user_id = {$iUserId}";
						$iResultForCalDescMark = $mysqli->query($sQcCalDescMark);
						$iTotalDescMks =0;

						while($aRowCalDescQueMark = $iResultForCalDescMark->fetch_row())
						{
							$iTotalDescMks += $aRowCalDescQueMark[2];
						}
					echo"<div id='idDivDesg6' class='classDivAddTest'>
							<span id='idSpanTabDesg6' class='classSpanTabText'>Marks Obtain</span>
							<span id='idSpanTabDesg:6' class='classSpan'>:</span>
							<span id='idSpanTabDesgIP6' class='classSpan'>";
								$iObtMks = $aRowForTimeTimeTk[5];
								echo $iObtMks;
					echo"</span>
						</div>";
				}
				else
				{
					echo"<div id='idDivDesg6' class='classDivAddTest'>
							<span id='idSpanTabDesg6' class='classSpanTabText'>Marks Obtain</span>
							<span id='idSpanTabDesg:6' class='classSpan'>:</span>
							<span id='idSpanTabDesgIP6' class='classSpan'>";
								$iObtMks = $aRowForTimeTimeTk[5];
								echo $iObtMks;
					echo"</span>
						</div>";
				}
				
				echo"<div id='idDivDesg7' class='classDivAddTest'>
					<span id='idSpanTabDesg7' class='classSpanTabText'>Percentage</span>
					<span id='idSpanTabDesg:7' class='classSpan'>:</span>
					<span id='idSpanTabDesgIP7' class='classSpan'>";

						if($iTotalMks == 0)
						{
							$iPercent = ($iObtMks/$iTM)*100;
							echo  round($iPercent,2) ; echo " %";	
						}
						else
						{
							$iPercent = ($iObtMks/$iTotalMks)*100 ;
						echo round($iPercent,2); echo " %";	
						}		

			echo"</span>
					</span>
				</div>
				<div id='idDivDesg7' class='classDivAddTest'>
					<span id='idSpanTabDesg8' class='classSpanTabText'>Remark</span>
					<span id='idSpanTabDesg:8' class='classSpan'>:</span>
					<span id='idSpanTabDesgIP8' class='classSpan'>";
							if( $iPercent > 60 )
							{
								echo "Excellent";
							}
							
							if( $iPercent >= 35 AND $iPercent <= 60 )
							{
								echo "Good";
							}
							if( $iPercent <= 35 )
							{
								echo "Very Poor";
							}
				echo"</span>
					</span>
				</div></div></div>";

			/* end of 2 */
			
	/* 1) This displays the total question in the exam and thir option . also display user answer and right answer */

		echo "<div id='idDisplayHeading' 
						style='font-size: 11px; 
						color: #000000;
						margin-left:0px; 
						background-color:#F8F8F8;
						-moz-border-radius: 10px;
						-webkit-border-radius: 10px;
    					border-radius: 10px;
    					border: 1px solid #C0C0C0;
    					margin-bottom:10px;
    					padding-left:30px;
    					padding-top:10px;
    					padding-bottom:10px;
    					padding-right:30px;'>Solved Test With Answer<br/><br/>";
								
		$ii=1;
		$iMulti=1;
		while($aRow = $iResult->fetch_row())	// this function fetches all question and their option from database
		{
			echo "<div id='idDivDisplayQuestion' class='classDivDisplayQuestion' style='font-size: 11px; color: #000000'>";
			echo "<div id='idDivDisplayQuestion2' class='classDivDisplayQuestion' style='font-size: 11px; color: #000000'>";

			//! display data in table
			$iQue_id = $aRow[1];
			$iTest_id = $aRow[2];
			$sQueType = $aRow[3];
			$iQueMarks = $aRow[4];

			$sQueryQtpFromImg = "select * from question_attachment as a where a.que_id = {$iQue_id} AND file_status = 1";
                                        /* 1] this query return af question have any attachment */     
            $iResultAttachment = $mysqli->query($sQueryQtpFromImg);
            $aFetchRowAttachment = $iResultAttachment->fetch_row();
            $sQtpInAttachment = $aFetchRowAttachment[2];
            $sAttachLocation = $aFetchRowAttachment[3];
            $sAttachMentName = $aFetchRowAttachment[4];
            /* end of - 1 */
			echo "<span style='font-size: 12px; color: #000000'>{$ii}. {$aRow[0]}</span>";			/* This displays question and question number */

			$sQuerySel = "select a.op_id as op_id, a.op_option as opt, a.op_correct_ans as cra 
			from option_table as a ,question_table as b , test_detail as c 
							where c.test_id = b.test_id 
							And b.que_id = a.que_id 
							AND c.test_id ={$iTestId} 
							AND b.que_id ={$iQue_id}";
					/* This query select option related data fetch from option table*/
			$iResult2 = $mysqli->query($sQuerySel);
			
			echo "<div id='idDivDispQueAns' style='font-size: 11px; color: #000000'>";
			$sQueryTOCheckQueType = "select que_type from question_table 
										where que_id = {$iQue_id} 
										AND test_id = {$iTestId}";
								/* This query select question type from question table */
			$iResultForQueType = $mysqli->query($sQueryTOCheckQueType);
			$aRowForQueType = $iResultForQueType->fetch_row();
			$sQuestionType = $aRowForQueType[0];
			$sQueryForCorrectAns = "select distinct b.op_option as OptionText ,b.op_id 
									from test_detail as a , option_table as b , question_table as c
									where a.test_id = c.test_id  
									AND b.que_id = c.que_id
									AND b.op_correct_ans = 1
									AND a.test_id = {$iTestId}
									AND c.que_id = {$iQue_id} limit 1";
					/* This query display correct answer for question from database */
			$aCorrectAnsRes = $mysqli->query($sQueryForCorrectAns);
			$aRowCorrectAnsRes = $aCorrectAnsRes->fetch_row();
			$sAns=$aRowCorrectAnsRes[0];

			$sQueryDispUserAns = "select distinct a.user_ans_answer  
									from stud_ans_table as a
									where  
									a.test_id = {$iTestId}
									AND a.user_id = {$iUserId}
									AND a.que_id = {$iQue_id} limit 1";
									/* This Query select user answer id from database */
			$aUserAnsIdRes = $mysqli->query($sQueryDispUserAns);
			$aRowUserAnsIdRes = $aUserAnsIdRes->fetch_row();
			$iUserAnsId = $aRowUserAnsIdRes[0];
			if($iUserAnsId != 0)
			{
			$sQueryForUserAns = "select 
									b.op_option
									from question_table as a , option_table as b
									where 
									b.op_id = {$iUserAnsId}
									AND a.test_id = {$iTestId}
									AND a.que_id = b.que_id 
									AND b.que_id = {$iQue_id} limit 1";
									/* This query use to check user answer is right or wrong */
			$aUserAnsRes = $mysqli->query($sQueryForUserAns);
			$aRowUserAnsRes = $aUserAnsRes->fetch_row();
			$sUAns = $aRowUserAnsRes[0];
			}
			else
			{
				$sUAns = $aRowUserAnsIdRes[0];
			}
				if($sQuestionType == 'Objective')		/* If Question is objective type then radio butten display by using this function */
				{	
					if($sQtpInAttachment == $sQuestionType)
                        {
                            echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'>
                            <img src='$sAttachLocation$sAttachMentName' style='-webkit-border-radius: 5px;border-radius: 5px;border: 1px solid #C0C0C0;' id='idQueAttachment' class='classQuestionAttachment' width='200' height='100'/></div>";
                        }
					$ij=1;
					while($aRowOpt = $iResult2->fetch_row())
					{
							$iOpidAns = $aRowOpt[0];
							 echo "<div>";
							 if($iOpidAns == $iUserAnsId)
							 {
									
									echo "<span width = '20' style='display:inline-table;margin-top:3px;'><input type ='radio' name ='ans{$ii}' value='$iOpidAns' checked='checked' disabled='disabled'></span>";
								 	echo "<span style='display:inline-table;margin-left:5px;vertical-align:5px;'>{$aRowOpt[1]}";
								
								if($aRowCorrectAnsRes[1] == $iOpidAns) /* This function compairs the user ans and display image right or wrong */
								 {
								 	echo "<span width = '40' style='display:inline-table;vertical-align:-5px;margin-left:20px;'><img src='images/right.jpg' width='18px' height='18px'></span>";
								 }
								 else
								 {
								 	echo "<span width = '40' style='display:inline-table;vertical-align:-5px;margin-left:20px;'><img src='images/wrong.jpg' width='18px' height='18px'></span>";
							 	 }

							 }
							 else
							 {
							 	if($aRowCorrectAnsRes[1] == $iOpidAns)
							 	{
								 	 echo "<span width = '20' style='display:inline-table;margin-top:3px;'><input type ='radio' name ='ans{$ii}' value='$iOpidAns' disabled='disabled'></span>";
									 echo "<span width = '280' style='display:inline-table;margin-left:5px;vertical-align:5px;'>{$aRowOpt[1]}</span><span width = '40' style='display:inline-table;vertical-align:-5px;margin-left:20px;'><img src='images/right.jpg' width='18px' height='18px'></span>";
								}
								else
								{
									echo "<span width = '20' style='display:inline-table;margin-top:3px;'><input type ='radio' name ='ans{$ii}' value='$iOpidAns' disabled='disabled'></span>";
									 echo "<span width = '280' style='display:inline-table;margin-left:5px;vertical-align:5px;'>{$aRowOpt[1]}</span>";
								}

							 }
							 echo "</div>";
							 $ij++;
					} 
				}
				if($sQuestionType == 'Descriptive')
				{
					if($sQtpInAttachment == $sQuestionType)
                        {
                            echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'>
                            	<img width='200' height='100' src='$sAttachLocation$sAttachMentName' style='-webkit-border-radius: 5px;border-radius: 5px;border: 1px solid #C0C0C0;' id='idQueAttachment' class='classQuestionAttachment'/></div>";
                        }

                        if($iUserAnsId == "")
                        {
                        	echo "<div>
									 <span style='display:inline-table' width='50'>Ans :-</span>
									 <span width = '250'>Not attempted.</span>
								</div>";
                        }
                        else
                        {
							echo "<div>
								 <span style='display:inline-table' width='50'>Ans :-</span>
								 <span width = '250'>$iUserAnsId</span>
							</div>";
						}
				 }
				 if($sQuestionType == 'Multiple')
                {
                    

                    $sQueryMulChoice = "select a.multi_op_id as op_id, a.multi_option as opt, a.multi_op_ans as cra from multi_option_table as a ,question_table as b , test_detail as c 
                    where c.test_id = b.test_id And b.que_id = a.que_id AND c.test_id ='$iTestId' AND b.que_id ='$iQue_id'";
                            /* This query select option related data fetch from option table*/
                    $iResultMulChoice = $mysqli->query($sQueryMulChoice);

                    $iChk = 1;
                    if($sQtpInAttachment == $sQuestionType)
                        {
                            echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'>
                            	<img width='200' height='100' style='-webkit-border-radius: 5px;border-radius: 5px;border: 1px solid #C0C0C0;' src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></div>";
                        }
                    echo "<div>";
                    while($aRowMultiOpt = $iResultMulChoice->fetch_row())
                    {
                            
                            $iOpidAns = $aRowMultiOpt[0];
                            /* this query return write answer for each question */
                            $sQueryRightMultiple = "select count(b.multi_op_id) 
                                                    from multi_option_table as b , question_table as c
                                                    where b.multi_op_ans  = 1 
                                                    AND b.que_id = c.que_id 
                                                    AND c.test_id = {$iTestId}
                                                    AND c.que_id = {$iQue_id}
                                                    AND b.multi_op_id = $iOpidAns";
                            $iResultMultiRight = $mysqli->query($sQueryRightMultiple);
                            $aRowFetchRightAns = $iResultMultiRight->fetch_row();   
                            /* end of multi choice */
                            if($aRowFetchRightAns[0] >= 1)
                            {
                                 echo "<div>";
                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
                                 
                                 echo "<input checked='checked' disabled='disabled' type ='checkbox' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
                                 echo "</span>";
                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $aRowMultiOpt[1] . "</span>";
                                 echo "<span id= 'idSpanRightImg' class='classRightWrongIcon'>
                                 <img  width='18px' height='18px' src='images/right.jpg'></span>";
                                 echo "</div>";

                            }
                            else
                            {
                                 echo "<div>";
                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
                                 
                                 echo "<input disabled='disabled' type ='checkbox' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
                                 echo "</span>";
                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $aRowMultiOpt[1] . "</span>";
                                 echo "</div>";
                            }
                            
                        
                             $iChk++;
                    }
                    
                    /* 5] this query returns all the ans given by user for perticular question */
                    $sQueryDIsplayUserAns = "select a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,b.multi_option
                                            from stud_ans_table as a , multi_option_table as b , question_table as c
                                            where a.user_ans_answer = b.multi_op_id 
                                            AND b.que_id = c.que_id 
                                            AND a.user_id = {$iUserId} 
                                            AND a.test_id = {$iTestId}
                                            AND c.que_id = {$iQue_id}";
                    $iResultDispUserAns = $mysqli->query($sQueryDIsplayUserAns);
                    if($iResultDispUserAns == true)
                    {   
                        echo "<div id='idDivDispUsersAns' class = 'classDivDispUsersAns'>";
                        echo "<span>User Answer :- </span>";
                        while ($aFetchUserAllAns = $iResultDispUserAns ->fetch_row())
                        {
                            $iUserAnsID = $aFetchUserAllAns[3];
                            $sUserAnsTxt = $aFetchUserAllAns[4];
                            /* This query returns user right answer from multiple choice */
                            $sQuerySelctUserRightAns = "select count(b.multi_option)
                                                        from stud_ans_table as a , multi_option_table as b , question_table as c
                                                        where a.user_ans_answer = b.multi_op_id 
                                                        AND b.que_id = c.que_id 
                                                        AND b.multi_op_ans = 1
                                                        AND a.user_id = {$iUserId} 
                                                        AND a.test_id = {$iTestId}
                                                        AND c.que_id = {$iQue_id}
                                                        AND b.multi_op_id = {$iUserAnsID}";
                            $iResultSelctUserRightAns = $mysqli->query($sQuerySelctUserRightAns);
                            
                            $aRowFetchUserRightAns = $iResultSelctUserRightAns->fetch_row();
                            if($aRowFetchUserRightAns[0] >= 1 )
                             {
                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
                                 echo "<input type ='checkbox' disabled='disabled' checked='checked' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
                                 echo "</span>";
                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $sUserAnsTxt . "</span>";
                                 echo "<span id= 'idSpanRightImg' class='classRightWrongIcon'>
                                 <img  width='18px' height='18px' src='images/right.jpg'></span>";
                            }
                            else
                            {
                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
                                 echo "<input type ='checkbox' disabled='disabled' checked='checked' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
                                 echo "</span>";
                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $sUserAnsTxt . "</span>";
                                 echo "<span id= 'idSpanRightImg' class='classRightWrongIcon'>
                                 </span>";
                            }
                             
                        }
                        echo "</div>";
                        
                    }
                        /* end of [5 */
                    echo "</div>";
                    $iMulti++;
                }
				
			$sQueryAns = "select op_id from option_table where op_correct_ans = 1 and que_id = $iQue_id";
			$iResult3 = $mysqli->query($sQueryAns);
			$aRowForAns = $iResult3->fetch_row();						
			
			echo "</div><br/>";
			echo "</div> ";
			
			$ii++;
			echo "</div>";
		}
		echo "";
	/* end of 1 */

		
		/* This query gives the user resume name from database */
	    $sQueryForResume = "select a.filename  
	                             from user_resume as a
	                             where a.user_id = $iUserId
	                             AND a.test_id = $iTestId limit 1";
	    $iResultForResume = $mysqli->query($sQueryForResume);
	    $aRowForUserResume = $iResultForResume->fetch_row();
	    $sUserResume = $aRowForUserResume[0];

	    /* -------------- */
	    //echo "<div id='idDivDispResume'><img src='Resume/$sUserResume'></div>";
?>
<br/>
</div>
<div align="center"><img src="images/emailheader.jpg" style="height: 40px; width: 500px;-moz-border-radius: 10px;
						-webkit-border-radius: 10px;
    					border-radius: 10px;
    					border: 1px solid #C0C0C0;"></div>
</body>
</html>