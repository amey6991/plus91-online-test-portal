<?php
    include ('classConnectQA.php');
   $iTestId = $_GET['id'];       //! get id from url
   $iUserId = $_GET['uid'];   //! get contactid from url
   $sctype="application/[extension]";
   
   $sFilePath = "Resume/";  //! the folder of the file that is downloaded , you can put the file in a folder on the server just for more order
   
   $sQuery = "select a.filename  
             from user_resume as a
             where a.user_id = $iUserId
             AND resume_status = 1 limit 1"; //! This query returns the file location.

   //! Execute query and compair result.
   
   $iResult = $mysqli->query($sQuery);  
		if (!$iResult) {
			echo 'Could not run query: ' . mysqli_error();
			exit;
		}

		$aRow = mysqli_fetch_row($iResult);

		$sfilename = $sFilePath.$aRow[0];

		if($sfilename == '')
		{
			exit();
		}
		else
		{
			//! required for IE, otherwise Content-disposition is ignored
			if(ini_get('zlib.output_compression'))
			ini_set('zlib.output_compression', 'Off');
			header("Pragma: public"); 					//!Send a raw HTTP header
			header("Expires: 0");
			header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
			header("Cache-Control: private",false); // required for certain browsers
			header("Content-Type: $sctype");
			header("Content-Disposition: attachment; filename=\"".basename($sfilename)."\";" );
			header("Content-Transfer-Encoding: binary");
			header("Content-Length: ".filesize($sfilename));
			readfile("$sfilename");
			exit();
		}
	$mysqli->close();
?>