<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Preview Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        <?php

                        // @file connect to the database.
                        include('classConnectQA.php');
						session_start();
						
						$iUid=$_SESSION['user_id'];
						if(isset($_SESSION['lid']))		// This is Use to check a Session
						{
							$iLoginId = $_SESSION['lid'];
						}
						else
						{
							header("location:index.php");
						}
						if(isset($_SESSION['ut']))  
			            {
			                $ut=$_SESSION['ut'];
			            }
			            else
			            {
			            	$ut=null;	
			            } 
						
						$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
											from login as a , user_details as b
											where a.login_id = b.login_id 
											AND a.login_id  = '$iLoginId' limit 1";
						$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
						$aRowForUserInfo = $iResultForUserInfo->fetch_row();

						$iGid=NUll;
						$iTestid=Null;
						$iTestid=Null;
						if(isset($_GET['gid']))
						{
							$iGid=$_GET['gid'];
						}
						if(isset($_GET['id']))
						{
							$iTestid=$_GET['id'];
						}
						$sTimerQuery = "select test_duration,test_name from test_detail where test_id= $iTestid";
						$iResTimerQuery = $mysqli->query($sTimerQuery);
						
						$aRowForTimer = $iResTimerQuery->fetch_row();
						
						$sQuery = "select a.que_question, a.que_id , a.test_id , a.que_type , a.que_marks ,b.test_name from question_table as a , test_detail as b where b.test_id = a.test_id AND b.test_id = '$iTestid'";
						$iResult = $mysqli->query($sQuery); 
						$iQue_Count = $iResult->num_rows;
						
						
						?>
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivMenuTop" class="classDivTopMenuUser">
				<?php
					echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
											<ul id='menu'>
					<li><a href='profile.php'>$aRowForUserInfo[2]</a>
					</li>
					<li>
						<a href='manageTest.php'>Home</a>		
					</li>";
					
					if($ut==2)
					{
						echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
					}
					else
					{
						echo "
						<li><a >Opportunity</a>
                                <ul>
                                <li>
                                    <a href='opportunityHTML.php'>Create</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Manage</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
							<a>Create</a>
							<ul>
								<li>
									<a href='groupHTML.php'>Create Group</a>		
								</li>
								<li>
									<a href='addTestHTML.php'>Create Test</a>				
								</li>
								<li>
									<a href='addUserHTML.php'>User</a>			
								</li>
								<li>
									<a href='excelReader/index.php'>Bulk Upload</a>			
								</li>
							</ul>
						</li>";
					} 
					
					echo "<li><a>Manage </a>  
								  <ul>
											<li>
								                <a href='manageGroup.php'>Manage Group</a>      
								            </li>
										<li>
											<a href='manageUser.php'>Manage User</a>			
										</li>
									  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
								</ul>	
							</li>
							<li>
								<a href='logout.php'>Logout </a>	
							</li>
							</ul>
							</div>";
				?>		
				
				</div>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->  
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
		<div id="idDivMiddleBody" class="classDivMiddleBody">
				
						<div id="idDivHeading" class="classDiv">
						<!-- Div to display the heading of the test. -->
							<div id="idDivHeadEdit" class="header_034"><span class="classSpanSmallWhite"><a href="editTest.php?id=<?php echo $iTestid; ?>"><b>[Edit]</b></a></span>Test Name : -<?php echo $aRowForTimer[1]; ?> <span id="idSpanTQ" class="classSpanTotalQue">Total Questions : <?php echo $iQue_Count; ?></span><span id="idSpanTT" class="classSpanTotalTime">|&nbsp;Time :- <?php echo $aRowForTimer[0]; ?> min</span></div>
							</div>
							<div class="classDivDisplayTime" style="font-weight:bold;margin-top:20px;margin-left:20px" id="idDivToDisplayTime"></div>
							<div class="class_form_div_quiz" id="id_form_div_addTest">							
								<div id="idDivDisplayQue" class="classDivTabForDescrAns">	
						
						<?php //! @brif this function figure out the total pages in the database
						echo "<span>Question Paper</span><br/><br/>";
						
						$ii=1;
							/*
								Below loop is use to display all the question with their options of a selected test.
							*/

						while($aRow = $iResult->fetch_row())
						{
							echo "<div class='classLineHeight'>";
						//! display data in table
							$iQue_id = $aRow[1];
							$iTest_id = $aRow[2];
							$sQueType = $aRow[3];
							$iQueMarks = $aRow[4];
						/* 
							below echo is use to link a page editQuestion.php. 
						*/	
							echo "<span>{$ii}. {$aRow[0]}</span>&nbsp;&nbsp;&nbsp;&nbsp;<span class=classSpanSmallWhite><a href='editQuestion.php?gid={$iGid}&id={$iTest_id}&qid={$iQue_id}'>[Edit]</a></span>";
						/*
							Query is to retrive the Question and its option.
						*/	
							$sQuerySel = "select a.op_id as op_id, a.op_option as opt, a.op_correct_ans as cra from option_table as a ,question_table as b , test_detail as c 
							where c.test_id = b.test_id And b.que_id = a.que_id AND c.test_id ='$iTestid' AND b.que_id ='$iQue_id'";	
							$iResult2 = $mysqli->query($sQuerySel);
							echo "<table class='classPreviewQuestionTable'><tr></tr>";
							$ij='A';
							while($aRowOpt = $iResult2->fetch_row())
							{
								$iOpidAns = $aRowOpt[0];
							  
								  echo "<tr>";
								  echo "<td width=10px> </td><td width=20px> $ij </td>";
								  
								  echo "<td>" . $aRowOpt[1] . "</td>";
								  echo "</tr>";	
								  $ij++;
							}
							$sQueryAns = "select op_id from option_table where op_correct_ans = 1 and que_id = $iQue_id";
							$iResult3 = $mysqli->query($sQueryAns);
							$aRowForAns = $iResult3->fetch_row();
							
							echo "</table><br/><br/>";
							echo "</div>";
							
							
							$ii++;
						}
						
						echo "<a href=manageTest.php?gid={$iGid}><input type='button' value='close' class='classButton classAddTestButtonSize'></a>";
						
                        //  !close database connection
                        $mysqli->close();
					
				?>
				<input type="hidden" name="totalQue" value="<?php echo $iQue_Count; ?>">
                </div>
				</div>
			 <div id="idDivNat" class="classDivTime">
                    <div id="idDivTq" class="classDivRight"></div>	
					<div id="idTT" class="classDivRight"></div>
					<hr></hr>
             </div>
		</div>
	</div>
  </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>