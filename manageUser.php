<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<title>Manage User</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<style type="text/css" title="currentStyle">
    @import "media/css/demo_page.css"; 
	@import "media/css/header.ccs";
	@import "media/css/demo_table_jui.css";
	@import "media/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>

<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#idDisplayAllTest').dataTable({
			"bJQueryUI": true,
			"sPaginationType": "full_numbers"
		});
    } );
</script>
	<?php
		session_start(); 
		 $iUMsg=Null;
        if(isset($_SESSION['msg']))
        {
             $iUMsg = $_SESSION['msg'];
             $sIdentifier = $_SESSION['identifier'];
             unset($_SESSION['msg']);
             unset($_SESSION['identifier']);
        } 
	?>			
<script  type="text/javascript" language="javascript">		

    var umsg = <?php echo $iUMsg; ?>;
</script>
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>
</head>
<body>
	<?php 
			include ('classConnectQA.php');	
			//ini_set('display_errors', 1); error_reporting(E_ALL);
			$ut=NUll;
			$iLoginId=Null;
			if(isset($_SESSION['ut']))	
			{
				$ut=$_SESSION['ut'];
			}	
			if(isset($_SESSION['lid']))		// This is Use to check a Session
			{
				$iLoginId = $_SESSION['lid'];
			}
			else
			{
				header("location:index.php");
			}

			//to show msg on server.
			 ini_set('display_errors', 1); error_reporting(E_ALL);
			
			
						$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();
			$_SESSION['user_id'] = $aRowForUserInfo[1];

	?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<div id="idSpanTopMenu" class="classSpanTopMenu header_044">
					<?php 
					if($ut==0||$ut==2)
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
									<ul id='menu'>
									<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
									</li>
									<li>
										<a href='manageTest.php'>Home</a>		
									</li>";
									
					
						if($ut==2)
						{
							echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
						}
						else
						{
							echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
								<ul>
									<li>
									<a href='groupHTML.php'>Create Group</a>		
									</li>
									<li>
										<a href='addTestHTML.php'>Create Test</a>		
									</li>
									<li>
										<a href='addUserHTML.php'>Create User</a>			
									</li>
									<li>
										<a href='excelReader/index.php'>Bulk Upload</a>			
									</li>
								</ul>
							</li>";
						} 
						
						echo "<li>
									<a>Manage </a>  
									  <ul>
									  		<li>
												<a href='manageGroup.php'>Manage Group</a>			
											</li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
									</ul>	
								</li>
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";
					}	
					else
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
								<ul id='menu'>
								<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
								</li>
								<li>
									<a href='manageTest.php'>Home</a>		
								</li>
								<li>
									<a href='showOpportunity.php'>Opportunity</a>
	                            </li>
								<li>
	                            	<a href='displayStudentResult.php'>Result</a>       
	                        	</li>
								
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";	
						}
						
					
						?>
						</div>
				</div>  	
		</div> <!-- end of menu -->
    </div>  <!-- end of header -->

</div> <!-- end of header wrapper -->
   <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div>
<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
          
       
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php
	 if($iUMsg!= null)
               {
               		echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
               		 if ($iUMsg == -1)					//code -1 : Technical Error Try again.
		            {
		            	echo "<div class=classMsg >Technical Error Try again.</div>";            	
		            }
		           if ($iUMsg == 123)					//code 123 : User Activated Successfully.
		            {
		            	echo "<div class=classMsg >User : <i><u>{$sIdentifier}</u></i> Activated Successfully.</div>";
		            }
		            if ($iUMsg == 234)		// code 234 : User Not Activated Try Again.
		            {
		            	echo "<div class=classMsg >User : <i><u>{$sIdentifier}</u></i> Not Activated Try Again.</div>";
		            }
		            if ($iUMsg == 345)		// code 345 : User Deactivated Successfully.
		            {
		            	echo "<div class=classMsg >User : <i><u>{$sIdentifier}</u></i> Deactivated Successfully.</div>";
		            }
		            if ($iUMsg == 456)		// code 234 : User Not Deactivated Try Again.
		            {
		            	echo "<div class=classMsg >User : <i><u>{$sIdentifier}</u></i> Not Activated Try Again.</div>";
		            }
		            echo "</div>";
               }
         ?>
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">
				<div id="idDivSignUp" class="header_0345">Manage User
				
				</div>
				<div class="classHorizHRSubHead"></div>
				<div id="idDivFname" class="classDivAddTest">
		<?php			
			/*@ when connetion failed.*/
			if ($mysqli->errno) 									
			{	
				$mysqli->close();
				echo "Technical Error Try again";
			}
			/*@ when connetion Established.*/
			else
			{	
				
				$iactive=0;
				$ideactive=0;
				$icounter=0;
				$msg="user";
				echo "<div id='idDivDispAllTest'>";
				echo "<table cellpadding='0' cellspacing='0' border='0' class='display classForTable' id='idDisplayAllTest' width='100%'>";
				echo "<thead><tr>
					<th class='classDTabdWidthSrno'>Sr. No.</th>
					<th class='classDTabdWidthTName'>Name</th>
					<th class='classDTabdWidthTName'>Email</th>
					<th class='classDTabdWidthTName'>Contact</th>
					<th class='classDTabdWidthTAttempt'>Detail</th>
					<th class='classDTabdWidthTAttempt'>Type </th>
					<th class='classDTabdWidthTAttempt'>Test Attempted</th>
					<th class=''>Test</th>
					<th class=''>Opportunity</th>
					<th class='classDTabdWidthTAttempt'>Status</th>
					<th class='classDTabdWidthTAttempt'>Switch Status</th>
					</tr></thead>";
				// loop through result of database query, displaying them in the table
				echo "<tbody>";
				//$aLoginIdRow=null;
				$sUserQuery="select b.* from login as a, user_details as b where a.type <> 0 and a.login_id=b.login_id group by b.user_id";				
				$Aresult=$mysqli->query($sUserQuery);				
				if($Aresult ==true)
				{
					
					while($Urow=$Aresult->fetch_row())
					{							
						$icounter++;					
						echo "<tr>";
						echo "<td class='classDTabdWidthSrno'>$icounter</td>";		// Sr. no.
						echo "<td class='classDTabdWidthTName'>$Urow[2]</td>";		// Test Name.
						echo "<td class='classDTabdWidthTName'>$Urow[3]</td>";		// Email address.
						echo "<td class='classDTabdWidthTName'>$Urow[5]</td>";		// Contact number.
						echo "<td class='classDTabdWidthTAttempt'><a href='profile.php?usid={$Urow[0]}' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='View User Detail'><img src='images/showTest.png' class='classAddIcon' id='idAddTestIcon'/></td>"; 
						/*
							The query following is use to get login id of each user to check the details
						*/							
						$sUserIdQuery="select a.type from login as a, user_details as b where b.user_id={$Urow[0]} and a.login_id = b.login_id";																					
						$bUTResult=$mysqli->query($sUserIdQuery);
						$aURow=$bUTResult->fetch_row();
						if($aURow[0] == 2)
						{
							echo "<td class='classDTabdWidthTAttempt'>Tester</td>";	
						}
						else   
						{
							echo "<td class='classDTabdWidthTAttempt'>User</td>";		
						}
						///////////////Use to Know Number of test given by the user		
						$sQuery="select a.test_id from score_board_table as a where a.user_id={$Urow[0]}";							
						$sTestResult = $mysqli->query($sQuery);														
						$testrow = $sTestResult->num_rows;							
					  	echo "<td class='classDTabdWidthTAttempt'>$testrow</td>";  // No of Test given by the user.
						///////////////
					  	if($testrow == 0)
						{
							echo "<td align='middle'></td>"; 
						}
						else  
						{
							echo "<td align='middle'><a href='manageUserTest.php?uid={$Urow[0]}' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='View User Test's'><img src='images/showTest.png' class='classAddIcon' id='idAddTestIcon'/></a></td>"; 	
						}
						
						/////////////// Following query is use to check wheater user applied for an opportunity or not.
						$sUserOppQuery="select user_id from apply_opportunity where user_id={$Urow[0]} and ap_opp_status=1";
						$bUserOppResult=$mysqli->query($sUserOppQuery);					
						$aOppRow=$bUserOppResult->num_rows;
						
						if($aOppRow == 0 || $aOppRow == null)
						{
							echo "<td align='middle'> </td>"; 
						}
						else
						{
							echo "<td align='middle'><a href='userOpportunity.php?uid={$Urow[0]}' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='View User Test's'><img src='images/showTest.png' class='classAddIcon' id='idAddTestIcon'/></td>";		// View User Opportunity.
						}					
						

						// Use to show edit option
						
						


						if($Urow[12]==1)
						{	
							echo "<td class='classDTabdWidthTAttempt'>Active</td>";
							echo "<td class='classDTabdWidthTAttempt'><a href='deactivate.php?id=$Urow[0]&msg=$msg' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Clik to Inactivate User'><img src='images/on.png' class='classAIIcon' id='idITestIcon'/></a></td>";
							$iactive++;
						}
						else
						{	
							echo "<td class='classDTabdWidthTAttempt'>Inactive</td>";
							echo "<td class='classDTabdWidthTAttempt'><a href='activate.php?id=$Urow[0]&msg=$msg' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Click to Activate User'><img src='images/off.png' class='classAIIcon' id='idATestIcon'/></a></td>";
							$ideactive++;
						}
						
						echo '</tr>';										
						
					}
					echo "</tbody>";
					echo "</table><br/><br/><br/>";
					$mysqli->close();				
					echo "<br/><br/>total User : '{$icounter}'<br/><br/>";
					echo "Active User :  {$iactive}<br/><br/>";
					echo "Inactive User : {$ideactive} <br/><br/>";
				}
				else
				{
					$mysqli->close();
					echo "Technical Error in Query Try again ";
				}
			}
			?>
				</div>
			</div>
            </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>