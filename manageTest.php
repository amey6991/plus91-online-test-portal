<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
<style type="text/css" title="currentStyle">
    @import "media/css/demo_page.css";
    @import "media/css/demo_table_jui.css";
    @import "media/examples/examples_support/themes/smoothness/jquery-ui-1.8.4.custom.css";
</style>
<script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>

<script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
        $('#idDisplayAllTest').dataTable({
            "bJQueryUI": true,
            "sPaginationType": "full_numbers"
        });
    } );
</script>
<!-- datatable js -->
<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>
<script>
$(function () {
$("[rel='popover']").popover();
}); 
</script>
<!-- Script for error message animation -->
<script>
$(document).ready(function(){
    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
   
    if($('#idDivDisplayError').is(':visible'))
    {
       
    }
       else
    {
        $("#idDivMiddleBody").animate({Top:'0px'});
        //$('#idDivMiddleBody').animate({
        //'marginTop' : "-=1px" //moves up
        //});
    }
});
</script>
<!-- end of error message script -->

<?php
    session_start();
     $iUMsg=Null;
     $sIdentifier=Null;
   
   if(isset($_SESSION['identifier']))
   {
           $sIdentifier = $_SESSION['identifier'];
             unset($_SESSION['identifier']);
   }
 ?>

</head>
<body>
<div id="id_header_wrapper">
    <div id="id_header">
       <div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
    </div>
        <div id="id_menu">
              <div id="id_menu_left">
                <div id="idDivUserNameTop" class="classDivTopMenuUser">
                    <?php
                    include ('classConnectQA.php');
                    if(isset($_SESSION['lid']))
                    {
                    }
                    else
                    {
                        header("location:index.php");
                    }
                    $icounter=0;                                                                           
                    $ut=$_SESSION['ut'];                                                           
                   
                    if(isset($_SESSION['user_id']))
                    {
                        $iUid=$_SESSION['user_id'];
                    }
                    else
                    {
                        $iUid="";
                    }
                   
                    $iLoginId = $_SESSION['lid'];
                    $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email
                                        from login as a , user_details as b
                                        where a.login_id = b.login_id
                                        AND a.login_id  = '$iLoginId' limit 1";
                    $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                    $aRowForUserInfo = $iResultForUserInfo->fetch_row();                   

                    if($ut==0||$ut==2)
                    {
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044 '>
                                    <ul id='menu'>
                                    <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>           
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>           
                                        </li>
                                    </ul>
                                    </li>
                                    <li>
                                        <a href='manageTest.php'>Home</a>       
                                    </li>";
                                   
                   
                        if($ut==2)
                        {
                            echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
                        }
                        else
                        {
                            echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>      
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>           
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
                                <ul>
                                    <li>
                                    <a href='groupHTML.php'>Create Group</a>       
                                    </li>
                                    <li>
                                        <a href='addTestHTML.php'>Create Test</a>       
                                    </li>
                                    <li>
                                        <a href='addUserHTML.php'>Create User</a>           
                                    </li>
                                    <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>           
                                    </li>
                                </ul>
                            </li>";
                        }
                       
                        echo "<li>
                                    <a>Manage </a> 
                                      <ul>
                                              <li>
                                                <a href='manageGroup.php'>Manage Group</a>           
                                            </li>
                                            <li>
                                                <a href='manageUser.php'>Manage User</a>           
                                            </li>
                                          <li>
                                                <a href='viewAllotedTestHTML.php'>Assign Test</a>    
                                              </li>
                                    </ul>   
                                </li>
                                <li>
                                    <a href='logout.php'>Logout </a>   
                                </li>
                                </ul>
                                </div>";
                    }   
                    else
                    {
                   
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                            <ul id='menu'>
                            <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                <ul>
                                    <li>
                                        <a href='profile.php'>Profile</a>       
                                    </li>
                                    <li>
                                        <a href='profileedit.php'>Update Profile</a>           
                                    </li>
                                    <li>
                                        <a href='changePassword.php'>Change Password</a>           
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href='manageTest.php'>Home</a>       
                            </li>
                            <li>
                                <a href='showOpportunity.php'>Opportunity</a>
                            </li>
                            <li>
                                <a href='displayStudentResult.php'>Result</a>      
                            </li>
                           
                            <li>
                                <a href='logout.php'>Logout </a>   
                            </li>
                            </ul>
                            </div>";   
                    }
                ?>

                </div>     
            </div> <!-- end of left menu -->
        </div>  <!-- end of men menu -->
    </div> <!-- end of header wrapper -->
<div id="idDivHorizBar" class="classDivHorizBar">
</div>
</div>
<div id="id_banner_wrapperHome">
    <div id="id_banner">
        <div id="id_banner_content">
            <div id="idDivWelcomMsg" class="header_01">Welcome to Plus91 Career Portal</div>
        </div>

    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php
 if(isset($_SESSION['msg']))
    {
         $iUMsg = $_SESSION['msg'];
        
         unset($_SESSION['msg']);
                   echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
                if ($mysqli->errno)                                    
                {   
                    echo "<div class=classMsg >Technical error.</div>";
                }
                /*@ when connetion Established.*/
                else
                {   
                           
                    if ($iUMsg == 012)        // code 012 : When  Test Active.
                    {
                        echo "<div class=classMsg >Test : <i>{$sIdentifier}</i> Activated.</div>";
                    }
                    if ($iUMsg == 023)        // code 023 : When  Test fail to Active.
                    {
                        echo "<div class=classMsg >Test : <i>{$sIdentifier}</i> Not Activated.</div>";
                    }
                    if ($iUMsg == 034)        // code 034 : When  Test Inactive.
                    {
                        echo "<div class=classMsg >Test : <i>{$sIdentifier}</i> Deactivated.</div>";
                    }
                    if ($iUMsg == 045)        // code 045 : When  Test fail to Deactive.
                    {
                        echo "<div class=classMsg >Test : <i>{$sIdentifier}</i> Not Deactivated.</div>";
                    }

                    if(isset($_GET['msg']))
                    {
                        if($_GET['msg']==-1)
                        {
                            echo "<div class=classMsg >Technical Error.</div>";
                        }
                        if($_GET['msg']=='T1')
                        {
                            echo "<div class=classMsg >Test : <i>{$sIdentifier}</i> Added Successfully.</div>";
                        }
                        if($_GET['msg']=='T0')
                        {
                            echo "<div class=classMsg >Test Not Added, try agian.</div>";
                        }
                    }
                }

            echo"</div>";

    }
?>
<div id="id_content_wrapper">
    <div id="id_content">
       
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">
                <div id="idDivSignUp" class="header_0345">

                    <?php
                    echo "HOME : ";
                    if($ut == 0)
                    {
                        echo " Manage Test";
                    }
                    else
                    {
                        echo " Test";
                    }
                    ?>
                   
                    <?php
            /*
                SESSION Variable info.
                @lid: is use for login id
                @uid: is for user id
                @id: is use for any other id such as group, test qustion
                @ut:  is use for the user type;
                @st:  use for status
            */
            /*@ when connetion failed.*/
           
                ?>
                </div>
                <div class="classHorizHRSubHead"></div>
                <div id="idDivFname" class="classDivAddTest">

                <?php       
               
                $id=Null;
                $ut=$_SESSION['ut'];
                $dCurrentDate=date("Y-m-d");           
                echo "<div id='idDivDispAllTest'>";
                echo "<table cellpadding='0' cellspacing='0' border='0' class='display classForTable' id='idDisplayAllTest' width='100%''>";

                echo "<thead><tr><th class='classDTabdWidthSrno'>Sr. No.</th>
                                 <th class='classDTabdWidthTName'>Test</th>
                                 <th class='classDTabdWidthDesc'>Description</th>
                                 <th class='classDTabdWidthSubject'>Subject</th>";
                if($ut==1)
                {
                    echo "<th>Apply Now</th>";
                    // Query For User.
                    $dTodayDate=date("Y-m-d");
                    // query  ready to view test as per allot ment.
                // Query to Displa Only that test which are Active Valid as per the date, and alloted to the user.               
                    $sQuery="select a.* from test_detail as a, allot_test as b, group_table as c
                                where a.test_status=1 and test_scope=1 and
                                a.test_id=b.test_id and
                                b.user_id={$iUid} and
                                b.allot_status=1 and c.group_id=a.group_id ORDER BY b.allot_id DESC";
                   
                }
                if($ut==0 || $ut==2)
                {
                    echo "<th class=''>Group</th>                           
                            <th class='classDTabdWidth6'>Result's</th>
                            <th class='classDTabdWidth6'>Add Question</th>
                            <th class='classDTabdWidth6'>Edit</th>
                            <th class='classDTabdWidthCode'>Test Status</th>
                            <th class='classDTabdWidth6'>Switch status</th>";   
                    // Query For Admin.       
                    if(isset($_GET['gid']))
                    {
                        $iGroupId=$_GET['gid'];
                        if($iGroupId != Null || $iGroupId != '')
                        {
                            $sQuery="select * from test_detail where group_id = {$iGroupId} ORDER BY test_id DESC";   
                        }                       
                        else
                        {
                            $sQuery="select * from test_detail ORDER BY test_id DESC";       
                        }
                       
                    }
                    else
                    {
                        $sQuery="select * from test_detail ORDER BY test_id DESC";                   
                    }                   
                }               
                echo "</tr></thead>";
                $iActive=0;
                $iInactive=0;
                for($iOnceMore=0; $iOnceMore<2;$iOnceMore++)        // for is use to display, All alloted test first and then all common test.
                {
					$result=$mysqli->query($sQuery);
					if($result==True)
					{					
						
						$iActive=0;
						$iInactive=0;
						$sMsg="test";
						while($row=$result->fetch_row())
						{		
														
							$iTestid = $row[0];		
							////////////To calculate no of questions./////
							$sCountTestQuery="select count(que_id) from question_table where que_status=1 and test_id={$row[0]}";
							
							$sQueResult=$mysqli->query($sCountTestQuery);
							
							$Qrow=$sQueResult->fetch_row();	//@ Qrow : Store no of Question Available in a test
							//////////////////////////////////////////////	
							if($Qrow[0]==0 && $ut==1)
							{	
								continue;	
							}											
								$icounter++;
								echo "<tr >";
								echo '<td class="classDTabdWidthSrno" >' . $icounter . '</td>';
								echo "<td class='classDTabdWidthTName'>								
									<div id='idDivTestNameScroll' class='classTestNameScrollUser'>";
								if($ut!=1 && $row[16]==2)
								{
									$testName=$row[2]." (PUBLIC)";
								}	
								else
								{
									$testName=$row[2];
								}
									$testName=$testName;
								 if(strlen($testName)>20)
						        {

						            echo $testName."...</div>";
						            echo "<div class='classDivReadMore'>.....<a href='#' rel='popover' data-placement='bottom' data-content='{$testName}' data-original-title='Test Name' class='classMinFont'>Read More</a></div>";          
						        }	
						        else
						        {
						        	echo $testName;
						        }
								
								echo "</td>"; // Test Name
								
								
								if($ut == 1)
								{
									//echo "<td class='classDTabdWidthDesc'><div id='idscrollDesc' class='classScrollDescUser'> $row[4] </div></td>"; // Test description
									if($row[4]!="")
									{
										echo "<td class='classDTabdWidthTName'>
										<div id='idscrollDesc' class='classScrollDescUser'>";
										 if(strlen($row[4])>50)
								        {
								            echo substr($row[4],0,60)."...</div>";  
								            echo "<div class='classDivReadMore'>.....<a href='#' rel='popover' data-placement='bottom' data-content='{$row[4]}' data-original-title='Test Name' class='classMinFont'>Read More</a></div>";         
								        }	
								        else
								        {
								        	echo $row[4];  
								        }
									
										echo "</td>"; // Test Description.
									}
									else
									{
										echo "<td class='classDTabdWidthDesc'></td>";
									}
								}
								else
								{
									if($row[4]!="")
									{
										echo "<td class='classDTabdWidthTName'>
										<div id='idscrollDesc' class='classScrollDescUser'>";
										 if(strlen($row[4])>30)
								        {
								            echo substr($row[4],0,60)."...</div>";  
								            echo "<div class='classDivReadMore'>.....<a href='#' rel='popover' data-placement='bottom' data-content='{$row[4]}' data-original-title='Test Name' class='classMinFont'>Read More</a></div>";         
								        }	
								        else
								        {
								        	echo $row[4];  
								        }
									
										echo "</td>"; // Test Description.
									}
									else
									{
										echo "<td class='classDTabdWidthDesc'></td>";
									}		
								}
								echo '<td class="classDTabdWidthSubject">' . $row[3] . '</td>'; // Test Subject
								////////////To calculate no of User attempt Test./////
								$sTestUser=$mysqli->query("select count(user_id) from score_board_table where test_id={$row[0]}");
								$TUrow=$sTestUser->fetch_row();			
								//////////////////////////////////////////////
								
								if($ut==1)
								{

                                    if($Qrow[0]==0)
                                    {
                                        echo "<td class='classDTabdWidthUserAt'><a href=''>Active</a></td>";
                                    }
                                    else
                                    {   
                                        $sQueryCheckTestAttempt = "select user_id , test_id from score_board_table where test_id = {$iTestid} AND user_id = {$iUid} limit 1";
                                        $iResCheckTestAttempt = $mysqli->query($sQueryCheckTestAttempt);    /* This query check user id and test id present in database or not */
                                        $aTestAttempt = $iResCheckTestAttempt->fetch_row();
                                      
                                      
                                        if($aTestAttempt[1] == $iTestid) /* This condition check if user attempt this test first time or he/she already submit this test */
                                        {   
                                            echo "<td class='classDTabdWidthUserAt'>Test Attempted</td>";
                                        }
                                        else
                                        {
                                            if($dCurrentDate>$row[7])    // IF Last Date of Test is Over.
                                            {
                                                echo "<td class='classDTabdWidthUserAt'><a rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Valid From=$row[6] \n Valid Till=$row[7]'>Schedule Closed</a></td>";       
                                            }
                                            else
                                            {
                                                if($dCurrentDate<$row[6])    // IF Date of Test is still not come.
                                                {
                                                    echo "<td class='classDTabdWidthUserAt'><a rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Valid From=$row[6] \n Valid Till=$row[7]'>Test Scheduled From $row[6] onward</a></td>";
                                                }
                                                else
                                                {
                                                    $sQueryCheckLoginHistry = "select count(user_id) , count(test_id) from login_system_info where test_id = {$iTestid} AND user_id = {$iUid} limit 1";
                                                    $iResCheckLoginHistry = $mysqli->query($sQueryCheckLoginHistry);    /* This query check user id and test id present in database or not */
                                                    $aLoginHistry = $iResCheckLoginHistry->fetch_row();
                                                    if($aLoginHistry[0] >= 1 && $aLoginHistry[1] >= 1)
                                                    {
                                                         echo "<td class='classDTabdWidthUserAt'>Test Attempted</td>";
                                                    }
                                                    else
                                                    {
                                                    echo "<td class='classDTabdWidthUserAt'><a href='uploadCV-StartTest.php?id={$row[0]}' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Click To Take Test'><img src='images/playbut.png' class='classAddIcon' /></a></i></td>"; // use to hook a Test and start it.           
                                                    }
                                                }
                                            }                                             
                                           
                                        }
                                    }
                                }

                                if($ut==0 || $ut==2)
                                {
                                    $sGroupQuery="select group_name from group_table where group_id={$row[1]}";
                                    $bGroupResult=$mysqli->query($sGroupQuery);
                                    if($bGroupResult==true)
                                    {
                                        $aGRow=$bGroupResult->fetch_row();                                   
                                        $iGroup=$aGRow[0];
                                    }
                                    else
                                    {
                                        $iGroup="No Group";   
                                    }
                                   
                                    echo "<td class='classDTabdWidthTName'>$iGroup</td>";            // Group Name.                           
                                    //echo '<td class="classDTabdWidth6">' . $TUrow[0] . '</td>'; //No of User Attempt Test
                                    if($TUrow[0]==0)
                                    {
                                        // Echo When not a single user attempted test.
                                        echo "<td class='classDTabdWidth6'></td>";   
                                    }
                                    else
                                    {
                                        // Echo when Test Result is available.
                                        echo "<td class='classDTabdWidth6'><a href='manageResult.php?id={$row[0]}' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='View All Quizzers'><img src='images/show.png' class='classAddIcon' id='idAddTestIcon'/></a></i></td>";   
                                    }
                                   
                                   
                                   
                                    if($row[13]==1)
                                    {   

                                        // Add Quetion Option Disable.
                                        echo "<td class='classDTabdWidth6'></td>"; //
                                       
                                        // Edit Option Disable.
                                        echo "<td class='classDTabdWidth6'></td>";   
                                       
                                        // Inactive Option
                                        // $Qrow[0] : return the nuber of Question

                                        if($Qrow[0]==0)
                                        {
                                            // Echo When Not a single Question added in a test
                                            echo "<td class=''>Inactive</td>";
                                            echo "<td class=''><a href='' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Add Question To Activate a Test'><img src='images/off.png' class='classAIIcon' id='idAddTestIcon'/></a></td>";   
                                           
                                        }
                                        else
                                        {

                                            // Echo When Test is ACTIVE.
                                            echo "<td class=''>Active</td>";
                                            echo "<td class=''><a href='deactivate.php?id=$row[0]&msg=$sMsg' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Click To Inactive Test'><img src='images/on.png' class='classAIIcon' id='idAddTestIcon'/></a></td>";   

                                        }
                                       
                                   
                                        $iActive++;
                                    }
                                    else
                                    {   
                                        // Add Question Option.
                                        echo "<td class='classDTabdWidth6'><a href='addQuestionHTML.php?id={$row[0]}' ><img src='images/Addc.png' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Add Question' class='classAddIcon' id='idAddTestIcon'/></td>";
                                       
                                        // Edit Test Option
                                        echo "<td class='classDTabdWidth6'><a href='editTest.php?id={$row[0]}'><img src='images/Edit.png' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Total Que=$TUrow[0] \n Valid From=$row[6] \n Valid Till=$row[7]' class='classAddIcon'/></a></td>";   
                                        //echo "<td class='classDTabdWidth6'><a href='editTest.php?id={$row[0]}' title='Total Que=$TUrow[0] \n Valid From=$row[6] \n Valid Till=$row[7]' rel='tooltip' data-toggle='tooltip' data-placement='bottom' ><i class='icon-pencil'></i> </a></td>";   
                                        // Active Option
                                        if($Qrow[0]==0)
                                        {
                                            // Echo When Not a single Question added in a test
                                            echo "<td class=''>Inactive</td>";
                                            echo "<td class=''><a href='' ><img src='images/off.png' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Add Question To Activate a Test' class='classAIIcon' id='idAddTestIcon'/></a></td>";   
                                        }
                                        else
                                        {
                                            // Echo When Test is INACTIVE.
                                            echo "<td class=''>Inactive</td>";
                                            echo "<td class=''><a href='activate.php?id=$row[0]&msg=$sMsg' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Click To Activate a Test'><img src='images/off.png' class='classAIIcon' id='idAddTestIcon'/></a></td>";
                                        }
                                       
                                       
                                        $iInactive++;
                                    }

                                }   
                                    echo '</tr>';
                        }
                    }   
                    if($ut==1)
                    {
                        //$sQuery="select a.* from test_detail as a, group_table as b where a.test_status=1 and group_scope=2 and a.group_id=b.group_id";
                        $sQuery="select a.* from test_detail as a where a.test_status=1 and test_scope=2 ";
                    }   
                    else
                    {
                        $iOnceMore=2;
                    }
                   

                }               
                echo '</table></div>';
                $mysqli->close();               
                if($ut==0)
                {
                    echo "<div id='idDivForInfo' class='classDivForInfo'>total Test : '{$icounter}'<br/><br/>";
                    echo "Active Test : '$iActive' <br/><br/>";
                    echo "Inactive Test : '$iInactive' <br/><br/></div>";
                }   
           
        ?>
                </div>
            </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div>
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>   
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR">
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>