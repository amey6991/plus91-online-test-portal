<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Change Password</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   

    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idChangePassword").validationEngine();
        });

    </script>
    <!--end of javascript to validate the textbox -->

<script language="javascript" type="text/javascript">
    function passwordStrength(password)
    {
        var desc = new Array();
        desc[0] = "Very Weak";
        desc[1] = "Weak";
        desc[2] = "Better";
        desc[3] = "Medium";
        desc[4] = "Strong";
        desc[5] = "Strongest";

        var score   = 0;

        //if password bigger than 4 give 1 point
        if (password.length > 4) score++;

        //if password has both lower and uppercase characters give 1 point  
        if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;

        //if password has at least one number give 1 point
        if (password.match(/\d+/)) score++;

        //if password has at least one special caracther give 1 point
        if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) score++;

        //if password bigger than 8 give another 1 point
        if (password.length > 8) score++;


         document.getElementById("passwordDescription").innerHTML = desc[score];
         document.getElementById("passwordStrength").className = "strength" + score;
    }
</script>

</head>
<body>
<?php 

 	include('classConnectQA.php');
 	include('config/config.php');
		session_start();
		    if(isset($_SESSION['user_id']))
        {
          $iUserId = $_SESSION['user_id'];
        }
        else
        {
          $iUserId ="";
        }
        if(isset($_SESSION['ut']))
        {
          $ut=$_SESSION['ut'];
        }
        else
        {
          $ut="";
        }
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }
       
        $sQueryUserInfo = "select * from user_details as a where a.login_id = {$iLoginId} limit 1";
        $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
        $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        if(isset($_GET['msg']))
        {
            $iMsg = $_GET['msg'];
        }
        else
        {
		    $iMsg ="";
        }
 ?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        

		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<?php 
          if($ut==0||$ut==2)
          {
            echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                  <ul id='menu'>
                  <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                  <ul>
                    <li>
                      <a href='profile.php'>Profile</a>   
                    </li>
                    <li>
                      <a href='profileedit.php'>Update Profile</a>      
                    </li>
                    <li>
                      <a href='changePassword.php'>Change Password</a>      
                    </li>
                  </ul>
                  </li>
                  <li>
                    <a href='manageTest.php'>Home</a>   
                  </li>";
                  
          
            if($ut==2)
            {
              echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
            }
            else
            {
              echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
                <ul>
                  <li>
                  <a href='groupHTML.php'>Create Group</a>    
                  </li>
                  <li>
                    <a href='addTestHTML.php'>Create Test</a>   
                  </li>
                  <li>
                    <a href='addUserHTML.php'>Create User</a>     
                  </li>
                  <li>
                    <a href='excelReader/index.php'>Bulk Upload</a>     
                  </li>
                </ul>
              </li>";
            } 
            
            echo "<li>
                  <a>Manage </a>  
                    <ul>
                        <li>
                        <a href='manageGroup.php'>Manage Group</a>      
                      </li>
                      <li>
                        <a href='manageUser.php'>Manage User</a>      
                      </li>
                      <li>
                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                        </li>
                  </ul> 
                </li>
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";
          } 
          else
          {
            if($ut==2)
            {

            echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                <ul id='menu'>

                </li>
                <li>
                  <a href='manageTest.php'>Home</a>   
                </li>               
                <li>
                  <a href='manageUser.php'>Manage User</a>  
                </li>
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";

            }
            else
            {
              echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                <ul id='menu'>
                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                  <ul>
                    <li>
                      <a href='profile.php'>Profile</a>   
                    </li>
                    <li>
                      <a href='profileedit.php'>Update Profile</a>      
                    </li>
                    <li>
                      <a href='changePassword.php'>Change Password</a>      
                    </li>
                  </ul>
                </li>
                <li>
                  <a href='manageTest.php'>Home</a>   
                </li>
                <li>
                  <a href='showOpportunity.php'>Opportunity</a>
                              </li>
                <li>
                                <a href='displayStudentResult.php'>Result</a>       
                            </li>
                
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";  
            }
            
          } 
          ?>
				</div>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
    <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				<div id="idDivSignUp" class="header_0345">Change Password</div>
            </div>
               <div class="classHorizHRSubHead"></div>        
            <form action="changePass.php" method="post" enctype="multipart/form-data" id="idChangePassword">
    				<div id="idDivDob" class="classDisplayProfile">
                   
            </div>
    				<div id="idDivOldPass" class="classChangePassword classTopMarginForChangePass">
               <span id="idSpanOldPass" class="classSpanSignUpInfo header_024"> Old Password </span>
                <span id="idSpanOldPassInput" class="classSpanSignUpInput">
                 	<input type="password" name="oldpass" id="idTxtOldPass" class="class_login_txt validate[required,minSize[6]] text-input"/>
                </span>
    				</div>
				    <div class="class_Div_login_content classChangePassword" id="id_Div_login_Pass">
              <span id="idSpanSignUp4" class="classSpanSignUpInfo header_024">New Password</span>
              <span id="idSpanSignUpInput4" class="classSpanSignUpInput">
                  <input type="Password" class="class_login_txt validate[required,minSize[6]] text-input" id="id_Pass" name="Password" value="" onkeyup="passwordStrength(this.value)"/>
              </span>
                <div id="idPasswordStrength" class="classRasswordStrength">
                            <div id="passwordDescription"></div>
                            <div id="passwordStrength" class="strength0"></div>
                        </div>
                    </div>
            <div class="class_Div_login_CPass class_Div_login_content classChangePassword" id="id_Div_login_CPass">
              <span id="idSpanSignUp5" class="classSpanSignUpInfo header_024">Confirm Password</span>
              <span id="idSpanSignUpInput5" class="classSpanSignUpInput">
                <input type="Password" class="class_login_txt validate[required,equals[id_Pass]] text-input" id="id_login5" name="CPassword" value=""/>
              </span>
            </div>
                <div id="idDivChangePasswordSubmit" class="classChangePassword">
                 <span><input type="submit" name="submit" value="Submit" class="btn btn-primary classSignUpButtenSize"/></span>
                 <?php
                  if($iMsg == 824)    
                  {
                      echo "<span id='idDivWarMsg' class='classDivTestDur classDivResumeText classWarningMsg classGreen'>
                          Password Changed Successfully.
                          </span>";
                  }
                   if($iMsg == 825)    
                  {
                      echo "<span id='idDivWarMsg' class='classDivTestDur classDivResumeText classWarningMsg classRed'>
                          The password you gave is incorrect.
                          </span>";
                  }

		            ?>
                </div>
                </form>
                <div id="idDisplayExcelSheetEr" class="classDisplayExcelFileUpload classDisplayProfile">
                <?php 
                if($iMsg == 695)
                {
                  echo "<div>Error in Excel File.</div>";
                }
                
                ?>
              </div>
          </div>

              
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>