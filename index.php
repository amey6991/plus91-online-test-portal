<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
 <?php
    session_start();
        
    if(isset($_GET['msg']))
    {
        $iMsg = $_GET['msg'];
    }
    else
    {
        $iMsg ="";
        unset( $_SESSION['fullname'] );
        unset( $_SESSION['conno'] );
        unset( $_SESSION['unm'] );
    }
    if($iMsg == 769)
    {
      $sCaptcha = 'NotEqual';
      $sCap ='NotEqual';
    }
    else
    {
      $sCaptcha = 'Equal';
      $sCap ='Equal';
      unset( $_SESSION['fullname'] );
      unset( $_SESSION['conno'] );
      unset( $_SESSION['unm'] );
    }
  ?>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Plus91 Career Portal | Login</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<!--Captch script -->
<script type="text/javascript" src="js/jquery.minc.js"></script>
<script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script type="text/javascript">
        $(document).ready(function(){         
                var capch = '<?php echo $sCap; ?>';
                var captchaNotMatch = '<?php echo $sCaptcha; ?>';
                if(captchaNotMatch == 'NotEqual')
                {
                  if(capch == 'NotEqual')
                  {
                      $('.cap_status').html("The characters you entered didn't match the word verification. Please try again.").addClass('classCap_status_error').fadeIn('slow');
                  }
                  else
                  {

                  }
                }    
            });       
        </script>
<!--end of captcha script -->

<script language="javascript" type="text/javascript">
    function passwordStrength(password)
    {
        var desc = new Array();
        desc[0] = "Very Weak";
        desc[1] = "Weak";
        desc[2] = "Better";
        desc[3] = "Medium";
        desc[4] = "Strong";
        desc[5] = "Strongest";
        var score   = 0;
        //if password bigger than 4 give 1 point
        if (password.length > 4) score++;
        //if password has both lower and uppercase characters give 1 point  
        if ( ( password.match(/[a-z]/) ) && ( password.match(/[A-Z]/) ) ) score++;
        //if password has at least one number give 1 point
        if (password.match(/\d+/)) score++;
        //if password has at least one special caracther give 1 point
        if ( password.match(/.[!,@,#,$,%,^,&,*,?,_,~,-,(,)]/) ) score++;
        //if password bigger than 8 give another 1 point
        if (password.length > 8) score++;
         document.getElementById("passwordDescription").innerHTML = desc[score];
         document.getElementById("passwordStrength").className = "strength" + score;
    }
</script>

<!--start of Script for check user name availability -->
<script type="text/javascript" src="js/jquery-1.4.1.min.js"></script>
<script type="text/javascript">
$(function()
{
  $('.username').change(function()
  {
    var username=$(this).val();
    username=trim(username);
    if(username!='')
    {
      $('.check').show();
      $('.check').fadeIn(400).html('<img src="images/ajax-loading.gif" /> ');

      var dataString = 'username='+ username;
      $.ajax
      ({
            type: "POST",
            url: "checkuser.php",
            data: dataString,
            cache: false,
            success: function(result)
            {
               var result=trim(result);
               if(result=='')
               {

                  /*Below code is use to show Available status only when Valid email id is entered and Email does not exists */
                  var filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;              
                  if (!filter.test(document.getElementById("id_login_txt3").value))
                  {                
                     $('.check').html('Invalid Email Id');
                     $('#id_login_submit').attr('disabled', 'disabled');
                     $(".username").removeClass("white");
                     $(".username").addClass("red");
                     $("#idDivDisplayAvailability").addClass("classNotAvailable");             
                  }
                  else
                  {
                     $('.check').html("Available");
                     $("#idDivDisplayAvailability").addClass("classAvailableText");
                     $("#idDivDisplayAvailability").removeClass("classNotAvailable");
                     document.getElementById("id_login_submit").disabled = false;
                     $(".username").removeClass("red");            
                  }                         
               }
               else
               {  
                 $('.check').html('This email is already registered');
                 $('#id_login_submit').attr('disabled', 'disabled');
                 $(".username").removeClass("white");
                 $(".username").addClass("red");
                 $("#idDivDisplayAvailability").addClass("classNotAvailable");
               }
            }
      });

   }
   else
   {
       $('.check').html('');
   }
  });
});

$(function()
{
  $('.Contact').change(function()
  {
    var Contact=$(this).val();
    Contact=trim(Contact);
    if(Contact!='')
    {
      $('.checkContact').show();
      $('.checkContact').fadeIn(400).html('<img src="images/ajax-loading.gif" /> ');

      var dataString = 'Contact='+ Contact;
      $.ajax
      ({
            type: "POST",
            url: "checkContact.php",
            data: dataString,
            cache: false,
            success: function(resultContact)
            {
               var resultContact=trim(resultContact);
               if(resultContact=='')
               {
                  /*Below code is use to show Available status only when Valid email id is entered and Email does not exists */
                  var filter =  /^[0-9]{10,13}$/; 
                  if (!filter.test(document.getElementById("id_login_txt6").value))
                  {                
                     $('.checkContact').html('Invalid Contact No.');
                     $('#id_login_submit').attr('disabled', 'disabled');
                     $(".Contact").removeClass("white");
                     $(".Contact").addClass("red");
                     $("#idDivDisplayContactAvailability").addClass("classNotAvailable");             
                  }
                  else
                  {
                     $('.checkContact').html("Available");
                     $("#idDivDisplayContactAvailability").addClass("classAvailableText");
                     $("#idDivDisplayContactAvailability").removeClass("classNotAvailable");
                     document.getElementById("id_login_submit").disabled = false;
                     $(".Contact").removeClass("red");            
                  }                         
               }
               else
               {  
                 $('.checkContact').html('This Contact is already registered');
                 $('#id_login_submit').attr('disabled', 'disabled');
                 $(".Contact").removeClass("white");
                 $(".Contact").addClass("red");
                 $("#idDivDisplayContactAvailability").addClass("classNotAvailable");
               }
            }
      });

   }
   else
   {
       $('.checkContact').html('');
   }
  });
});
function trim(str){
     var str=str.replace(/^\s+|\s+$/,'');
     return str;
}
</script>

    <!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   

    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#id_login_form").validationEngine();
        });

    </script>
    <!--end of javascript to validate the textbox -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
    <?php flush(); ?>


</head>
<body>
<div id="id_header_wrapperIndex">
  <div id="id_header">
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxt">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
			<form action="login.php" method="POST">
				<div id="idDivUserNameTop" class="classDivUserNameTop">
					<div id="idDivHeadUname" class="header_04">Email</div>
					<div id="idDivHeadUnametxt" class="classHeadInputUname"><input type="text" id="idLoginUname" name="UserName" value="" class="classUserName" tabindex="1"></div>
				  <div id="idDivForgPass" class="classForgPass"><a href="forgotpassword.php" class="">Forgot your password?</a></div>
        </div>
				<div id="idDivPassTop" class="classDivPassTop">
					<div id="idDivHeadUname" class="header_04">Password</div>
					<div id="idDivHeadPasstxt" class="classHeadInputPass"><input type="password" id="idLoginPass" name="password" value="" class="classUserName" tabindex="2"></div>
				</div>
				<div id="idDivSubmitTop" class="classDivSubmitTop">
					<input type="submit" id="submit" name="login" value="Go" class="btn btn-primary" tabindex="3">
				</div>
			</form>
			</div>
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBarIndex">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapperIndex">
	<div id="id_banner">
        <div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01Index">Welcome to Plus91 Career Portal</div>
        </div>
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
$iUMsg= Null;
    if(isset($_SESSION['msg']))
    {
         $iUMsg = $_SESSION['msg'];
         
         echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
                if($iMsg ==456)
                {
                    unset( $_SESSION['fullname'] );
                    unset( $_SESSION['conno'] );
                    unset( $_SESSION['unm'] );
                    echo "<div id='idDivVarifymsg' class='classDivVarifymsg'>User Verified Successfully, You Can Login Now</div>";
                }
               
                if($iMsg ==777 || $iMsg == 788)
                {
                    unset( $_SESSION['fullname'] );
                    unset( $_SESSION['conno'] );
                    unset( $_SESSION['unm'] );
                    echo "<div id='idDivVarifymsg' class='classDivVarifymsg'>You already verify your account.</div>";
                }
                if ($iUMsg === 0)        // code 234 is use for admin
                {
                   unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                   echo "<div id='idDivUsermsg' class='classDivVarifymsg'>User Name Already exist.</div>";    //code 0 : UserName Already exist.
                }
                if ($iUMsg == 1)        // code 234 is use for admin
                {
                    unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                    echo "<div id='idDivSignupmsg' class='classDivVarifymsg'>Successfully Signup.</div>";      //code 1 : Signup Successfully.
                }
                if ($iUMsg == 2)        // code 234 is use for admin
                {
                     unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                     echo "<div id='idDivNothingmsg' class='classDivVarifymsg classRed'>Try Again.</div>";      //code 2 : Try Again.
                }
               
                if ($iUMsg == 123)                    // code 123 You are Deactiveted
                {
                     unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                     echo "<div id='idDivDeactivemsg' class='classDivVarifymsg classRed'>You Are Deactivated.</div>"; 
                }
                if ($iUMsg == -1)                 // code -1 Technical error try again.
                {
                     unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                     echo "<div id='idDivTNothingmsg' class='classDivVarifymsg'>Technical Error, try again.</div>"; 
                }
                 if ($iUMsg == -2)                    // code -2 Fill Data Properly
                {
                     unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                     echo "<div id='idDivFillDatamsg' class='classDivVarifymsg'>Fill Data Properly.</div>";
                }
                if ($iUMsg == 234)        // code 234 is use for admin
                {
                     unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                     echo "<div id='idDivInvalidUsermsg' class='classDivVarifymsg classRed'>Invalid UserName And Password.</div>";
                }
                if ($iUMsg == 887)        // code 234 is use for admin
                {
                     unset( $_SESSION['fullname'] );
                        unset( $_SESSION['conno'] );
                        unset( $_SESSION['unm'] );
                     echo "<div id='idDivInvalidUsermsg' class='classDivVarifymsg classRed'>Invalid email and verification code.</div>";
                }
                unset($_SESSION['msg']);
         echo "</div>";
    } 

?>
<div id="id_content_wrapper">
	<div id="id_content">
        
    	<div id="column_w530">
            <div id="idDivTransImg" class="classDivTransImg">	
            </div>
        </div>
        <div id="column_w300">
        
        	<div id="idDivSignUp" class="header_03 gradient_background">Sign Up</div>
           <div class="classHorizHRSubHead"></div>
            <div class="classColumn_01">
            	<form name="LoginForm" class="class_Login_form formular" id="id_login_form" action="signUp.php" method="POST">
				<div class="class_Div_login_content" id="id_Div_login_UserName">					
					<span id="idSpanSignUp" class="classSpanSignUpInfo header_024">Name</span> 
					  <?php 
						  $uName="";
						 if(isset($_SESSION['fullname']))
						  { 
							$uName=$_SESSION['fullname'];
						  }
						  $iContNo="";
						  if(isset($_SESSION['conno']))
						  {
							$iContNo=$_SESSION['conno'];
						  }
						  $sEmail ="";
						  if(isset($_SESSION['unm']))
						  {
							$sEmail = $_SESSION['unm'];
						  }
					  ?>
					<span id="idSpanSignUpInput" class="classSpanSignUpInput"><input tabindex="4" type="text" name="Name" class="class_login_txt validate[required,custom[onlyLetterSp]] text-input" id="id_login_txt1"  value="<?php echo $uName; ?>"/>	</span>				
				</div>
				<div class="class_Div_login_content" id="id_Div_login_Contact">					
					<span id="idSpanSignUp1" class="classSpanSignUpInfo header_024">Mobile No.</span>
					<span id="idSpanSignUpInputContact" class="classSpanSignUpInput">
              <span class="input-prepend">
                  <span class="add-on">
                  +91
                  </span>
            <input tabindex="5"type="text" class="class_login_txtGrph validate[required,minSize[10]] text-input Contact" id="id_login_txt6" maxlength="10" name="Contact" value="<?php echo $iContNo; ?>"/>
            </span>
          </span>				
				   <div class="checkContact classDivUserNameAvailable" id="idDivDisplayContactAvailability" >
           </div>
        </div>
				
				<div class="class_Div_login_content" id="id_Div_login_UName">					
					<span id="idSpanSignUp3" class="classSpanSignUpInfo header_024">Email</span>
					<span id="idSpanSignUpInput3" class="classSpanSignUpInput">
            <span class="input-prepend">
                  <span class="add-on">
                  <i class="icon-envelope"></i>
                  </span>
            <input tabindex="6" type="text" class="class_login_txtGrph validate[required] text-input username" id="id_login_txt3" name="sUsername" value="<?php echo $sEmail; ?>"/>	</span>
                </span>
                <div class="check classDivUserNameAvailable" id="idDivDisplayAvailability" >
                </div>
                </div>
					
				<div class="class_Div_login_content" id="id_Div_login_Pass">
					<span id="idSpanSignUp4" class="classSpanSignUpInfo header_024">Password</span>
					<span id="idSpanSignUpInput4" class="classSpanSignUpInput"><input tabindex="7" type="Password" class="class_login_txt validate[required,minSize[6]] text-input" id="id_Pass" name="sPassword" value="" onkeyup="passwordStrength(this.value)"/></span>
    				<div id="idPasswordStrength" class="classRasswordStrength">
                        <div id="passwordDescription"></div>
                        <div id="passwordStrength" class="strength0"></div>
                    </div>
                </div>
				<div class="class_Div_login_CPass class_Div_login_content" id="id_Div_login_CPass">
					<span id="idSpanSignUp5" class="classSpanSignUpInfo header_024">Confirm Password</span>
					<span id="idSpanSignUpInput5" class="classSpanSignUpInput"><input tabindex="8" type="Password" class="class_login_txt validate[required,equals[id_Pass]] text-input" id="id_login5" name="CPassword" value=""/></span>
				</div>

				<div id="idDivDisplayCaptcha" class="class_Div_login_Pssword class_Div_login_content">
				  <div id="idDivCaptcha" class="header_024 classDivCaptcha">Enter the contents of image</div>
				  <div id="idDivCaptchaInput" class="classDivCaptchaInput">
					<input tabindex="9" type="text" name="captcha" id="captcha" maxlength="6" size="6" class="validate[required] text-input classCaptchaTxt"/>
				  </div>
				  <div id="idDivCaptchaImage" class="classDivCaptchaImage">
					<img src="captcha.php" id="idCaptchaImg" class="classCaptchaImageDisp"/>
				  </div>
				  <div class="cap_status" id="idDivCaptchaErrorMsg">
				  </div>
				</div>

				<div class="class_Div_SubmitSignUp" id="id_Div_login_SubmitButton">
				  <input tabindex="10" type="submit" class="btn btn-primary classSignUpButtenSize" id="id_login_submit" name="Submit" value="SignUp"/>
				</div>
			</div>

			</form>
        </div>     
           
        
		<div id="idDivOverTransperent" class="header_02">Plus91 Technologies is always on the lookout for bright 
      and innovative talent to help us reach our business and your personal goals. 
      In case you find any opportunities to work with us which fit your liking and 
      you think you can fit into our hands-on and go-getting culture do contact us.
		</div>

    </div></div> <!-- end of content -->
</div> <!-- end of content wrapper -->
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>