<?php

			/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@gid: is use for GroupId
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status 
			
			*/
			session_start();

		include ('classConnectQA.php');
		if(isset($_SESSION['lid']))		// This is Use to check a Session
		{
			$iLoginId = $_SESSION['lid'];
		}
		else
		{
			header("location:index.php");
		}
		/*
			@ All Fields are mandetory.	
		*/
				
		$iGid=addslashes($_POST['testGroup']);	
		$sTestName = addslashes($_POST['testName']);
		$sTestSubject = addslashes($_POST['testSubject']);
		$sTestValidFrom = addslashes($_POST['testValidFrom']);
		$sTestValidTill = addslashes($_POST['testValidTill']);
		$sTestDuration = addslashes($_POST['testDuration']);
		$sTestMsg = addslashes($_POST['testMsg']);
		$sTestMsg = htmlspecialchars($sTestMsg, ENT_QUOTES);
		$iTestScope=1;
		if(isset($_GET['testScope']))
		{
			$iTestScope=2;
		}

		if(isset($_POST['testNegMarksOff']))
		{
			$iTestNegMarksOff = $_POST['testNegMarksOff'];	
		}
		else
		{
			$iTestNegMarksOff = 0;
		}
		$sTestName = htmlspecialchars($sTestName);		
		//$sTestResultType = $_POST['testResultType'];
		$sTestResultType = 1;
		$sTestCode = addslashes($_POST['hiddenCode']);
		//$bTestStatus = $_POST['testStatus'];			----This funtionality now not in use.
		$bTestStatus=0;
		$sTestDescription = addslashes($_POST['testDescription']);
		$sTestDescription = htmlspecialchars($sTestDescription);
		$ddoc = addslashes(date("Y-m-d H:i:s")); 		
		
		$iTotalQue=0;
		$iNoOfAttempt=1;
		
			if($sTestName!=NULL)
			{
			/*@ when connetion failed.*/
				if ($mysqli->errno) 									
				{	
					header('location: manageTest.php?msg=-1');					
				}
				/*@ when connetion Established.*/
				else
				{					
					
						$sQueryTest ="INSERT INTO quiz_online.test_detail (test_id, group_id, test_name, test_subject, test_desc, test_doa, test_from, test_to, test_duration, test_total_que, test_attempt_limit, test_result_type, test_code, test_neg_status, test_status,test_msg, test_scope, test_extra) VALUES
																(NULL, '{$iGid}', '{$sTestName}', '{$sTestSubject}', '{$sTestDescription}', '{$ddoc}', '{$sTestValidFrom}', '{$sTestValidTill}','{$sTestDuration}',{$iTotalQue},{$iNoOfAttempt},'{$sTestResultType}','{$sTestCode}', '{$iTestNegMarksOff}', '{$bTestStatus}','{$sTestMsg}', '{$iTestScope}', NULL)";																
						$result=$mysqli->query($sQueryTest);						
						if($result==True)
						{
							$sLastTestId=$mysqli->query("select max(test_id) from test_detail");
							$iTestId=null;;
							if($sLastTestId==True)
							{
								$row=$sLastTestId->fetch_row();
								$iTestId=$row[0];
							}
							header("location: showTest.php?msg=1&id={$iTestId}");
						}
						else
						{	

							header("location: addTestHTML.php?msg=0");																				
						}
				}
			}
			else
			{
				header("location: addTestHTML.php?msg=-2");
										
			}
	?>		