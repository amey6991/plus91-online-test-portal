		
<?php
		
		session_start();
        // @file connect to the database.
        include('classConnectQA.php');		               
		$sTestName = addslashes($_POST['testName']);
		$sTestName = htmlspecialchars($sTestName, ENT_QUOTES);	
		$sTestSubject = addslashes($_POST['testSubject']);								
		$sTestValidFrom = addslashes($_POST['testValidFrom']);
		$sTestValidTill = addslashes($_POST['testValidTo']);
		$sTestDesc = addslashes($_POST['testDescription']);
		$sTestDesc = htmlspecialchars($sTestDesc, ENT_QUOTES);
		$sTestDuration = addslashes($_POST['testDuration']);
		$sTestMsg = addslashes($_POST['testMsg']);
		$sTestMsg = htmlspecialchars($sTestMsg, ENT_QUOTES);
		$sTestScope=1;
		if(isset($_POST['testScope']))
		{
			$sTestScope=2;
		}	
		if(isset($_POST['testNegMarksOff']))
		{
			$iTestNegMarksOff = $_POST['testNegMarksOff'];	
		}
		else
		{
			$iTestNegMarksOff = 0;
		}
				
		//$sTestResultType = $_POST['testResultType'];								
		$iTestid=addslashes($_POST['testId']);	
		$iGroupId = $_POST['testGroup'];	
		$iMsg=Null;
		$iGid=Null;
		if(isset($_GET['id']))	
		{
			$iTestid=$_GET['id'];
		}
		if(isset($_GET['gid']))	
		{
			$iGid=$_GET['gid'];
		}
		if(isset($_GET['msg']))
		{
			$iMsg=$_GET['msg'];
		}
		
		if($mysqli->errno)
		{
			//echo "DB Error";
			header("location: editTest.php?id={$iTestid}&msg=-1");
		}
		else
		{
			if($sTestName!=Null)
			{
				$sQueryUpdate="update test_detail set test_name='{$sTestName}', test_subject='{$sTestSubject}', test_to='{$sTestValidTill}', test_desc='{$sTestDesc}', test_from='{$sTestValidFrom}',
							 test_duration='{$sTestDuration}', group_id='{$iGroupId}', test_neg_status='{$iTestNegMarksOff}', test_msg='{$sTestMsg}', test_scope='{$sTestScope}' where test_id={$iTestid}";			
				//var_dump($sQueryUpdate);
				$result=$mysqli->query($sQueryUpdate);
				
				if($result!=NULL)
				{
					$mysqli->close();
					//echo "update complete";
					header("location: editTest.php?id={$iTestid}&msg=1");
				}
				else
				{
					$mysqli->close();
					//echo "update not complete";
					header("location: editTest.php?id={$iTestid}&msg=0");
				}			
			}
			else
			{
				header("location: editTest.php?id={$iTestid}&msg=0");
			}
		}											
		?>
