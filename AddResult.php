<?php
	include('classConnectQA.php');
	/* This Session used to calculate actual time taken by the user to solve test */
	session_start();
	ini_set('display_errors', 1); error_reporting(E_ALL);
	$_SESSION['testEndTime'] = date("y-m-d H:i:s", time());
	$_SESSION['testend2'] = time();

	$dTestStartTime = $_SESSION['testStartTime'];
	
	$dTestEndTime = $_SESSION['testEndTime'];

	$dStartTime2 = $_SESSION['teststarttile2'];
	$dEndTime2 = $_SESSION['testend2'];

	$iTimeDiffInSec = $dEndTime2 - $dStartTime2;

	function sec2hms ($sec, $padHours = false) 
	  {
	    // start with a blank string
	    $hms = "";
	    
	    // do the hours first: there are 3600 seconds in an hour, so if we divide
	    // the total number of seconds by 3600 and throw away the remainder, we're
	    // left with the number of hours in those seconds
	    $hours = intval(intval($sec) / 3600); 	/* intval — Get the integer value of a variable */

	    // add hours to $hms (with a leading 0 if asked for)
	    $hms .= ($padHours) 
	          ? str_pad($hours, 2, "0", STR_PAD_LEFT). ":"			/* str_pad — Pad a string to a certain length with another string */
	          : $hours. ":";
	    
	    // dividing the total seconds by 60 will give us the number of minutes
	    // in total, but we're interested in *minutes past the hour* and to get
	    // this, we have to divide by 60 again and then use the remainder
	    $minutes = intval(($sec / 60) % 60); 

	    // add minutes to $hms (with a leading 0 if needed)
	    $hms .= str_pad($minutes, 2, "0", STR_PAD_LEFT). ":";

	    // seconds past the minute are found by dividing the total number of seconds
	    // by 60 and using the remainder
	    $seconds = intval($sec % 60); 

	    // add seconds to $hms (with a leading 0 if needed)
	    $hms .= str_pad($seconds, 2, "0", STR_PAD_LEFT);

	    // done!
	    return $hms;
	    
	  }

	$iTimeTaken = sec2hms($iTimeDiffInSec);
	/* end of time calculation */
	$iUserId = $_SESSION['user_id'];
	$iTotalQue = addslashes($_POST['totalQue']);
	$iTestId = addslashes($_POST['test_id']);
	$iCountMutichiceQue = addslashes($_POST['totalMultiQue']);
	//var_dump($_POST);

		for($ii = 1; $ii <= $iTotalQue ; $ii ++) /* This for loop receives multiple value from page and put it one by one in query */
			{
				$iQid = addslashes($_POST['qid' . $ii]);
				if(isset($_POST['ans' . $ii]))
				{
					$iAns = addslashes($_POST['ans' . $ii]);
					
					$iAnsTrim = htmlspecialchars($iAns, ENT_QUOTES);
					$sQueryIns = "INSERT INTO `stud_ans_table` ( `user_id`,`test_id`, `que_id`, `user_ans_answer` ) 
						VALUES ($iUserId, $iTestId , $iQid ,'$iAnsTrim')";
					$iResult2 = $mysqli->query($sQueryIns);/* This query insert student answer to the database */

					//var_dump($sQueryIns);
				}	
			}

		for($ij = 1; $ij <= $iCountMutichiceQue ; $ij++)  /* This code stores the multiple question ans to database */
		{
			if(isset($_POST['Multiqid' . $ij]))
			{
				$iMultiQueId = addslashes($_POST['Multiqid' . $ij]);
				for($iChk = 1 ; $iChk <= 8 ; $iChk++)
				{
					if(isset($_POST['chkVal'. $iMultiQueId . $iChk]))
					{
					 	$iChkboxID = addslashes($_POST['chkVal'. $iMultiQueId . $iChk]);
					 
					 	$sQueryInsMultiCh = "INSERT INTO `stud_ans_table` ( `user_id`,`test_id`, `que_id`, `user_ans_answer` ) 
						VALUES ($iUserId, $iTestId , $iMultiQueId ,'$iChkboxID')";
						//var_dump($sQueryInsMultiCh);
						$iResultMultiChoiceQue = $mysqli->query($sQueryInsMultiCh);
					}	
				}
			}
		}

		/*This code give test result like - total number of question in test , number of question attempted by the student,
		number of right question , number of wrong question , total mark and percentage  */
		$sStdResQuery = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
						b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks ,b.op_option as OptionText
						from stud_ans_table as a , option_table as b , question_table as c
						where a.user_ans_answer = b.op_id 
						AND b.op_correct_ans  = 1 
						AND b.que_id = c.que_id 
						AND a.user_id = {$iUserId} 
						AND a.test_id = {$iTestId}";
						/* this query returns right question given by user */
		$iStdResult = $mysqli->query($sStdResQuery);		
						/* $iStdResult - this variable stores query result */
		//var_dump($sStdResQuery);

		$iRowCount = 0;
		$iObtenMarks=0;

		while($aRowStdResult = $iStdResult->fetch_row())
		{
			$iObtenMarks += $aRowStdResult[6];
			$iRowCount = $iRowCount + 1;
		}
		

		$iTotalAttemptQueQuery = "select distinct a.que_id  from stud_ans_table as a where a.user_ans_answer <> '' AND a.user_id = {$iUserId} AND a.test_id = {$iTestId}"; 
						/* This query gives number of attempted question by the user for perticular test */
		$iAtempQueResult = $mysqli->query($iTotalAttemptQueQuery);
		$iAttemptQue_Count = $iAtempQueResult->num_rows;

		//var_dump($iTotalAttemptQueQuery);

		$sQueryForMarks = "select test_id,que_id,que_question,que_marks,que_neg_marks 
							from question_table where test_id = {$iTestId}";
					/* This query gives number of marks for each question and negative marks for each question */
		$iQueForMrkResult = $mysqli->query($sQueryForMarks);
		
		//var_dump($sQueryForMarks);

		$iTotalMks = 0;
		$iTM = 1;
		while($aRowTotalMks = $iQueForMrkResult->fetch_row())
		{
			$iTotalMks += $aRowTotalMks[3];
		}

		$iTotalNegMark = 0;
		$sQueryToDisNegMark ="select test_neg_status from test_detail where test_id={$iTestId}";
		$iResultDispNegMark = $mysqli->query($sQueryToDisNegMark);
		
		//var_dump($sQueryToDisNegMark);

		$sWrongQue = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
					b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks , c.que_neg_marks as Negativemark 
					from stud_ans_table as a , option_table as b , question_table as c
					where a.user_ans_answer = b.op_id 
					AND b.op_correct_ans  != 1 
					AND b.que_id = c.que_id
					AND a.que_id = b.que_id
					AND c.que_type LIKE '%Objective%'
					AND a.user_id = {$iUserId} 
					AND a.test_id = {$iTestId}";	
					/* This query calculate number of wrong question and thir mark*/ 
		$iWrongQueResult = $mysqli->query($sWrongQue);
		$iCountWrongQue = $iWrongQueResult->num_rows;

		if($iResultDispNegMark == true)
		{
			$aFetchDispNegMark = $iResultDispNegMark ->fetch_row();
			if($aFetchDispNegMark[0] == 1)
			{
				//var_dump($sWrongQue);
				$sMsgForResult = "";
				if($iWrongQueResult == true)
				{
					while($aRowWrongQueResult = $iWrongQueResult->fetch_row())
					{
						$iTotalNegMark += $aRowWrongQueResult[7];	
						/* This calculates the negative mark for wrong question */
					}
				}
			}	

		}
		
		
		$iObtMks = $iObtenMarks - $iTotalNegMark;
		$sGetDescQue = "select count(a.que_type)
						from question_table as a
						where 
						a.test_id = {$iTestId} AND 
						(a.que_type LIKE '%Descriptive%'
						OR a.que_type LIKE '%Multiple%')";	
					/* this query return number of descriptive question in the test */
		$iGDescQueResult = $mysqli->query($sGetDescQue);
		$aRowGDescQueResult = $iGDescQueResult->fetch_row();

		//var_dump($sGetDescQue);

		$iCountDescQue = $aRowGDescQueResult[0];
		if($iCountDescQue >= 1)
			{
				$sQueryTstDetails = "INSERT INTO `score_board_table` (`user_id`,`sb_doa`, `sb_time_taken`, `sb_attempt_que`, `sb_correct_ans` ,`sb_wrong_ans` ,`sb_marks_out_of`,`test_id`,`sb_in_time`,`sb_out_time`) 
					VALUES ($iUserId ,NOW() ,'$iTimeTaken' ,$iAttemptQue_Count ,$iRowCount , $iCountWrongQue ,'$iTotalMks', $iTestId,'$dTestStartTime','$dTestEndTime')";
				//var_dump($sQueryTstDetails);
				$iResultForScoreBord = $mysqli->query($sQueryTstDetails);
			}
			else
			{
				$sQueryTstDetails = "INSERT INTO `score_board_table` (`user_id`,`sb_doa`, `sb_time_taken`, `sb_attempt_que`, `sb_correct_ans` ,`sb_wrong_ans` ,`sb_marks_obt` ,`sb_marks_out_of`,`test_id`,`sb_in_time`,`sb_out_time`) 
					VALUES ($iUserId ,NOW() ,'$iTimeTaken' ,$iAttemptQue_Count ,$iRowCount , $iCountWrongQue , '$iObtMks' ,'$iTotalMks', $iTestId,'$dTestStartTime','$dTestEndTime')";
				$iResultForScoreBord = $mysqli->query($sQueryTstDetails);
				//var_dump($sQueryTstDetails);
			}

		unset( $_SESSION['test_time'] );
		unset( $_SESSION['TimeRemainForTest'] );
		unset($_SESSION['testStartTime']);
		unset($_SESSION['testEndTime']);
		unset($_SESSION['teststarttile2']);
		unset($_SESSION['testend2']);
		
		$sGetDescQue = "select count(a.que_type) 
						from question_table as a , test_detail as b 
						where b.test_id = a.test_id 
						AND 
						(a.que_type LIKE '%Descriptive%'
						OR a.que_type LIKE '%Multiple%')
						AND b.test_id ={$iTestId} limit 1";	/* this query select test name from test details */
		$iGDescQueResult = $mysqli->query($sGetDescQue);
		$aRowGDescQueResult = $iGDescQueResult->fetch_row();
		$iCountDescQue = $aRowGDescQueResult[0];
		if($iCountDescQue >= 1)
		{
			$iMsg = 123;
	      	header("Location:DispTestResult.php?id={$iTestId}&msg={$iMsg}");
		}
		else
		{
			header("Location:QuizMailer.php?id={$iTestId}");
		}
	/* transfer to next page */
?>