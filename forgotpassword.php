<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Forgot Password</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>
<div id="id_header_wrapperIndex">
  <div id="id_header">
    
   	<div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxt">
      <a href="index.php"><span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo">
        <img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span></a>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
			<form action="login.php" method="POST">
				<div id="idDivUserNameTop" class="classDivUserNameTop">
					<div id="idDivHeadUname" class="header_04">Email</div>
					<div id="idDivHeadUnametxt" class="classHeadInputUname"><input tabindex="1" type="text" id="idUname" name="UserName" value="" class="classUserName"></div>
				    <div id="idDivForgPass" class="classForgPass"><a href="forgotpassword.php" class="">Forgot your password?</a></div>
                </div>
				<div id="idDivPassTop" class="classDivPassTop">
					<div id="idDivHeadUname" class="header_04">Password</div>
					<div id="idDivHeadPasstxt" class="classHeadInputPass"><input tabindex="2" type="password" id="idPass" name="password" value="" class="classUserName"></div>
				</div>
				<div id="idDivSubmitTop" class="classDivSubmitTop">
					<input type="submit" id="submit" name="login" value="Go" tabindex="3" class="btn btn-primary">
				</div>

			</form>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBarIndex radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapperIndex">
	<div id="id_banner">
        <div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to Plus91 Career Portal</div>
        </div>
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<div id="id_content_wrapper">
	<div id="id_content">
    	<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">
                <div id="idDivSignUp" class="">
                    <div id="idDivDesg7" class="classSignUpResponceText">
                        <div id="idDivThanksSigner" class="classDivThanksSigner">
                            <form action="forgotpass.php" method="POST" id="idFormForgotPass" class="classFormForgotPass">
                                <div id="idDivTextInSignupResp" class="classDivTextInSignupResp">
                                Enter your email address to have your password mailed to you.
                                <div id="idDivEmailTxtInput" class="classDivEmailTxtInput classForgtTxt">
                                    <span id="idSpanForgTxt" class="classForgtTxt">Your Email :</span>
                                    <span id="idSpanForgTxtIp" class="classForgtTxt">
                                        <div class="input-prepend">
                                            <span class="add-on">
                                            <i class="icon-envelope"></i>
                                            </span>
                                            <input tabindex="4" type="text" name="email" id="idInputEmail" class="class_login_txt">
                                        </span>
                                    </div>
                                </div>
                                <div id="idDivSubmitButton" class="classDivTextInSignupResp">
                                <span><input id='idForgotPassword' tabindex="5" class='btn btn-primary classAddTestButtonSize' type='submit' value='Submit' name='forgitpassword'/></a></span>
                                <span>
                                <?php 
                                 if(isset($_GET['msg']))
                                {
                                    $iMsg = $_GET['msg'];
                                }
                                else
                                {
                                    $iMsg ="";
                                }
                                
                                if($iMsg ==788)
                                {
                                    echo "<span class='classDivMsgOnForgPass'>Account details sent on your email.</span>";
                                }
                                if($iMsg ==789)
                                {
                                    echo "<span id='idDivVarifymsg' class='classRed'>Enter email.</span>";
                                }
                                if($iMsg ==7890)
                                {
                                    echo "<span id='idDivVarifymsg' class='classRed'>Entered email id is not registered.</span>";
                                }
                                ?>
                                </span>
                                </div>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                    <div id="idDivResrndMail" class="classDivThanksSigner">
                        
                        
                        
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>