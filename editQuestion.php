<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Edit Test Question's</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- script for text counter in text area -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
      
    var characterLimit = 500;  
      
    google.setOnLoadCallback(function(){  
          
        $('#remainingCharacters').html(characterLimit);  
          
        $('#idAddQuestion').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
              
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            }  
  
        });  
    });  
</script>
<!-- end of script for text counter in text area -->
<!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idFormQueEdit").validationEngine();
        });

    </script>               
<!--end of javascript to validate the textbox -->

<!--Start of javascript to prevent submit form enter -->
<script type="text/javascript">
	$(document).ready(function() {
	  $(window).keydown(function(event){
	    if(event.keyCode == 13) {
	      event.preventDefault();
	      return false;
	    }
	  });
	});
</script>
<script type="text/javascript">
	function formSubmit()
	{
		document.editQuestionForm.submit();
	}
</script>
<!--end of javascript to prevent submit form enter -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body onload="setOption()">
	<?php 
			session_start();
			include('classConnectQA.php'); 
			if(isset($_SESSION['user_id']))
			{
				$iUid=$_SESSION['user_id'];	
			}	
			else
			{
				$iUid=Null;	
			}
			if(isset($_SESSION['lid']))		// This is Use to check a Session
			{
				$iLoginId = $_SESSION['lid'];
			}
			else
			{
				header("location:index.php");
			}
			if(isset($_SESSION['ut']))  
            {
                $ut=$_SESSION['ut'];
            }
            else
            {
            	$ut=null;	
            } 
			$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();
	?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
				<?php
					echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>";
								echo "<ul id='menu'>
                            <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                <ul>
                                    <li>
                                        <a href='profile.php'>Profile</a>       
                                    </li>
                                    <li>
                                        <a href='profileedit.php'>Update Profile</a>            
                                    </li>
                                    <li>
                                        <a href='changePassword.php'>Change Password</a>            
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href='manageTest.php'>Home</a>       
                            </li>";
                            if($ut==2)
                            {
                            	echo "<li> <a href='opportunityHTML.php'>Opportunity</a></li>";
                            }
                            else
                            {
                            	echo "<li><a href='opportunity.php'>Opportunity</a>
                                <ul>
                                <li>
                                    <a href='opportunityHTML.php'>Create</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Manage</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
                                <a>Create</a>
                                <ul>
                                    <li>
                                    <a href='groupHTML.php'>Create Group</a>        
                                    </li>
                                    <li>
                                    <li>
                                        <a href='addTestHTML.php'>Create Test</a>               
                                    </li>
                                    <li>
                                    <a href='addUserHTML.php'>Create User</a>          
                                </li>
                                <li>
                                    <a href='excelReader/index.php'>Bulk Upload</a>            
                                </li>
                                </ul>
                            </li>";
                            }
                            
                           echo "<li>
                                <a>Manage </a>  
									  <ul>
									  		<li>
							                	<a href='manageGroup.php'>Manage Group</a>      
							              </li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
									</ul>    
                            </li>
                            <li>
                                <a href='logout.php'>Logout </a>    
                            </li>
                            </ul>
							</div>";
				?>		
				
				</div>
			</div>  	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
           <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->

</div> <!-- end of banner wrapper -->

<?php
			/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@gid: is use for Group Id.
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status.
			*/
			

			if($mysqli->errno)
			{
				header("location: editTest.php?id={$tid}&msg=-1");
			}
			else
			{
				/*
					To recive the value like id passes via address string.
				*/
				$iGid=Null;
				$iTestid=Null;
				$iQueid=Null;
				$iMsg=Null;
				if(isset($_GET['gid']))	
				{
					$iGid=$_GET['gid'];
				}
				if(isset($_GET['id']))	
				{
					$iTestid=$_GET['id'];
				}
				if(isset($_GET['qid']))
				{
					$iQueid=$_GET['qid'];
				}
				if(isset($_GET['msg']))
				{
					$iMsg=$_GET['msg'];
					 echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
					 if($iMsg==1)
						{
							echo "<div class=classMsg >Question updated successfully</div>";
						}
						if($iMsg==0)
						{
							echo "<div class=classMsg >Try again: Question not edit</div>";
						}
						if($iMsg==-1)
						{
							echo "<div class=classMsg >DB Error in editing qestion</div>";
						}
						if($iMsg==-2)
						{
							echo "<div class=classMsg >Mendatory fields must not be filled</div>";
						}
						if($iMsg==-3)
						{
							echo "<div class=classMsg >Question not updated,Because blank option is chosen as a answer.</div>";
						}						
						if($_GET['msg']==4512)
						{
							echo "<div class=classMsg >Attachment added.</div>";
						}
						if($_GET['msg']==4511)
						{
							echo "<div class=classMsg >Attachment removed.</div>";
						}
					 echo "</div>";
				}	
			/*
				Query string use to retrive a single question of a test.
			*/
				$sQueQuery = "select * from question_table where test_id= {$iTestid} and que_id= {$iQueid}";
				$qResult = $mysqli->query($sQueQuery);
				$row = $qResult->fetch_row();
				$sQueType=$row[3];									;
				if($sQueType=="Objective")
				{
					$sQueQuery = "select * from option_table where que_id= {$iQueid} order by op_id" ;
				}
				else
				{
					if($sQueType=="Multiple")
					{
						$sQueQuery = "select * from multi_option_table where que_id= {$iQueid} order by multi_op_id" ;						
					}
					else
					{
						// Use for the next type of quesions.
					}
					
				}

				$opResult = $mysqli->query($sQueQuery);				
				if($opResult == True && $sQueType == "Objective" || $sQueType == "Multiple")
				{	
					$ii=0;
					$opArray = array();
					$opIdArray = array();
					$opAnsArray = array();
					for($jj=0;$jj<7;$jj++)
					{
						$opIdArray[$jj]=null;
						$opArray[$jj]=null;
						$opAnsArray[$jj]=null;
						if($opRow = $opResult->fetch_row())	
						{
							$opIdArray[$ii]=$opRow[0];
							$opArray[$ii]=$opRow[2];
							$opAnsArray[$ii]=$opRow[3];											
							$ii++;
						}	

					}										
				}
				else
				{					
					$opArray = array(Null);
					$opIdArray = array(Null);
					$opAnsArray = array(Null);
					for($ii=0;$ii<7;$ii++)
					{
						
					$opIdArray[$ii]=null;
					$opArray[$ii]=null;
					$opAnsArray[$ii]=null;	
					}
				}				
			}	

			?>
			
	
<div id="id_content_wrapper">
	<div id="id_content">
        
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">
			<form action="upQue.php?id=<?php echo $iTestid; ?>" method="POST" name="editQuestionForm" id="idFormQueEdit">
				<input type="hidden" name="hiddenTestId" value="<?php echo $iTestid; ?>"></input>
				<input type="hidden" name="hiddenQueId" value="<?php echo $iQueid; ?>"></input>
				<div id="idDivSignUp" class="header_0345">Edit Question
				<?php
					$sTestName=$mysqli->query("select test_name, test_neg_status from test_detail where test_id={$iTestid}");
					if($sTestName == true)
					{
						$aTestRow=$sTestName->fetch_row();
						echo " of :  <u>{$aTestRow[0]}</u> Test";
						$negMarksOff = 	$aTestRow[1];						
					}
					else
					{
						$negMarksOff = 	Null;							
					}
					echo $iGid;
				?>
				</div>
				<div class="classHorizHRSubHead"></div>
				<div id="idDivAddQuestion" class="classDivAddQuestionOnly">
                <div id="idDivQue" class="classDivAddTest">
                    <span id="idSpanTabQue" class="classSpanAddTestDesc">Question <span class="classRed">*</span></span>
                    <span id="idSpanTabQue:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQueIP" class="classSpanTabIP">
                    	<textarea name="queName" id = "idAddQuestion" class="classTADescription classInputBackColor validate[required] text-input"><?php echo $row[2]; ?></textarea>                      
                    </span>
                </div>
               
			   <div id="idDivQMarks" class="classDivAddTest">
                    <span id="idSpanTabQMarks" class="classSpanAddTestDesc">Question Marks<span class="classRed">*</span></span>
                    <span id="idSpanTabQMarks:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQMarksIP" class="classSpanTabIP">
                        <input type="text" name="queMarks" id = "" size="35" class="classUserDtInput classInputBackColor validate[required,custom[integer]] text-input" value="<?php echo $row[4]; ?>">
                    </span>
                </div>
				
				<div id="idDivNMarks" class="classDivAddTest">
                    <span id="idSpanTabNMarks" class="classSpanAddTestDesc">Negative Marks<span class="classRed">*</span></span>
                    <span id="idSpanTabNMarks:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabNMarksIP" class="classSpanTabIP">
                        <input type="text" name="negMarks" id = "idNegMarks" size="35" class="classUserDtInput classInputBackColor validate[required,custom[integer]] text-input" value="<?php echo $row[5]; ?>">
                   		<input type="hidden" name="negMarksOff" id="idNegMarksOff" class="classNegMarksOff" value="<?php echo $negMarksOff; ?>" />
                    </span>
                </div>
				
                <div id="idDivQType" class="classDivAddTest">
                    <span id="idSpanTabQType" class="classSpanAddTestDesc">Question Type<span class="classRed">*</span></span>
                    <span id="idSpanTabQType:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQTypeIP" class="classSpanTabIP">
					<select id="id_TestAddYY" onchange="setOption()" class="classUserDtInputSelect class_light_black classFont" name="queType">
					<?php 
						if($row[3]=="Objective")
						{
							echo "<option value='Objective' selected='selected'>Objective</option>
							<option value='Descriptive' >Descriptive</option>
							<option value='Multiple'>Multiple Choice</option>";
						}
						else
						{
							if($row[3]=="Descriptive")
							{

							echo "<option value='Objective'>Objective</option>
							<option value='Descriptive' selected='selected'>Descriptive</option>
							<option value='Multiple'>Multiple Choice</option>";

							}
							else
							{
								if($row[3]=="Multiple")
								{									
									echo "<option value='Objective'>Objective</option>
									<option value='Descriptive'>Descriptive</option>
									<option value='Multiple'  selected='selected'>Multiple Choice</option>";
								}
								else
								{										
									echo "<option value='Objective'>Objective</option>
									<option value='Descriptive'>Descriptive</option>
									<option value='Multiple'>Multiple Choice</option>";
								}
							}
							
						}

					?>
						
                        </select></span> 
				</div>
                        
					<script type="text/javascript"> 
				
					function setOption()
					{	
						var e = document.getElementById("id_TestAddYY");
						var neg = document.getElementById("idNegMarksOff").value;
						var temp = e.options[e.selectedIndex].value;											
						if(temp == "Objective" && neg == 1)
						{
							document.getElementById('idNegMarks').disabled=false;						
							
						}					
						else
						{
							document.getElementById('idNegMarks').disabled=true;							
						}						
					
						if(e.options[e.selectedIndex].value  == "Descriptive")	// When Question Type is of Descriptive.
						{	
							document.getElementById('idDivAddOp').style.visibility = 'hidden';							
							//following code is use to disable the option when question type is descriptive.
							var el = document.getElementById('idDivAddOp'),
					        all = el.getElementsByTagName('input'),ii;					        
						    for (ii = 0; ii < all.length; ii++) 
						    {
						        all[ii].disabled = true;
						         all[ii].style.visibility='hidden';
						    }	

						}
						else 
						{	
							if(e.options[e.selectedIndex].value  == "Multiple")				// When Question Type is of Multiple Choice.
							{								
								//document.getElementById('idNegMarks').disabled=true;
								//document.getElementById('idNegMarks').value='';
								document.getElementById('idDivAddOp').style.visibility = 'visible';
								var el = document.getElementById('idDivAddOp'),
						        all = el.getElementsByTagName('input'),ii;					    
							    for (ii = 0; ii < all.length; ii++) 
							    {	
							    	if(all[ii].name=='op')
							    	{
							    		all[ii].disabled = true;
							    		all[ii].style.visibility='hidden';
							    	}	
							    	else
							    	{

							    		all[ii].disabled = false;	
							    		all[ii].style.visibility='visible';						        	
							    	}						    	
							        
							    }								     
							}
							else 														// When Question Type is of Objective.
							{
								if(e.options[e.selectedIndex].value  == "Objective")
								{									
									//document.getElementById('idNegMarks').disabled=false;
									//document.getElementById('idNegMarks').value=0;
									document.getElementById('idDivAddOp').style.visibility = 'visible';
									var el = document.getElementById('idDivAddOp'),
							        all = el.getElementsByTagName('input'),ii;		
							        var ij=1;			    
								    for (ii = 0 ; ii < all.length; ii++) 
								    {		
								    	
								    	if(all[ii].name=='checkOpp'+ij)
								    	{									    		
								    		all[ii].disabled = true;
								    		all[ii].style.visibility='hidden';
								    		ij++;
								    	}	
								    	else
								    	{					    		
								    		all[ii].disabled = false;	
								    		all[ii].style.visibility='visible';						        	
								    	}	
								    }
								}	
								else
								{	//document.getElementById('idNegMarks').disabled=true;
									//document.getElementById('idNegMarks').value='';
									document.getElementById('idDivAddOp').style.visibility = 'hidden';
									all = el.getElementsByTagName('input'),ii;					    
								    for (ii = 0; ii < all.length; ii++) 
								    {
								        all[ii].disabled = true;
								        all[ii].style.visibility='visible';
								    }
								}

							}
						}
						
					}
					
				</script>
				
				 <div id="idDivQDesc" class="classDivAddTest">
                    <span id="idSpanTabQDesc" class="classSpanAddTestDesc">Que Description</span>
                    <span id="idSpanTabQDesc:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQDescIP" class="classSpanTabIP">
                        <textarea name="queDescription" id = "idQueDesc" class="classTADescription classInputBackColor" ><?php echo $row[6]; ?></textarea>						
                    </span>
                    <script type='text/javascript' language='javascript'>  
				    google.load('jquery', '1.4.2');  
				      
				    var characterLimit = 500;  
				      
				    google.setOnLoadCallback(function(){  
				          
				        $('#remainingCharacters').html(characterLimit);  
				          
				        $('#idQueDesc').bind('keyup', function(){  
				            var charactersUsed = $(this).val().length;  
				              
				            if(charactersUsed > characterLimit){  
				                charactersUsed = characterLimit;  
				                $(this).val($(this).val().substr(0, characterLimit));  
				                $(this).scrollTop($(this)[0].scrollHeight);  
				            }  
  
				        });  
				    });  
				</script>
                </div>
                <div id="idDivAttachment" class="classDivAddTest">
                    <span id="idSpanTabQDesc" class="classSpanAddTestDesc">Attach image</span>
                    <span id="idSpanTabQDesc:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabQDescIP" class="classSpanTabIP">
                    	<!--Start Style for using formatted File tag -->	
						  <style>
                            .idiv{display:inline-block;width:230px;height:30px;}
                            .iidiv1{opacity:0.8;width:230;height:30px;position:absolute;}
                            .iidiv2{opacity:0.0;width:230;height:30px;margin-top: 0px;}
                            .iinputFile{height:40px;width: 50px;margin-top: 0px;}
                            .iinputBut{height:30px;width: 50px;margin-top: -10px;}
                            .iinputTxt{overflow:hidden;font-size:14px;height:28px;width: 150px;border: 1px solid rgb(192, 192, 192);display:inline-block;margin-top: 0px;}
                            
                        </style>
						<!--END of Style for using formatted File tag -->	
                    	<!--Start HTML Code for using formatted File tag -->
							<!-- START0 Outer Container div which holds textbox, button and file tag. -->
							<div class="idiv"> 
								<!-- START1 Inner Container1 div which holds textbox and button. -->
								<div class=iidiv1>	
									<input type="text" class="iinputTxt classRounded_Radius_5" id="idBrowseFileName"/>
									<input type="button"class="iinputBut but btn btn-primary" value="Browse">
									</div>							<div class=iidiv2>	
									<input type="file"class="iinputFile" id="fileToUpload" name="fileToUpload" onchange="return ajaxFileUpload(this.value);">			
								
								</div>

								
								<!-- END1 Inner Container1 div which holds textbox and button. -->
								<!-- START2 Inner Container2 div which holds File. -->
								
								<!-- END2 Inner Container2 div which holds File. -->
							</div>
							<!-- START0 Outer Container div which holds textbox, button and file tag. -->
						<!--END HTML Code for using formatted File tag -->
                      <br/><span id='idResponceMsg' class='classSmallFontForOpp' ></span>
                    </span>
                    <div id="idDivDisplayUploadProceessImg" class="classDivDisplayUploadProceessImg">
                    	<span id="idShowImage"><img id="loading" src="images/loading.gif" style="display:none;">
                    	<?php
                    		$sAttachmentQuery="select * from question_attachment where que_id={$iQueid} and file_status=1";
                    		$bAttachmentResult=$mysqli->query($sAttachmentQuery);
                    		
                    			$aAttachmentRow=$bAttachmentResult->fetch_row();
                    			
                    			$sAttachmentPath=$aAttachmentRow[3];
                    			$sAttachmentName=$aAttachmentRow[4];
                    		
	                    	$sQueryCheckAttachment = "select count(que_id) from question_attachment where que_id = {$iQueid} AND file_status = 1";
	                    	$iCheckAttach = $mysqli->query($sQueryCheckAttachment);
				            $aCheckRowFileAttach = $iCheckAttach->fetch_row();
				            if($aCheckRowFileAttach[0] >=1)
				            {
				            	echo "<span class='classLikeAnchor' onclick='removeImage();'><u>Remove Attachment</u></span>";				            	
				            	echo "<div><img id='idAttachment' class='classRounded_Radius_5 classDispAttachmentEdit' src='$sAttachmentPath$sAttachmentName'></div>";
				            } 
				            else
				            {
				            	// Do  Nothing.
				            }
	                    	

                    	?></span>
                    </div>
                </div>
                   <br/>
               
                   <span id="idSpanCountry" class="classSpanUserInfoTxt"><span class="classRed">*</span> Fields are Mandatory.</span>
                   <span id="idSpanCountryDb" class="classSpanUserDetailInput">
                    
                   </span>
               
                <br/>

				  <div id="idDivSub" class="classDivAddTest">
                    <span id="idSpanTabSub" class="classSpanAddTestDesc"></span>
                    <span id="idSpanTabSub:" class="classSpanTabCol">   </span>
                     <div id="idDivAddQue" class="classAddQuestionButten classNoDecoration">
                     	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     	&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
						

						<input id="idsubmitQue" class="btn btn-primary classAddTestButtonSize" type="submit" value="Update Question" name="submitQue" />

						<span class="classNoDecoration"><a href="viewTest.php?id=<?php echo $iTestid; ?>"><input id="id_add_test_cancel" class="btn btn-primary classAddTestButtonSize" type="button" value="View Test" name="cancelBut"/></a></span>
						
                    </div>
                </div>
				 <div id="idDivManda" class="classDivAddTest classHidden">
                    <span id="idSpanTabMandaIP" class="classSpanTabIP">
										                       
                    </span>
                </div>
            </div>
				<div class="classDivAddOp classRounded_Radius_5" id="idDivAddOp">
					
					<div class="classDivOptions classRounded_Radius_5" >
						<div class="classOpLeft classFont16" id="idOptionHeading">Option's</div>
						<div class="classOpRight classFont16" id="idAnswerHeading">Answer</div>
					</div>
					<?php
					$cAlphabet='A';
						
						for($ii=0,$ij=1;$ii<7;$ii++,$cAlphabet++,$ij++)
						{	
							echo "<div class='classDivOptions classRounded_Radius_5'>{$cAlphabet} :";		?>

							<input type="text" name="<?php echo "optext".$ij; ?>" value="<?php echo $opArray[$ii]; ?>" id="<?php echo "idOpText".$ij; ?>" class="classInputRadioText classFont">
							<?php 
							//echo "{$cAlphabet}<input type=text name='optext{$ij}' value='{htmlspecialchars($opArray[$ii])}' id='idOpText{$ij}' class='classInputRadioText classFont'>";
							echo "<input type='hidden' name='opid{$ij}' value='{$opIdArray[$ii]}'>";
							if($opAnsArray[$ii]==1)
							{	
								if($sQueType=="Objective")
								{
									echo "<input type='radio' name='op'  value='optext{$ij}' checked='checked' id='idOpRadio{$ij}' class='validate[required] radio'>";
									echo "<input type='checkbox' name='checkOpp{$ij}' value='optext{$ij}' class='classCheckBox' id='idCheckOpp{$ii}'>";
								}
								else
								{
									echo "<input type='checkbox' name='checkOpp{$ij}' value='optext{$ij}' class='classCheckBox' id='idCheckOpp{$ii}' checked='checked'>";
									echo "<input type='radio' name='op'  value='optext{$ij}' id='idOpRadio{$ij}' class='validate[required] radio'>";	
								}							
							}
							else
							{
								if($sQueType=="Objective")
								{
									echo "<input type='radio' name='op'  value='optext{$ij}' id='idOpRadio{$ij}' class='validate[required] radio'>";	
									echo "<input type='checkbox' name='checkOpp{$ij}' value='optext{$ij}' class='classCheckBox' id='idCheckOpp{$ii}'>";
								}
								else
								{
									echo "<input type='checkbox' name='checkOpp{$ij}' value='optext{$ij}' class='classCheckBox' id='idCheckOpp{$ii}'>";	
									echo "<input type='radio' name='op'  value='optext{$ij}' id='idOpRadio{$ij}' class='validate[required] radio'>";	
								}							
							}
							
							echo "</div>";
						}
					?>
			
					
			</form>
            </div>
           </div> 
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->
<!-- Upload file using ajax -->
 
  <script type="text/javascript" src="js/ajaxfileupload.js"></script>
  <script type="text/javascript">
function showImage(val)
{
	
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
      $("#idShowImage").html(xmlhttp.responseText);
    }
  }
xmlhttp.open("GET","fetchImage.php?tid=<?php echo $iTestid; ?>&qid=<?php echo $iQueid; ?>&note='edit'",true);
xmlhttp.send();

document.getElementById('idBrowseFileName').value=val;
}

  function removeImage()
{
	
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
   	  $("#idShowImage").html(xmlhttp.responseText);
    }
  }
xmlhttp.open("GET","removeQuestionAttachment.php?tid=<?php echo $iTestid; ?>&qid=<?php echo $iQueid; ?>",true);
xmlhttp.send();
document.getElementById('idBrowseFileName').value="";
}

  function ajaxFileUpload(val)
  {
  	var e = document.getElementById("id_TestAddYY");
  	var strQtp = e.options[e.selectedIndex].value;
  	//alert(strQtp);
    $("#loading")
    .ajaxStart(function(){
      $(this).show();
    })
    .ajaxComplete(function(){
      $(this).hide();
    });

    $.ajaxFileUpload
    (
      {
        url:"questionAttachment.php?quid=<?php echo $iQueid; ?>&qtp=" + strQtp,
        secureuri:false,
        type: "POST",
        fileElementId:'fileToUpload',
        dataType: 'json',
        data: strQtp,
        success: function (data, status)
        {
          if(typeof(data.error) != 'undefined')
          {
            if(data.error != '')
            {
              alert(data.error);
            }else
            {
              alert("File Uploaded Successfully.");
             showImage(val);              
            }
          }
        },
        error: function (data, status, e)
        {
          alert(e);
        }
      }
    )
    return false;
  }
  </script>
 <!-- end of ajax-->
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About Us</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>