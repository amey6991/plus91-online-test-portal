<?php
	include('classConnectQA.php');
    session_start();
	if(isset($_GET['usid']))
	{
		$iUserId = $_GET['usid'];
	}
	else
	{
		$iUserId = "";
	}
    /* Opportunity Id recived when resume is going to upload or update from opportunity.
    -----------------------------------------------------------------------------------*/
    if(isset($_GET['oppid']))       
    {
        $iOppId=$_GET['oppid'];
    }
    else
    {
        $iOppId=null;
    }
	$error = "";
	$msg = "";
	$fileElementName = 'fileToUpload';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize, You cant upload more than 2 MB. ';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE,You cant upload more than 2 MB.';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;
			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else 
	{

			$msg .= " File Name: " . $_FILES['fileToUpload']['name'] . ", ";
			$msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']);

			$sOrgFileName = $_FILES["fileToUpload"]["name"];
            $sFileContentType = $_FILES["fileToUpload"]["type"];
            $iFileSize = $_FILES["fileToUpload"]["size"];

            if($iFileSize >= 2097152)
            {

                $iFileBigMsg = 1231;
                if(is_null($iOppId))
                {
                    header("location:profileedit.php?id={$iTestId}&msg={$iFileBigMsg}");
                }   
                else
                {
                    header("location:applyOpportunity.php?oppid={$iOppId}&uid={$iUserId}&msg={$iFileBigMsg}");
                }                 
                
                exit();
            }

            $sGetFileExt = substr($sOrgFileName, -3, 3);/* This Finds file extension from file name */
            $sFileExt ="";

            if( $sGetFileExt == "pdf" )
            {
              $sFileExt = ".pdf";
            }
            if( $sGetFileExt == "doc" )
            {
              $sFileExt = ".doc";
            }
            if( $sGetFileExt == "ocx" )
            {
              $sFileExt = ".docx";
            }
            if($sFileExt == ".doc" || $sFileExt == ".docx" || $sFileExt == ".pdf")
            {
               
            }
            else
            {
                $error = "You can upload only .doc or .docx or .pdf only";
                //$sUnknownFileFormat = 249;
                echo "{";
                echo    "error: '" . $error . "',\n";
                echo    "msg: '" . $msg . "'\n";                
                echo "}";
                //header("location:profileedit.php?msg={$sUnknownFileFormat}");
                exit();
            }

            $iRandomDigit = uniqid($iUserId); //! This generate the unique number using uniqid function.
            $iTestId = 0;
            
            $sNewFileName = $iUserId."_".md5($sOrgFileName . $iRandomDigit).$sFileExt;
            $sLocation = "Resume/";
			$sResumeCheckQuery = "select count(user_id) from user_resume where user_id = {$iUserId} AND resume_status = 1";
            $iCheckResume = $mysqli->query($sResumeCheckQuery);
            $iCheckRowResume = $iCheckResume->fetch_row();
            if($iCheckRowResume[0] >=1)
            {
                $sUpdateResume = "UPDATE user_resume SET resume_status = 0 where user_id = $iUserId AND resume_status = 1";
                $mysqli->query($sUpdateResume);

                //! Add resume location and name to database .
                $sInsertQuery = "INSERT INTO user_resume (user_id, test_id, location, filename ,resume_status) 
                                VALUES ('$iUserId','$iTestId', '$sLocation' , '$sNewFileName' , 1)";
                $mysqli->query($sInsertQuery);
                 //! move uploaded file to the specified foder with new name
                move_uploaded_file($_FILES['fileToUpload']["tmp_name"],"Resume/" . $sNewFileName);
            }
            else
            {
                //! Add resume location and name to database .
                $sInsertQuery = "INSERT INTO user_resume (user_id, test_id, location, filename ,resume_status) 
                                VALUES ('$iUserId','$iTestId', '$sLocation' , '$sNewFileName' , 1)";
                $mysqli->query($sInsertQuery);
                 //! move uploaded file to the specified foder with new name
                move_uploaded_file($_FILES['fileToUpload']["tmp_name"],"Resume/" . $sNewFileName);
            }
           
	
	}		
	echo "{";
	echo "error: '" . $error . "',\n";
	echo "msg: '" . $msg . "'\n";    
	echo "}";
?>