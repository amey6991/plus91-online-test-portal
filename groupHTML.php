<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Group</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- script for text counter in text area -->
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
      
    var characterLimit = 500;  
      
    google.setOnLoadCallback(function(){  
          
        $('#remainingCharacters').html(characterLimit);  
          
        $('#idTAGDis').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
              
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            }  
  
        });  
    });  
</script>
<!-- end of script for text counter in text area -->
 <!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
    <script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idGroupForm").validationEngine();
        });

    </script>               
<!--end of javascript to validate the textbox -->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
</head>
<body>
  <?php
        session_start();
        include ('classConnectQA.php'); 

        if(isset($_SESSION['lid']))     // This is Use to check a Session
        {
             $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }
        if(isset($_SESSION['ut']))  
        {
            $ut=$_SESSION['ut'];
        }
        else
        {
             $ut="";
        }   
        if(isset($_GET['gid'])) // Is Use when group is to edit, because same page is using for the edit and add Group.
        {
            $iGroupId=$_GET['gid'];
        }
        else
        {
            $iGroupId=Null;   
        } 

        $sGroupName=null;
        $sGroupDesc=null;
        $bGroupStatus=null;
        $bGroupScope= null;
        
        if(isset($_GET['gid'])) // Editing Group.
        {
            $sGroupQuery="SELECT group_name, group_desc, group_status FROM quiz_online.group_table WHERE group_id='{$iGroupId}'";            
            
            $bGResult=$mysqli->query($sGroupQuery);
            if($bGResult==true)                         // Simple if.
            {               
                $aGRow=$bGResult->fetch_row();
                $sGroupName= $aGRow[0];
                $sGroupDesc= $aGRow[1];                
                $bGroupStatus= $aGRow[2];
            }
        }
        else
        {
            // Nothing if Gid not pass because passign gid eans for view group or edit group.
        }
        
        ?>
<div id="id_header_wrapper">
  <div id="id_header">
   	<div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
    </div>
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
                    <?php                    
                     $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                        from login as a , user_details as b
                                        where a.login_id = b.login_id 
                                        AND a.login_id  = '$iLoginId' limit 1";
                    $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                    $aRowForUserInfo = $iResultForUserInfo->fetch_row();
                    
                   echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                    <ul id='menu'>
                            <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                <ul>
                                    <li>
                                        <a href='profile.php'>Profile</a>       
                                    </li>
                                    <li>
                                        <a href='profileedit.php'>Update Profile</a>            
                                    </li>
                                    <li>
                                        <a href='changePassword.php'>Change Password</a>            
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href='manageTest.php'>Home</a>       
                            </li>
                            <li><a href='opportunity.php'>Opportunity</a>
                                <ul>
                                <li>
                                    <a href='opportunityHTML.php'>Create</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Manage</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
                                <a>Create</a>
                                <ul>
                                    <li>
                                    <a href='groupHTML.php'>Create Group</a>        
                                    </li>
                                    <li>
                                    <li>
                                        <a href='addTestHTML.php'>Create Test</a>               
                                    </li>
                                    <li>
                                    <a href='addUserHTML.php'>Create User</a>          
                                </li>
                                <li>
                                    <a href='excelReader/index.php'>Bulk Upload</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
                                <a>Manage </a>  
									  <ul>
                                            <li>
                                                <a href='manageGroup.php'>Manage Group</a>      
                                            </li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
                                                <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                              </li>
									</ul>    
                            </li>
                            <li>
                                <a href='logout.php'>Logout </a>    
                            </li>
                            </ul>
                            </div>";
                    ?>
                </div>
			</div>  	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
    <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
       
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php
    if(isset($_GET['msg']))
    {
        echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
       // messages for the Group add. 
        if($_GET['msg']==1)
        {
            echo "<div class=classMsg >Group Added Successfully</div>";
        }
        if($_GET['msg']===0)
        {
            echo "<div class=classMsg >Try again: Group not Added</div>";
        }
        if($_GET['msg']==-1)
        {
            echo "<div class=classMsg >DB Error in Group</div>";                                
        }
        if($_GET['msg']==-2)
        {
            echo "<div class=classMsg >Mendatory fields must be filled</div>";
        }

       // messages for the Group Edit. 
        if($_GET['msg']==11)
        {
            echo "<div class=classMsg >Group Updated Successfully</div>";
        }
        if($_GET['msg']==10)    //For group not Update.
        {
            echo "<div class=classMsg >Try again: Group Not Updated</div>";
        }                           
        if($_GET['msg']==-22)
        {
            echo "<div class=classMsg >Mendatory fields must be filled</div>";
        }
        echo"</div>";
    }
?>
<div id="id_content_wrapper">
	<div id="id_content">   
		
		<div id="idDivMiddleBody" class="classDivMiddleBody">
          <div id="idDivGroup" class="classDiv">                         
			<form action="group.php" method="POST" id="idGroupForm" class="classGroupForm">               
				<div id="idDivSignUp" class="header_0345">Group
                    
                </div> 
                <div class="classHorizHRSubHead"></div>             
                <!-- Contain Group Id when addGroup is called foe edit-->
                <input type="hidden" name="groupId" class="classGID" id="idGID" value="<?php echo $iGroupId; ?>" />
				<div id="idDivGname" class="classDivAddTest classChangePassword classTopMarginForChangePass"> 
						<p></p>	<br/>			
                    <span id="idSpanTabGN" class="classSpanAddTestDesc">Group Name<span class="classRed">*</span></span>
                    <span id="idSpanTab:GN" class="classSpanTabCol">:</span>
                    <span id="idSpanTabGN" class="classSpanTabIP">
							    <input type="text" name="groupName" id = "idGname" size="35" class="classUserDtInput validate[required] text-input" value="<?php echo $sGroupName; ?>">
                    </span>
                </div>
                <br/>
               
                <div id="idDivGD" class="classDivAddTest classChangePassword">
                    <span id="idSpanTabGD" class="classSpanAddTestDesc">Group <br/><br/>Description<span class="classRed">*</span></span>
                    <span id="idSpanTabGD:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabGD" class="classSpanTabIP">
                        <textarea id="idTAGDis" class="classTADis classSpanUserAddress validate[required] text-input" name="description"><?php echo $sGroupDesc;?></textarea>
                    </span>
                </div>
                <br/>
            <!--    
               <div id="idDivGD" class="classDivAddTest classChangePassword">
                    <span id="idSpanTabGScope" class="classSpanAddTestDesc">Group Scope</span>
                    <span id="idSpanTabGScope:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabGScope" class="classSpanTabIP">
                        <?php  /*                       
                            if($bGroupScope == 1 || $bGroupScope == null)
                            {
                                echo "<input type='checkbox' name='groupScope' class='classGroupScope' id='idCommonGroup1' value ='2'/> Public";
                                echo "<span class='classSmallFontForOpp'>(Group test available for all user's)</span>";
                            }
                            else
                            {                                       
                                echo "<input type='checkbox' name='groupScope' class='classGroupScope' id='idCommonGroup1' value ='2'  checked/> Public";
                                echo "<span class='classSmallFontForOpp'>&nbsp;&nbsp;&nbsp;&nbsp;(Group test available for all user's.)</span>";
                                echo "<br/><br/>";                               
                            }
                                */
                        ?>                        
                    </span>
                </div>
            -->
                <br/>
                <br/>
                <div id="idDivGD" class="classDivAddTest classChangePassword">
                    <span id="idSpanTabGS" class="classSpanAddTestDesc">Group Status</span>
                    <span id="idSpanTabGS:" class="classSpanTabCol">:</span>
                    <span id="idSpanTabGS" class="classSpanTabIP">
                        <?php 
                            if($bGroupStatus == 1 || $bGroupStatus==null)
                            {
                                echo "<input type='radio' name='groupStatus' class='classGroupStatus1' id='idActiveGroup1' value ='1'  checked='checked'/> Active ";
                                echo "<br/><br/>";
                                echo "<input type='radio' name='groupStatus' class='classGroupStatus0' id='idActiveGroup0' value ='2' /> Inactive";
                            }
                            else
                            {                                
                                echo "<input type='radio' name='groupStatus' class='classGroupStatus1' id='idActiveGroup1' value ='1'/> Active ";
                                echo "<br/><br/>";
                                echo "<input type='radio' name='groupStatus' class='classGroupStatus0' id='idActiveGroup0' value ='2' checked='checked'/> Inactive";
                            }
                        ?>                        
                    </span>
                </div>

                <br/>
                <br/>
                <br/>
                <div id="idDivGD" class="classDivAddTest classChangePassword">
                    <span><span class="classRed">*</span> Fields are Mandatory.</span>
                </div>
                 <br/><br/>               
                <div id="idDivBut" class="classDivAddTest">
                    <span id="idSpanTabMob" class="classSpanAddTestDesc"></span>
                    <span id="idSpanTabMob:" class="classSpanTabCol">   </span>
                    <span id="idSpanTabMobIP" class="classSpanTabIP classNoDecoration">
                        <?php
                        if($iGroupId==null)
                        {
                          echo "<input id='idSubmit' class='btn btn-primary classAddTestButtonSize' type='submit' value='Create Group'/>";  
                        }
                        else
                        {
                            echo "<input id='idSubmit' class='btn btn-primary classAddTestButtonSize' type='submit' value='Update'/>";     
                        }
						?>                        
                        <a href='manageGroup.php' title='Back'><input id='id_add_test_cancel' class='btn btn-primary classAddTestButtonSize' type='button' value='Back'/></a>                                
                    </span>
                </div>
				</form>
            </div>
            
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>
	
					