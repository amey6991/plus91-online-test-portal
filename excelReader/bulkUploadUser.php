<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload User Detail's</title>
<meta name="description" content="online quiz application" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<script src='../js/ExcelSecur.js'></script>     <!-- javascript for file validation -->
</head>
<body>
<?php 

 	include("../classConnectQA.php");
 	include("../config/config.php");
		session_start();
		    if(isset($_SESSION['user_id']))
        {
          $iUserId = $_SESSION['user_id'];
        }
        else
        {
          $iUserId ="";
        }
        if(isset($_SESSION['ut']))
        {
          $ut=$_SESSION['ut'];
        }
        else
        {
          $ut="";
        }
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:{$sURL}index.php");
        }
        
        $sQueryUserInfo = "select * from user_details as a where a.login_id = {$iLoginId} limit 1";
        $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
        $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        if(isset($_GET['msg']))
        {
            $iMsg = $_GET['msg'];
        }
        else
        {
		    $iMsg ="";
        }
 ?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
		<div id="idDivHeadTxt" class="classDivHeadTxt">e-Quiz</div>
	</div>
        

		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<?php 
                      if($ut==0)
                      {
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                  <ul id='menu'>
                                  <li><a href='{$sURL}profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='{$sURL}profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='{$sURL}profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='{$sURL}changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                  </li>
                                  <li>
                                    <a href='{$sURL}manageTest.php'>Home</a>   
                                  </li>
                                  <li>
                                    <a>Create</a>
                                    <ul>
                                    <li>
                                      <a href='groupHTML.php'>Create Group</a>    
                                      </li>                                   
                                      <li>
                                        <a href='{$sURL}addTestHTML.php'>Create Test</a>       
                                      </li>
                                      <li>
                                       <a href='{$sURL}addUserHTML.php'>Create User</a>     
                                      </li>
                                      <li>
                                        <a href='bulkUploadUser.php'>Bulk Upload</a>      
                                      </li>
                                    </ul>
                                  </li>
                                  <li>
                                    <a href='{$sURL}manageUser.php'>Manage User</a>  
                                  </li>
                                  <li>
                                    <a href='{$sURL}logout.php'>Logout </a>  
                                  </li>
                                  </ul>
                                  </div>";
                      }
                      ?>
				</div>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				<div id="idDivSignUp" class="header_034">Upload User Detail's</div>
            </div>
                   
            <form action="uploadxml.php" method="post" enctype="multipart/form-data" onsubmit="return Checkfiles();">
      				
      				<div id="idDivUserfullName" class="classDisplayExcelFileUpload classDisplayProfile">
	               <span id="idSpanFullName" class="classFN"> Select File :</span>
                   <span id="idSpanFNDb" class="classFN">
                   	<input type="file" name="excelFileUpload" id="idExcelFileUpload" onchange="return Checkfiles();" class="classFileUploadInput" />
                   </span>
      				</div>
				      
                <div id="idDivDob" class="classDisplayProfile ">
                 <span><input type="submit" name="submit" value="Upload User" class="classButton classSignUpButtenSize"/></span>
                 <?php
		             if($iMsg == 824)    
		                    {
		                        echo "<span id='idDivWarMsg' class='classDivTestDur classDivResumeText classWarningMsg classGreen'>
		                            User Added Successfully.
		                            </span>";
		                    }
		            ?>
                </div>
                <div id="idDivDownloadDemo" class="classDivDownloadDemo classDisplayProfile">
                    <span class="classSpanDisplayOp">View demo excel file</span>
                     <span class="classSpanDisplayOp"><a href="demoSampleExcel.php"><img src="../images/xlslogo.png" title="View Demo Excel File" id="idImgViewDemoExcel" class="classEditTestIcon"></a></span>
                </div>  
                </form>
                <div id="idDisplayExcelSheetEr" class="classDisplayExcelFileUpload classDisplayProfile">
                <?php 
                if($iMsg == 695)
                {
                  echo "<div class='classRed'>Error in Excel File.</div>";
                }
                if(isset($_SESSION['contErCount']))
                {
                  $iCountErConNo = $_SESSION['contErCount'];
                  
                  for($ii = 0;$ii <  $iCountErConNo; $ii++)
                  {
                    
                    echo "<div class='classRed'>". $_SESSION["ErConNo{$ii}"]. "</div>";
                  }
                }
                if(isset($_SESSION['emlErCount']))
                {
                  $iCountErEml = $_SESSION['emlErCount'];
                  for($ii = 0;$ii <  $iCountErEml; $ii++)
                    {
                      echo "<div id='idDivUserfullName' class='classRed'>".
                      $_SESSION["ErEml{$ii}"] ."</div>";
                    } 
                }
                ?>
              </div>
          </div>

              
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">

	<div id="id_footer">
    	
        <div class="section_w180">
        	<div class="header_05"><a href="#" >Help</a></div>
            <div class="section_w180_content"> 
            	<ul class="footer_menu_list">
                    <li></li>
                                
                </ul>
			</div>
        </div>
        
        <div class="section_w180">
        	<div class="header_05"><a href="#" >Feedback</a></div>
            <div class="section_w180_content">
                <ul class="footer_menu_list">
                    <li></li>          
                </ul>
            </div>
        </div>
                
        <div class="section_w180">
        	<div class="header_05"><a href="#" >About</a></div>
            <div class="section_w180_content"> 
                <ul class="footer_menu_list">
                    <li></li>      
                </ul>
			</div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169; <a href="#">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</html>