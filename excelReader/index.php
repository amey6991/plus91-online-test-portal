<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Upload User Detail's</title>
<link rel="icon" href="../images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="../images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="../images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="../css/style.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="../css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="../js/bootstrap.min.js"></script>
<script src='../js/ExcelSecur.js'></script>     <!-- javascript for file validation -->
<script type="text/javascript">
  function Checkfiles(val)  
  {
    document.getElementById('idBrowseFileName').value=val;   
    document.getElementById('idResponceMsg').innerHTML="Now, Upload User";
  }
</script>

<script>
/* start of use of ajax to Upload excel file and display error */

function showTest()
{
if (str=="")
  {
  document.getElementById("idDivDispTestSelectBox").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("idDivDispTestSelectBox").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("POST","uploadxml.php",true);
xmlhttp.send();
}
/* end of use of ajax to fetch test in a selected group */
</script>
</head>
<body>
<?php 

 	include("../classConnectQA.php");
 	include("../config/config.php");
		session_start();
		    if(isset($_SESSION['user_id']))
        {
          $iUserId = $_SESSION['user_id'];
        }
        else
        {
          $iUserId ="";
        }
        if(isset($_SESSION['ut']))
        {
          $ut=$_SESSION['ut'];
        }
        else
        {
          $ut="";
        }
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:{$sURL}index.php");
        }
        
        $sQueryUserInfo = "select * from user_details as a where a.login_id = {$iLoginId} limit 1";
        $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
        $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        if(isset($_GET['msg']))
        {
            $iMsg = $_GET['msg'];
        }
        else
        {
		    $iMsg ="";
        }
 ?>
<div id="id_header_wrapper">
  <div id="id_header">
    
  <div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="../images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
  </div>
        

		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<?php 
                      if($ut==0)
                      {
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                  <ul id='menu'>
                                  <li><a href='{$sURL}profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='{$sURL}profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='{$sURL}profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='{$sURL}changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                  </li>
                                  <li>
                                    <a href='{$sURL}manageTest.php'>Home</a>   
                                  </li>
                                  <li><a href='{$sURL}opportunity.php'>Opportunity</a>
                                    <ul>
                                    <li>
                                        <a href='{$sURL}opportunityHTML.php'>Create</a>       
                                    </li>
                                    <li>
                                        <a href='{$sURL}showOpportunity.php'>Manage</a>            
                                    </li>
                                    </ul>
                                </li>
                                  <li>
                                    <a>Create</a>
                                    <ul>
                                    <li>
                                      <a href='{$sURL}groupHTML.php'>Create Group</a>    
                                      </li>                                   
                                      <li>
                                        <a href='{$sURL}addTestHTML.php'>Create Test</a>       
                                      </li>
                                      <li>
                                       <a href='{$sURL}addUserHTML.php'>Create User</a>     
                                      </li>
                                      <li>
                                        <a href='index.php'>Bulk Upload</a>      
                                      </li>
                                    </ul>
                                  </li>
                                  <li>
                                    <a>Manage </a>  
                                      <ul>
                                      <li>
                                          <a href='{$sURL}manageGroup.php'>Manage Group</a>      
                                      </li>
                                        <li>
                                          <a href='{$sURL}manageUser.php'>Manage User</a>      
                                        </li>
                                        <li>
                                        <a href='{$sURL}viewAllotedTestHTML.php'>Assign Test</a>     
                                        </li>
                                    </ul>    
                                  </li>
                                  <li>
                                    <a href='{$sURL}logout.php'>Logout </a>  
                                  </li>
                                  </ul>
                                  </div>";
                      }
                      ?>
				</div>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
 <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
				<div id="idDivSignUp" class="header_0345">Upload User Detail's</div>
        <div class="classHorizHRSubHead"></div>
            </div>
                   
            <form action="uploadxml.php" method="post" enctype="multipart/form-data" onsubmit="return Checkfiles();">
      				
      				<div id="idDivUserfullName" class="classDisplayExcelFileUpload classDisplayProfile">
	               <span id="idSpanFullName" class="classFN"> Select File :</span>
                   <span id="idSpanFNDb" class="classFN">
                    <!--START of Style for using formatted File tag --> 
                    <style>
                          .idiv{display:inline-block;width:230px;height:30px;}
                          .iidiv1{opacity:0.8;width:230;height:30px;position:absolute;}
                          .iidiv2{opacity:0.0;width:230;height:30px;margin-top: 0px;}
                          .iinputFile{height:40px;width: 50px;margin-top: 0px;}
                          .iinputBut{height:30px;width: 50px;margin-top: -10px;}
                          .iinputTxt{overflow:hidden;font-size:14px;height:28px;width: 150px;border: 1px solid rgb(192, 192, 192);display:inline-block;margin-top: 0px;}
                          
                      </style>
                      <!--END of Style for using formatted File tag --> 
                                <!--Start HTML Code for using formatted File tag -->
                        <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                        <div class="idiv"> 
                          <!-- START1 Inner Container1 div which holds textbox and button. -->
                          <div class=iidiv1>  
                            <input type="text" class="iinputTxt classRounded_Radius_5" id="idBrowseFileName"> 
                            <input type="button"class="iinputBut but btn btn-primary" value="Browse">
                            </div>              <div class=iidiv2>  
                            <input type="file"class="iinputFile" id="idExcelFileUpload" name="excelFileUpload" onchange="return Checkfiles(this.value);">     
                            
                          </div>                          
                          <!-- END1 Inner Container1 div which holds textbox and button. -->
                          <!-- START2 Inner Container2 div which holds File. -->
                          
                          <!-- END2 Inner Container2 div which holds File. -->
                        </div>
                        <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                      <!--END HTML Code for using formatted File tag -->                      	
    <!-- <input type="file" name="excelFileUpload" id="idExcelFileUpload" onchange="return Checkfiles();" class="classFileUploadInput" /> -->
                    
                   </span>
      				</div>
				      
                <div id="idDivDob" class="classDisplayProfile ">
                 <span><input type="submit" name="submit" value="Upload User" onchange="showTest()" class="btn btn-primary classSignUpButtenSize"/></span>
                 <span id='idResponceMsg' class='classSmallFontForOpp' ></span>                    
                 <?php

		             if($iMsg == 824)    
		                    {
		                       /* echo "<span id='idDivWarMsg' class='classDivTestDur classDivResumeText classWarningMsg classGreen'>
		                            User Added Successfully.
		                            </span>";*/
		                    }
                  if($iMsg == 123)    
                        {
                            echo "<span id='idDivWarMsg' class='classDivTestDur classDivResumeText classWarningMsg classRed'>
                                Selected file is too big , You can upload file up to 2 MB.
                                </span>";
                        }

		            ?>
                </div>
                <div id="idDivDownloadDemo" class="classDivDownloadDemo">
                    <span class="classSpanDisplayOp">View demo excel file</span>
                     <span class="classSpanDisplayOp"><a href="demoSampleExcel.php"><img src="../images/xlslogo.png" title="View Demo Excel File" id="idImgViewDemoExcel" class="classEditTestIcon"></a></span>
                </div>  
                </form>
                <div id="idDisplayExcelSheetEr" class="classDisplayExcelFileUpload classDisplayProfile">
                <?php 
                /* 1] this code clears all the session in which excel file error are stored */
                if(isset($_SESSION['starttime']))
                {
                  $iSttm = $_SESSION['starttime'] ;
                  $_SESSION['nawtime'] =time();
                  $tNewTime = $_SESSION['nawtime'];
                  $iDiff = $tNewTime - $iSttm;
                  if($iDiff >= 3)
                  {
                      if(isset($_SESSION['countErFullName']))
                      {
                        $iCountErConNo = $_SESSION['countErFullName'];
                        
                        for($ii = 0;$ii <  $iCountErConNo; $ii++)
                        {
                          if(isset($_SESSION["ErFullName{$ii}"]))
                          {
                            unset($_SESSION["ErFullName{$ii}"]);
                          }
                          
                        }
                        unset($_SESSION['countErFullName']);
                      }
                      if(isset($_SESSION['contErCount']))
                      {
                        $iCountErConNo = $_SESSION['contErCount'];
                        
                        for($ii = 0;$ii <  $iCountErConNo; $ii++)
                        {
                          unset($_SESSION["ErConNo{$ii}"]);
                        }
                        unset($_SESSION['contErCount']);
                      }
                      if(isset($_SESSION['emlErCount']))
                      {
                        $iCountErEml = $_SESSION['emlErCount'];
                        for($ii = 0;$ii <  $iCountErEml; $ii++)
                          {
                            if(isset($_SESSION["ErEml{$ii}"]))
                            {
                              unset($_SESSION["ErEml{$ii}"]); 
                            }
                          } 
                          unset($_SESSION['emlErCount']);
                      }
                      if(isset($_SESSION['CountRegEmlEr']))
                        {
                        $iCountErRegEml = $_SESSION['CountRegEmlEr'];
                        for($ii = 0;$ii <  $iCountErRegEml; $ii++)
                          {
                            if(isset($_SESSION["ErRegEml{$ii}"]))
                            {unset($_SESSION["ErRegEml{$ii}"]);} 
                          } 
                          unset($_SESSION['CountRegEmlEr']);
                      }
                      if(isset($_SESSION["ErRegDpEml"]))
                      {
                        if(isset($_SESSION["ErRegDpEml"]))
                        {unset($_SESSION["ErRegDpEml"]);}
                      }
                    if(isset($_SESSION['countRegCon']))
                    {
                      $iCountErConNo = $_SESSION['countRegCon'];
                      
                      for($ii = 0;$ii <  $iCountErConNo; $ii++)
                      {
                        unset($_SESSION["ErRegCon{$ii}"]);
                      }
                      unset($_SESSION['countRegCon']);
                    }
                      if(isset($_SESSION['countErinPass']))
                      {
                        $iCountErConNo = $_SESSION['countErinPass'];
                        
                        for($ii = 0;$ii <  $iCountErConNo; $ii++)
                        {
                          if(isset($_SESSION["ErInPass{$ii}"]))
                          {
                            unset($_SESSION["ErInPass{$ii}"]);
                          }
                          
                        }
                        unset($_SESSION['countErinPass']);
                      }
                      
                  }
                }
                else
                {
                  
                }
                /* 1] end */
                if(isset($_SESSION['numberRowsAdded']))
                {
                  echo "<div class='classGreen'>".$_SESSION['numberRowsAdded']." - User added successfully.</div>";
                  unset($_SESSION['numberRowsAdded']);
                }
                if((isset($_SESSION['contErCount'])) || (isset($_SESSION['emlErCount'])) || (isset($_SESSION['CountRegEmlEr'])) || (isset($_SESSION["ErRegDpEml"])))
                {
                    if($iMsg == 695 || $iMsg == 858)
                    {
                      echo "<div class='classRed'>Error in Excel File.</div>";
                    }
                    if(isset($_SESSION['countErFullName']))
                    {
                      $iCountErConNo = $_SESSION['countErFullName'];
                      
                      for($ii = 0;$ii <  $iCountErConNo; $ii++)
                      {
                        if(isset($_SESSION["ErFullName{$ii}"]))
                        {
                          echo "<div class='classRed'>". $_SESSION["ErFullName{$ii}"]. "</div>";
                        }
                        
                      }
                    }
                    if(isset($_SESSION['emlErCount']))
                    {
                      $iCountErEml = $_SESSION['emlErCount'];
                      for($ii = 0;$ii <  $iCountErEml; $ii++)
                        {
                          if(isset($_SESSION["ErEml{$ii}"]))
                          {
                            echo "<div id='idDivUserfullName' class='classRed'>".$_SESSION["ErEml{$ii}"]."</div>";
                          }
                        } 
                    }
                    if(isset($_SESSION['CountRegEmlEr']))
                      {
                      $iCountErRegEml = $_SESSION['CountRegEmlEr'];
                      for($ii = 0;$ii <  $iCountErRegEml; $ii++)
                        {
                          if(isset($_SESSION["ErRegEml{$ii}"]))
                          {
                            if(isset($_SESSION["ErRegEml{$ii}"]))
                            {
                              echo "<div id='idDivUserfullName' class='classRed'>".$_SESSION["ErRegEml{$ii}"]."</div>";
                            }
                          }
                        } 
                    }
                    
                    if(isset($_SESSION['contErCount']))
                    {
                      $iCountErConNo = $_SESSION['contErCount'];
                      
                      for($ii = 0;$ii <  $iCountErConNo; $ii++)
                      {
                        if(isset($_SESSION["ErConNo{$ii}"]))
                        {
                          echo "<div class='classRed'>". $_SESSION["ErConNo{$ii}"] . "</div>";
                        }
                        
                      }
                    }
                    if(isset($_SESSION['countRegCon']))
                    {
                      $iCountErConNo = $_SESSION['countRegCon'];
                      
                      for($ii = 0;$ii <  $iCountErConNo; $ii++)
                      {
                        if(isset($_SESSION["ErRegCon{$ii}"]))
                        {
                          echo "<div class='classRed'>". $_SESSION["ErRegCon{$ii}"] . "</div>";
                        }
                        
                      }
                    }
                    if(isset($_SESSION['countErinPass']))
                    {
                      $iCountErConNo = $_SESSION['countErinPass'];
                      
                      for($ii = 0;$ii <  $iCountErConNo; $ii++)
                      {
                        if(isset($_SESSION["ErInPass{$ii}"]))
                        {
                          echo "<div class='classRed'>". $_SESSION["ErInPass{$ii}"]. "</div>";
                        }
                        
                      }
                    }
                    if(isset($_SESSION["ErRegDpEml"]))
                    {
                      if(isset($_SESSION["ErRegDpEml"]))
                      {
                        if(isset($_SESSION["ErRegDpEml"]))
                        {
                            echo "<div id='idDivUserfullName' class='classRed'>".
                                $_SESSION["ErRegDpEml"] ."</div>";
                        }
                      }
                    }
                 } 
                ?>
                <div id="idDivDispTestSelectBox" class="classDivTstSelectBox">
                </div>
              </div>
          </div>     
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="<?php echo $sURL; ?>help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="<?php echo $sURL; ?>feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="<?php echo $sURL; ?>aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>