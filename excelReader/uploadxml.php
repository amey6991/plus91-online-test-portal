<?php
	include("../config/config.php");
	ini_set('display_errors', 1); error_reporting(E_ALL);

	if (is_uploaded_file($_FILES["excelFileUpload"]["tmp_name"])) 
		{

		}
		else
		{
			header("Location:index.php?msg=123");
	    	exit();
		}
		
	 if ($_FILES["excelFileUpload"]["error"] > 0)
	    {
	    	header("Location:index.php");
	    	exit();
	    	
	    }
	  else
	    {
	    echo $iFileSize = ($_FILES["excelFileUpload"]["size"]/1024);

	    if($iFileSize >= 2000)
		{

			header("Location:index.php?msg=123");
	    	exit();

		}
		
	    $sFileName = $_FILES["excelFileUpload"]["name"];
	    move_uploaded_file($_FILES["excelFileUpload"]["tmp_name"],
	    "excelFile/" . $_FILES["excelFileUpload"]["name"]);
	    $sFileToDelete = "excelFile/" . $_FILES["excelFileUpload"]["name"];
	    }
		session_start();
		$_SESSION['starttime'] = time();	
		if(isset($_SESSION['excelError'])) 
		{
			unset($_SESSION['excelError']);
		}
		if(isset($_SESSION['emlErCount']))	/* This code unset all the session variable those stores errors in excel file*/
		{
			$iCountErEml = $_SESSION['emlErCount'];
	          for($ii = 0;$ii <  $iCountErEml; $ii++)
	            {
	              unset($_SESSION["ErEml{$ii}"]); 
	            } 

			unset($_SESSION['emlErCount']);
		}
		if(isset($_SESSION['contErCount']))
		{
			
			$iCountErConNo = $_SESSION['contErCount'];
                  
	          for($ii = 0;$ii <  $iCountErConNo; $ii++)
	          {
	            unset($_SESSION["ErConNo{$ii}"]);
	          }
	          unset($_SESSION['contErCount']);
		}

		include ("../classConnectQA.php");
		include ("../excelReader/reader.php");
    	$excel = new Spreadsheet_Excel_Reader();
    	$excel->read("excelFile/{$sFileName}");   /* this code read the excel file */
			$iX=3;
			$iCountEr = 0;
			$iCountErEml = 0;

			$iCountErConNo = 0;
			$iCountRegEml = 0;
			$iContNoRegDB = 0;
			$iCountErInFullName = 0;
			$iCountErInPass =0;
			$sErrorInPassword ="";
			$sErrorInConNo = "";
			$sErrorInFullName = "";
			$sErrorInEmail = "";
			$iW = 0;
			$iZ = 0;
			$objPHPExcel = "";
			
			while($iX<=$excel->sheets[0]['numRows']) /* This code reads sheet first in excel file */
			 {
				$sFullNname = isset($excel->sheets[0]['cells'][$iX][1]) ? $excel->sheets[0]['cells'][$iX][1] : '';
				$sEmail = isset($excel->sheets[0]['cells'][$iX][2]) ? $excel->sheets[0]['cells'][$iX][2] : '';
				$sConNo = isset($excel->sheets[0]['cells'][$iX][3]) ? $excel->sheets[0]['cells'][$iX][3] : '';
				$sPassword = isset($excel->sheets[0]['cells'][$iX][4]) ? $excel->sheets[0]['cells'][$iX][4] : '';
				$sUserStatus = isset($excel->sheets[0]['cells'][$iX][5]) ? $excel->sheets[0]['cells'][$iX][5] : '';

				$iX++;
				
				$sFilterFullName = filter_var($sFullNname ,FILTER_SANITIZE_STRING, FILTER_FLAG_STRIP_LOW | FILTER_FLAG_STRIP_HIGH);
								
				$iSanConno = filter_var($sConNo, FILTER_SANITIZE_NUMBER_INT);
				$sTrmCon = trim($iSanConno);
				$iContactLength = strlen($sTrmCon);
				$sErrorInConNoD =Null;
				$sErrorInEmailD =Null;
				
				if($sFilterFullName =="")
				{

				}
				else
				{	   /* eregi - Regular expression match - Searches a string for matches to the regular expression given in pattern in a case-sensitive way.*/
					if (@eregi('^[A-Z a-z]{3,100}$',$sFilterFullName))
					{
						//$valid_name = $sFilterFullName;
						//unset($_SESSION['ErConNo']);
					}
					else
					{
						$iWx = $iX - 1;
						$sErrorInFullName = $sFilterFullName ." - Invalid Name at row [$iWx] and column [1]";
						$_SESSION["ErFullName{$iCountErInFullName}"] = $sErrorInFullName;
						$iCountErInFullName++;
						$iCountEr++;
					}
				}

				if($sConNo =="")
				{

				}
				else
				{
					if (@eregi('^[0-9]{10,10}$',$sConNo))
					{
				        
			           	$sSelectContNoFromDB ="select count(user_contact) from user_details where user_contact ='$sConNo' limit 1";
						$aResult_SelectContNoFrmDB = $mysqli->query($sSelectContNoFromDB);
						$aRowForConNoFrmDb = $aResult_SelectContNoFrmDB->fetch_row();

						$iContactNoDB = $aRowForConNoFrmDb[0];
						if($iContactNoDB >= 1 )
						{
							$iContNo = $iX - 1;
							$sErrorInContNo = $sConNo. " - This contact number is allready registered - row [$iContNo] and column [2]";
			 
							$_SESSION["ErRegCon{$iContNoRegDB}"] = $sErrorInContNo;
							$iContNoRegDB++;
							$iCountEr++;
						}

			           	unset($_SESSION['ErEml{$iX}']); 
					}
					else
					{
						$iW = $iX - 1;
						$sErrorInConNoD = $sConNo ." - Invalid phone no. at row [$iW] and column [3]";
						$_SESSION["ErConNo{$iCountErConNo}"] = $sErrorInConNoD;
						$iCountErConNo++;
						$iCountEr++;
						
					}
				}	
				// Password min 6 char max 12 char
				if($sPassword =="")
				{
					
				}
				else
				{
					if (@eregi('^[A-Za-z0-9!@#$%^&*()_]{6,12}$',$sPassword))
					{
						
					}
					else
					{
							$iWp = $iX - 1;
							$sErrorInPassword = $sPassword ." - Invalid password at row [$iWp] and column [4]";
							$_SESSION["ErInPass{$iCountErInPass}"] = $sErrorInPassword;
							$iCountErInPass++;
							$iCountEr++;
					}
				}	
				if($sEmail == "")
				{

				}
				else
				{
					if (@eregi('^[a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.([a-zA-Z]{2,4})$', $sEmail))
					{
						$sFilterEmail = filter_var($sEmail, FILTER_SANITIZE_EMAIL);  
				        $sErrorInEmail ="";
				        
			           	$sSelectEmailFromDB ="select count(user_name) from login where user_name ='$sEmail' limit 1";
						$aResult_SelectEmlFrmDB = $mysqli->query($sSelectEmailFromDB);
						$aRowForEmlFrmDb = $aResult_SelectEmlFrmDB->fetch_row();

						$iLoginEmail = $aRowForEmlFrmDb[0];
						if($iLoginEmail >= 1 )
						{
							$iR = $iX - 1;
							$sErrorInEmailD = $sFilterEmail. " - This email address is allready registered - row [$iR] and column [2]";
			 
							$_SESSION["ErRegEml{$iCountRegEml}"] = $sErrorInEmailD;
							$iCountRegEml++;
							$iCountEr++;
						}

			           	unset($_SESSION['ErEml{$iX}']); 	               
					        
					}
					else
					{
						$iZ = $iX - 1;
			        	$sErrorInEmailD = $sEmail. " - Invalid email address at row [$iZ] and column [2]";
			        	$_SESSION["ErEml{$iCountErEml}"] = $sErrorInEmailD;
			        	$iCountErEml++;
			        	$iCountEr++;
					}

					 
			        
			    }		 

			}

			$_SESSION['emlErCount']=$iCountErEml;
			$_SESSION['contErCount'] = $iCountErConNo;
			$_SESSION['CountRegEmlEr'] = $iCountRegEml;
			$_SESSION['countRegCon'] = $iContNoRegDB;
			$_SESSION['countErFullName'] = $iCountErInFullName;
			$_SESSION['countErinPass'] = $iCountErInPass;
			/* end of excel file read */
			/* encrypt password using base 64 encode */
			function fnEncrypt($sValue, $sSecretKey)
			{
			    return rtrim(
			        base64_encode(					
			            mcrypt_encrypt(
			                MCRYPT_RIJNDAEL_256,
			                $sSecretKey, $sValue, 
			                MCRYPT_MODE_ECB, 
			                mcrypt_create_iv(
			                    mcrypt_get_iv_size(
			                        MCRYPT_RIJNDAEL_256, 
			                        MCRYPT_MODE_ECB
			                    ), 
			                    MCRYPT_RAND)
			                )
			            )
			        );
			}
			
			$sKey = "quizapplication";
			/* This code add all excel data to database */
			
			if($iCountEr == 0)
			{
				$iX=3;
				while($iX<=$excel->sheets[0]['numRows']) /* This code reads sheet first in excel file */
				 {
					$sFullNname = isset($excel->sheets[0]['cells'][$iX][1]) ? $excel->sheets[0]['cells'][$iX][1] : '';
					$sEmail = isset($excel->sheets[0]['cells'][$iX][2]) ? $excel->sheets[0]['cells'][$iX][2] : '';
					$sConNo = isset($excel->sheets[0]['cells'][$iX][3]) ? $excel->sheets[0]['cells'][$iX][3] : '';
					$sPassword = isset($excel->sheets[0]['cells'][$iX][4]) ? $excel->sheets[0]['cells'][$iX][4] : '';
					$sUserStatus = 1;//isset($excel->sheets[0]['cells'][$iX][5]) ? $excel->sheets[0]['cells'][$iX][5] : '';
					$sUserType = 1;
					$sUniqueCodeForUser = md5($sFullNname . $sEmail . $sConNo);
					
					$sFilterEmail = filter_var($sEmail, FILTER_SANITIZE_EMAIL);  
			        $sErrorInEmail ="";
			        $iCountRegDPEml =0;

			        if (filter_var($sFilterEmail, FILTER_VALIDATE_EMAIL)) 
			        {  
			           $sSelectEmailFromDB ="select user_name from login where user_name ='$sFilterEmail' limit 1";
						$aResult_SelectEmlFrmDB = $mysqli->query($sSelectEmailFromDB);
						$aRowForEmlFrmDb = $aResult_SelectEmlFrmDB->fetch_row();
						$_SESSION['numberRowsAdded'] = $iX - 2;
						$iLoginEmail = $aRowForEmlFrmDb[0];
						if($iLoginEmail == $sFilterEmail)
						{
							$iR = $iX - 1;
							$sErrorInEmailD = $sFilterEmail. " - This email address is allready registered - row [$iR] and column [2]";
			 
							$_SESSION["ErRegDpEml"] = $sErrorInEmailD;
							$iCountRegDPEml++;
							$iCountEr++;

							$iMsg = 858;
							header("Location:index.php?msg={$iMsg}");
							exit();

						}

			           	unset($_SESSION['ErRegDpEml{$iX}']);
			           
			        }

		        	if($sFullNname != "" && $sFilterEmail != "" && $sPassword != "")
		        	{
		        		
						
						$sPasswordEncrypted = fnEncrypt($sPassword, $sKey);

						$sInsLogin ="INSERT INTO login (user_name , password , type ) 
										VALUES ('$sEmail','$sPasswordEncrypted','$sUserType')";
						$aResult_InsLogin = $mysqli->query($sInsLogin);/* This query add data in login table */

						$sSelectLoginId ="select login_id from login where user_name ='$sEmail' limit 1";
						$aResult_SelectLoginID = $mysqli->query($sSelectLoginId);
						$aRowForLoginID = $aResult_SelectLoginID->fetch_row();
						$iLoginID = $aRowForLoginID[0];	/* This query select loginid from login table */

						$sInsQuery ="INSERT INTO user_details (login_id,user_full_name , user_contact , user_email,user_status,user_doreg,varification_code) 
										VALUES ($iLoginID , '$sFullNname' , '$sConNo' , '$sEmail' , '{$sUserStatus}', NOW() , '$sUniqueCodeForUser') ";
						$aResult_insert = $mysqli->query($sInsQuery); // This query Save user details to user details table
						
					}
					$iX++;
				}

				$iMsg = 824;
				header("Location:index.php?msg={$iMsg}");
								
			}
			else
			{
				$iMsg = 695;
				header("Location:index.php?msg={$iMsg}");

			}
			
			/* end of adding data from excel file */


			if(unlink($sFileToDelete))
    		{

    		}
    		else
    		{
    			unlink($sFileToDelete);
    		}
    		
    		
	?>