  <?php
        session_start();
        include ('classConnectQA.php'); 

        if(isset($_SESSION['lid']))     // This is Use to check a Session
        {
             $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }
        if(isset($_SESSION['ut']))  
        {
            $ut=$_SESSION['ut'];
        }
        else
        {
             $ut="";
        }   
        if(isset($_GET['oppid'])) 
        {
            $iOppId=$_GET['oppid'];
        }
        else
        {
            $iOppId=Null;   
        } 
        if(isset($_GET['uid'])) 
        {
            $iUserId = $_GET['uid'];
        }
        else
        {
            $iUserId=null;
        }
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Apply via. Opportunity</title>
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/jquery-1.8.2.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<meta name="description" content="online quiz application" />

<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
<script type="text/javascript" src="js/ajaxfileupload.js"></script>
  <script type="text/javascript">
  function ajaxFileUpload(val)
  {
    $("#loading")
    .ajaxStart(function(){
      $(this).show();
    })
    .ajaxComplete(function(){
      $(this).hide();
    });

    $.ajaxFileUpload
    (
      {
        url:"uploadResume.php?usid=<?php echo $iUserId; ?>",
        secureuri:false,
        fileElementId:'fileToUpload',
        dataType: 'json',
        data:{name:'logan', id:'id'},
        success: function (data, status)
        {
          if(typeof(data.error) != 'undefined')
          {
            if(data.error != '')
            {
              alert(data.error);
            }else
            {
              alert("File Uploaded Successfully.");
               document.getElementById('idBrowseFileName').value=val;
              document.getElementById('idResponceMsg').innerHTML="Resume uploaded successfully";
            }
          }
        },
        error: function (data, status, e)
        {
          alert(e);
        }
      }
    )
    
    return false;

  }
  </script>
 <!-- end of ajax-->
</head>
<body>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
                    <?php                    
                     $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                        from login as a , user_details as b
                                        where a.login_id = b.login_id 
                                        AND a.login_id  = '$iLoginId' limit 1";
                    $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                    $aRowForUserInfo = $iResultForUserInfo->fetch_row();
                    
                   echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                <ul id='menu'>
                                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href='manageTest.php'>Home</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Opportunity</a>
                                </li>
                                <li>
                                    <a href='displayStudentResult.php'>Result</a>       
                                </li>
                                <li>
                                    <a href='logout.php'>Logout </a>    
                                </li>
                                </ul>
                                </div>";
                    ?>
                </div>
			</div>  	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
    <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
if(isset($_GET['msg']))
{
    $iMsg = $_GET['msg'];
    echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
        if($iMsg == 123)
        {
            echo "<div class=classMsg >Application sent.</div>";
        }
    echo "</div>";
}
?>
<?php   
    /* Query To retrive opprotunity name, user name, Resume upload or not
    -----------------------------------------------------------------------*/
    $sAppOpportunityQuery="select test_id from opportunity_test where opp_id='{$iOppId}' and Opp_test_status=1";    
    $bAppOpportunityResult=$mysqli->query($sAppOpportunityQuery);
    $aAppOppTest= array(null);          // Using array to store the Test_id.
    $ii=0;                              // Using as a counter
    if($bAppOpportunityResult==true)
    {
        while($aAppOppTestRow=$bAppOpportunityResult->fetch_row())
        {
            $aAppOppTest[$ii]=$aAppOppTestRow[0];               // To get the test_id of all test added with opportunity.
            $ii++;
        }

    }
    else
    {
        $aAppOppTest=null;          // Not a single test available.
    } 
?>
<div id="id_content_wrapper">
	<div id="id_content">   
		<div id="idDivMiddleBody" class="classDivMiddleBody">
          <div id="idDivApplyOpp" class="classDiv">             			
                    <div id='idDivOppName' class='header_0345'>Opportunity :
                       <?php
                            
                            /* Query use to fetch the user name
                            ----------------------------------------------*/
                            $sUserNameQuery="select opp_name from opportunity_table where opp_id='{$iOppId}' and opp_status=1";
                           
                            $bUserNameResult = $mysqli->query($sUserNameQuery);
                            if($bUserNameResult==true)
                            {
                                $aUserName = $bUserNameResult -> fetch_row();
                                echo " {$aUserName[0]}";                            // Opportunity Name.
                            }
                            else
                            {
                                // Opportunity Name not available because query not executed.
                            }                          
                        ?>
                     </div>
                <div class="classHorizHRSubHead"></div>                              				
                <br/>
                 <br/>
                  <br/>
                <!-- Form to update or upload user resume --> 

                <div id="idDivRD" class="classDivAddTest classChangePassword">                    
                  <form class="classUpdateUserResume" id="idUpdateUserResume" name="UpdateUserResumeForm" enctype='multipart/form-data' method="POST"  action="">
                    <div id="idDivResumeOption" class="classDivResumeText">                        
                        <span id="idSpanTabRD" class="classSpanTabIP">
                            <?php
                                $sQueryForMarks = '';                           
                                $sQueryCheckTestAttempt = "select distinct user_id from user_resume where resume_status = 1 AND user_id = {$iUserId} limit 1";                            
                                $bResCheckTestAttempt = $mysqli->query($sQueryCheckTestAttempt);    /* This query check user id and test id present in database or not */
                                $aTestAttempt = $bResCheckTestAttempt->fetch_row();
                                 
                                 if($aTestAttempt[0] == $iUserId)
                                    {
                                        echo "<div id='idDivResumeText' class='classDivResumeText classFont'>
                                         Update your resume.                                      
                                        </div>
                                        <div id='idDivForFileUpload' class='classDivForFileUpload classDivResumeText header_054'>";
                                     ?>  
                                        <!--Start Style for using formatted File tag --> 
                                        <style>
                                            .idiv{display:inline-block;width:230px;height:30px;}
                                            .iidiv1{opacity:0.8;width:230;height:30px;position:absolute;}
                                            .iidiv2{opacity:0.0;width:230;height:30px;margin-top: 0px;}
                                            .iinputFile{height:40px;width: 50px;margin-top: 0px;}
                                            .iinputBut{height:30px;width: 50px;margin-top: -10px;}
                                            .iinputTxt{overflow:hidden;font-size:14px;height:28px;width: 150px;border: 1px solid rgb(192, 192, 192);display:inline-block;margin-top: 0px;}
                                            
                                        </style>
                                        <!--END of Style for using formatted File tag --> 
                                                  <!--Start HTML Code for using formatted File tag -->
                                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                                          <div class="idiv"> 
                                            <!-- START1 Inner Container1 div which holds textbox and button. -->
                                            <div class=iidiv1>  
                                              <input type="text" class="iinputTxt classRounded_Radius_5" id="idBrowseFileName" />
                                              <input type="button"class="iinputBut but btn btn-primary" value="Browse">
                                              </div>              <div class=iidiv2>  
                                              <input type="file"class="iinputFile" id="fileToUpload" name="fileToUpload" onchange="return ajaxFileUpload(this.value);">     
                                            </div>

                                            
                                            <!-- END1 Inner Container1 div which holds textbox and button. -->
                                            <!-- START2 Inner Container2 div which holds File. -->
                                            
                                            <!-- END2 Inner Container2 div which holds File. -->
                                          </div>
                                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                                        <!--END HTML Code for using formatted File tag -->                                         
                                               
                        <!--<span><input id='fileToUpload' type='file' size='45' name='fileToUpload' class='' onchange='return ajaxFileUpload();'></span>-->
                                        
                                     <?php       
                                        echo "<span><img id='loading' src='images/loading.gif' style='display:none;'></span>
                                        </div>";                                
                                    }
                                    else
                                    {
                                        echo "<div id='idDivResumeText' class='classDivResumeText classFont'>
                                         Upload your resume.
                                         </div>
                                        <div id='idDivForFileUpload' class='classDivForFileUpload classDivResumeText header_054'>";
                                     ?>   
                    <!-- <span><input id='fileToUpload' type='file' size='45' name='fileToUpload' class='' onchange='return ajaxFileUpload();'></span> -->
                                       <!--Start Style for using formatted File tag --> 
                                        <style>
                                            .idiv{display:inline-block;width:230px;height:30px;}
                                            .iidiv1{opacity:0.8;width:230;height:30px;position:absolute;}
                                            .iidiv2{opacity:0.0;width:230;height:30px;margin-top: 0px;}
                                            .iinputFile{height:40px;width: 50px;margin-top: 0px;}
                                            .iinputBut{height:30px;width: 50px;margin-top: -10px;}
                                            .iinputTxt{overflow:hidden;font-size:14px;height:28px;width: 150px;border: 1px solid rgb(192, 192, 192);display:inline-block;margin-top: 0px;}
                                            
                                        </style>
                                        <!--END of Style for using formatted File tag --> 
                                                  <!--Start HTML Code for using formatted File tag -->
                                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                                          <div class="idiv"> 
                                            <!-- START1 Inner Container1 div which holds textbox and button. -->
                                            <div class=iidiv1>  
                                              <input type="text" class="iinputTxt classRounded_Radius_5" id="idBrowseFileName" />
                                              <input type="button"class="iinputBut but btn btn-primary" value="Browse">
                                              </div>              <div class=iidiv2>  
                                              <input type="file"class="iinputFile" id="fileToUpload" name="fileToUpload" onchange="return ajaxFileUpload(this.value);">     
                                            </div>

                                            
                                            <!-- END1 Inner Container1 div which holds textbox and button. -->
                                            <!-- START2 Inner Container2 div which holds File. -->
                                            
                                            <!-- END2 Inner Container2 div which holds File. -->
                                          </div>
                                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                                        <!--END HTML Code for using formatted File tag -->                         
                                    <?php
                                        echo "<span>
                                                <img id='loading' src='images/loading.gif' style='display:none;'></span>
                                        </div>";
                                    }    
                                echo "<span id='idResponceMsg' class='classSmallFontForOpp' ></span>";
                                echo "<span id='idSpanTabResumeNote' class='classSpanTabResumeNote'>(Maximum file size 2 MB)</span>";
                               
                            ?>
                        </span>
                    </div>

                   </form> 
                </div>
                <br/>
                <br/>
                <br/>
                 <div id="idDivAttemptedTest" class="classDivAddTest classChangePassword">  
                  <div id="idAttemptedTabOppTestIP">                                        
                    <caption class='classDivTestTxt'>Test required for Opportunity</caption>
                      <?php      
                            echo "<table id='idOppTestTable' class='table' width='700px'>";                                                        
                            echo "<thead><tr><th >Sr. No.</th> 
                                              <th>Test</th>
                                              <th>Date Of Attempt</th>
                                              <th>Score</th>";   
                            echo "</tr></thead>";
                            $sOppTestQuery="select test_id from opportunity_test where opp_id={$iOppId} and Opp_test_status=1";                            
                            $bOppTestResult=$mysqli->query($sOppTestQuery);                                                     
                            $icounter=1;
                            $sApplyFlag=null;
                            if($bOppTestResult==true)
                            {   
                                while($aOppTestRow=$bOppTestResult->fetch_row())
                                { 
                                    $sTestNameQuery="select test_name from test_detail where test_id={$aOppTestRow[0]}";
                                    $bsUserOppTestNameResult=$mysqli->query($sTestNameQuery);
                                    $aTestNameRow=$bsUserOppTestNameResult->fetch_row();
                                    
                                    $sUserOppTestQuery="select b.sb_doa, b.sb_marks_obt, b.sb_marks_out_of
                                                    from test_detail as a, score_board_table as b, opportunity_test as c
                                                    where a.test_id=b.test_id and b.test_id=c.test_id and 
                                                    c.opp_id={$iOppId} and b.test_id={$aOppTestRow[0]} and user_id={$iUserId} ";                                         
                                    $bsUserOppTestResult=$mysqli->query($sUserOppTestQuery);
                                    $aRow=$bsUserOppTestResult->fetch_row();                                   
                                    
                                    if($aRow[1] == Null)
                                    {
                                        
                                        $sAllotQuery="select b.sb_marks_obt, b.sb_doa from opportunity_test as a, score_board_table as b
                                                     where a.opp_id={$iOppId} and b.user_id={$iUserId} and a.opp_test_status=1 and 
                                                     a.test_id=b.test_id and a.test_id={$aOppTestRow[0]}";
                                       
                                        $bAllotResult=$mysqli->query($sAllotQuery);
                                        $aAllotRow=$bAllotResult->fetch_row();                                        
                                        if($aAllotRow[1] == null && $aRow[0] == Null)
                                            {
                                                $sScore="Not Attempted";   
                                                $sApplyFlag="Pending";    
                                            }
                                            else
                                            {
                                                $sScore="Pending";       
                                            }                                       
                                        
                                    }
                                    else
                                    {
                                        $sScore=$aRow[1]."/".$aRow[2];                                                                                 

                                    }
                                    echo "<tr>";
                                    echo "<td>" . $icounter . "</td>";
                                    echo "<td>" . $aTestNameRow[0]. "</td>"; // Test Name
                                    echo "<td>" . $aRow[0] . "</td>"; // Test Attempt Date                                  
                                    echo "<td>" . $sScore . "</td>"; // Test Score.
                                    echo "</tr>";                               
                                    $icounter++;
                                } 
                                if($icounter==1)        // This stop user from applying for an Opportunity that donot contain any test.
                                {
                                    $sApplyFlag="Pending";       
                                }
                            }
                            else
                            {
                                $sApplyFlag="Pending";
                            }
                       
                                 
                    echo "</table><hr/></div></div>";
                  ?>  
                <br/>

                <div id="idDivBut" class="classDivAddTest">
                    <span id="idSpanTabMob" class="classSpanAddTestDesc"></span>
                    <span id="idSpanTabMob" class="classSpanAddTestDesc"></span>
                    <span id="idSpanTabMobIP" class=" classNoDecoration">
                    <!--<span id="idSpanTabMobIP" class="classSpanTabIP classNoDecoration">-->
                     
                        <!-- Submit a form to apply through opportunity. -->
                        <?php 
                                              
                            if($sApplyFlag=="Pending" && $sScore="Not Attempted")
                            {
                                                              
                                echo "<div class='classMsgOnApplyOpp'><div class='alert alert-block' style='font-size:20px;'>    
                                        Notification! <br/>Attempt all mandatory test for applying an opportunity.
                                        </div>";
                                //echo "<span id='idSpanApplyNow' class='classSpanApplyNow'><a title='Take All Mandatory Test to Apply'><input type='button' value='Apply' name='oppapply' class='btn btn-primary classAddTestButtonSize' id='idSubApplyOpp'/></a></span>";
                            }   
                            else
                            {
                                $sApplyQuery="select * from apply_opportunity where opp_id={$iOppId} and user_id={$iUserId} and ap_opp_status=1";
                                $bApplyResult=$mysqli->query($sApplyQuery);
                                $aApplyRow=$bApplyResult->fetch_row();
                                if($aApplyRow == null)  // This is use to check, wether ueer applied before or not.
                                {
                                    
                                     echo "<div class='classMsgOnApplyOpp'><div class='alert alert-block' style='font-size:20px;'>    
                                        Notification!<br/> Now you can apply for an opportunity.
                                        </div></div>";
                                        echo "<span id='idSpanApplyNow' class='classSpanApplyNow'><a href='opportunityMail.php?oppid={$iOppId}' title='Apply'><input type='button' value='Apply' name='oppapply' class='btn btn-primary classAddTestButtonSize' id='idSubApplyOpp'/></a></span>";
                                }
                                else
                                {
                                    
                                    echo "<div class='classMsgOnApplyOpp'><div class='alert alert-block' style='font-size:20px;'>    
                                        Notification! <br/> Once you already applied for an opportunity, now you can't.
                                        </div></div>";
                                    //echo "<span id='idSpanApplyNow' class='classSpanApplyNow'><a href='' title='Already applied'><input type='button' value='Apply' name='oppapply' class='btn btn-primary classAddTestButtonSize' id='idSubApplyOpp'/><a/></span>";
                                }
                                                           
                            }
                            
                        ?>
                    </span>
               </div> 
				
            </div>
           </div> 
        </div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>
	
					