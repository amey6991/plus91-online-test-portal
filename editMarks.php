<?php

			/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@gid: is use for GroupId
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status 
			
			*/
		session_start();
			
		include ('classConnectQA.php');
		
		if(isset($_SESSION['lid']))		// This is Use to check a Session
		{
			$iLoginId = $_SESSION['lid'];
		}
		else
		{
			header("location:index.php");
		}
		if(isset($_GET['id'])) //Test id
		{
			$iTestId= $_GET['id'];
		}
		else
		{
			$iTestId= Null;
		}
		if(isset($_GET['usid'])) // User id
		{
			$iUserId = $_GET['usid'];
		}
		else
		{
			$iUserId = Null;	
		}		
		if(isset($_POST['totalDesQue']))
		{
			$iTotalDesQue=$_POST['totalDesQue'];			
		}
		else
		{
			$iTotalDesQue=0;	
		}
			$aDesQueMarks=array($iTotalDesQue);
			$aDesQueId=array($iTotalDesQue);
			for($ii=0;$ii<$iTotalDesQue;$ii++)
			{
				if(isset($_POST['desAns'.$ii]))
				{
					$aDesQueMarks[$ii]=$_POST['desAns'.$ii];
				}
				else
				{
					$aDesQueMarks[$ii] = 0;
				}
				if(isset($_POST['desQueId'.$ii]))
				{
					$aDesQueId[$ii]=$_POST['desQueId'.$ii];
				}
				else
				{
					$aDesQueId[$ii] = 0;
				}				
			}			
			
			/*@ when connetion failed.*/
			if ($mysqli->errno) 									
			{					
				header("location: editResult.php?usid={$iUserId}&id={$iTestId}&msg=-1");					
			}
			/*@ when connetion Established.*/
			else
			{	
				$ic=0;	
				for($ii=0;$ii<$iTotalDesQue;$ii++)
				{
					$sQueryDesMarksAdd="select des_marks from des_marks_table where user_id={$iUserId} and test_id={$iTestId} and que_id={$aDesQueId[$ii]}";					
					
					$bDesMarksAdd=$mysqli->query($sQueryDesMarksAdd);
					$iMarksExist=$bDesMarksAdd->fetch_row();
					if($iMarksExist[0]==Null)						
					{						
						$sQueryAddDesMarks="INSERT INTO quiz_online.des_marks_table (des_id, user_id, test_id, que_id, des_marks) VALUES (NULL, {$iUserId}, {$iTestId}, {$aDesQueId[$ii]}, {$aDesQueMarks[$ii]})";
						$DesMarksResult=$mysqli->query($sQueryAddDesMarks);
						if($DesMarksResult==True)
						{
							$ic++;
						}
						
						
					}	
					else
					{
						$sQueryDesMarks ="update des_marks_table set des_marks = {$aDesQueMarks[$ii]} where user_id={$iUserId} and test_id={$iTestId} and que_id={$aDesQueId[$ii]}";						
						
						$bDesResult=$mysqli->query($sQueryDesMarks);										
						if($bDesResult==True)
						{
							$ic++;
						}
					}

				}
			}

			$sStdResQuery = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
							b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks ,b.op_option as OptionText
							from stud_ans_table as a , option_table as b , question_table as c
							where a.user_ans_answer = b.op_id 
							AND b.op_correct_ans  = 1 
							AND b.que_id = c.que_id 
							AND a.que_id = b.que_id
							AND a.user_id = {$iUserId} 
							AND a.test_id = {$iTestId}";
			/* this query select user test data from database.*/
			$iStdResult = $mysqli->query($sStdResQuery);
			$iObtenMarksDesc = 0;
			while($aRowStdResult = $iStdResult->fetch_row())
			{
				$iObtenMarksDesc += $aRowStdResult[6];
				$iRowCount2 = $iRowCount2 + 1;
			}
			$sWrongQue = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
							b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks , c.que_neg_marks as Negativemark 
							from stud_ans_table as a , option_table as b , question_table as c
							where a.user_ans_answer = b.op_id 
							AND b.op_correct_ans  != 1 
							AND b.que_id = c.que_id
							AND a.que_id = b.que_id
							AND c.que_type LIKE '%Objective%'
							AND a.user_id = {$iUserId} 
							AND a.test_id = {$iTestId}";	
			/* This query calculate number of wrong question and their mark*/ 
			$iWrongQueResult = $mysqli->query($sWrongQue);

			/* this code return users negative marks */
			$iTotalNegMark = 0;
			$sQueryToDisNegMark ="select test_neg_status from test_detail where test_id={$iTestId}";
			$iResultDispNegMark = $mysqli->query($sQueryToDisNegMark);
			if($iResultDispNegMark == true)
			{
				$aFetchDispNegMark = $iResultDispNegMark ->fetch_row();
				if($aFetchDispNegMark[0] == 1)
				{
			    	while($aRowWrongQueResult = $iWrongQueResult->fetch_row())
					{
						$iTotalNegMark += $aRowWrongQueResult[7];
					}
				}
				else
				{

				}
		    }

			/* This query calculates the descriptive question mark */
			$sQcCalDescMark = "select a.test_id ,a.user_id , a.des_marks
								from des_marks_table as a 
								where a.test_id ={$iTestId} AND a.user_id = {$iUserId}";
			$iResultForCalDescMark = $mysqli->query($sQcCalDescMark);

			$iTotalDescMks =0;
			$iCalcRightDescAns = 0;
			$iCalcWrongDescAns = 0;
			while($aRowCalDescQueMark = $iResultForCalDescMark->fetch_row())
			{
				$iTotalDescMks += $aRowCalDescQueMark[2];
				if($aRowCalDescQueMark[2]>= 1)
				{
					$iCalcRightDescAns++;
				}
				else
				{
					$iCalcWrongDescAns++;
				}
			}
			/* 2 ] - this calculate write and wrong question */
			$sWrongQue = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
					b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks , c.que_neg_marks as Negativemark 
					from stud_ans_table as a , option_table as b , question_table as c
					where a.user_ans_answer = b.op_id 
					AND b.op_correct_ans  != 1 
					AND b.que_id = c.que_id 
					AND a.user_id = {$iUserId} 
					AND a.test_id = {$iTestId}";	
					/* This query calculate number of wrong question and thir mark*/ 
			$iWrongQueResult = $mysqli->query($sWrongQue);	
			$iCountWrongQue = $iWrongQueResult->num_rows;

			$sStdResQuery = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
									b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks ,b.op_option as OptionText
									from stud_ans_table as a , option_table as b , question_table as c
									where a.user_ans_answer = b.op_id 
									AND b.op_correct_ans  = 1 
									AND b.que_id = c.que_id
									AND a.que_id = b.que_id 
									AND a.user_id = {$iUserId} 
									AND a.test_id = {$iTestId}";
									/* this query select user test data from database.*/
			$iStdResult = $mysqli->query($sStdResQuery);		/* $iStdResult - this variable stores query result */
			$iRowCount = $iStdResult->num_rows;
			
			$sQueryForTimeTimeTk = "select sb_attempt_que
									from score_board_table as a
									where a.user_id = {$iUserId}
									AND a.test_id = {$iTestId} limit 1";
		    $iResultForTimeTimeTk = $mysqli->query($sQueryForTimeTimeTk);
		    $aRowForTimeTimeTk = $iResultForTimeTimeTk->fetch_row();

		    $iAttemptQueUser = $aRowForTimeTimeTk[0];
		   
		    $iStoreToDbRightAns = $iRowCount + $iCalcRightDescAns;
		    $iStoreToDbWrongAns = $iAttemptQueUser - $iStoreToDbRightAns;//$iCountWrongQue + $iCalcWrongDescAns;

		    /* end of 2] */
		   
		    $iObtMks = $iObtenMarksDesc - $iTotalNegMark + $iTotalDescMks;	
		    
			

			$sQueryAddMrkObt = "update score_board_table 
								set 
								sb_marks_obt = {$iObtMks} , 
								sb_correct_ans = {$iStoreToDbRightAns} , 
								sb_wrong_ans = {$iStoreToDbWrongAns}
								where user_id = {$iUserId}
								AND test_id = {$iTestId}";
		    $iResultAddMarkObt = $mysqli->query($sQueryAddMrkObt);


			if($ic==$iTotalDesQue)
			{
				header("location: testResultPreview.php?usid={$iUserId}&id={$iTestId}&msg=1");
			}	
			else
			{
				header("location: editResult.php?usid={$iUserId}&id={$iTestId}&msg=0");					
			}
			
			

	?>		