<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Allot Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script >
/* start of use of ajax to fetch test in a selected group */
function showTest(str)
{
if (str=="")
  {
  document.getElementById("idDivDispTestSelectBox").innerHTML="";
  return;
  }
if (window.XMLHttpRequest)
  {// code for IE7+, Firefox, Chrome, Opera, Safari
  xmlhttp=new XMLHttpRequest();
  }
else
  {// code for IE6, IE5
  xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
  }
xmlhttp.onreadystatechange=function()
  {
  if (xmlhttp.readyState==4 && xmlhttp.status==200)
    {
    document.getElementById("idDivDispTestSelectBox").innerHTML=xmlhttp.responseText;
    }
  }
xmlhttp.open("GET","ajaxFetchTest.php?gid="+str,true);
xmlhttp.send();
}
/* end of use of ajax to fetch test in a selected group */
</script>
<!-- Datatable link library -->
<style type="text/css" title="currentStyle">
            @import "media/css/demo_page.css";
            @import "media/css/demo_table.css";
        </style>
        <script type="text/javascript" language="javascript" src="media/js/jquery.js"></script>
        <script type="text/javascript" language="javascript" src="media/js/jquery.dataTables.js"></script>
        <script type="text/javascript" charset="utf-8">
            $(document).ready(function() {
                $('#idDisplayAllTest').dataTable();
            } );
/* start of select all checkbox script */
 $(function() {  
        $('#select_all').click(function() {
            var checkboxes = $(this).closest('form').find(':checkbox');
            if($(this).is(':checked')) {
                checkboxes.attr('checked', 'checked');
            } else {
                checkboxes.removeAttr('checked');
            }
        });
            });
 /* end of select all checkbox script */

        </script>
<!--end of Datatable link library -->

</head>
<body>
<?php 

 	include('classConnectQA.php');
 	include('config/config.php');
		session_start();
		    if(isset($_SESSION['user_id']))
        {
          $iUserId = $_SESSION['user_id'];
        }
        else
        {
          $iUserId ="";
        }
        if(isset($_SESSION['ut']))
        {
          $ut=$_SESSION['ut'];
        }
        else
        {
          $ut="";
        }
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }
       
        $sQueryUserInfo = "select * from user_details as a where a.login_id = {$iLoginId} limit 1";
        $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
        $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        if(isset($_GET['msg']))
        {
            $iMsg = $_GET['msg'];
        }
        else
        {
		    $iMsg ="";
        }
 ?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        

		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<?php 
                      if($ut==0||$ut==2)
          {
            echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                  <ul id='menu'>
                  <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                  <ul>
                    <li>
                      <a href='profile.php'>Profile</a>   
                    </li>
                    <li>
                      <a href='profileedit.php'>Update Profile</a>      
                    </li>
                    <li>
                      <a href='changePassword.php'>Change Password</a>      
                    </li>
                  </ul>
                  </li>
                  <li>
                    <a href='manageTest.php'>Home</a>   
                  </li>";
          
            if($ut==2)
            {
              echo "<li> <a href='opportunityHTML.php'>Opportunity</a></li>";
            }
            else
            {
              echo "<li>
                <a>Create</a>
                <ul>
					<li>
						<a href='groupHTML.php'>Create Group</a>			
					</li>
                  <li>
                    <a href='addTestHTML.php'>Create Test</a>   
                  </li>
                  <li>
                    <a href='addUserHTML.php'>Create User</a>     
                  </li>
                  <li>
                    <a href='excelReader/index.php'>Bulk Upload</a>      
                  </li>
                </ul>
              </li>";
            } 
            
            echo "<li>
                  <a>Manage </a>  
					  <ul>
              <li>
                <a href='manageGroup.php'>Manage Group</a>      
              </li>
							<li>
								<a href='manageUser.php'>Manage User</a>			
							</li>
						  <li>
                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                        </li>
					</ul>  
                </li>
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";
          } 
          else
          {
            if($ut==2)
            {

            echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                <ul id='menu'>

                </li>
                <li>
                  <a href='manageTest.php'>Home</a>   
                </li>               
                <li>
                  <a>Manage </a>  
					  <ul>
							<li>
								<a href='manageUser.php'>Manage User</a>			
							</li>
						  <li>
                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                        </li>
					</ul>  
                </li>
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";

            }
            else
            {
              echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                <ul id='menu'>
                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                  <ul>
                    <li>
                      <a href='profile.php'>Profile</a>   
                    </li>
                    <li>
                      <a href='profileedit.php'>Update Profile</a>      
                    </li>
                    <li>
                      <a href='changePassword.php'>Change Password</a>      
                    </li>
                  </ul>
                </li>
                <li>
                  <a href='manageTest.php'>Home</a>   
                </li>
                
                <li>
                  <a href='logout.php'>Logout </a>  
                </li>
                </ul>
                </div>";  
            }
            
          }
          ?>
				</div>
			</div>   	
		</div> <!-- end of menu -->
    </div>  <!-- end of header -->
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">
             
				<div id="idDivSignUp" class="header_0345">Allocate Test
        </div>
       <div class="classHorizHRSubHead"></div>
        <?php 
                 if($iMsg == 123)    
                  {
                     echo "<div class=classMsg >Test allotted to selected student.</div>";
                  }
                  if($iMsg == 124)    
                  {
                     echo "<div class=classMsg >Select student.</div>";
                  }
                  if($iMsg == 125)    
                  {
                     echo "<div class=classMsg >Select test.</div>";
                  }
                   if($iMsg == 1255)    
                  {
                     echo "<div class=classMsg >Select test and group.</div>";
                  }
              ?>
            </div>
                       
          <form action="allotTest.php" method="POST" id="idAllotTest" name="allocateTest" class="classAllocateTest">
    				<div id="idDivDispUser" class="classDivAllotGPTS">
              <?php 
                $sQueryToSelectGroup ="SELECT group_id,group_name FROM group_table where group_status = 1";
                $iResultFetchGroup = $mysqli->query($sQueryToSelectGroup);
                
                echo "Select Group : <select id='id_TestAddSub' class='classAllotTstInputSelect' name='testGroup' onchange='showTest(this.value)'>";
                    echo"<option value='blank'>Select group</option>";
                while($aRowFetchGroup = $iResultFetchGroup->fetch_row())
                  {
                     echo"<option value='{$aRowFetchGroup[0]}' >{$aRowFetchGroup[1]}</option>";                      
                  }
                echo "</select>";
              ?>   
    				</div>
             <div id="idDivDispTestSelectBox" class="classDivTstSelectBox">
              <?php  

              echo "Select Test : <select id='id_TestAddSub' class='classAllotTstInputSelect' name='testSubject'>";
               echo"<option value='blank'>Select Test</option>";
              echo "</select>";
              ?>
            </div>
            <div id="idDivDispSubmitAllot" class="classDivTstSubmitAllot">
              <input type="submit" value="Allocate Test" class="classButton classAddTestButtonSize">
            </div>
				    <div class="classDivDispUserAllotTest" id="idDivDispUserAllotTest">

            <?php 

                $iactive=0;
                $ideactive=0;
                $icounter=0;
                $msg="user";
                echo "<table cellpadding='0' cellspacing='0' border='0' class='display classForTable' id='idDisplayAllTest' width='100%'>";
                echo "<thead><tr>
                  <th class='classDTabdWidthSrno'>Select All<input type='checkbox' id='select_all' name='chkAllChk' value=''></th>
                  <th class='classDTabdWidthTName'>Name</th>
                  <th class='classDTabdWidthTName'>User Email</th>
                  <th class='classDTabdWidthTName'>User Contact</th>
                  <th class='classDTabdWidthTName'>No of Test Taken</th>                  
                  <th class='classDTabdWidthTName'>Detail</th>
                  <th class='classDTabdWidthTName'>User Type</th>
                  
                  </tr></thead>";
                // loop through result of database query, displaying them in the table
                echo "<tbody>";
                $aLoginIdRow=null;
                $sAQuery="select login_id from login where type=1 or type=2";
                if($Aresult=$mysqli->query($sAQuery))
                {
                  
                  while($aLoginIdRow=$Aresult->fetch_row())
                  {   
                    
                    $sUQuery="select * from user_details where login_id={$aLoginIdRow[0]}";
                    if($Uresult=$mysqli->query($sUQuery))
                    { 
                      $icounter++;
                      $Urow=$Uresult->fetch_row();              
                      echo "<tr>";
                      echo "<td class='classDTabdWidthSrno'>
                              <input type='checkbox' name='chkAllotTest$icounter' value='{$Urow[0]}' class='child'>
                            </td>";
                      echo "<td class='classDTabdWidthTName'>$Urow[2]</td>";
                      echo "<td class='classDTabdWidthTName'>$Urow[3]</td>";
                      echo "<td class='classDTabdWidthTName'>$Urow[5]</td>";
                      
                      ///////////////Use to Know Number of test given by the user   
                      $sTestResult = $mysqli->query("select test_id from score_board_table where user_id={$Urow[0]}");
                      $testrow = $sTestResult->num_rows;
                      echo "<td class='classDTabdWidthTName'>{$testrow}</td>";  // No of Test given by the user.
                      ///////////////
                                  
                      // Use to show edit option
                      echo "<td class='classDTabdWidthTName'><a href='profile.php?usid={$Urow[0]}'>Detail</td>"; 
                      /*
                        The query following is use to get login id of each user to check the details
                      */  
                      $sUserIdQuery="select a.type from login as a, user_details as b where b.user_id={$Urow[0]} and a.login_id = b.login_id";                
                      
                      $bUTResult=$mysqli->query($sUserIdQuery);
                      $aURow=$bUTResult->fetch_row();
                      if($aURow[0] == 2)
                      {
                        echo "<td class='classDTabdWidthTName'>tester</td>";  
                      }
                      else
                      {
                        echo "<td class='classDTabdWidthTName'>User</td>";    
                      }
                      echo '</tr>';            
                    }
                  }
                  echo "</tbody>";
                  echo "</table>";

                  $_SESSION['countAllotTest'] = $icounter;
                }
            ?>
            </div>
            </form><br/><br/>
             
          </div>  

    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>