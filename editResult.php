<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Evaluate Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<meta name="description" content="online quiz application" />
<script src="js/jquery-1.8.2.min.js" type="text/javascript">
</script>
<link href="css/style.css" rel="stylesheet" type="text/css" />

<!--start of javascript to validate the textbox -->
<link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   

<script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
</script>
<script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
</script>
<script>
    jQuery(document).ready(function(){
        // binds form submission and fields to the validation engine
        jQuery("#idResultForm").validationEngine();
    });

</script>
<!--end of javascript to validate the textbox -->

<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->

<!-- Script for checkbox style -->
<link rel="stylesheet" href="js/prettyCheckable/prettyCheckable.css">
<script src="js/prettyCheckable/prettyCheckable.js"></script>
<script>
$().ready(function(){

  $('input.myClass').prettyCheckable();

});
</script>
<!-- end of Script checkbox style -->
<!-- Scroll bar script -->
<!-- the mousewheel plugin -->
<link type="text/css" href="js/scrollbar/jquery.jscrollpane.css" rel="stylesheet" media="all" />
<script type="text/javascript" src="js/scrollbar/jquery.mousewheel.js"></script>
<!-- the jScrollPane script -->
<script type="text/javascript" src="js/scrollbar/jquery.jscrollpane.min.js"></script>
<script type="text/javascript" id="sourcecode">
    $(document).ready(function()
    {
        $('.scroll-pane').jScrollPane();
    });
    $(document).ready(function()
    {
        $('.scroll-pane').jScrollPane(
            {
                verticalDragMinHeight: 50,
                verticalDragMaxHeight: 70,
                horizontalDragMinWidth: 20,
                horizontalDragMaxWidth: 20,
                showArrows: true,
                arrowScrollOnHover: true
            }
        );
    });
</script>
<!-- end of scrollbar script -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>
</head>
<body>
	<?php
			include('classConnectQA.php');
			session_start();
	 		if(isset($_SESSION['user_id']))
	 		{
	 			$iUid=$_SESSION['user_id'];
	 		}
	 		else
	 		{
	 			$iUid="";
	 		}
			if(isset($_SESSION['lid']))		// This is Use to check a Session
			{
				$iLoginId = $_SESSION['lid'];
			}
			else
			{
				header("location:index.php");
			}
			if(isset($_SESSION['ut']))  
            {
                $ut=$_SESSION['ut'];
            }
            else
            {
            	$ut=null;	
            } 
			$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();

		$iCDes=0; // @ iCDes is a var use to count the number of descriptive questions.
		$iTestId=null;
		$iUserId=null;
		$sMsg=null;
		if(isset($_GET['id']))
		{
			$iTestId= $_GET['id'];
		}
		if(isset($_GET['usid']))
		{
			$iUserId = $_GET['usid'];
		}
		
			$iLoginId = $_SESSION['lid'];
			$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
								from login as a , user_details as b
								where a.login_id = b.login_id 
								AND a.login_id  = '$iLoginId' limit 1";
			$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
			$aRowForUserInfo = $iResultForUserInfo->fetch_row();
	?>

<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivMenuTop" class="classDivTopMenuUser">
				<?php
					if($ut==0||$ut==2)
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
									<ul id='menu'>
									<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
									</li>
									<li>
										<a href='manageTest.php'>Home</a>		
									</li>";
					
						if($ut==2)
						{
							echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";                                               
                                
						}
						else
						{
							echo "
							<li><a >Opportunity</a>
                                <ul>
                                <li>
                                    <a href='opportunityHTML.php'>Create</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Manage</a>            
                                </li>
                                </ul>
                            </li>
                            <li>
								<a>Create</a>
								<ul>
									<li>
									<a href='groupHTML.php'>Create Group</a>		
									</li>
									<li>
										<a href='addTestHTML.php'>Create Test</a>		
									</li>
									<li>
										<a href='addUserHTML.php'>Create User</a>			
									</li>
									<li>
										<a href='excelReader/index.php'>Bulk Upload</a>			
									</li>
								</ul>
							</li>";
						} 
						
						echo "<li>
									<a>Manage </a>  
									  <ul>
									  		<li>
								                <a href='manageGroup.php'>Manage Group</a>      
								            </li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
									</ul>	
								</li>
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";
					}	
					else
					{
				
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
							<ul id='menu'>
							<li><a href='profile.php'>$aRowForUserInfo[2]</a>
								<ul>
									<li>
										<a href='profile.php'>Profile</a>		
									</li>
									<li>
										<a href='profileedit.php'>Update Profile</a>			
									</li>
									<li>
										<a href='changePassword.php'>Change Password</a>			
									</li>
								</ul>
							</li>
							<li>
								<a href='manageTest.php'>Home</a>		
							</li>
							
							<li>
								<a href='logout.php'>Logout </a>	
							</li>
							</ul>
							</div>";	
					}
					
				?>		
				</div>
			</div>   	
		</div> <!-- end of menu -->
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->  
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
if(isset($_GET['msg']))
	{
		$sMsg=$_GET['msg'];
		echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
			if($_GET['msg']==1)
			{
				echo "<div class=classMsg >Result calculated Successfully.</div>";
			}
			if($_GET['msg']==0)
			{
				echo "<div class=classMsg >Try again: Result not Updated.</div>";
			}
			if($_GET['msg']==-1)
			{
				echo "<div class=classMsg >DB Error in Result Update.</div>";
			}
		echo"</div>";
	}
?>
<div id="id_content_wrapper">
	<div id="id_content">
		<div id="idDivMiddleBody" class="classDivMiddleBody">
				
					
			 <div id="idDivNat" class="classDivTime">
			 	<div id="idDivSignUp" class="header_0345">User :
					<?php 
					/*
						To view the user name.
					*/
					$sUserQuery="select user_full_name from user_details where user_id={$iUserId}";

					$aResult=$mysqli->query($sUserQuery);
					$Urow=$aResult->fetch_row();
					echo "$Urow[0]";
					?>
				</div>
				<div class="classHorizHRSubHead"></div>
				  <?php 

/* This selects user information from database */
    $sQueryForUserDetail = "select a.login_id, b.user_id, b.user_full_name, b.user_email from login as a, user_details as b where a.login_id = b.login_id AND b.user_id  = {$iUserId} limit 1";   
    $iResultForUserInfo = $mysqli->query($sQueryForUserDetail);
    $aRowForUserInfo = $iResultForUserInfo->fetch_row();    
    $sUserFullName = $aRowForUserInfo[2];

/* Ths query finds time taken to exam */
    $sQueryForTimeTimeTk = "select a.sb_time_taken,a.sb_doa,a.sb_attempt_que,a.sb_correct_ans,a.sb_wrong_ans,a.sb_marks_obt,a.sb_marks_out_of
							from score_board_table as a
							where a.user_id = {$iUserId}
							AND a.test_id = {$iTestId}";   
    $iResultForTimeTimeTk = $mysqli->query($sQueryForTimeTimeTk);
    $aRowForTimeTimeTk = $iResultForTimeTimeTk->fetch_row();    
    

 /* Ths query select IP of system */
    $sQueryForSysIP = "select a.sys_ip
							from login_system_info as a
							where a.user_id = {$iUserId}
							AND a.test_id = {$iTestId} limit 1";   
    $iResultForSysIP = $mysqli->query($sQueryForSysIP);
    $aRowForSysIP = $iResultForSysIP->fetch_row();    
    $sUserSysIP = $aRowForSysIP[0];

/* if sebjective question not checked by admin or tester user then this query hide total score and percentage */
			$sQrToChkFUllRes = "select a.test_id , a.user_id
								from des_marks_table as a 
								where a.test_id ={$iTestId} and user_id={$iUserId} limit 1";
					/* This query gives number of marks for each question and negative marks for each question */
			$iQueForToChkFUllRes = $mysqli->query($sQrToChkFUllRes);
			$aRowForToChkFUllRes = $iQueForToChkFUllRes->fetch_row();
			$iFetchedTestId = $aRowForToChkFUllRes[0];
			$iFetchedUserId = $aRowForToChkFUllRes[1];
/* 2) This code give test result like - total number of question in test , number of question attempted by the student,
		number of right question , number of wrong question , total mark and percentage  */
		$sQuery = "select a.que_question, a.que_id , a.test_id , a.que_type , a.que_marks ,b.test_name ,a.que_neg_marks
					from question_table as a , test_detail as b 
					where b.test_id = a.test_id 
					AND b.test_id = {$iTestId}";
		$iResult = $mysqli->query($sQuery); 	/* This query displayes the question from database */
		$iQue_Count = $iResult->num_rows;

		$sStdResQuery = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
						b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks ,b.op_option as OptionText
						from stud_ans_table as a , option_table as b , question_table as c
						where a.user_ans_answer = b.op_id 
						AND b.op_correct_ans  = 1 
						AND b.que_id = c.que_id 
						AND a.user_id = {$iUserId} 
						AND a.test_id = {$iTestId}";
						/* this query select user test data from database.*/
		$iStdResult = $mysqli->query($sStdResQuery);		/* $iStdResult - this variable stores query result */
		
		$iRowCount = 0;
		$iObtenMarks=0;
		$iRowCount2 = 0;
		// Use to Show the Result Edit Option.
		echo "<div id='idDivResultHead'>
			<span class='classSpanDlR'>Editing Result</span> 
			<span class='classSpanEDLogo classHidden'>			
			<a><img src='images/Edit.png' class='classEditTestIcon classHidden' alt='Click to Edit Test'/></span></a><h1>
			
			</div>";
		$sQueryRemShareRes = "select count(user_id) from share_result where user_id = {$iUserId} and test_id = {$iTestId} AND share_status = 1";
        $bResultRemoveShare = $mysqli->query($sQueryRemShareRes);
        $aRowRemoveSharelink = $bResultRemoveShare->fetch_row();
			if($iFetchedTestId === $iTestId || $iFetchedUserId === $iUserId )
            {
                echo"<div id='idDivDownloadShareLink' class='classForDownloadLink'>";
            }
            else
            {
                echo"<div id='idDivDownloadShareLink' class='classForDownloadLink classLeftPadForDownloadLink'>";
            }

				if($iFetchedTestId === $iTestId || $iFetchedUserId === $iUserId )
					{
					echo "<div>
                        <span id='idSpanTabT10' class='classSpanDownloadButton classSpanWidthForDownloadText'>
                            Download Resume
                        </span>

                        <span id='idSpanTabT10' class='classForSendMail classSpanWidthForDownloadText'>
                            Mail result to Admin
                        </span>";
                    	if($aRowRemoveSharelink[0] >= 1)
                            {
                            echo "<span id='idSpanTabT10' class='classForSendMail classSpanWidthForDownloadText'>
                                    Remove shared result
                                </span>";
                            }
                            else
                            {
                                echo "<span id='idSpanTabT10' class='classForSendMail classSpanWidthForDownloadText'>
                                    Share result with student
                                </span>";
                            }
                    }
                    else
                    {
                    	echo "<div>
                        <span id='idSpanTabT10' class='classSpanDownloadButton classSpanWidthForDownloadText'>
                            Download Reume
                        </span>";	
                    }
                    echo "</div>

					<span id='idSpanTabT10' class='classSpanDownloadButton classSpanWidthForDownloadText'>";
						if($iFetchedTestId === $iTestId || $iFetchedUserId === $iUserId )
						{
						echo"<a href='downloadResume.php?id={$iTestId}&uid={$iUserId}'>
							<img src='images/DownloadIcon.jpg' id='idDownloadIcon' class='classDownloadResIcon' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Download Resume'>
							</a>
							</span>
							<span id='idSpanTabT10' class='classForSendMail classSpanWidthForDownloadText'>
							<a href='QuizMailer.php?id={$iTestId}&uid={$iUserId}'>
								<img src='images/mailsym.png' id='idMailImg' class='classMailLogo' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Mail Result to Admin'>
							</a>
							</span>
							<span id='idSpanTabT10' class='classForSendMail classSpanWidthForDownloadText'>";
							
                            if($aRowRemoveSharelink[0] >= 1)
                            {
                                echo "<a href='share_result.php?tid={$iTestId}&usid={$iUserId}'>
                                <img src='images/removeShare.gif' id='idShareImg' class='classDownloadIcon' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Remove Share result'>
                                </a>";
                            }
                            else
                            {
                                echo "<a href='share_result.php?tid={$iTestId}&usid={$iUserId}'>
                                <img src='images/Share-Button.jpg' id='idShareImg' class='classDownloadIcon' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Share result with student'>
                                </a>";
                            }
						}
						else
						{
							echo"<a href='downloadResume.php?id={$iTestId}&uid={$iUserId}'>
							<img src='images/DownloadIcon.jpg' id='idDownloadIcon' class='classDownloadResIcon' rel='tooltip' data-toggle='tooltip' data-placement='bottom' title='Download Resume'>
							</a>
						</span>";
						}
					echo "</span>
			</div>";
		echo"<div class='classHorizHRSubHead'></div>";
		/*
			To Calculate Total Marks.
		*/
		$sQueryDesMarksAdd="select des_marks from des_marks_table as a, question_table as b where a.user_id={$iUserId} and a.test_id={$iTestId} and a.que_id = b.que_id";			
		
		$bDesMarksAdd=$mysqli->query($sQueryDesMarksAdd);
		$iObtenMarks = 0;
		while($iMarksExist=$bDesMarksAdd->fetch_row())
		{
			$iObtenMarks += $iMarksExist[0];		
			$iRowCount = $iRowCount + 1;
		}

		$iObtenMarksDesc = 0;
		while($aRowStdResult = $iStdResult->fetch_row())

		{
			$iObtenMarksDesc += $aRowStdResult[6];
			$iRowCount2 = $iRowCount2 + 1;
		}
		
		$sGetTestNameQuery = "select test_name, test_duration from test_detail where test_id={$iTestId}";	/* this query select test name from test details */
		$iGTNResult = $mysqli->query($sGetTestNameQuery);
		$aRowGTNResult = $iGTNResult->fetch_row();

		$iTotalAttemptQueQuery = "select * from stud_ans_table where user_ans_answer <> '' AND user_id = {$iUserId} AND test_id = {$iTestId}"; 
										/* This query gives number of attempted question by the user for perticular test */
		$iAtempQueResult = $mysqli->query($iTotalAttemptQueQuery);
		$iAttemptQue_Count = $iAtempQueResult->num_rows;

		

		$sQueryForMarks = "select test_id,que_id,que_question,que_marks,que_neg_marks 
							from question_table where test_id = {$iTestId}";
					/* This query gives number of marks for each question and negative marks for each question */
		$iQueForMrkResult = $mysqli->query($sQueryForMarks);
		$iTotalMks =0;

		while($aRowTotalMks = $iQueForMrkResult->fetch_row())
		{
		$iTotalMks += $aRowTotalMks[3];
		}

		$sWrongQue = "select distinct a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,
					b.op_id as op,b.op_correct_ans as correctans , c.que_marks as Marks , c.que_neg_marks as Negativemark 
					from stud_ans_table as a , option_table as b , question_table as c
					where a.user_ans_answer = b.op_id 
					AND b.op_correct_ans  != 1 
					AND b.que_id = c.que_id 
					AND a.user_id = {$iUserId} 
					AND a.test_id = {$iTestId}";	
					/* This query calculate number of wrong question and their mark*/ 
		$iWrongQueResult = $mysqli->query($sWrongQue);
		$iTotalNegMark = 0;
		$sMsgForResult = "";
		while($aRowWrongQueResult = $iWrongQueResult->fetch_row())
		{
			$iTotalNegMark += $aRowWrongQueResult[7];
		}
		$iCountWrongQue = $iWrongQueResult->num_rows;
	echo "<div class='classDivReduceSpace'>";	
	echo "<div class='classDivPReviewResult classBlack'>                          
                <div id='idDivUName1' class='classDivAddTest classDivPReviewResult'>  
                    <span id='idSpanTabF1' class='classSpanTabText classFont_18'>Test Name</span>
                    <span id='idSpanTabS:1' class='classSpan classFont_18'>:</span>                              
                    <span id='idSpanTabT1' class='classBlack classFont_18'>";
                        echo $aRowGTNResult[0];    
        echo "</span>
                </div>";
        echo "<div class='classDivHorizantalLine' ></div>
                </div>";
		echo "<div class='classDivPReviewResult'>";
        echo "<div id='idDivTMarks3' class='classDivAddTest'>
            
            <span id='idSpanTabF3' class='classSpanTabText classFont_18'>Total Marks</span>
            <span id='idSpanTabS3' class='classSpan classFont_18'>:</span>
            <span id='idSpanTabT3' class='classBlack classSpanResultText classFont_18'>";
                echo $aRowForTimeTimeTk[6];
        echo"</span>
         <span id='idSpanTabF35' class='classSpanTabText classFont_18'>Total Time</span>
                    <span id='idSpanTabS35' class='classSpan classFont_18'>:</span>
                    <span id='idSpanTabT35' class='classBlack classSpanResultText classFont_18'>";
                        echo $aRowGTNResult[1] . "   &nbsp;&nbsp;Min.";
        echo "</span>
            </div>";
                
        echo"<div id='idDivTMarks45' class='classDivAddTest'>

        <span id='idSpanTabF2' class='classSpanTabText classFont_18'>Total Question</span>
            <span id='idSpanTabS2' class='classSpan classFont_18'>:</span>
            <span id='idSpanTabT2' class='classBlack classSpanResultText classFont_18'>";
            echo $iQue_Count;
            echo "</span>";
        echo "<span id='idSpanTabF4' class='classSpanTabText classFont_18'>Question Attempt</span>
        <span id='idSpanTabS4' class='classSpan classFont_18'>:</span>
        <span id='idSpanTabS4' class='classBlack classSpanResultText classFont_18'>";
            echo $aRowForTimeTimeTk[2];
            echo "</span> 
            </div>";

          echo"<div id='idDivTMarks454' class='classDivAddTest'>
        <span id='idSpanTabF354' class='classSpanTabText classFont_18'>Time Taken</span>
        <span id='idSpanTabS354' class='classSpan classFont_18'>:</span>
        <span id='idSpanTabT354' class='classBlack classSpanResultText classFont_18'>";
            echo $aRowForTimeTimeTk[0];
            echo "</span>";

        $sQueryToDisNegMark ="select test_neg_status from test_detail where test_id={$iTestId}";
        $iResultDispNegMark = $mysqli->query($sQueryToDisNegMark);
        if($iResultDispNegMark == true)
        {
            $aFetchDispNegMark = $iResultDispNegMark ->fetch_row();
            if($aFetchDispNegMark[0] == 1)
            {
                echo "<span id='idSpanTab1NegMarksOff' class='classSpanTabText classFont_18'>Negative Marks</span>
                            <span id='idSpanTab2NegMarksOff' class='classSpan classFont_18'>:</span>
                            <span id='idSpanTab3NegMarksOff' class='classBlack classFont_18 classSpanResultText'>
                                Applicable
                            </span>";
            }
            else
            {
                echo "<span id='idSpanTab1NegMarksOff' class='classSpanTabText classFont_18'>Negative Marks</span>
                            <span id='idSpanTab2NegMarksOff' class='classSpan classFont_18'>:</span>
                            <span id='idSpanTab3NegMarksOff' class='classBlack classFont_18 classSpanResultText'>
                                Not Applicable
                            </span>";
            }
        }
        echo "</div>";
        echo "<div id='idDivQAttempt4' class='classDivAddTest'>
            <span id='idSpanTabF354' class='classSpanTabText classFont_18'>System IP</span>
            <span id='idSpanTabS354' class='classSpan classFont_18'>:</span>
            <span id='idSpanTabT354' class='classBlack classFont_18 classSpanResultText'>$sUserSysIP</span>
            </div>";
    echo "</div>";
    echo "<div class='classDivHorizantalLine' ></div>";

			if($iFetchedTestId === $iTestId && $iFetchedUserId === $iUserId)
			{
				
				echo "<div class='classDivPReviewResult'>";
            echo"<div id='idDivMObtain7' class='classDivAddTest'>
                    <span id='idSpanTabF7' class='classSpanTabText classFont_18'>Marks Obtain</span>
                    <span id='idSpanTabS7' class='classSpan classFont_18'>:</span>
                    <span id='idSpanTabT7' class='classBlack classFont_18 classSpanResultText'>";
                        
                        $iObtMks = $aRowForTimeTimeTk[5];
                        echo $iObtMks;
            echo"</span>
                    <span id='idSpanTabF8' class='classSpanTabText classFont_18'>Percentage</span>
                    <span id='idSpanTabS8' class='classSpan classFont_18'>:</span>
                    <span id='idSpanTabS8' class='classBlack classFont_18 classSpanResultText'>";
                        $iTM = 1;
                        if($iTotalMks == 0)
                        {
                            $iPercent = ($iObtMks/$iTM)*100;
                            echo  round($iPercent,2) ; echo " %";   
                        }
                        else
                        {
                            $iPercent = ($iObtMks/$iTotalMks)*100 ;
                            echo round($iPercent,2); echo " %"; 
                        }           
            echo"</span>
                    
                </div>";
            echo "<div id='idDivARight5' class='classDivAddTest'>
            <span id='idSpanTabF5' class='classSpanTabText classFont_18'>Answer Right</span>
            <span id='idSpanTabS5' class='classSpan classFont_18'>:</span>
            <span id='idSpanTabT5' class='classBlack classFont_18 classSpanResultText'>";
                echo $aRowForTimeTimeTk[3];
            echo"</span>
                    <span id='idSpanTabF6' class='classSpanTabText classFont_18'>Answer wrong</span>
                    <span id='idSpanTabS6' class='classSpan classFont_18'>:</span>
                    <span id='idSpanTabT6' class='classBlack classFont_18 classSpanResultText'>";
                        echo $aRowForTimeTimeTk[4];
            echo"</span>
                </div>";

                echo"<div id='idDivRemark9' class='classDivAddTest'>
                    <span id='idSpanTabF9' class='classSpanTabText classFont_18'>Remark</span>
                    <span id='idSpanTabS9' class='classSpan classFont_18'>:</span>
                    <span id='idSpanTabT9' class='classBlack classFont_18 classSpanResultText'>";
                            if( $iPercent > 60 )
                            {
                                echo "Excellent";
                            }
                            
                            if( $iPercent >= 35 AND $iPercent <= 60 )
                            {
                                echo "Good";
                            }
                            if( $iPercent <= 35 )
                            {
                                echo "Very Poor";
                            }
                echo "</div>
                </div>";
        echo "<div class='classDivHorizantalLine' ></div>";
   echo "</div>"; 
   echo "</div>";
				
			}
			else
			{
			
			}
			
			/* end of 2 */


		echo "<form name='editResultForm' class='classResultForm' id='idResultForm' method='POST' action='editMarks.php?usid={$iUserId}&id={$iTestId}'>";

		
	/* 1) This displays the total question in the exam and thir option . also display user answer and right answer */

		echo "<div id='idDisplaySolveQue'><h3>Solved Paper</h3>";
		echo"<div class='classHorizHRSubHead'></div>";
		
		$ii=1;
		while($aRow = $iResult->fetch_row())	// this function fetches all question and their option from database
		{
			echo "<div class='classDivDisplayQuestion'>";
			echo "<div class='classDivDisplayQuestion'>";

			//! display data in table
			$iQue_id = $aRow[1];
			$iTest_id = $aRow[2];
			$sQueType = $aRow[3];
			$iQueMarks = $aRow[4];	
				
			/*
				To Show The score of each question in preview result.
			*/

			/* 5] this query returns all the ans given by user for perticular question */
            $sQueryDIsplayUserAns = "select a.user_id as usid ,a.test_id as tid,a.que_id as qid,a.user_ans_answer as usans ,b.multi_option,c.que_marks as QueMark
                                    from stud_ans_table as a , multi_option_table as b , question_table as c
                                    where a.user_ans_answer = b.multi_op_id 
                                    AND b.que_id = c.que_id 
                                    AND a.que_id = b.que_id
                                    AND a.user_id = {$iUserId} 
                                    AND a.test_id = {$iTestId}
                                    AND c.que_id = {$iQue_id}";
            $iResultDispUserAns = $mysqli->query($sQueryDIsplayUserAns);
            $iCountUserMultiChAns = $iResultDispUserAns->num_rows;
				///////////////////////////
			$sQueryQtpFromImg = "select * from question_attachment as a where a.que_id = {$iQue_id} AND file_status = 1";
	                                    /* 1] this query return af question have any attachment */     
	        $iResultAttachment = $mysqli->query($sQueryQtpFromImg);
	        $aFetchRowAttachment = $iResultAttachment->fetch_row();
	        $sQtpInAttachment = $aFetchRowAttachment[1];
	        $sAttachLocation = $aFetchRowAttachment[3];
	        $sAttachMentName = $aFetchRowAttachment[4];
	        /* end of - 1 */
			/////////////////////////////			
								
				/*
						To Show The score of each question in preview result.
				*/
			/////////////////////////////			
			$sQueryForCorrectAns = "select distinct b.op_option as OptionText ,b.op_id 
									from test_detail as a , option_table as b , question_table as c
									where a.test_id = c.test_id  
									AND b.que_id = c.que_id
									AND b.op_correct_ans = 1
									AND a.test_id = {$iTestId}
									AND c.que_id = {$iQue_id} limit 1";
					/* This query display correct answer for question from database */

			$sQuerySel = "select a.op_id as op_id, a.op_option as opt, a.op_correct_ans as cra 
			from option_table as a ,question_table as b , test_detail as c 
							where c.test_id = b.test_id 
							And b.que_id = a.que_id 
							AND c.test_id ={$iTestId} 
							AND b.que_id ={$iQue_id}";
					/* This query select option related data fetch from option table*/

			$sQueryDispUserAns = "select distinct a.user_ans_answer, a.que_id  
									from stud_ans_table as a
									where  
									a.test_id = {$iTestId}
									AND a.user_id = {$iUserId}
									AND a.que_id = {$iQue_id} limit 1";
									/* This Query select user answer id from database */
			$aUserAnsIdRes = $mysqli->query($sQueryDispUserAns);
			$aRowUserAnsIdRes = $aUserAnsIdRes->fetch_row();

			$iUserAnsId = Null;	
			
			// Query is use to know the question type.
			$sQTQuery="select que_type from question_table where que_id={$aRowUserAnsIdRes[1]}";
			$bQTResult=$mysqli->query($sQTQuery);
			if($bQTResult == true)
			{
				$aQTRow=$bQTResult->fetch_row();
				if($aQTRow=="Objective")
				{
					$iUserAnsId = $aRowUserAnsIdRes[0];
				}
				else
				{	
					$iUserAnsId = $aRowUserAnsIdRes[1];
				}		
			}

			$aCorrectAnsRes = $mysqli->query($sQueryForCorrectAns);
			$aRowCorrectAnsRes = $aCorrectAnsRes->fetch_row();
			$sAns=$aRowCorrectAnsRes[0];			
		
			$iResultForScore = $mysqli->query($sStdResQuery);
			$aRowForScore=$iResultForScore->fetch_row();
			/* 4 ] This query gives correct ans of user */
			$iSelectQueryUserCorrectAns = "select count(d.user_ans_answer)
											from test_detail as a , question_table as b , option_table as c , stud_ans_table as d
											where a.test_id = b.test_id  
											AND b.que_id = c.que_id
											AND c.op_correct_ans  = 1
											AND d.que_id = c.que_id
											AND d.user_ans_answer = c.op_id
											AND a.test_id = {$iTestId}
											AND c.que_id = {$iQue_id} limit 1";
			$bResultCorrectAns = $mysqli->query($iSelectQueryUserCorrectAns);
			$aRowUserCorrectAns = $bResultCorrectAns->fetch_row();
			$iUserRightAnsFromDb = $aRowUserCorrectAns[0];
			/* End 4 */							
			$sQueryTOCheckQueType = "select que_type from question_table 
										where que_id = {$iQue_id} 
										AND test_id = {$iTestId}";
								/* This query select question type from question table */
			$iResultForQueType = $mysqli->query($sQueryTOCheckQueType);
			$aRowForQueType = $iResultForQueType->fetch_row();
		
			$iResult2=$mysqli->query($sQuerySel);
			$iUserAnsId = $aRowUserAnsIdRes[0];
			/* 7 } -- //////////////////  This code returns the question marks and negative mark and mark obtain for each question to user \\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
			
			$iScoreFlag=0;
			$iScoreFlagDesc=0;
			$iScoreFlagMC=0;

			if($sQueType == 'Objective')		/* If Question is objective type then radio butten display by using this function */
				{	
					echo "<span id='idSpanDispNum' class='classSpanDispB'>{$ii}. </span>
								<span id='idSpanDisplayQue' class='classSpanDisplayQue '>{$aRow[0]}</span>
								<span id='idSpanDisplayMarks' class='classSpanDiaplayMarks'>[ Marks : {$aRow[4]} | Negative Mark : {$aRow[6]} | Score : ";
					
					if($iUserAnsId != Null)
					{
					 if($iUserRightAnsFromDb >= 1) 
	                    {
	                        echo "{$aRow[4]} ] </span>";
	                        /* This displays Score done by user at each question*/
	                        $iScoreFlag=1;                              
	                    }
	                    else
	                    {
	                        echo " 0 ]</span>"; 
	                    }
	                }
	                else
	                {
	                	echo " Not attempted ]</span>"; 

	                }
					

				}

				if($sQueType=="Descriptive")
				{
					echo "<span id='idSpanDispNum' class='classSpanDispB'>{$ii}. </span>
					<span id='idSpanDisplayQue' class='classSpanDisplayQue '>{$aRow[0]}</span>
					<span id='idSpanDisplayMarks' class='classSpanDiaplayMarks'>[ Marks : {$aRow[4]} | Score : ";
					$sQueryDesMarksAddTab ="select des_marks from des_marks_table where user_id={$iUserId} and test_id={$iTestId} and que_id = {$iQue_id}";									
					
					$bDesMarksAddTab = $mysqli->query($sQueryDesMarksAddTab);
					if($iUserAnsId != Null)
					{
						if($bDesMarksAddTab==True)
							{
								$aRowDesScore =$bDesMarksAddTab->fetch_row();
								if($aRowDesScore[0] != "")
				                    {

				                        echo "{$aRowDesScore[0]} ]</span>";/* This displays Score done by user at each question*/    
				                    }
				                    else
				                    {
				                         echo " Pending ]";
				                    }
		                   					
							}
							else
							{
								echo "0";
								$iScoreFlagDesc=1;
							}
						
					}
					else
					{
						echo " Not attempted ]";
					}
				}

				if($sQueType=="Multiple")
				{
					echo "<span id='idSpanDispNum' class='classSpanDispB'>{$ii}. </span>
					<span id='idSpanDisplayQue' class='classSpanDisplayQue '>{$aRow[0]}</span>
					<span id='idSpanDisplayMarks' class='classSpanDiaplayMarks'>[ Marks : {$aRow[4]} | Score : ";
					$sQueryMultiChAnsMark ="select des_marks from des_marks_table where user_id={$iUserId} and test_id={$iTestId} and que_id = {$iQue_id}";									
					
					$bDesMarksMultiChoice =$mysqli->query($sQueryMultiChAnsMark);
					if($iUserAnsId != Null)
					{
						if($iCountUserMultiChAns >=1)
	                    {
							if($bDesMarksMultiChoice ==True)
							{
								$aRowMultiChMark = $bDesMarksMultiChoice->fetch_row();
								if($aRowMultiChMark[0] != "")
		                        {
		                            echo "{$aRowMultiChMark[0]} ]</span>";/* This displays Score done by user at each question*/
		                        }
		                        else
		                        {
		                            echo " Pending ]";
		                        }

								if($aRowMultiChMark[0]==Null)
								{
									$iScoreFlagMC=0;	
								}	
								else
								{
									$iScoreFlagMC=1;								
								}						
							}
							else
							{
								echo "0";
								$iScoreFlagMC=1;
							}
						}
						else
						{
							echo " Not attempted ]";
						}
					}
					else
					{
						echo " Not attempted ]";
					}
				}
					
			/* 7 } -- end  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\ */
			
			$sQuerySel = "select a.op_id as op_id, a.op_option as opt, a.op_correct_ans as cra 
			from option_table as a ,question_table as b , test_detail as c 
							where c.test_id = b.test_id 
							And b.que_id = a.que_id 
							AND c.test_id ={$iTestId} 
							AND b.que_id ={$iQue_id} ORDER BY a.op_id ASC";
					/* This query select option related data fetch from option table*/
			$iResult2 = $mysqli->query($sQuerySel);
			
			echo "<div id='idDivDispQueAns'>";
			$sQueryTOCheckQueType = "select que_type from question_table 
										where que_id = {$iQue_id} 
										AND test_id = {$iTestId}";
								/* This query select question type from question table */
			$iResultForQueType = $mysqli->query($sQueryTOCheckQueType);
			$aRowForQueType = $iResultForQueType->fetch_row();
			
			$sQueryForCorrectAns = "select distinct b.op_option as OptionText ,b.op_id 
									from test_detail as a , option_table as b , question_table as c
									where a.test_id = c.test_id  
									AND b.que_id = c.que_id
									AND b.op_correct_ans = 1
									AND a.test_id = {$iTestId}
									AND c.que_id = {$iQue_id} limit 1";
					/* This query display correct answer for question from database */
			$aCorrectAnsRes = $mysqli->query($sQueryForCorrectAns);
			$aRowCorrectAnsRes = $aCorrectAnsRes->fetch_row();
			$sAns=$aRowCorrectAnsRes[0];

			$sQueryDispUserAns = "select distinct a.user_ans_answer  
									from stud_ans_table as a
									where  
									a.test_id = {$iTestId}
									AND a.user_id = {$iUserId}
									AND a.que_id = {$iQue_id} limit 1";
									/* This Query select user answer id from database */
			$aUserAnsIdRes = $mysqli->query($sQueryDispUserAns);
			$aRowUserAnsIdRes = $aUserAnsIdRes->fetch_row();
			$iMulti = 1;
			if($iUserAnsId != 0)
			{
				if($aQTRow=="Objective")
				{
					$sQueryForUserAns = "select 
									b.op_option
									from question_table as a , option_table as b
									where 
									b.op_id = {$iUserAnsId}
									AND a.test_id = {$iTestId}									 
									AND b.que_id = {$iQue_id}
									OR a.que_id = b.que_id limit 1";
									/* This query use to check user answer is right or wrong */
				}
				else
				{
					$sQueryForUserAns = "select 
									b.op_option
									from question_table as a , option_table as b
									where 
									a.test_id = {$iTestId}									 
									AND b.que_id = {$iQue_id}
									OR a.que_id = b.que_id limit 1";
									/* This query use to check user answer is right or wrong */
			
				}
			$aUserAnsRes = $mysqli->query($sQueryForUserAns);
			$aRowUserAnsRes = $aUserAnsRes->fetch_row();
			$sUAns = $aRowUserAnsRes[0];
			}
			else
			{
				$sUAns = $aRowUserAnsIdRes[0];
			}

			$iCountWrightMultipleQuestion = 0;
			
				if($aRowForQueType[0] == 'Objective')		/* If Question is objective type then radio butten display by using this function */
				{	
					if($sQtpInAttachment == $iQue_id)
                        {
                            echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'><a href='$sAttachLocation$sAttachMentName'><img src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></a></div>";
                        }
					$ij=1;
					$opNo='A';
					while($aRowOpt = $iResult2->fetch_row())
					{
							$iOpidAns = $aRowOpt[0];
							 echo "<div class='classFont16'>";
							 if($iOpidAns == $iUserAnsId)
							 {
									
									echo "<span id='idSpanDisplayOption' class='classSpanDisplayOp'>{$opNo} </span>
										<span id='idSpanDisplayOption' class='classSpanDisplayOp'><input class='myClass' type ='radio' name ='ans{$ii}' value='$iOpidAns' checked='checked' disabled='disabled'></span>";
								 	echo "<span id='idSpanDisplayQueTxt' class='classSpanDisplayQueTxt'>{$aRowOpt[1]}</span>";
								
								if($aRowCorrectAnsRes[1] == $iOpidAns) /* This function compairs the user ans and display image right or wrong */
								 {
								 	echo "<span id= 'idSpanRightImg' class='classRightWrongIcon'><img  class='classAddIcon' src='images/right.jpg'></span>";
								 }
								 else
								 {
								 	echo "<span id= 'idSpanWrongImg' class='classRightWrongIcon'><img class='classAddIcon' src='images/wrong.jpg'></span>";
							 	 }

							 }
							 else
							 {
							 	if($aRowCorrectAnsRes[1] == $iOpidAns)
							 	{
								 	 echo "<span id='idSpanDisplayOption' class='classSpanDisplayOp'>{$opNo} </span>
								 	 	<span id='idSpanDisplayOption' class='classSpanDisplayOp'><input class='myClass' type ='radio' name ='ans{$ii}' value='$iOpidAns' disabled='disabled'></span>";
									 echo "<span id='idSpanDisplayQueTxt' class='classSpanDisplayQueTxt'>{$aRowOpt[1]}</span><span class='classRightWrongIcon'><img class='classAddIcon' src='images/right.jpg'></span>";
								}
								else
								{
									echo "<span id='idSpanDisplayOption' class='classSpanDisplayOp'>{$opNo} </span>
										<span id='idSpanDisplayOption' class='classSpanDisplayOp'><input class='myClass' type ='radio' name ='ans{$ii}' value='$iOpidAns' disabled='disabled'></span>";
									 echo "<span id='idSpanDisplayQueTxt' class='classSpanDisplayQueTxt'>{$aRowOpt[1]}</span>";
								}

							 }
							 echo "</div>";
							 $ij++;
							 $opNo++;
					}
				}
				else
				 {
				 	if($aRowForQueType[0] == 'Descriptive')	
				 	{
				 		if($sQtpInAttachment == $iQue_id)
	                        {
	                            echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'><a href='$sAttachLocation$sAttachMentName'><img src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></a></div>";
	                        }	 	
					 	//////////////// Use to Edit the Test
					 	// @ iCDes is use to count the no of descriptive Questions which help in piosting the number of Tesxt box values.
					
						 // @ iCDes is a var use to count the number of descriptive questions.
						$sQueryDesMarks="select des_marks from des_marks_table where user_id={$iUserId} and test_id={$iTestId} and que_id={$iQue_id}";
						$bDesMarksResult=$mysqli->query($sQueryDesMarks);					
						if($bDesMarksResult==True)
						{
							$aDesMarks=$bDesMarksResult->fetch_row();
							if($iUserAnsId != Null)
							{
								/*
									The Below if is use to chek wether the negaive marks are allow or not.
								*/
								if(1)
								{
									// Show Negative marks and also allow to update them.
								}
								else
								{
									// Do Not Show Negative marks and also allow allow to update them.
								}
									$sConAns = nl2br($iUserAnsId);
									 echo "<div class='classFont16'>
                                    <div>Ans :-</div>
                                            <div class='scroll-pane classRounded_Radius_5'>
                                                <p>$sConAns</p>
                                            </div>
                                    </div>";
			                         echo "<div class='classFont16 classDivInputMarkManual'>
									 Marks :<input type='text' name='desAns{$iCDes}'  value='{$aDesMarks[0]}' size='2' class='classInputDescMarks validate[required,custom[number],max[$aRow[4]]] text-input' id='idMarkObt{$iCDes}'/> out of {$aRow[4]}
									 		<input type='hidden' name='desQueId{$iCDes}' value='{$iQue_id}' />
									 		<input type='hidden' value='{$aRow[4]}' class='' id='idDesAns{$iCDes}'/>
									</div>";
							}
							else
							{
								echo "<div class='classFont16'>
									 <p>Ans :- Not attempted.</p>
									 </div>";
							}							
						}
						else
						{
							echo "<div class='classFont16'>
									 <p>Ans :- {$sUAns}</p>
									 <span class='classSmall'>Data Base Error in editing test.</span>
								</div>";	
						}
					}
					if($aRowForQueType[0] == 'Multiple')	
				 	{
				 		
				 		$sQueryMulChoice = "select a.multi_op_id as op_id, a.multi_option as opt, a.multi_op_ans as cra from multi_option_table as a ,question_table as b , test_detail as c 
	                    where c.test_id = b.test_id And b.que_id = a.que_id AND c.test_id ='$iTestId' AND b.que_id ='$iQue_id'";
	                            /* This query select option related data fetch from option table*/
	                    $iResultMulChoice = $mysqli->query($sQueryMulChoice);

	                    $iChk = 1;
	                    if($sQtpInAttachment == $iQue_id)
	                        {
	                            echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'><a href='$sAttachLocation$sAttachMentName'><img src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></a></div>";
	                        }
	                    echo "<div>";
	                    while($aRowMultiOpt = $iResultMulChoice->fetch_row())
	                    {
	                            
	                            $iOpidAns = $aRowMultiOpt[0];
	                            /* this query return write answer for each question */
	                            $sQueryRightMultiple = "select count(b.multi_op_id) 
	                                                    from multi_option_table as b , question_table as c
	                                                    where b.multi_op_ans  = 1 
	                                                    AND b.que_id = c.que_id 
	                                                    AND c.test_id = {$iTestId}
	                                                    AND c.que_id = {$iQue_id}
	                                                    AND b.multi_op_id = $iOpidAns";
	                            $iResultMultiRight = $mysqli->query($sQueryRightMultiple);
	                            $aRowFetchRightAns = $iResultMultiRight->fetch_row();   
	                            /* end of multi choice */
	                            if($aRowFetchRightAns[0] >= 1)
	                            {
	                                 echo "<div>";
	                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
	                                 
	                                 echo "<input class='myClass' checked='checked' disabled='disabled' type ='checkbox' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
	                                 echo "</span>";
	                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $aRowMultiOpt[1] . "</span>";
	                                 echo "<span id= 'idSpanRightImg' class='classRightWrongIcon'><img  class='classAddIcon' src='images/right.jpg'></span>";
	                                 echo "</div>";

	                            }
	                            else
	                            {
	                                 echo "<div>";
	                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
	                                 
	                                 echo "<input class='myClass' type ='checkbox' disabled='disabled' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
	                                 echo "</span>";
	                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $aRowMultiOpt[1] . "</span>";
	                                 echo "</div>";
	                            }
	                            
	                        
	                            $iChk++;
				 		}
				 		
	                    if($iResultDispUserAns == true)
	                    {   
	                        echo "<div id='idDivDispUsersAns' class = 'classDivDispUsersAns classFont16'>";
	                        if($iCountUserMultiChAns >=1 )
                        	{
                        		
                        		echo "<span>User Answer :- </span>";
		                        while ($aFetchUserAllAns = $iResultDispUserAns ->fetch_row())
		                        {
		                        	
		                            $iUserAnsID = $aFetchUserAllAns[3];
		                            $sUserAnsTxt = $aFetchUserAllAns[4];
		                            //$iQuestionMark = $aFetchUserAllAns[5];

		                            /* This query returns user right answer from multiple choice */
		                            $sQuerySelctUserRightAns = "select count(b.multi_option)
		                                                        from stud_ans_table as a , multi_option_table as b , question_table as c
		                                                        where a.user_ans_answer = b.multi_op_id 
		                                                        AND b.que_id = c.que_id 
		                                                        AND b.multi_op_ans = 1
		                                                        AND a.user_id = {$iUserId} 
		                                                        AND a.test_id = {$iTestId}
		                                                        AND c.que_id = {$iQue_id}
		                                                        AND b.multi_op_id = {$iUserAnsID}";
		                            $iResultSelctUserRightAns = $mysqli->query($sQuerySelctUserRightAns);
		                            
		                            $aRowFetchUserRightAns = $iResultSelctUserRightAns->fetch_row();
		                            if($aRowFetchUserRightAns[0] >= 1 )
		                             {
		                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
		                                 echo "<input class='myClass' type ='checkbox' checked='checked' disabled='disabled' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
		                                 echo "</span>";
		                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $sUserAnsTxt . "</span>";
		                                 echo "<span id= 'idSpanRightImg' class='classRightWrongIcon'><img  class='classAddIcon' src='images/right.jpg'></span>";
		                                 //echo "<input type='text' name='txtAutoCal{$iQue_id}' value='{$iQueMarks}'/>";
		                            }
		                            else
		                            {
		                                 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>";
		                                 echo "<input class='myClass' type ='checkbox' checked='checked' disabled='disabled' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >";
		                                 echo "</span>";
		                                 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $sUserAnsTxt . "</span>";
		                                 echo "<span id= 'idSpanRightImg' class='classRightWrongIcon'></span>";
		                            }
		                            

		                        }

		                    }
		                    else
		                    {
		                    	echo "<div class='classFont16'>
                                     <p>User Answer :- Not attempted.</p>                       
                                </div>";
		                    }
	                        echo "</div>";
   
	                    }

	                    
	                    
	                    /* This query returns mrks for multiple choice question */
	                    $sQueryMulChoceMark ="select des_marks from des_marks_table where user_id={$iUserId} and test_id={$iTestId} and que_id={$iQue_id}";
						$bDesMarksMultiChoice =$mysqli->query($sQueryMulChoceMark);
						if($bDesMarksMultiChoice==True)
						{
							$aRowMultiChMark =$bDesMarksMultiChoice->fetch_row();
							$iMarkObtMultiCh = $aRowMultiChMark[0];
						}
						else
						{
							$iMarkObtMultiCh = "";
						}	
                		if($iCountUserMultiChAns >=1 )
                        {

	                        /* This query returns number of right attempted question from multiple choice question*/
	                        $sQuerySelctUserNumRightQue = "select DISTINCT count(b.multi_option)
														from stud_ans_table as a , multi_option_table as b , question_table as c
														where a.user_ans_answer = b.multi_op_id 
														AND b.que_id = c.que_id 
														AND b.multi_op_ans = 1
														AND c.que_type LIKE '%Multiple%'
	                                                    AND a.user_id = {$iUserId} 
	                                                    AND a.test_id = {$iTestId}
	                                                    AND c.que_id = {$iQue_id}";
	                        $iResultSelctUserNumRightQue = $mysqli->query($sQuerySelctUserNumRightQue);
	                        
	                        $aRowFetchUserNumRightQue = $iResultSelctUserNumRightQue->fetch_row();
		                    if($aRowFetchUserNumRightQue[0] >=1)
		                    {
			                     echo "<div class='classFont16 classDivInputMarkManual'>			 
								 Marks :<input type='text' name='desAns{$iCDes}'  value='{$aRow[4]}' size='2' class='classInputDescMarks validate[required,custom[number],max[$aRow[4]]] text-input' id='idMarkObt{$iCDes}'/> out of {$aRow[4]}
								 		<input type='hidden' name='desQueId{$iCDes}' value='{$iQue_id}' />
								 		<input type='hidden' value='{$aRow[4]}' class='' id='idDesAns{$iCDes}'/>
										</div>";
		                    	
		                    }
		                    else
		                    {
			                     echo "<div class='classFont16 classDivInputMarkManual'>					 
								 Marks :<input type='text' name='desAns{$iCDes}'  value='{$iMarkObtMultiCh}' size='2' class='classInputDescMarks validate[required,custom[number],max[$aRow[4]]] text-input' id='idMarkObt{$iCDes}'/> out of {$aRow[4]}
								 		<input type='hidden' name='desQueId{$iCDes}' value='{$iQue_id}' />
								 		<input type='hidden' value='{$aRow[4]}' class='' id='idDesAns{$iCDes}'/>
										</div>";
		                    }	

						}
	                        /* end of [5 */
	                    echo "</div>";
	                    $iMulti++;

				 	}					
				 ///////////////

						$iCDes++;
				 }
				
			$sQueryAns = "select op_id from option_table where op_correct_ans = 1 and que_id = $iQue_id";
			$iResult3 = $mysqli->query($sQueryAns);
			$aRowForAns = $iResult3->fetch_row();						
			
			echo "</div><br/>";
			echo "</div> ";			
			$ii++;
			echo "</div>";
		}
		//echo "<input type='text' name='txtAutoCal{$iQue_id}' value='{$iCountWrightMultipleQuestion}'/>";
		/* end of 1 */

		/* This query gives the user resume name from database */
	    $sQueryForResume = "select a.filename  
	                             from user_resume as a
	                             where a.user_id = $iUserId
	                             AND a.test_id = $iTestId limit 1";
	    $iResultForResume = $mysqli->query($sQueryForResume);
	    $aRowForUserResume = $iResultForResume->fetch_row();
	    $sUserResume = $aRowForUserResume[0];

	    echo "<input type='submit' value='Update Result' class='btn btn-primary classAddTestButtonSize' id='idTRPCancelButton'/>";	

	    if($sMsg=='tw')
	    {
	    	echo "<span class='classNoDecoration'><a href='manageResult.php?id={$iTestId}' class='classCancel' id='idTestResultPreview'><input type='button' value='Back' class='btn btn-primary classAddTestButtonSize' id='idTRPCancelButton'/></a></span>";
	    }
	    else
	    {
	    	echo "<span class='classNoDecoration'><a href='manageUserTest.php?uid={$iUserId}' class='classCancel' id='idTestResultPreview'><input type='button' value='Back' class='btn btn-primary classAddTestButtonSize' id='idTRPCancelButton'/></a></span>";	
	    }
	    echo "<input type='hidden' name='totalDesQue' value='{$iCDes}'/>";
	echo "</form>";   
?>				
			</div>		
             </div>
		</div>
	</div>
</div>	

  </div> <!-- end of content wrapper -->
 <!--<div id="idDivButtons" class="classFixButtonOnEditTest">
 </div>-->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>
