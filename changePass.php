<?php 
include('classConnectQA.php');
	session_start();
		$iUserId = $_SESSION['user_id'];
        $ut=$_SESSION['ut'];
        
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }

     $sOldPass = addslashes($_POST['oldpass']);
     $sNewPass = addslashes($_POST['Password']);
     

     $sQuerySelectPass = "select password from login where login_id = {$iLoginId} limit 1";
		$iResultSelectPass = $mysqli->query($sQuerySelectPass); 	/* This query displayes the question from database */
		$aSelectPass = $iResultSelectPass->fetch_row();
		$sUserOldPass = $aSelectPass[0];
		/* this function decrypt password */
		function fnDecrypt($sValue, $sSecretKey)
		{
		    return rtrim(
		        mcrypt_decrypt(
		            MCRYPT_RIJNDAEL_256, 
		            $sSecretKey, 
		            base64_decode($sValue), 
		            MCRYPT_MODE_ECB,
		            mcrypt_create_iv(
		                mcrypt_get_iv_size(
		                    MCRYPT_RIJNDAEL_256,
		                    MCRYPT_MODE_ECB
		                ), 
		                MCRYPT_RAND
		            )
		        )
		    );
		}

		$sKey = "quizapplication";
		$sDecPass = fnDecrypt($sUserOldPass, $sKey);

		/* encrypt password using base 64 encode */
		function fnEncrypt($sValue, $sSecretKey)
		{
		    return rtrim(
		        base64_encode(			/* base64_encode — Encodes data with MIME base64 */				
		            mcrypt_encrypt(		/* mcrypt_decrypt — Decrypts crypttext with given parameters */
		                MCRYPT_RIJNDAEL_256,
		                $sSecretKey, $sValue, 
		                MCRYPT_MODE_ECB, 
		                mcrypt_create_iv(	/* mcrypt_create_iv — Creates an initialization vector (IV) from a random source */
		                    mcrypt_get_iv_size(	/* mcrypt_get_iv_size — Returns the size of the IV belonging to a specific cipher/mode combination */
		                        MCRYPT_RIJNDAEL_256, 
		                        MCRYPT_MODE_ECB		/* One of the MCRYPT_MODE_modename constants, or one of the following strings: "ecb", "cbc", "cfb", "ofb", "nofb" or "stream". */
		                    ), 
		                    MCRYPT_RAND)
		                )
		            )
		        );
		}
		
		$sKey = "quizapplication";
		
		$sNewPasswordEncrypted = fnEncrypt($sNewPass, $sKey);

		/* end of encrypt */
	if($sDecPass == $sOldPass)
	{
	     $sQueryIns = "UPDATE `login` SET `password`='{$sNewPasswordEncrypted}'
	     				WHERE  `login_id`={$iLoginId} AND `password` = '{$sUserOldPass}' LIMIT 1";
		 $iResult2 = $mysqli->query($sQueryIns);
		 /* This query update the user information */

		 $iMsg = 824;
		 header("Location:changePassword.php?msg={$iMsg}");
	}
	else
	{
		$iMsg = 825;
		 header("Location:changePassword.php?msg={$iMsg}");
	}

?>