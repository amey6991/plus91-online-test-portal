<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php 

  include('classConnectQA.php');
    session_start();
    if(isset($_SESSION['user_id']))
        {
          $iUserId = $_SESSION['user_id'];
        }
        else
        {
          $iUserId ="";
        }
        if(isset($_SESSION['ut']))
        {
          $ut=$_SESSION['ut'];
        }
        else
        {
          $ut="";
        }
        if(isset($_SESSION['lid']))
        {
            $iLoginId = $_SESSION['lid'];
        }
        else
        {
            header("location:index.php");
        }
        if(isset($_GET['usid']))
        {
          $iUser=$_GET['usid'];
        }
        else
        {
            $iUser=Null;
        }
    /* This query fetches user profile details from userd_details table */
      if($iUser==Null) // only for user
        {
          $sQueryUserInfo = "select * from user_details where login_id = {$iLoginId}";    
        }
        else
        {
          $sQueryUserInfo ="select * from user_details where user_id = {$iUser} limit 1";
        }       
       
    $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
    $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        
 ?>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Update Profile</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<script src='js/fileSecur.js'></script>     <!-- javascript for file validation -->
<script type= "text/javascript" src = "js/countries.js"></script><!-- js for country and state list -->
<!--Calender script -->
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.2/themes/smoothness/jquery-ui.css" />
<script src="js/jquery-1.9.1.js"></script>

<!--start of javascript to validate the textbox -->
    <link rel="stylesheet" href="css/validationEngine.jquery.css" type="text/css"/>   
    <script src="js/jquery-1.8.2.min.js" type="text/javascript">
    </script>
    <script src="js/languages/jquery.validationEngine-en.js" type="text/javascript" charset="utf-8">
    </script>
    <script src="js/jquery.validationEngine.js" type="text/javascript" charset="utf-8">
    </script>
<script>
        jQuery(document).ready(function(){
            // binds form submission and fields to the validation engine
            jQuery("#idFormUpdateProfile").validationEngine();
        });
      /*end of javascript to validate the textbox*/
    
    /* start of Script For datepicker */
    
   $(document).ready(function() {
// get the current date
var date = new Date();
var m = date.getMonth(), d = date.getDate(), y = date.getFullYear();
// Disable all dates till today
$('#idInputDob').datepicker({
changeMonth: true, changeYear: true, yearRange: '-50:+0',
dateFormat: "yy-mm-dd",
});
}); 
    /* end of Script For datepicker */
</script>

 <!-- Upload file using ajax -->
 
  <script type="text/javascript" src="js/ajaxfileupload.js"></script>
  <script type="text/javascript">
  function ajaxFileUpload(val)
  {
    $("#loading")
    .ajaxStart(function(){
      $(this).show();
    })
    .ajaxComplete(function(){
      $(this).hide();
    });

    $.ajaxFileUpload
    (
      {
        url:"uploadResume.php?usid=<?php echo $iUserId; ?>",
        secureuri:false,
        fileElementId:'fileToUpload',
        dataType: 'json',
        data:{name:'logan', id:'id'},
        success: function (data, status)
        {
          if(typeof(data.error) != 'undefined')
          {
            if(data.error != '')
            {
              alert(data.error);
              document.getElementById('idResponceMsg').innerHTML="Try Again, Resume failed to upload";
            }else
            {
              
              alert("File Uploaded Successfully.");
              document.getElementById('idBrowseFileName').value=val;
              document.getElementById('idResponceMsg').innerHTML="Resume uploaded successfully";
            }
          }
        },
        error: function (data, status, e)
        {
          alert(e);
        }
      }
    )
    
    return false;

  }
  </script>
 <!-- end of ajax-->
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
<script>
$(function()
{
  $('.userconno').change(function()
  {
    var Contact=$(this).val();
    Contact=trim(Contact);
    if(Contact!='')
    {
      $('.checkContact').show();
      $('.checkContact').fadeIn(400).html('<img src="images/ajax-loading.gif" /> ');

      var dataString = 'Contact='+ Contact;
      $.ajax
      ({
            type: "POST",

            url: "<?php 
            if(isset($_GET['usid']))
            {
              echo 'checkContact.php?id={$iUser}';
            }
            else
            {
              echo 'checkContact.php';
            }
            ?>",
            data: dataString,
            cache: false,
            success: function(resultContact)
            {
               var resultContact=trim(resultContact);
               if(resultContact=='')
               {
                  /*Below code is use to show Available status only when Valid email id is entered and Email does not exists */
                  var filter =  /^[0-9]{10,13}$/; 
                  if (!filter.test(document.getElementById("idInputConno").value))
                  {                
                     $('.checkContact').html('Invalid Contact No.');
                     $('#idUpdateProfileSubmit').attr('disabled', 'disabled');
                     $(".Contact").removeClass("white");
                     $(".Contact").addClass("red");
                     $("#idDivDisplayContactAvailability").addClass("classNotAvailable");             
                  }
                  else
                  {
                     $('.checkContact').html("Available");
                     $("#idDivDisplayContactAvailability").addClass("classAvailableText");
                     $("#idDivDisplayContactAvailability").removeClass("classNotAvailable");
                     document.getElementById("idUpdateProfileSubmit").disabled = false;
                     $(".Contact").removeClass("red");            
                  }                         
               }
               else
               { 
                var existingno = <?php echo $aRowForUserInfo[5] ?>;
                 if(existingno == Contact)
                 {
                    document.getElementById("idUpdateProfileSubmit").disabled = false;
                    $('.checkContact').html('No change.');
                 }
                 else
                 {
                 $('.checkContact').html('This Contact is already registered');
                 $('#idUpdateProfileSubmit').attr('disabled', 'disabled');
                 $(".Contact").removeClass("white");
                 $(".Contact").addClass("red");
                 $("#idDivDisplayContactAvailability").addClass("classNotAvailable");
               }
               }
            }
      });

   }
   else
   {
       $('.checkContact').html('');
   }
  });
});
function trim(str){
     var str=str.replace(/^\s+|\s+$/,'');
     return str;
}
</script>
<!-- script for text counter in text area --
<script type="text/javascript" src="http://www.google.com/jsapi"></script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
      
    var characterLimit = 500;  
      
    google.setOnLoadCallback(function(){  
          
     
          
        $('#idInputAddress').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
              
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            }  
              
            var charactersRemaining = characterLimit - charactersUsed;  
              
            
        });  
    });  
</script>
<script type='text/javascript' language='javascript'>  
    google.load('jquery', '1.4.2');  
      
    var characterLimit = 1000;  
      
    google.setOnLoadCallback(function(){  
          
      
          
        $('#idInputAbout').bind('keyup', function(){  
            var charactersUsed = $(this).val().length;  
              
            if(charactersUsed > characterLimit){  
                charactersUsed = characterLimit;  
                $(this).val($(this).val().substr(0, characterLimit));  
                $(this).scrollTop($(this)[0].scrollHeight);  
            }  
              
            var charactersRemaining = characterLimit - charactersUsed;  
              
           
        });  
    });  
</script>-->
</head>
<body onload="print_state('state',this.selectedIndex,'<?php echo $aRowForUserInfo[10]; ?>">

<div id="id_header_wrapper">
  <div id="id_header">
   	<div id="site_logo">
  <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>	
  </div>
    <div id="id_menu">
          <div id="id_menu_left">
        <div id="idDivUserNameTop" class="classDivTopMenuUser">
          <?php 
          if($iUser==Null)  // start :for admin and tester and editing their own profile.
                        {
                          echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                        <ul id='menu'>
                                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                  <ul>
                                      <li>
                                        <a href='profile.php'>Profile</a>   
                                      </li>
                                      <li>
                                        <a href='profileedit.php'>Update Profile</a>      
                                      </li>
                                      <li>
                                        <a href='changePassword.php'>Change Password</a>      
                                      </li>
                                    </ul>
                                </li>
                                <li>
                                  <a href='manageTest.php'>Home</a>   
                                </li>";
                                
                                  if($ut==0)
                                {
                                  echo "
                                      <li><a >Opportunity</a>
                                        <ul>
                                          <li>
                                              <a href='opportunityHTML.php'>Create</a>       
                                          </li>
                                          <li>
                                              <a href='showOpportunity.php'>Manage</a>            
                                          </li>
                                        </ul>
                                    </li>
                                    <li>
                                    <a>Create</a>
                                    <ul>
                                          <li>
                                            <a href='groupHTML.php'>Create Group</a>    
                                          </li>
                                      <li>
                                        <a href='addTestHTML.php'>Create Test</a>       
                                      </li>
                                      <li>
                                        <a href='addUserHTML.php'>Create User</a>      
                                      </li>
                                      <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>      
                                      </li>
                                      
                                    </ul>
                                  </li>";
                                }
                                if($ut == 2)
                                {
                                  echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
                                } 
                                
                               if($ut==1)
                               {
                                  echo"
                                <li>
                                  <a href='showOpportunity.php'>Opportunity</a>
                                </li>
                                <li>
                                    <a href='displayStudentResult.php'>Result</a>       
                                </li>";
                               }
                               else{
                                  echo "<li>
                                      <a>Manage </a>  
                                          <ul>
                                           <li>
                                              <a href='manageGroup.php'>Manage Group</a>      
                                                </li>
                                            <li>
                                              <a href='manageUser.php'>Manage User</a>      
                                            </li>

                                            <li>
                                            <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                            </li>
                                        </ul> 
                                    </li>";
                               }                 
                                
                                echo "<li>

                                      <a href='logout.php'>Logout </a>  

                                    </li>
                                    </ul>
                                    </div>"; 
                                  
                        }  // End :for admin and tester and editing their own profile.
                        else
                        {    // Start : For AAdmin and Tester user but for editing user profile not self profile.            
                          $sSUserQuery="select a.user_full_name from user_details as a, login as b where b.login_id={$iLoginId} and a.login_id={$iLoginId}";

                          $bSUserResult=$mysqli->query($sSUserQuery);
                          $aSURow=$bSUserResult->fetch_row();                      
                          echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                       <ul id='menu'>
                                <li><a href='profile.php'>$aSURow[0]</a>
                                  <ul>
                                    <li>
                                      <a href='profile.php'>Profile</a>   
                                    </li>
                                    <li>
                                      <a href='profileedit.php'>Update Profile</a>      
                                    </li>
                                    <li>
                                      <a href='changePassword.php'>Change Password</a>      
                                    </li>
                                  </ul>
                                
                                </li>
                                <li>
                                  <a href='manageTest.php'>Home</a>   
                                </li>";
                                
                                if($ut==0)
                                {
                                  echo "
                                  <li><a >Opportunity</a>
                                        <ul>
                                          <li>
                                              <a href='opportunityHTML.php'>Create</a>       
                                          </li>
                                          <li>
                                              <a href='showOpportunity.php'>Manage</a>            
                                          </li>
                                        </ul>
                                    </li>
                                    <li>
                                    <li>
                                    <a>Create</a>
                                    <ul>
                                      <li>
                                        <a href='groupHTML.php'>Create Group</a>    
                                      </li>
                                      <li>
                                        <a href='addTestHTML.php'>Create Test</a>       
                                      </li>
                                      <li>
                                        <a href='addUserHTML.php'>Create User</a>      
                                      </li>
                                      <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>      
                                      </li>
                                    </ul>
                                  </li>";
                                }
                                else
                                {
                                  // no access to user and tester user
                                } 
                               if($ut==1)
                               {

                               }
                               else{
                                  echo "<li>
                                      <a>Manage </a>  
                                          <ul>
                                             <li>
                                              <a href='manageGroup.php'>Manage Group</a>      
                                                </li>
                                            <li>
                                              <a href='manageUser.php'>Manage User</a>      
                                            </li>
                                           <li>
                                            <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                            </li>
                                        </ul> 
                                    </li>";
                               }                 
                                
                                echo "<li>

                                      <a href='logout.php'>Logout </a>  

                                    </li>
                                    </ul>
                                    </div>"; 
                        }
          ?>
        </div>
      </div>    
    </div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
    <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
        </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php 
  if(isset($_GET['msg']))
  {
    $iMsg = $_GET['msg'];
    echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
    if ($iMsg == 2490)    
      {
        echo "<div class=classMsg >Selected file is too big , You can upload file up to 2 MB.</div>";
      }
      if ($iMsg == 420)    
      {
        echo "<div class=classMsg >Invalid data.</div>";
      }
      echo "</div>";
  }
?>
 <form id="idFormUpdateProfile" onsubmit="return validateForm();" action="updateprofile.php?usid=<?php echo $iUser; ?>" class="formular" enctype="multipart/form-data" method="post">
<div id="id_content_wrapper">
	<div id="id_content">
        <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDiv">

             
				<div id="idDivSignUp" class="header_0345"><?php echo $aRowForUserInfo[2]; ?> Profile </div>
        <div class="classHorizHRSubHead"></div>
            </div>
            <br/>
				<div id="idDivUserfullName" class="classDisplayProfile">
	               <span id="idSpanFullName" class="classSpanUserInfoTxt"> Full Name<span class="classRed">*</span> :</span>
                   <span id="idSpanFNDb" class="classSpanUserDetailInput"> 
                    <input type="text" id="idInputFullName" name="fullname" class="classUserDtInput validate[required,custom[onlyLetterSp]] text-input" value="<?php echo $aRowForUserInfo[2]; ?>"/>
                   </span>
				</div>
                <div id="idDivDob" class="classDisplayProfile">
                   <?php 
                      if($aRowForUserInfo[6] == 0000-00-00)
                        {
                            echo "<span id='idSpanDOB' class='classSpanUserInfoTxt'> DOB :</span>
                                   <span id='idSpanDOBDb' class='classSpanUserDetailInput'> 
                                   <span class='input-prepend'>
                                   <span class='add-on'>
                                  <i class='icon-calendar'></i>
                                  </span>
                                   <input type='text' id='idInputDob' name='userdob' class='classContactNoUpProf validate[custom[date]] text-input'/>
                                  </span>
                                  </span>";
                        }
                        else
                        {
                            echo "<span id='idSpanDOB' class='classSpanUserInfoTxt'> DOB<span class='classRed'>*</span> :</span>
                                   <span id='idSpanDOBDb' class='classSpanUserDetailInput'> 
                                   <span class='input-prepend'>
                                   <span class='add-on'>
                                  <i class='icon-calendar'></i>
                                  </span>
                                   <input type='text' id='idInputDob' name='userdob' class='classContactNoUpProf validate[custom[date]] text-input'/>
                                  </span>
                                  </span>
                                  </span>";
                        }
                    ?>
                   
                </div>
                <div id="idDivContactNo" class="classDisplayProfile">
                   <span id="idSpanConNo" class="classSpanUserInfoTxt"> Mobile No.<span class='classRed'>*</span> :</span>
                   <span id="idSpanConNoDb" class="classSpanUserDetailInput">
                  <span class="input-prepend">
                  <span class="add-on">
                  +91
                  </span> 
                    <input type="text" id="idInputConno" name="userconno" maxlength="10" class="classContactNoUpProf validate[required,minSize[10]] text-input userconno" value="<?php echo $aRowForUserInfo[5]; ?>"/>
                   </span>
                 </span>
                <span class="checkContact classDivUserNameAvailableProfile" id="idDivDisplayContactAvailability" >
                </span>
                </div>
                <div id="idDivUserAddress" class="classDisplayProfile">
                   <span id="idSpanAddress" class="classSpanUserInfoTxt"> Address :</span>
                   <span id="idSpanAddressDb" class="classSpanUserDetailInput">
                    <textarea id="idInputAddress" name="useraddress" class="classSpanUserAddress" ><?php echo $aRowForUserInfo[7]; ?></textarea>
                   </span>
                </div>
                <div id="idDivUserCity" class="classDisplayProfile">
                   <span id="idSpanCity" class="classSpanUserInfoTxt"> City :</span>
                   <span id="idSpanCityDb" class="classSpanUserDetailInput">
                    <input type="text" id="idInputCity" name="usercity" class="classUserDtInput validate[custom[onlyLetterSp]] text-input" value="<?php echo $aRowForUserInfo[8]; ?>"/>
                   </span>
                </div>
                <div id="idDivUserDist" class="classDisplayProfile">
                   <span id="idSpanDist" class="classSpanUserInfoTxt"> District :</span>
                   <span id="idSpanCityDb" class="classSpanUserDetailInput">
                    <input type="text" id="idInputDist" name="userdist" class="classUserDtInput validate[custom[onlyLetterSp]] text-input" value="<?php echo $aRowForUserInfo[9]; ?>"/>
                   </span>
                </div>
               
                <div id="idDivUserCountry" class="classDisplayProfile">
                   <span id="idSpanCountry" class="classSpanUserInfoTxt"> Country :</span>
                   <span id="idSpanCountryDb" class="classSpanUserDetailInput">                  
                    <select onchange="print_state('state',this.selectedIndex,'<?php echo $aRowForUserInfo[10]; ?>');" id="country" name ="usercountry" class="classStateInputSelect">
                      <?php 
                        if($aRowForUserInfo[11] != null)      // If Country not available in user_details then it show null.
                        {
                          $sCountry=$aRowForUserInfo[11];
                        }
                        else
                        {
                          $sCountry="Select Country";
                        }

                      ?>
                        <!--<option value="<?php echo $sCountry; ?>"><?php echo $sCountry; ?> </option>-->
                    </select>
                     
                    </span>

                </div>
                 <div id="idDivUserState" class="classDisplayProfile">
                   <span id="idSpanState" class="classSpanUserInfoTxt"> State :</span>
                   <span id="idSpanStateDb" class="classSpanUserDetailInput"> 
                    <?php 
                      if($aRowForUserInfo[11] != Null || $aRowForUserInfo[10] != Null)
                      {
                        $sState=$aRowForUserInfo[10];       // if country and State available.
                      }
                      else
                      {
                        if($aRowForUserInfo[11] != Null && $aRowForUserInfo[10] == Null)
                        {
                            $sState="Select State";       // if country available.
                        }
                        else
                        {
                            $sState=null;       // When Country is also not available.
                        }
                       
                      }
                      if($aRowForUserInfo[10] == Null || $aRowForUserInfo[10] == '')
                      {
                        $sState="Select State";
                      }
                    ?>
                    <select name ="userstate" id ="state" class="classStateInputSelect">
                      <option value="<?php echo $sState; ?>"><?php echo $sState; ?></option>
                        <script language="javascript">print_country("country","<?php echo $aRowForUserInfo[11]; ?>");</script>
                    </select>                                        
                
                    </span>
                </div>
                <div id="idDivUserTwtLink" class="classDisplayProfile">
                   <span id="idSpanTwtLink" class="classSpanUserInfoTxt"> Twiter profile link :</span>
                   <span id="idSpanTwtLinkDb" class="classSpanUserDetailInput">
                    <input type="text" id="idInputTwtLink" name="userTwtLink" class="classUserDtInput validate[custom[url]] text-input" value="<?php echo $aRowForUserInfo[15]; ?>"/>
                   </span>
                </div>
                <div id="idDivUserlinkdinlink" class="classDisplayProfile">
                   <span id="idSpanlinkdinlink" class="classSpanUserInfoTxt"> Linkdin profile link :</span>
                   <span id="idSpanlinkdinlinkDb" class="classSpanUserDetailInput">
                    <input type="text" id="idInputlinkdinlink" name="userLinkdnLink" class="classUserDtInput validate[custom[url]] text-input" value="<?php echo $aRowForUserInfo[16]; ?>"/>
                   </span>
                </div>
                <div id="idDivUserGithublink" class="classDisplayProfile">
                   <span id="idSpanGithublink" class="classSpanUserInfoTxt"> Github profile link :</span>
                   <span id="idSpanGithublinkDb" class="classSpanUserDetailInput">
                    <input type="text" id="idInputGithublink" name="userGitLink" class="classUserDtInput validate[custom[url]] text-input" value="<?php echo $aRowForUserInfo[17]; ?>"/>
                   </span>
                </div>
                <div id="idDivUserOwnWeb" class="classDisplayProfile">
                   <span id="idSpanOwnWeb" class="classSpanUserInfoTxt"> Website :</span>
                   <span id="idSpanOwnWebDb" class="classSpanUserDetailInput">
                    <input type="text" id="idInputOwnWeb" name="userOwnWeb" class="classUserDtInput validate[custom[url]] text-input" value="<?php echo $aRowForUserInfo[18]; ?>"/>
                   </span>
                </div>
                <div id="idDivUserAbout" class="classDisplayProfile">
                   <span id="idSpanAbout" class="classSpanUserInfoTxt"> About :</span>
                   <span id="idSpanAboutDb" class="classSpanUserDetailInput">
                    <textarea id="idInputAbout" name="userabout" class="classSpanUserAddress" ><?php echo $aRowForUserInfo[14]; ?></textarea>
                   </span>
                </div>
                <?php
                  if($ut==0)
                  {

                  }
                  else
                  {
                   echo"<div id='idDivUploadResume' class='classDisplayProfile'>
                      <span id='idSpanUploadRes' class='classSpanUserInfoTxt'> Select resume :</span>
                       <span id='idSpanUploadInp' class='classSpanUserDetailInput'>";
                  ?>     
                       <!--Start Style for using formatted File tag --> 
                        <style>
                            .idiv{display:inline-block;width:230px;height:30px;}
                            .iidiv1{opacity:0.8;width:230;height:30px;position:absolute;}
                            .iidiv2{opacity:0.0;width:230;height:30px;margin-top: 0px;}
                            .iinputFile{height:40px;width: 50px;margin-top: 0px;}
                            .iinputBut{height:30px;width: 50px;margin-top: -10px;}
                            .iinputTxt{overflow:hidden;font-size:14px;height:28px;width: 150px;border: 1px solid rgb(192, 192, 192);display:inline-block;margin-top: 0px;}
                            
                        </style>
                        <!--END of Style for using formatted File tag --> 
                                  <!--Start HTML Code for using formatted File tag -->
                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                          <div class="idiv"> 
                            <!-- START1 Inner Container1 div which holds textbox and button. -->
                            <div class=iidiv1>  
                              <input type="text" class="iinputTxt classRounded_Radius_5" id="idBrowseFileName" />
                              <input type="button"class="iinputBut but btn btn-primary" value="Browse">
                              </div>              <div class=iidiv2>  
                              <input type="file"class="iinputFile" id="fileToUpload" name="fileToUpload" onchange="return ajaxFileUpload(this.value);">     
                            </div>

                            
                            <!-- END1 Inner Container1 div which holds textbox and button. -->
                            <!-- START2 Inner Container2 div which holds File. -->
                            
                            <!-- END2 Inner Container2 div which holds File. -->
                          </div>
                          <!-- START0 Outer Container div which holds textbox, button and file tag. -->
                        <!--END HTML Code for using formatted File tag -->                        
                 <?php  
                   echo"</span>
                       <span><img id='loading' src='images/loading.gif' style='display:none;'></span>";
                  echo "<span id='idResponceMsg' class='classSmallFontForOpp' ></span>
                   </div>";
                 }
                ?>
                <?php
                 if($ut==0 && $iUser != null)
                { 

                  echo "<div id='idDivChangeUser' class='classDisplayProfile'>
                 <span id='idSpanChangeUTitle' class='classSpanUserInfoTxt'> User Type<span class='classRed'>*</span> :</span>
                   <span id='idSpanChangeUType' class='classSpanUserDetailInput'> 
                      <select id='idSelectChangeUser' class='classUserDtInputSelect class_light_black classFont' name='userType'>";
                        $sUserTypeQuery="select type from login as a, user_details as b where a.login_id=b.login_id and b.user_id={$iUser}";
                        $bUType=$mysqli->query($sUserTypeQuery);
                        $aUTRow=$bUType->fetch_row();                        
                        if($aUTRow[0]==1)
                        {
                          echo "<option selected='selected' value='1'>User</option>";
                          echo "<option value='2'>Tester</option>";
                        }
                        else
                        {                           
                          if($aUTRow[0]==2)
                          {
                            echo "<option selected='selected' value='2'>Tester</option>";  
                              echo "<option value='1'>User</option>";
                          }
                                               
                        }
                        echo "</select></span></div>";
                }
                else
                {

                }
                ?>
                <br/>
                <div id="idDivUserCountry" class="classDisplayProfile">
                   <span id="idSpanCountry" class="classSpanUserInfoTxt"><span class="classRed">*</span> Fields are Mandatory.</span>
                   <span id="idSpanCountryDb" class="classSpanUserDetailInput">
                    
                   </span>
                </div>
                <br/>
                <div id="idDivUserCountry" class="classDisplayProfile">
                  <span id="idSpanUpdateProfile" class="classSpanUserInfoTxt"> &nbsp;</span>
                 <input type="submit" id="idUpdateProfileSubmit" value="Update Profile" class="btn btn-primary classSignUpButtenSize">
                </div>
              </form>
              
        </div>
		</div>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
  <div id="id_footer">
        <div class="section_w180">
          <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
          <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
          <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
    <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
    </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>