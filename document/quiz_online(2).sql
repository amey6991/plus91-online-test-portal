-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 15, 2013 at 03:35 PM
-- Server version: 5.5.31
-- PHP Version: 5.3.10-1ubuntu3.6

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `allot_test`
--

CREATE TABLE IF NOT EXISTS `allot_test` (
  `allot_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primark Key',
  `user_id` int(11) NOT NULL COMMENT 'To Refer the User details',
  `group_id` int(11) NOT NULL COMMENT 'To Refer Group details',
  `test_id` int(11) NOT NULL COMMENT 'To Refer Test Details',
  `allot_status` int(10) NOT NULL,
  PRIMARY KEY (`allot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Allow to allot test individually' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `des_marks_table`
--

CREATE TABLE IF NOT EXISTS `des_marks_table` (
  `des_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `user_id` int(11) NOT NULL COMMENT 'To identify the User',
  `test_id` int(11) NOT NULL COMMENT 'To identify the test',
  `que_id` int(11) NOT NULL COMMENT 'To identify the que',
  `des_marks` varchar(11) NOT NULL COMMENT 'To store the marks score by the user for descriptive question',
  PRIMARY KEY (`des_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table to store the marks score by the user for descriptive question' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `group_table`
--

CREATE TABLE IF NOT EXISTS `group_table` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'unique for group table',
  `group_name` varchar(100) DEFAULT NULL COMMENT 'group name given by admin',
  `group_desc` varchar(500) DEFAULT NULL COMMENT 'description about group',
  `group_doc` varchar(100) DEFAULT NULL COMMENT 'Date Of Creating Group ',
  `group_status` tinyint(1) DEFAULT NULL COMMENT 'Active:(1) or Inactive:(0) the Group',
  `group_extra` varchar(500) DEFAULT NULL COMMENT 'Extra field, future oriented',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='manage the group created by admin' AUTO_INCREMENT=2 ;

--
-- Dumping data for table `group_table`
--

INSERT INTO `group_table` (`group_id`, `group_name`, `group_desc`, `group_doc`, `group_status`, `group_extra`) VALUES
(1, 'Plus91 Fresher Recruitment ', 'Test available for the freshers.', '2013-05-10 08:18:12', 1, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique to value for each  Login',
  `user_name` varchar(100) NOT NULL COMMENT 'user name to login',
  `password` varchar(100) NOT NULL COMMENT 'password to login',
  `type` tinyint(1) NOT NULL COMMENT '1 for user, 0 for admin and 2 for Tester.',
  PRIMARY KEY (`login_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Table to store admin username and password. ' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`login_id`, `user_name`, `password`, `type`) VALUES
(1, 'Admin', 'ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=', 0),
(2, 'laxmikant.killekar@plus91.in', 'ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=', 1);

-- --------------------------------------------------------

--
-- Table structure for table `login_system_info`
--

CREATE TABLE IF NOT EXISTS `login_system_info` (
  `logId` int(20) NOT NULL AUTO_INCREMENT COMMENT 'this field stores unique id for each row in this table',
  `user_id` int(20) NOT NULL COMMENT 'this field stores user id of user',
  `test_id` int(20) NOT NULL COMMENT 'this field stores test id of test that user selected',
  `sys_ip` varchar(100) NOT NULL COMMENT 'this field stores the ip address of user system',
  `sys_os` varchar(100) NOT NULL COMMENT 'this field stores operating system name of users system',
  `sys_browser` varchar(100) NOT NULL COMMENT 'this field stores browser name that user uses to acces our website',
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='this table stores information like IP , operating system , browser of user system.' AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `multi_option_table`
--

CREATE TABLE IF NOT EXISTS `multi_option_table` (
  `multi_op_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'is a primary key',
  `que_id` int(11) NOT NULL COMMENT 'Question Id to maintain the relationship with que table.',
  `multi_option` varchar(1000) NOT NULL COMMENT 'Storing the multiple opption.',
  `multi_op_ans` varchar(100) NOT NULL COMMENT 'Use to know which answer is acceptable or not. Value: 1 is for answer acceptable and 0 depend on evaluator.',
  PRIMARY KEY (`multi_op_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Use to Maintain the multiple choice question' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `opportunity_table`
--

CREATE TABLE IF NOT EXISTS `opportunity_table` (
  `opp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `opp_name` varchar(500) NOT NULL COMMENT 'Opportunity Name',
  `opp_code` varchar(100) NOT NULL COMMENT 'Opportunity Code',
  `opp_des` varchar(5000) NOT NULL COMMENT 'Opportunity Description',
  `opp_dateofopp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Creating Opportunity',
  `opp_status` int(1) NOT NULL COMMENT 'Opportunity Status',
  PRIMARY KEY (`opp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contain the Opportunity Details' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `option_table`
--

CREATE TABLE IF NOT EXISTS `option_table` (
  `op_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique for each option',
  `que_id` int(10) DEFAULT NULL COMMENT 'To identify the question for the option.',
  `op_option` varchar(500) DEFAULT NULL COMMENT 'Contia option values, may be more than two for a same question.',
  `op_correct_ans` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`op_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Manage no. of option for a question.' AUTO_INCREMENT=49 ;

--
-- Dumping data for table `option_table`
--

INSERT INTO `option_table` (`op_id`, `que_id`, `op_option`, `op_correct_ans`) VALUES
(1, 1, 'XYZ', '1'),
(2, 1, 'UVW', '0'),
(3, 1, 'VWX', '0'),
(4, 1, 'WXY', '0'),
(5, 1, 'None of these', '0'),
(6, 4, '99', '0'),
(7, 4, '100', '0'),
(8, 4, '110', '0'),
(9, 4, '120', '1'),
(10, 4, 'None of the above', '0'),
(11, 5, '1/7', '0'),
(12, 5, '2/7', '0'),
(13, 5, '3/7', '0'),
(14, 5, '1', '0'),
(15, 5, 'None of the above', '1'),
(16, 6, '12 or 6', '0'),
(17, 6, '8 or 4', '0'),
(18, 6, '18', '0'),
(19, 6, '18 or 15', '0'),
(20, 6, 'None of the above', '1');

-- --------------------------------------------------------

--
-- Table structure for table `question_attachment`
--

CREATE TABLE IF NOT EXISTS `question_attachment` (
  `attach_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `que_id` int(10) DEFAULT NULL COMMENT 'question id',
  `que_type` varchar(100) DEFAULT NULL COMMENT 'question type',
  `location` varchar(100) DEFAULT NULL COMMENT 'question type',
  `file_name` varchar(500) DEFAULT NULL COMMENT 'question type',
  `file_status` int(11) DEFAULT NULL COMMENT 'question type',
  PRIMARY KEY (`attach_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 COMMENT='this table stores attache file address with any question' AUTO_INCREMENT=9 ;

--
-- Dumping data for table `question_attachment`
--

INSERT INTO `question_attachment` (`attach_id`, `que_id`, `que_type`, `location`, `file_name`, `file_status`) VALUES
(7, 8, 'Descriptive', 'questionAttachMent/', '8_47b87623fe0a3ccf8ba7ba33964a9134.png', 1);

-- --------------------------------------------------------

--
-- Table structure for table `question_table`
--

CREATE TABLE IF NOT EXISTS `question_table` (
  `que_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique question id for each question',
  `test_id` int(10) DEFAULT NULL COMMENT 'Test id to include question in the specified test.',
  `que_question` varchar(500) DEFAULT NULL COMMENT 'Question to be ask.',
  `que_type` varchar(100) DEFAULT NULL COMMENT 'Question either objective or Describtive ',
  `que_marks` varchar(100) DEFAULT NULL COMMENT 'marks per question',
  `que_neg_marks` varchar(100) DEFAULT NULL COMMENT 'Nagative mark for the question.',
  `que_ans_desc` varchar(500) DEFAULT NULL COMMENT 'Answer Description',
  `que_status` tinyint(1) DEFAULT NULL COMMENT 'active:(1) or inactive:(0) ',
  `que_extra` varchar(500) DEFAULT NULL COMMENT 'Extra feld, Future Oriented',
  PRIMARY KEY (`que_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Managing all the question required for test.' AUTO_INCREMENT=22 ;

--
-- Dumping data for table `question_table`
--

INSERT INTO `question_table` (`que_id`, `test_id`, `que_question`, `que_type`, `que_marks`, `que_neg_marks`, `que_ans_desc`, `que_status`, `que_extra`) VALUES
(1, 1, 'Complete the series\r\nBCD, HIJ, OPQ, ......?Select Answer', 'Objective', '4', '', '', 1, ''),
(2, 1, 'Look at this series :\r\nJ14, L16, _ , P20, R22\r\n\r\nWhat number should fill the blank?', 'Descriptive', '4', '', '', 1, ''),
(3, 1, 'Find the least number which when divided by 35, leaves reminder 25, when divided by 45, leaves reminder 35 and when divided by 55 leaves reminder 45.', 'Descriptive', '4', '', '', 1, ''),
(4, 1, 'How many result lies betwen 300 and 500 in which, 4 comes only one time', 'Objective', '4', '', '', 1, ''),
(5, 1, 'What is the probability of getting 53 Friday in a leap year ?', 'Objective', '4', '', '', 1, ''),
(6, 1, 'S1 & S2 are the two sets of parallel lines. The number of lines in S1 is greater than the number of lines in S2, They intersect at 12 points. The number of parallelogram that S1 and S2 may form is', 'Objective', '4', '', '', 1, ''),
(7, 1, 'Write a simple CSS file for a simple three column Website', 'Descriptive', '4', '', '', 1, ''),
(8, 1, 'Given the image of website template shown below. Design this website without using tables such that it works in IE6, IE7, IE8.', 'Descriptive', '4', '', '', 1, ''),
(9, 1, 'What are the advantages and need for having a website for a clinic or a hospital in these days and age ? ', 'Descriptive', '4', '', '', 1, ''),
(10, 1, 'What is the first thing you want to do as a plus91 employee ?', 'Descriptive', '4', '', '', 1, ''),
(11, 1, 'What are the Microsoft projects called in the USA ?', 'Descriptive', '4', '', '', 1, ''),
(12, 1, 'Which type of Doctors are Tech Savvy ?  Give reasons as to why they are. This is in term of specialty.', 'Descriptive', '4', '', '', 1, ''),
(13, 1, 'Explain how to increase interaction using the internet. Feel free to innovate and come up with your own answer.', 'Descriptive', '4', '', '', 1, ''),
(14, 1, 'What tag line do you think is ideal for Plus91 Technologies-An Indian Healthcare IT firm ?', 'Descriptive', '4', '', '', 1, ''),
(15, 1, 'What are the various Operating system you have heard of, list the name and company ?', 'Descriptive', '4', '', '', 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `score_board_table`
--

CREATE TABLE IF NOT EXISTS `score_board_table` (
  `score_board_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'unque id for each score',
  `user_id` int(10) DEFAULT NULL COMMENT 'To identify the user',
  `test_id` int(10) DEFAULT NULL COMMENT 'To idenify the test',
  `sb_doa` datetime DEFAULT NULL COMMENT 'doa: Date Of Attempting test',
  `sb_time_taken` varchar(50) DEFAULT NULL COMMENT 'test start time.',
  `sb_in_time` datetime DEFAULT NULL COMMENT 'test end time',
  `sb_out_time` datetime DEFAULT NULL COMMENT 'test end time',
  `sb_attempt_que` int(10) DEFAULT NULL COMMENT 'no. of question attempt by the user for each test',
  `sb_correct_ans` int(10) DEFAULT NULL COMMENT 'no. of corect answer.',
  `sb_wrong_ans` int(10) DEFAULT NULL COMMENT 'no. of wrong answe.',
  `sb_marks_obt` varchar(100) DEFAULT NULL COMMENT 'marks obtain to the user for each test.',
  `sb_marks_out_of` varchar(100) DEFAULT NULL COMMENT 'out of marks i.e. sum of all question marks in a test',
  `sb_result_status` tinyint(1) NOT NULL,
  `sb_extra` varchar(500) DEFAULT NULL COMMENT 'extra field, future oriented.',
  PRIMARY KEY (`score_board_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Record of all user attempted test.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `share_result`
--

CREATE TABLE IF NOT EXISTS `share_result` (
  `share_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `test_id` int(10) DEFAULT NULL,
  `share_date` datetime DEFAULT NULL,
  `share_status` int(10) DEFAULT NULL,
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='this table stores record of shared result' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stud_ans_table`
--

CREATE TABLE IF NOT EXISTS `stud_ans_table` (
  `user_ans_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id for every answer',
  `user_id` int(10) DEFAULT NULL COMMENT 'User id to identify the user',
  `test_id` int(10) DEFAULT NULL COMMENT 'test id to identify the test',
  `que_id` int(10) DEFAULT NULL COMMENT 'question id to identify the quetion',
  `user_ans_answer` varchar(5000) DEFAULT NULL COMMENT 'answer given by the user.',
  `op_correct_ans` varchar(500) NOT NULL,
  `attempt` int(20) NOT NULL COMMENT 'this field stores the attempt of exam',
  PRIMARY KEY (`user_ans_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Managing the student record of solve  test.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_detail`
--

CREATE TABLE IF NOT EXISTS `test_detail` (
  `test_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique test id for each test',
  `group_id` int(11) DEFAULT NULL COMMENT 'group id to add test in the available group',
  `test_name` varchar(100) DEFAULT NULL COMMENT 'test name given by the admin',
  `test_subject` varchar(100) DEFAULT NULL COMMENT 'test base on the subject',
  `test_desc` varchar(500) DEFAULT NULL COMMENT 'test description',
  `test_doa` date DEFAULT NULL COMMENT 'Date Of Adding Test ',
  `test_from` date DEFAULT NULL COMMENT 'Date of Test  Valid From  ',
  `test_to` date DEFAULT NULL COMMENT 'Date of Test  Valid Till  ',
  `test_duration` int(11) DEFAULT NULL COMMENT 'Time given to solve the test',
  `test_total_que` int(10) DEFAULT NULL COMMENT 'Total no. of question  in the test',
  `test_attempt_limit` int(10) DEFAULT NULL COMMENT 'No. of attempt given for a test to an individual user',
  `test_result_type` varchar(100) DEFAULT NULL COMMENT 'Display result instantly or pending .',
  `test_code` varchar(100) DEFAULT NULL COMMENT 'Code for each test, as a key to start the test. ',
  `test_status` tinyint(1) DEFAULT NULL COMMENT 'active:(1) or inactive (0) test',
  `test_neg_status` int(10) NOT NULL COMMENT 'Use To On or Off a Negative Marks in a test',
  `test_msg` varchar(500) NOT NULL COMMENT 'Message Fr User after completing the test',
  `test_extra` varchar(500) DEFAULT NULL COMMENT 'extra field, future oriented',
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Admin Managing the test detail here. ' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `test_detail`
--

INSERT INTO `test_detail` (`test_id`, `group_id`, `test_name`, `test_subject`, `test_desc`, `test_doa`, `test_from`, `test_to`, `test_duration`, `test_total_que`, `test_attempt_limit`, `test_result_type`, `test_code`, `test_status`, `test_neg_status`, `test_msg`, `test_extra`) VALUES
(1, 1, 'Plus91 Freshers', 'Common', 'Test For Freshers applying in plus91 2013', '2013-05-10', '2013-05-10', '2013-05-13', 60, 15, 1, '1', 'bFACD1', 0, 0, '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique to identify the user',
  `login_id` int(10) NOT NULL COMMENT 'use to identify username and password',
  `user_full_name` varchar(100) DEFAULT NULL COMMENT 'User Full  Name ',
  `user_email` varchar(100) DEFAULT NULL COMMENT 'User Email',
  `user_doreg` date DEFAULT NULL COMMENT 'Date of Registration (i.e. d-o-reg)',
  `user_contact` varchar(200) DEFAULT NULL COMMENT 'User Contact',
  `user_dob` date DEFAULT NULL COMMENT 'User Contact',
  `user_address` varchar(500) DEFAULT NULL COMMENT 'User Contact',
  `user_city` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_dist` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_state` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_country` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_status` tinyint(1) DEFAULT NULL COMMENT 'Active:(1) or Inactive:(0) User',
  `varification_code` varchar(300) DEFAULT NULL COMMENT 'This field stores unique code for each user and after varifying this through email user login activated',
  `user_about` varchar(1000) DEFAULT NULL COMMENT 'this field stores - something about user',
  `user_twt_link` varchar(300) DEFAULT NULL COMMENT 'this field stores - twiter profile link',
  `user_linkdin_link` varchar(300) DEFAULT NULL COMMENT 'this field stores - linkdin profile link of user',
  `user_git_link` varchar(300) DEFAULT NULL COMMENT 'this field stores - git hub link of user profile',
  `user_website` varchar(300) DEFAULT NULL COMMENT 'this field stores - own url of user',
  `user_extra` varchar(500) DEFAULT NULL COMMENT 'extra field',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='This table store User Details.' AUTO_INCREMENT=3 ;

--
-- Dumping data for table `user_details`
--

INSERT INTO `user_details` (`user_id`, `login_id`, `user_full_name`, `user_email`, `user_doreg`, `user_contact`, `user_dob`, `user_address`, `user_city`, `user_dist`, `user_state`, `user_country`, `user_status`, `varification_code`, `user_about`, `user_twt_link`, `user_linkdin_link`, `user_git_link`, `user_website`, `user_extra`) VALUES
(1, 1, 'Admin', 'updateadminemail@admin.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 'laxmi', 'laxmikant.killekar@plus91.in', '2013-05-11', '9876543210', '2013-05-11', '', '', '', '', '', 1, 'e172e202cdc03888b18c56051e37b727', '', '', '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `user_resume`
--

CREATE TABLE IF NOT EXISTS `user_resume` (
  `res_id` int(50) NOT NULL AUTO_INCREMENT COMMENT 'this field stores unique id for every resume location.',
  `user_id` int(50) NOT NULL COMMENT 'this field stores user id of user',
  `test_id` int(50) NOT NULL COMMENT 'This field stores the test id of the test for which he/she apply',
  `location` varchar(500) NOT NULL COMMENT 'this field stores the location and resume  file name',
  `filename` varchar(500) NOT NULL COMMENT 'this field Stores file name',
  `resume_status` int(11) NOT NULL COMMENT 'this field Stores file name',
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table is used to store student resume' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
