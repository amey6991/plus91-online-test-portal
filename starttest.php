<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Online Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- script for text counter in text area -->
<!-- Diplay process widow after submit -->
<script src="js/jquery-1.8.2.min.js"></script>
<script src="js/blackUI/jquery.min.js"></script>
<script src="js/blackUI/jquery-ui.min.js"></script>
<script src="js/blackUI/jquery.blockUI.js"></script>
<script src="js/blackUI/chili-1.7.pack.js"></script>
<script type="text/javascript" src="js/blackUI/jquery.blockUI.js"></script>
<script type="text/javascript"> 
    // unblock when ajax activity stops 
    $(document).ajaxStop($.unblockUI); 
    $(document).ready(function($) { 

         $('#demo10').click(function() { 
        $.blockUI({ 
            message: '<h2>Please wait......!</h2>', 
            timeout: 80000 
        }); 
    }); 
        
    }); 
 
</script>
<!-- Diplay process widow after submit -->

<!-- script to submit form -->
<script>
 function submitform()
    {
      document.QuizDisplay.submit();
    }
</script>
<!-- end of submit form -->
<!-- Script for timer  -->
<script type="text/javascript">
	function display_c(start)
	{
	    window.start = parseFloat(start);
	    var end = 0 // change this to stop the counter at a higher value
	    var refresh=1000; // Refresh rate in milli seconds
	    var flag = 0;
	    if(window.start >= end )
	    {

	        mytime=setTimeout('display_ct()',refresh)
	        if(window.start >= 300 )
	        {
	            var element = document.getElementById("idLabelTimerta");
	            element.style.color='green';
	        }
	        else
	        {
	        	if(window.start >= 180 )
	        	{
	        		var element = document.getElementById("idLabelTimerta");
	            	element.style.color='#FFA500';
	        	}
	        	else
	        	{
        			var element = document.getElementById("idLabelTimerta");
	            	element.style.color='red';
	            	//element.style.text-decoration=line-through;          	
	        	}
	            
	        }

	    }
	    else 
	    {
	        document.QuizDisplay.quizSubmit.click();
	    }
	}

	function display_ct() 
	{
	    // Calculate the number of days left
	    var days=Math.floor(window.start / 86400);
	    // After deducting the days calculate the number of hours left
	    var hours = Math.floor((window.start - (days * 86400 ))/3600)
	    // After days and hours , how many minutes are left
	    var minutes = Math.floor((window.start - (days * 86400 ) - (hours *3600 ))/60)
	    // Finally how many seconds left after removing days, hours and minutes.
	    var secs = Math.floor((window.start - (days * 86400 ) - (hours *3600 ) - (minutes*60)))

	    var x = "Time Left :  " + hours + " Hr " + minutes + " Min " + secs + " Sec ";


	    document.getElementById('idLabelTimerta').innerHTML = x;
	    window.start= window.start- 1;
	    var element = document.getElementById("idLabelTimerta");
	    element.style.color='white';
	    tt=display_c(window.start);

	}
</script>
<!-- end  -->

<!-- Script for checkbox style -->
<link rel="stylesheet" href="js/prettyCheckable/prettyCheckable.css">
<script src="js/prettyCheckable/prettyCheckable.js"></script>
<script>
$.noConflict();
jQuery(document).ready(function($){

  $('input.myClass').prettyCheckable();

});

</script>
<!-- end of Script checkbox style -->


<?php 
		include ('classConnectQA.php');
        session_start();
        $ut=$_SESSION['ut'];
        if(isset($_SESSION['lid']))		// This is Use to check a Session
		{
			$iLoginId = $_SESSION['lid'];
			if(isset($_SESSION['testStartTime']))
			{

			}
			else
			{
				$_SESSION['testStartTime'] = date("y-m-d H:i:s", time());
				$_SESSION['teststarttile2'] = time();
			}
		}
		else
		{
			header("location:index.php");
		}
		
        $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                            from login as a , user_details as b
                            where a.login_id = b.login_id 
                            AND a.login_id  = '$iLoginId' limit 1";
        $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
        $aRowForUserInfo = $iResultForUserInfo->fetch_row();
        $iUserId = $_SESSION['user_id'];
		$iTestid=$_GET['id'];					

		$sQueryCheckTestAttempt = "select distinct user_id , test_id from score_board_table where test_id = $iTestid AND user_id = $iUserId limit 1";
		$iResCheckTestAttempt = $mysqli->query($sQueryCheckTestAttempt);
		$aTestAttempt = $iResCheckTestAttempt->fetch_row();
		$sTestAttemptMsg = "";
		$iTimeLeftInMinutes = 0;
		if($aTestAttempt[1] == $iTestid)
		{
			$sTestAttemptMsg = "This test is already submitted by You.";
		}
		else
		{
			$sTimerQuery = "select test_duration,test_name ,test_attempt_limit from test_detail where test_id= $iTestid limit 1 ";
			$iResTimerQuery = $mysqli->query($sTimerQuery);	/* This query select test data from database*/
			$aRowForTimer = $iResTimerQuery->fetch_row();		
			$iTestAttemptLimit = $aRowForTimer[2];
			$sQuery = "select a.que_question, a.que_id , a.test_id , a.que_type , a.que_marks ,b.test_name , a.que_neg_marks, a.que_ans_desc
						from question_table as a , test_detail as b where b.test_id = a.test_id AND b.test_id = $iTestid";
			$iResult = $mysqli->query($sQuery); 	/* This query displayes the question from database */
			$iQue_Count = $iResult->num_rows;
			

			$sTimeOutMsg ="";
				/* This session use for timer for the test */
			$iTestDurationInSecind = $aRowForTimer[0] * 60;		/* This functin convert minitues to seconds */
			if(isset($_SESSION['test_time']))		/* If page is refrash by the user then this function provide actual remaining time for test */
			{
			      if($_SESSION['TimeRemainForTest'] > 0)			/* If session is previously created then this block is exicuted */
			        {
			            
			            $_SESSION['test_time'];		/* This stores the current time in session variable */
			           
			            $CurrentTime = time();
			           
			            $EntryTime = $_SESSION['test_time'];
			            $Difference = $CurrentTime - $EntryTime;		/* This calculate the differance between entry time and currnt time */
			            
			            $RemTime = $iTestDurationInSecind - $Difference;		
			            $_SESSION['TimeRemainForTest'] = $RemTime;
			            $iTimeLeftInMinutes = ($RemTime);		/* This convert secind to minutes */
			           
			        }
			        else
			        {
				         //session_destroy();			/* If time is over then this destroys all the session */
				         unset( $_SESSION['test_time'] );
				         unset( $_SESSION['TimeRemainForTest'] );

				         $sTimeOutMsg = "Time Over";
			        }
			}
			else
			{
			      $_SESSION['test_time'] = time();
			      $RemTime = $iTestDurationInSecind;
			      $_SESSION['TimeRemainForTest'] = $RemTime;
			      $iTimeLeftInMinutes = floor($RemTime);
			}
		}
		$iTimeinSec = ($iTimeLeftInMinutes);
	?>
	<script type="text/javascript" language="javascript">

        $(function () {

            $(this).bind("contextmenu", function (e) {

                e.preventDefault();

            });

        });

      

    </script>
</head>

<?php 
if($sTestAttemptMsg == "")
{
	echo "<body onload='display_c($iTimeinSec);' class='classNoCopy'>";
}
else
{
	echo "<body  class='classNoCopy'>";
}

?>	
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
					<?php 
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                <ul id='menu'>
                                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href='manageTest.php'>Home</a>       
                                </li>
                                <li>
									<a href='showOpportunity.php'>Opportunity</a>
	                            </li>
								<li>
	                            	<a href='displayStudentResult.php'>Result</a>       
	                        	</li>
                                <li>
                                    <a href='logout.php'>Logout </a>    
                                </li>
                                </ul>
                                </div>";

                    ?>			
				</div>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
 <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->  
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
	<div id="id_content">
		<div id="idDivMiddleBody" class="classDivMiddleBody">
		<form action="AddResult.php"  method="post" name="QuizDisplay" id="idFormQuiz" >

<?php

$iCounterForTextAreaNameJS = 0; 
if($sTestAttemptMsg == "" )
{
	
	echo "<div id='idDiv' class='classDivBodyFormat'>
	<div id='idDivSignUp' class='header_034'>
		<div>Test Name : -$aRowForTimer[1] </div>
		<div id='idSpanTQ' class=''>Total Questions : $iQue_Count</div>
		<div id='idSpanTT' class=''>Time :- $aRowForTimer[0] min</div>";
	
	$sQueryToDisNegMark ="select test_neg_status from test_detail where test_id={$iTestid}";
	$iResultDispNegMark = $mysqli->query($sQueryToDisNegMark);
	if($iResultDispNegMark == true)
	{
		$aFetchDispNegMark = $iResultDispNegMark ->fetch_row();
		if($aFetchDispNegMark[0] == 1)
		{
			echo "<div><strong> <span class='classRed'>*</span> Negative marking applicable. </strong></div>";
		}
		else
		{
			
		}

	}
	echo "</div>
	<input type='hidden' name='test_nm' value='$aRowForTimer[1]'>
	</div>";
	
	echo "<div class='classDivDisplayTime' id='idDivToDisplayTime' >
	<span id='idLabelTimerta' class='classTimertextarea'></span>
	
	</div>
	
	<div class='class_form_div_quiz' id='id_form_div_addTest'>							
	<div id='idDivFname' class='classDivTabForDescrAns'>	
	<div>";
  
	if($sTimeOutMsg =="")
	{
		echo "<span class='classDivBodyFormat'><strong> Questions </strong></span><br/><br/>";
		
		$ii=1;
		$iMulti = 1;
		while($aRow = $iResult->fetch_row())	// this function fetches all question and their option from database
		{
			echo "<div id='idDivDisplayQuestion'class='classDivDisplayQuestion classDivBodyFormat'>";
			
			
			
			//! display data in table
			$iQue_id = $aRow[1];
			$iTest_id = $aRow[2];
			echo "<input type='hidden' name='test_id' value='$iTest_id'/>";
			$sQueType = $aRow[3];
			$iQueMarks = $aRow[4];
			$iNegMeksPerQue = $aRow[6];
			$sQueryQtpFromImg = "select * from question_attachment as a where a.que_id = {$iQue_id} AND file_status = 1";
				/* this query return af question have any attachment */		
			$iResultAttachment = $mysqli->query($sQueryQtpFromImg);
			$aFetchRowAttachment = $iResultAttachment->fetch_row();
			$sQtpInAttachment = $aFetchRowAttachment[1];
			$sAttachLocation = $aFetchRowAttachment[3];
			$sAttachMentName = $aFetchRowAttachment[4];

			echo "<span id='idSpanDispNum' class='classSpanDispBold'>{$ii}. </span>
			<span id='idSpanDisplayQue' class='classSpanDisplayQue'><strong>{$aRow[0]}</strong></span>
					<span id='idSpanDisplayMarks' class='classSpanDiaplayMarks'>[ Marks : {$iQueMarks} ]</span>";			/* This displays question number, Question and Qustion Marks */
			echo "<input type='hidden' name='qid{$ii}' value='$iQue_id'>";
			if($aRow[7] != ''){
				echo "<span><pre>{$aRow[7]}</pre></span>";	
			}
			
			$sQuerySel = "select a.op_id as op_id, a.op_option as opt, a.op_correct_ans as cra from option_table as a ,question_table as b , test_detail as c 
			where c.test_id = b.test_id And b.que_id = a.que_id AND c.test_id ='$iTestid' AND b.que_id ='$iQue_id' order by a.op_id asc";
					/* This query select option related data fetch from option table*/
			$iResult2 = $mysqli->query($sQuerySel);
			
			echo "<div id='idQuestionTab' class='classQuestionTab'>";
			$sQueryTOCheckQueType = "select que_type from question_table where que_id = '$iQue_id' AND test_id = '$iTestid'";
								/* This query select question type from question table */
			$iResultForQueType = $mysqli->query($sQueryTOCheckQueType);
			$aRowForQueType = $iResultForQueType->fetch_row();
				
			$aRowForQueType[0];
				if($aRowForQueType[0] == 'Objective')		/* If Question is objective type then radio butten display by using this function */
				{

					if($sQtpInAttachment == $iQue_id)
						{
							echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'><a href='$sAttachLocation$sAttachMentName'><img src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></a></div>";
						}
					$ij='A';
					while($aRowOpt = $iResult2->fetch_row())
					{
							$iOpidAns = $aRowOpt[0];
							
							 echo "<div>";
							 echo "<span id='idSpanDisplayOption' class='classSpanDisplayOp'>
							 		<input class='myClass' type ='radio' name ='ans{$ii}' value='$iOpidAns' id='op$iOpidAns'></span>";
							  
							 echo "<span id='idSpanDisplayQueTxt' class='classSpanDisplayQueTxt'>" . $aRowOpt[1] . "</span>";
							 echo "</div>";
							 $ij++;
					}
						 
				}
				else
				{
					if($aRowForQueType[0] == 'Descriptive')
					{
						if($sQtpInAttachment == $iQue_id)
							{
								echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'><a href='$sAttachLocation$sAttachMentName'><img src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></a></div>";
							}
					 	echo " <script type='text/javascript' language='javascript'>  
								    google.load('jquery', '1.4.2');  
								      
								    var characterLimit = 1500;  
								      
								    google.setOnLoadCallback(function(){  
								          
								        $('#remainingCharacters').html(characterLimit);  
								          
								        $('#idDescAns{$iCounterForTextAreaNameJS}').bind('keyup', function(){  
								            var charactersUsed = $(this).val().length;  
								              
								            if(charactersUsed > characterLimit){  
								                charactersUsed = characterLimit;  
								                $(this).val($(this).val().substr(0, characterLimit));  
								                $(this).scrollTop($(this)[0].scrollHeight);  
								            }  
								              
								            var charactersRemaining = characterLimit - charactersUsed;  
								              
								            $('#remainingCharacters{$iCounterForTextAreaNameJS}').html(charactersRemaining);  
								        });  
								    });  
								</script>";
						echo "<div class='classSpanDisplayAnsTA'>
						
							<span id='idSpanDisplayAnsTA' class='classSpanDisplayAnsTA'>
							<textarea oncopy='return false' oncut='return false' onpaste='return false' name ='ans{$ii}' id='idDescAns{$iCounterForTextAreaNameJS}' class='classStudentDescAns' row=10 col= 10></textarea>
							<p><span id='remainingCharacters{$iCounterForTextAreaNameJS}' class='classMinFont classMarFont'>1500</span> <span class='classMinFont'> - Characters remaining.</span></p> 
							</span>

						</div>";
						$iCounterForTextAreaNameJS++;
					}
					if($aRowForQueType[0] == 'Multiple')
					{
						echo "<input type='hidden' name='Multiqid{$iMulti}' value='$iQue_id'>";
						//echo "<input type='hidden' name='MultiCh{$iMulti}' value='$iQue_id'>";
						$sQueryMulChoice = "select a.multi_op_id as op_id, a.multi_option as opt, a.multi_op_ans as cra from multi_option_table as a ,question_table as b , test_detail as c 
						where c.test_id = b.test_id And b.que_id = a.que_id AND c.test_id ='$iTestid' AND b.que_id ='$iQue_id'";
								/* This query select option related data fetch from option table*/
						$iResultMulChoice = $mysqli->query($sQueryMulChoice);

						$iChk = 1;
						if($sQtpInAttachment == $iQue_id)
							{
								echo "<div id='idDivDispAttach' class='classDispQuestionAttachment'><a href='$sAttachLocation$sAttachMentName'><img src='$sAttachLocation$sAttachMentName' id='idQueAttachment' class='classQuestionAttachment'/></a></div>";
							}
						while($aRowMultiOpt = $iResultMulChoice->fetch_row())
						{
								$iOpidAns = $aRowMultiOpt[0];
								
								 echo "<div >";
								 echo "<span id='idSpanDisplayChoice' class='classSpanDisplayOp'>
								 		<input class='myClass' type ='checkbox' name ='chkVal{$iQue_id}{$iChk}' value='$iOpidAns' >
								 	  
								 	   </span>";
								 
								 echo "<span id='idSpanDisplayCheckbx' class='classSpanDisplayQueTxt'>" . $aRowMultiOpt[1] . "</span>";
								 echo "</div>";
								 $iChk++;
						}
						$iMulti++;
					}
				}
			
			$sQueryAns = "select op_id from option_table where op_correct_ans = 1 and que_id = $iQue_id";
			$iResult3 = $mysqli->query($sQueryAns);
			$aRowForAns = $iResult3->fetch_row();
			
			//echo "<input type='hidden' name='Correct_ans{$ii}' value='$aRowForAns[0]'>";							
			
			echo "</div><br/><br/>";
			echo "</div>";
			
			$ii++;
		}
		echo "<input type='hidden' name='totalMultiQue' value='$iMulti'>";
		echo "<input type='button' onclick='submitform();'' value='Submit Test' id='demo10' class='btn btn-primary classAddTestButtonSize classSubmitTest demo' name='quizSubmit'>";
		echo "<input type='hidden' name='totalQue' value='$iQue_Count'>";
		//echo "<input type='hidden' name='TimeTaken' id='idHiddenTimer'>";

	}
	else
	{
		echo "<span>$sTimeOutMsg</span><br/><br/></div></div>";

	}
}
else
{
	echo "<div id = 'idDivDisplyAttemptMsg' class='classDivDisplyAttemptMsg'>$sTestAttemptMsg</div>";
}	
//  !close database connection
$mysqli->close();

?>
<div id="domMessage" style="display:none;"> 
    <h1>We are processing your request.  Please be patient.</h1> 
</div> 
	</div>
    </div>
	</div>
 <div id="idDivNat" class="classDivTime">
        <div id="idDivTq" class="classDivRight"></div>	
		<div id="idTT" class="classDivRight"></div>
		
 </div>
</div>
</div></form>
    </div> <!-- end of content wrapper -->
</div> <!-- end of content wrapper -->
<script>
/* This script disable ctrl + s button */	
var isCtrl = false;
document.onkeyup=function(e){
    if(e.keyCode == 17) isCtrl=false;
}

document.onkeydown=function(e){
    if(e.keyCode == 17) isCtrl=true;
    if(e.keyCode == 83 && isCtrl == true) {
        //run code for CTRL+S -- ie, save!
        return false;
    }
}
</script>
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>

