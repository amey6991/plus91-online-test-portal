<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View Test</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<script src="js/jquery-1.8.2.min.js"></script>
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- Script for error message animation -->
<script>
$(document).ready(function(){

    $("#idDivDisplayError").fadeIn(4000);
    $("#idDivDisplayError").fadeOut(5000);
  
});
</script>
<!-- end of error message script -->
<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>
</head>
<body>
	<?php 

		session_start();
		include ('classConnectQA.php');	
		
		if(isset($_SESSION['user_id']))
		{
			$iUid=$_SESSION['user_id'];
		}
		else
		{
			$iUid="";
		}
		if(isset($_SESSION['lid']))		// This is Use to check a Session
		{
			$iLoginId = $_SESSION['lid'];
		}
		else
		{
			header("location:index.php");
		}
		if(isset($_SESSION['ut']))  
        {
            $ut=$_SESSION['ut'];
        }
        else
        {
        	$ut=null;	
        } 	
		$sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
							from login as a , user_details as b
							where a.login_id = b.login_id 
							AND a.login_id  = '$iLoginId' limit 1";
		$iResultForUserInfo = $mysqli->query($sQueryUserInfo);
		$aRowForUserInfo = $iResultForUserInfo->fetch_row();

	?>
<div id="id_header_wrapper">
  <div id="id_header">
    
   	<div id="site_logo">
	<div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
	</div>
        
		<div id="id_menu">
      		<div id="id_menu_left">
				<div id="idDivUserNameTop" class="classDivTopMenuUser">
				<?php
				if($ut==0||$ut==2)
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
									<ul id='menu'>
									<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
									</li>
									<li>
										<a href='manageTest.php'>Home</a>		
									</li>";
									
					
						if($ut==2)
						{
							echo "<li> <a href='showOpportunity.php'>Opportunity</a></li>";
						}
						else
						{
							echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
								<ul>
									<li>
									<a href='groupHTML.php'>Create Group</a>		
									</li>
									<li>
										<a href='addTestHTML.php'>Create Test</a>		
									</li>
									<li>
										<a href='addUserHTML.php'>Create User</a>			
									</li>
									<li>
										<a href='excelReader/index.php'>Bulk Upload</a>			
									</li>
								</ul>
							</li>";
						} 
						
						echo "<li>
									<a>Manage </a>  
									  <ul>
									  		<li>
												<a href='manageGroup.php'>Manage Group</a>			
											</li>
											<li>
												<a href='manageUser.php'>Manage User</a>			
											</li>
										  <li>
												<a href='viewAllotedTestHTML.php'>Assign Test</a>     
											  </li>
									</ul>	
								</li>
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";
					}	
					else
					{
						echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
								<ul id='menu'>
								<li><a href='profile.php'>$aRowForUserInfo[2]</a>
									<ul>
										<li>
											<a href='profile.php'>Profile</a>		
										</li>
										<li>
											<a href='profileedit.php'>Update Profile</a>			
										</li>
										<li>
											<a href='changePassword.php'>Change Password</a>			
										</li>
									</ul>
								</li>
								<li>
									<a href='manageTest.php'>Home</a>		
								</li>
								<li>
									<a href='showOpportunity.php'>Opportunity</a>
	                            </li>
								<li>
	                            	<a href='displayStudentResult.php'>Result</a>       
	                        	</li>
								
								<li>
									<a href='logout.php'>Logout </a>	
								</li>
								</ul>
								</div>";	
						}
					?>		
				
				</div>
			</div>   	
		</div> <!-- end of menu -->
    
    </div>  <!-- end of header -->
<div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div> <!-- end of header wrapper -->

<div id="id_banner_wrapper">
	<div id="id_banner">
        <!--<div id="id_banner_content">
        	<div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div> -->
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->
<?php
					

                    // @file connect to the database.
                    include('classConnectQA.php');
                   if($mysqli->errno)
					{
						//echo "DB Error";
						header('location: manageTest.php?msg=-1');
					}
					else
					{				
						$iTestid=Null;
						$iMsg=Null;
						$iGid=Null;
						if(isset($_GET['id']))	
	                    {
	                    	$iTestid=$_GET['id'];
	                    }
						if(isset($_GET['gid']))	
	                    {
	                    	$iGid=$_GET['gid'];
	                    }
	                    if(isset($_GET['msg']))
	                    {

	                    	$iMsg=$_GET['msg'];
	                    	if($iMsg != "")
	                    	{
	                    	echo "<div id='idDivDisplayError' class='classDisplayMessagesToUser'>";
	                    	if($_GET['msg']==1)
							{
								echo "<div class=classMsg>Test Updated Successfully</div>";
							}
	                    	echo "</div>";
	                    	}
	                    }
						
						

						$sTestQuery = "select test_duration,test_name,test_code,test_from,test_to,test_subject,test_desc, group_id, test_neg_status, test_scope from test_detail where test_id= {$iTestid}";
						$iResTestQuery = $mysqli->query($sTestQuery);
						$aRowTest = $iResTestQuery->fetch_row();
						$sQuery = "select * from question_table where test_id = {$iTestid}";
						$iResult = $mysqli->query($sQuery); 
						$iQue_Count = $iResult->num_rows;
					}	
						?>
  <div id="id_content_wrapper">
	<div id="id_content">
		<div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">
				<div id="idDivSignUp" class="header_0345">

					<div id="idDivEditTest" class="classDivEditTest"><br/>Test Name :- <?php echo $aRowTest[1]; ?>
						<span id="" class="classSpanTabLink classNoDecoration">			                      							
							<a rel="tooltip" data-toggle="tooltip" data-placement="bottom" class="" title="Edit Test" href="editTest.php?id=<?php echo $iTestid; ?>"><i class="icon-pencil"></i> Edit</a>
						</span>
					</div>
				</div>
				<div class="classHorizHRSubHead"></div> 
			</div>
	
				<div class="class_form_div_quiz" id="id_form_div_addTest">

					<div STYLE="line-height: 20px;" id="idDivTabForDescrAns1" class="classDivTabForDescrAns classDivBodyFormat" line>					
							
							
							<div id="idDivNat" class="classDivAddTest">
			                    <span id="" class="classSpanAddTestDesc">Test Subject</span>
			                    <span id=":" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP"><?php echo htmlspecialchars($aRowTest[5]); ?></span>
			                     <span id="" class="classSpanAddTestDesc">Test Code</span>
			                    <span id="" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP">
			               			<?php echo $aRowTest[2]; ?>
			                    </span>
 							</div>
 							<div id="idDivNat" class="classDivAddTest">
 							<span id="" class="classSpanAddTestDesc">Test Group</span>
			                    <span id=":" class="classSpanTabCol">:</span>
			                    <?php
			                    	$sGroupName=null; 
			                    	$sTGQuery="select group_name from group_table where group_id={$aRowTest[7]}";									
									$bTGResult=$mysqli->query($sTGQuery);
									$aTGRow=$bTGResult->fetch_row();
									$sGroupName=$aTGRow[0];
			                    ?>
			                    <span id="" class="classSpanTabIP"><?php echo htmlspecialchars($sGroupName); ?></span>

			                    <span id="" class="classSpanAddTestDesc">Test Scope</span>
 								<span id=":" class="classSpanTabCol">:</span>
 								<span id="" class="classSpanTabIP">
 								<?php 

 									if($aRowTest[9]==2)
 									{
 										echo "<input type='checkbox' disabled='true' checked />";
 									}
 									else
 									{
 										echo "<input type='checkbox' disabled='true'/> ";	
 									}
 								echo "&nbsp;&nbsp;Public<span class='classSmallFontForOpp'>(This make test common,for all user.)</span>"; 
 								?></span>
			                 </div>   
 							<div id="idDivTotQue" class="classDivAddTest">
 								<span id="" class="classSpanAddTestDesc">Total Questions</span>
 								<span id=":" class="classSpanTabCol">:</span>
 								<span id="" class="classSpanTabIP"><?php echo $iQue_Count; ?></span>
 							
 								<span id="idSpanTabMob" class="classSpanAddTestDesc">Time</span> 
 								<span id="idSpanTabMob:" class="classSpanTabCol">:</span>
			                    <span id="idSpanTabMobIP" class="classSpanTabIP">
 									<?php echo $aRowTest[0]; ?>min
 								</span>
 							</div>
 							<div id="idDivNat" class="classDivAddTest">
			                     <span id="" class="classSpanAddTestDesc">Test Valid From</span>
			                    <span id=":" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP">
			                      <?php echo $aRowTest[3]; ?>
			                    </span>
			                	
			                	<span id="idSpanTabMob" class="classSpanAddTestDesc">Negative Marks</span> 
 								<span id="idSpanTabMob:" class="classSpanTabCol">:</span>
			                    <span id="idSpanTabMobIP" class="classSpanTabIP">
 									<?php 
 									if($aRowTest[8]==1)
 									{
 										echo "<input type='checkbox' disabled='true' checked /> Applicable";
 									}
 									else
 									{	
 										echo "<input type='checkbox' disabled='true'/> &nbsp;Not applicable";
 									}
 									 ?>
 								</span>
			                </div>
			                <div id="idDivNat" class="classDivAddTest">
			                     <span id="" class="classSpanAddTestDesc">Test Valid Till</span>
			                    <span id=":" class="classSpanTabCol">:</span>
			                    <span id="" class="classSpanTabIP">
			                      <?php echo $aRowTest[4]; ?>
			                    </span>	
			                <?php
			                if($aRowTest[6] != "")
			                	{ 
						        echo "<span id='idSpanTabMob' class='classSpanAddTestDesc'>Test Description</span>
			                    <span id='idSpanTabMob:' class='classSpanTabCol'>:</span>
			                    <span id='idSpanTabMobIP' class='classSpanTabIP'>
			                         <div class='classScrollTestDesc classRounded_Radius_5'>$aRowTest[6]</div>
			                    </span>";
			                	}
			                ?>
			                </div>
			                <div id="idDivNat" class="classDivAddTest">
			                </div> 
               		</div>
				</div>
				<hr>
				 <?php include('viewQue.php'); ?>	
			 <div id="idDivNat" class="classDivTime">
                    <div id="idDivTq" class="classDivRight"></div>	
					<div id="idTT" class="classDivRight"></div>
             </div>
             
			</div>
		</div> <!-- end of content wrapper -->
	</div> <!-- end of content wrapper -->
  </form>
<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
	<div id="id_footer">
        <div class="section_w180">
        	<div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
        	<div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
        	<div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
		<div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
		</div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>