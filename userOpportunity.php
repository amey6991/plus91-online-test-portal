<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>View User Opportunity</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript" src="js/jquery-1.8.2.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
<!-- tooltip script -->
<script type="text/javascript">
$(function () {
$("[rel='tooltip']").tooltip();
});
</script>
</head>
<body>

  <div id="id_header_wrapper">
  <div id="id_header">
    <div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
  </div>
    <div id="id_menu">
          <div id="id_menu_left">
        <div id="idDivUserNameTop" class="classDivTopMenuUser">
                <?php
                session_start();
                include ('classConnectQA.php'); 
                $ut=NUll;               
                if(isset($_SESSION['ut']))  
                {
                    $ut=$_SESSION['ut'];
                }
                if(isset($_GET['uid']))
                {
                    $iUserId=$_GET['uid'];     
                }
                else
                {
                    $iUserId=null;
                }               

               if(isset($_SESSION['lid']))      // This is Use to check a Session
                {
                    $iLoginId = $_SESSION['lid'];
                }
                else
                {
                    header("location:index.php");
                }
                $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                    from login as a , user_details as b
                                    where a.login_id = b.login_id 
                                    AND a.login_id  = '$iLoginId' limit 1";
                $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                $aRowForUserInfo = $iResultForUserInfo->fetch_row();
                    
                    if($ut==0||$ut==2)
                    {
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                    <ul id='menu'>
                                    <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                    </li>
                                    <li>
                                        <a href='manageTest.php'>Home</a>       
                                    </li>";                                   
                    
                        if($ut==2)
                        {
                            echo " <li>
                                    <a href='showOpportunity.php'>Opportunity</a>
                                </li>";
                        }
                        else
                        {
                            echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
                                <ul>
                                    <li>
                                    <a href='groupHTML.php'>Create Group</a>        
                                    </li>
                                    <li>
                                        <a href='addTestHTML.php'>Create Test</a>       
                                    </li>
                                    <li>
                                        <a href='addUserHTML.php'>Create User</a>           
                                    </li>
                                    <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>         
                                    </li>
                                </ul>
                            </li>";
                        } 
                        
                        echo "<li>
                                <a>Manage </a>  
                                  <ul>
                                        <li>
                                            <a href='manageGroup.php'>Manage Group</a>          
                                        </li>
                                        <li>
                                            <a href='manageUser.php'>Manage User</a>            
                                        </li>
                                      <li>
                                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                      </li>
                                    </ul>   
                                </li>
                                <li>
                                    <a href='logout.php'>Logout </a>    
                                </li>
                                </ul>
                                </div>";
                    }   
                    else
                    {
                         echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                <ul id='menu'>
                                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href='manageTest.php'>Home</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Opportunity</a>
                                </li>
                                <li>
                                    <a href='displayStudentResult.php'>Result</a>       
                                </li>
                                <li>
                                    <a href='logout.php'>Logout </a>    
                                </li>
                                </ul>
                                </div>";    
                        }
                ?>      
                
      </div>    
    </div> <!-- end of menu -->
    
    </div>  <!-- end of header -->

</div> <!-- end of header wrapper -->
    <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div>
<div id="id_banner_wrapper">
  <div id="id_banner">
        <!--<div id="id_banner_content">
          <div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
  <div id="id_content">
        
    <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">   
                <?php
                    $sUserNameQuery="select user_full_name from user_details where user_id = {$iUserId}";
                    $bUserNameResult=$mysqli->query($sUserNameQuery);
                    $aUserName=$bUserNameResult->fetch_row();
                ?> 
                <div id="idDivSignUp" class="header_0345">User : <?php echo $aUserName[0]; ?>
                </div> 
                <div class="classHorizHRSubHead"></div>
                <?php 
                    
                    echo "<div id='idDivDisplayOpportunity' class='classDivDisplayOpportunity'>";
                   $ii=0;
                   $iR=0;
                   $sOppQuery="select Distinct a.opp_name, b.opp_id, b.ap_opp_doa from opportunity_table as a, apply_opportunity as b, opportunity_test as c, allot_test as d
                                where b.ap_opp_status=1 and b.user_id={$iUserId} and a.opp_id=b.opp_id and c.opp_id=b.opp_id and a.opp_id=c.opp_id and d.user_id={$iUserId} order by b.ap_opp_id";                  
                   $bOppResult=$mysqli->query($sOppQuery);
                    if($bOppResult==true)
                    {                        
                        echo "<div id='accordion'>";
                        while ($aOppRow=$bOppResult->fetch_row())
                        {    
                            $sOppName=$aOppRow['0'];
                            $iOppId=$aOppRow['1'];     
                            $sOppApplyDate=$aOppRow['2'];
                            echo "<div class='classRounded_Radius_5 classSpanOppEachTestDiv' id='idDivClickShow{$iR}'>&#8226; {$sOppName}";
                            echo "<span class='classSmallFontForOpp'>( Applied on : {$sOppApplyDate} )</span>"; 
                                                        
                            $sOppTestQuery="select a.test_name, b.sb_doa, b.sb_marks_obt, b.sb_marks_out_of from test_detail as a, score_board_table as b, opportunity_test as c
                                                    where a.test_id=b.test_id and b.user_id={$iUserId} and b.test_id=c.test_id and c.opp_id={$iOppId}";                            
                            $bOppTestResult=$mysqli->query($sOppTestQuery);
                            $ii=1;
                            echo "<br>";
                            echo "<fieldset id='idSpanOppEachTest{$ii}' class='classSpanOppEachTestDetailShowOp' style='background-color:#F7FBF7;'>";  
                            echo "<legend style='font-size:12px;'>Test Details</legend>"; 
                            while($aOppTestRow=$bOppTestResult->fetch_row())
                            {
                                                            
                                echo "<span class='classSmallFontForOpp'>Test Name : </span> {$aOppTestRow[0]}";
                                echo "<br/>";
                                echo "<span class='classSmallFontForOpp'>Test Attempted on : </span> {$aOppTestRow[1]}";
                                echo "<br/>";
                                if($aOppTestRow[2]==null)
                                {
                                    echo "<span class='classSmallFontForOpp'>Test Score : </span> Pending";    
                                }
                                else
                                {
                                    echo "<span class='classSmallFontForOpp'>Test Score : </span> {$aOppTestRow[2]}/{$aOppTestRow[3]}";    
                                }
                                echo "<br/>";
                               echo "<hr width=80%/>";

                              $ii++;  
                            }
							echo "</fieldset>";
						    
                            echo "</div>";
                             echo "<br>";
                              echo "<br>";
                            $iR++;
                        }                        
                            echo "</div>"; //end of accordion div                             
                    }  
                     else
                    {   
                        echo "<div class='classOpportunity' id='idOpportunity'>
                                <div class='classOpp1'>";
                        echo "Query Error ! In Viewing User Opportunity."; 
                        echo "</div></div>";
                    }
                    echo "</div>";
                ?>

            </div>                                       
       </div>
    </div> <!-- end of content wrapper -->
</div>

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>