<?php
/*
		SESSION Variable info.
		@lid: is use for login id
		@uid: is for user id
		@gid: is use for GroupId
		@id: is use for any other id such as group, test qustion
		@ut:  is use for the user type;
		@st:  use for status 
	
	*/
	session_start();

include ('classConnectQA.php');
$ij=0;
$ii=0;
$iAllotStatus=1;
$sMsg=null;
$userArray= array();
$userArray=$_POST;
$iTestId=$userArray['test'];
$iGroupId=$userArray['testGroup'];
unset($userArray['test']);
unset($userArray['testGroup']);
unset($userArray['idDisplayAllTest_length']);
unset($userArray['chkAllChk']);
if ($mysqli->errno) 									
{	
	$sMsg=-1;					// code -1 : Technical Error Try again.			
}
else
{

	$sAllotQuery="update allot_test set allot_status=0 where test_id={$iTestId} and group_id={$iGroupId} and allot_status=1";
	$bResult=$mysqli->query($sAllotQuery);
	
	if($bResult==true)
	{
		$sMsg=1;
	}
	else
	{
		$sMsg=0;	
	}
	foreach ($userArray as $value) 
	{
		$sAllotUser="select * from allot_test where user_id={$value} and test_id={$iTestId} and group_id={$iGroupId}";
		$bResult=$mysqli->query($sAllotUser);		
		
		$iUserAllotCounter=$mysqli->affected_rows;
		
		if ($iUserAllotCounter==0)
		 {

			$sAllotQuery="Insert into allot_test (user_id,test_id,group_id,allot_status) 
									  values ({$value},{$iTestId},{$iGroupId},{$iAllotStatus})";		
		}
		else
		{
			$sAllotQuery="update allot_test set allot_status=1 where user_id={$value} and test_id={$iTestId} and group_id={$iGroupId}";		
		}	
		
		$bAllotResult=$mysqli->query($sAllotQuery);
		
		if($bAllotResult==false)
		{
			$sMsg=10;
			break;
		}
		else
		{
			$sMsg=1;
		}
	}	
}

header("location: viewAllotedTestHTML.php?msg={$sMsg}");
?>
