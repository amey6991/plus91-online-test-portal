<?php
ob_start("ob_gzhandler"); /* This php code compress the output of page */
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Opportunity Detail's</title>
<link rel="icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="shortcut icon" href="images/titleLogo.ico" type="image/x-icon" />
<link rel="icon" type="image/gif" href="images/titleLogo.gif">
<meta name="description" content="online quiz application" />
<link href="css/style.css" rel="stylesheet" type="text/css" />
<script src="js/accordion/jquery-1.9.1.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<link href="css/bootstrap-responsive.min.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="js/bootstrap.min.js"></script>
</head>
<body>

  <div id="id_header_wrapper">
  <div id="id_header">
    <div id="site_logo">
    <div id="idDivHeadTxt" class="classDivHeadTxtInner">
      <span id="idSpanHeadImageLogo" class="classSpanHeadImageLogo"><img src="images/plus91-pune.gif" id="idImgLog" class="classHeadLogo img-polaroid"></span>
      <span id="idSpaCureersPortal" class="classSpaCureersPortal">Career Portal</span>
    </div>
  </div>
    <div id="id_menu">
          <div id="id_menu_left">
        <div id="idDivUserNameTop" class="classDivTopMenuUser">
                <?php
                session_start();
                include ('classConnectQA.php'); 
                $ut=NUll;               
                if(isset($_SESSION['ut']))  
                {
                    $ut=$_SESSION['ut'];
                }
                if(isset($_SESSION['user_id']))
                {
                    $iUserId=$_SESSION['user_id'];     
                }
                else
                {
                    $iUserId=null;
                }               

               if(isset($_SESSION['lid']))      // This is Use to check a Session
                {
                    $iLoginId = $_SESSION['lid'];
                }
                else
                {
                    header("location:index.php");
                }
                $sQueryUserInfo = "select a.login_id ,b.user_id  ,b.user_full_name ,b.user_email 
                                    from login as a , user_details as b
                                    where a.login_id = b.login_id 
                                    AND a.login_id  = '$iLoginId' limit 1";
                $iResultForUserInfo = $mysqli->query($sQueryUserInfo);
                $aRowForUserInfo = $iResultForUserInfo->fetch_row();
                    
                    if($ut==0||$ut==2)
                    {
                        echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                    <ul id='menu'>
                                    <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                    </li>
                                    <li>
                                        <a href='manageTest.php'>Home</a>       
                                    </li>";
                                    
                    
                        if($ut==2)
                        {
                            echo " <li>
                                    <a href='showOpportunity.php'>Opportunity</a>
                                </li>";
                        }
                        else
                        {
                            echo "<li><a >Opportunity</a>
                                    <ul>
                                      <li>
                                          <a href='opportunityHTML.php'>Create</a>       
                                      </li>
                                      <li>
                                          <a href='showOpportunity.php'>Manage</a>            
                                      </li>
                                    </ul>
                                </li>
                                <li><a>Create</a>
                                <ul>
                                    <li>
                                    <a href='groupHTML.php'>Create Group</a>        
                                    </li>
                                    <li>
                                        <a href='addTestHTML.php'>Create Test</a>       
                                    </li>
                                    <li>
                                        <a href='addUserHTML.php'>Create User</a>           
                                    </li>
                                    <li>
                                        <a href='excelReader/index.php'>Bulk Upload</a>         
                                    </li>
                                </ul>
                            </li>";
                        } 
                        
                        echo "<li>
                                <a>Manage </a>  
                                  <ul>
                                        <li>
                                            <a href='manageGroup.php'>Manage Group</a>          
                                        </li>
                                        <li>
                                            <a href='manageUser.php'>Manage User</a>            
                                        </li>
                                      <li>
                                        <a href='viewAllotedTestHTML.php'>Assign Test</a>     
                                      </li>
                                    </ul>   
                                </li>
                                <li>
                                    <a href='logout.php'>Logout </a>    
                                </li>
                                </ul>
                                </div>";
                    }   
                    else
                    {
                         echo "<div id='idSpanTopMenu' class='classSpanTopMenu header_044'>
                                <ul id='menu'>
                                <li><a href='profile.php'>$aRowForUserInfo[2]</a>
                                    <ul>
                                        <li>
                                            <a href='profile.php'>Profile</a>       
                                        </li>
                                        <li>
                                            <a href='profileedit.php'>Update Profile</a>            
                                        </li>
                                        <li>
                                            <a href='changePassword.php'>Change Password</a>            
                                        </li>
                                    </ul>
                                </li>
                                <li>
                                    <a href='manageTest.php'>Home</a>       
                                </li>
                                <li>
                                    <a href='showOpportunity.php'>Opportunity</a>
                                </li>
                                <li>
                                    <a href='displayStudentResult.php'>Result</a>       
                                </li>
                                <li>
                                    <a href='logout.php'>Logout </a>    
                                </li>
                                </ul>
                                </div>";    
                        }
                ?>      
                
      </div>    
    </div> <!-- end of menu -->
    
    </div>  <!-- end of header -->

</div> <!-- end of header wrapper -->
    <div id="idDivHorizBar" class="classDivHorizBar radial-center">
</div>
</div>
<div id="id_banner_wrapper">
  <div id="id_banner">
        <!--<div id="id_banner_content">
          <div id="idDivWelcomMsg" class="header_01">Welcome to e-Quiz! </div>
        </div>-->
            
    </div> <!-- end of banner -->
</div> <!-- end of banner wrapper -->

<div id="id_content_wrapper">
  <div id="id_content">
        
    <div id="idDivMiddleBody" class="classDivMiddleBody">
            <div id="idDiv" class="classDivBodyFormat">    
                <div id="idDivSignUp" class="header_0345">Opportunity
                </div> 
                <div class="classHorizHRSubHead"></div>
                <?php 
                    
                    echo "<div id='idDivDisplayOpportunity' class='classDivDisplayOpportunity'>";
                   $ii=0;
                    if($ut==0 || $ut ==2)
                    {
                        $sOppQuery="select * from opportunity_table order by opp_dateofopp desc";
                    }
                    else
                    {
                        $sOppQuery="select * from opportunity_table where opp_status=1 order by opp_dateofopp desc";

                    }                    
                    $bOppResult=$mysqli->query($sOppQuery);
                    if($bOppResult==true)
                    {                        
                        echo "<div id='accordion'>";
                        while ($aOppRow=$bOppResult->fetch_row())
                        {    
                            $iOppId=$aOppRow['0'];                        
                            $sOppName=$aOppRow['1'];
                            $sOppCode=$aOppRow['2'];
                            $sOppDesc=$aOppRow['3'];
                            $bOppStatus=$aOppRow['5'];
                                /*echo "<script>
                                        $(document).ready(function() {
                                            $('#idDivDisplay$ii').hide(600);
                                             $('#idDivClickShow$ii').click(function() {
                                                  $('#idDivDisplay$ii').animate({
                                                       height: 'toggle'
                                                       }, 1000
                                                  );
                                             });
                                        });
                                        </script>";*/
                                    echo "<div class='classDivClickShow classRounded_Radius_5' id='idDivClickShow$ii'>&#8226; <a>$sOppName</a></div>";
                                     echo "<div class='classRounded_Radius_5'>";
                                    echo "<div class='classDivClickDisplay ' id='idDivDisplay$ii'>";
                            if($ut==0)           
                             {
                                echo "<div class='classEditOpurnity'>
                                    <span class='classSpanSmallWhite'>
                                        <a href='opportunityHTML.php?oppid=$iOppId'>
                                            Edit
                                    </span>
                                    <span class='classSpanSmallWhite'>
                                        <img src='images/Edit.png' class='classEditTestIcon' alt='Edit Opportunity'/>
                                    </span>
                                    </a>
                                    </div>";
                             }
                             if($ut==1)
                             {
                                echo "<div class='classEditOpurnity'>
                                    <span class='classSpanSmallWhite'>";
                                $sOppTestQuery="select test_id from opportunity_test where opp_id={$iOppId} and opp_test_status=1";
                                $bOppTestResult=$mysqli->query($sOppTestQuery);
                                $iNoOfTest=$bOppTestResult->num_rows;    
                                if($iNoOfTest>0)
                                {
                                    echo "<a href='applyOpportunity.php?oppid={$iOppId}&uid={$iUserId}'>                                    
                                        <img src='images/applyNow.gif' class='classApplyNowIcon' alt='Apply via. Opportunity'/>
                                        </a>";
                                }   
                                else
                                {
                                    ///// No Option available for user if opportunity test not available.
                                } 
                                
                                echo "</span>                                    
                                    </div>";
                             }       
                            
                                    echo "<div>";
                                    echo "<span class='classDescOnOpportunity'>Code : </span> <span class='classDescOnOpportunity'>$sOppCode </span>";
                                    echo "</div>";
                            if($ut==0)
                            {
                                if($bOppStatus==1)
                                {                                        
                                    echo "<div class='classOpp1'>
                                        <span class='classDescOnOpportunity'>Opportunity : </span>                            
                                        <span class='classDescOnOpportunity'>Active</span>
                                    </div>";
                                }
                                else
                                {
                                echo "<div class='classOpp1'>
                                    <span class='classDescOnOpportunity'>Opportunity : </span>                            
                                    <span class='classDescOnOpportunity'>Inactive</span>
                                </div>";
                                }

                            }
                            echo "<div>";
                            $textDesc = $sOppDesc; 
                            $sDescNLBR = nl2br($textDesc);
                            echo "<span class='classDescOnOpportunity'>Description :</span> <div class='classDescOnOpportunityDesc'>$sDescNLBR</div>";
                            echo "</div>";

                            /* To view Opportuntiy Test */
                            $dCurrentDate=date("Y-m-d");
							$aOppTestQuery="select a.test_id, a.test_name, a.test_from, a.test_to from test_detail as a, opportunity_test as b 
											where b.opp_id={$iOppId} and b.opp_test_status=1 and a.test_status=1 
											and a.test_id=b.test_id order by b.opp_test_id asc";                                    
							$bOppTestResult=$mysqli->query($aOppTestQuery);
							$iCountTest=$bOppTestResult->num_rows;                           
							if($iCountTest != null || $iCountTest != 0)                                        
							{                                                            
								echo "<fieldset id='idFrameTabOppTestIP'>
										<legend> Mandatory test </legend>";
								$ii=1;
								while($aOppTestRow=$bOppTestResult->fetch_row())
								{
									if($ut==1)      // Only for User
									{                              
																
										$sCheckAllotedTestQuery="select a.* from test_detail as a, allot_test as b, opportunity_test as c
																where a.test_status=1 and a.test_id=b.test_id and b.user_id={$iUserId} and 
																b.allot_status=1 and c.test_id=b.test_id and c.test_id={$aOppTestRow[0]} and c.opp_id={$iOppId} ORDER BY b.allot_id DESC";  
									   
										$bCheckAllotedTestResult=$mysqli->query($sCheckAllotedTestQuery);

										if($bCheckAllotedTestResult==true)
										{   
											$iCount=$bCheckAllotedTestResult->num_rows;
											if($iCount==1)
											{
                                                $aRow=$bCheckAllotedTestResult->fetch_row();                                                
												echo "<fieldset id='idSpanOppEachTest{$ii}' class='classSpanOppEachTestShowOp' style='border-color:#009933;background-color:#F7FBF7;'>";  
												echo "<legend style='font-size:12px;color:#009933;'>Test {$ii}</legend>";                                                       
												if($aRow[6] <= $dCurrentDate && $aRow[7] >= $dCurrentDate)
                                                {
                                                    $sScoreBordQuery="select test_id from score_board_table where test_id={$aOppTestRow[0]} and user_id={$iUserId}";
                                                    $bScoreBordResult=$mysqli->query($sScoreBordQuery);
                                                    $aScoreBoardRow=$bScoreBordResult->fetch_row();
                                                    if($aScoreBoardRow[0]==null)
                                                    {
                                                        echo "<a href='uploadCV-StartTest.php?id={$aOppTestRow[0]}' title='Start Test'>{$aOppTestRow[1]}</a>";     
                                                    }
                                                    else
                                                    {
                                                        echo "<a style='cursor:pointer;' title='Test already attempted'>{$aOppTestRow[1]}</a>"; 
                                                    }
                                                    
                                                }
                                                else
                                                {
                                                    echo "<a class='classLikeAnchor' title='Schedule \n Valid From=$aRow[6] \n Valid Till=$aRow[7]'>{$aOppTestRow[1]}</a>"; 
                                                }
                                                
												 echo "</fieldset>";                
											}
											else
											{   
												echo "<fieldset id='idSpanOppEachTest{$ii}' class='classSpanOppEachTestShowOp'>";  
												echo "<legend style='font-size:10px;color:#9999f9;'>Test {$ii}</legend>";                                                          
												echo "{$aOppTestRow[1]}";            
												 echo "</fieldset>";               
											}
											
										}                        
										else
										{                                                       
											echo "<fieldset id='idSpanOppEachTest{$ii}' class='classSpanOppEachTestShowOp'>";  
											echo "<legend style='font-size:10px;color:#9999f9;'>Test {$ii}</legend>";                                                     
											echo "{$aOppTestRow[1]}";            
											echo "</fieldset>";                                                                          
										}

									}
									else                // For Tester User and Admin
									{
										if($aOppTestRow[0]==null)
										{
											echo "<fieldset id='idSpanOppEachTest{$ii}' class='classSpanOppEachTestShowOp'>";  
											echo "<legend style='font-size:10px;color:#9999f9;'>Test {$ii}</legend>";
											echo "<a href='addTestHTML.php'>Add Test</a>";
											echo "</fieldset>";                
										}
										else
										{    

											echo "<fieldset id='idSpanOppEachTest{$ii}' class='classSpanOppEachTestShowOp'>";  
											echo "<legend style='font-size:10px;color:#9999f9;'>Test {$ii}</legend>";
                                            if($aOppTestRow[2] <= $dCurrentDate && $aOppTestRow[3] >= $dCurrentDate)
                                            {
                                                echo "<span>{$aOppTestRow[1]}</span>";            
                                            }
                                            else
                                            {
                                                echo "<a class='classLikeAnchor title='Schedule \n Valid From=$aOppTestRow[2] \n Valid Till=$aOppTestRow[3]'>{$aOppTestRow[1]}</a>";            
                                            }
											
											echo "</fieldset>";                            
										}
										
									}
									
									$ii++;
								}
								echo "</fieldset>";
							}
                            echo "</div></div>";
                             $ii++;
                        
                    }
                         
                        echo "</div>"; //end of accordion div

                }
                 else
                {                      
                    
                    echo "<div class='classOpportunity' id='idOpportunity'>
                            <div class='classOpp1'>";
                    echo "No opportunity available";                                                
                    echo "</div></div>";
                }

                if($ii==0 && $ut==0)
                {
                    echo "<div class='classOpportunity' id='idOpportunity'>
                            <div class='classOpp1'>";
                    echo "No opportunity available";                        
                    echo "<span class='classSpanSmallWhite'>
                    <a href='opportunityHTML.php'> - add new opportunity</a></span>";   
                    echo "</div></div>";
                }
                else
                {
                    if($ii==0)
                    {
                    echo "<div class='classOpportunity' id='idOpportunity'>
                            <div class='classOpp1'>";
                    echo "No opportunity available";                        
                    echo "</div></div>";
                    }
                    else
                    {

                    }   
                }
                echo "</div>";
                ?>

            </div>                                       
       </div>
    </div> <!-- end of content wrapper -->
</div>

<div id="id_footer_wrapper">
<div id="idDivHorizBar" class="classDivHorizBarFooter radial-center">
</div> 
    <div id="id_footer">
        <div class="section_w180">
            <div class="header_05"><a href="help.php" >Help</a></div>
        </div>
        <div class="section_w180">
            <div class="header_05"><a href="feedback.php" >Feedback</a></div>
        </div>    
        <div class="section_w180">
            <div class="header_05"><a href="aboutus.php" >About</a></div>
        </div>
        <div id="idDivFooterCopyR" class="classDivFooterCopyR"> 
        Copyright &#169;  <a href="http://www.plus91.in" target="_blank">plus91.in</a>
        </div>
    </div> <!-- end of footer -->
</div> <!-- end of footer -->
</body>
</html>