<?php

			/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@gid: is use for GroupId
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status 
			
			*/
		session_start();		
		if(isset($_SESSION['lid']))		// This is Use to check a Session
		{

		}
		else
		{
			header("location:index.php");
		}
		include ('classConnectQA.php');						
		$iTestid=$_POST['hiddenTestId'];		
		$sQueName = addslashes($_POST['queName']);
		$sQueName = htmlspecialchars($sQueName, ENT_QUOTES);
		$iQueMarks = addslashes($_POST['queMarks']);
		if(isset($_POST['negMarks']))
		{
			$iNegMarks = addslashes($_POST['negMarks']);
		}
		else
		{
			$iNegMarks=0;
		}
		
		$iNegMarksOff = $_POST['negMarksOff'];
		if($iNegMarksOff===0)
		{
			$iNegMarks=0;
		}
		$sQueTypeValue = addslashes($_POST['queType']);
		$sQueDescription =  addslashes($_POST['queDescription']);
		$sQueDescription = htmlspecialchars($sQueDescription, ENT_QUOTES);		
		if (isset($_POST['op']))
		{			
			$sOption = addslashes($_POST['op']);
		}
		else
		{
			$sOption = Null;
		}
		$aMultipleOption=array();
		if($sQueTypeValue == "Multiple")
		{			
						// @ $aMultipleOption : Use to store the values from the checkboxes.
			for($ii=1, $ij=0 ;$ii<=7 ; $ii++)	// loop use to collect the values of all check boxes.
			{			
				if(isset($_POST['checkOpp'.$ii]))
				{
					$aMultipleOption[$ij]= addslashes($_POST['checkOpp'.$ii]);
					$ij++;
				}	
			}
		}
		else
		{
			// when Multiple choice question added and not a single option is inserted.			
		}	
	/*
		@ opArray is Use to fix the number of option.
	*/		
		$bAnsFlag=0;
		$opArray = new SplFixedArray(8);
		//loop to recive the text box and radio button values.
		for($ii=0,$ij=1;$ii<7;$ii++,$ij++)
		{
			$opArray[$ii]= addslashes($_POST['optext'.$ij]);					
		}
		
		// Special Case : When Null value is selected as a answer.
	
		$iTextBoxNo=substr($sOption, -1,1);			
		if($_POST['optext'.$iTextBoxNo]==Null  && $sQueTypeValue=="Objective")
		{
			header("Location: addQuestionHTML.php?id={$iTestid}&msg=-3"); // Code -3 : Is use to Display Msg that : Question Not Added, Becase Null Option is Choosen as a Answer.
			exit();
		}
		
		$bQueStatus = 1;
		$sQueExtra=NULL;
			if($sQueName!=NULL)
			{
			/*@ when connetion failed.*/
				if ($mysqli->errno) 									
				{	
					header("Location: addQuestionHTML.php?id={$iTestid}&msg=-1");
				
				}
				/*@ when connetion Established.*/
				else
				{	
				
					$sQueryQue ="INSERT INTO quiz_online.question_table (que_id, test_id, que_question, que_type, que_marks, que_neg_marks, que_ans_desc, que_status, que_extra) VALUES 
															(NULL,'{$iTestid}', '{$sQueName}', '{$sQueTypeValue}', '{$iQueMarks}', '{$iNegMarks}', '{$sQueDescription}', '{$bQueStatus}', '{$sQueExtra}')";
					
					$temp =$mysqli->query($sQueryQue);
					
						
					if($temp==True)
					{

						if($sQueTypeValue=="Objective")			// Use To store the Objective Question .
						{	
							// Below Query is use to know the qustion id for which a Objective Question option  are store.
							$sQuery="select max(que_id) from  quiz_online.question_table ";							
							$result=$mysqli->query($sQuery);
							$row=$result->fetch_row();
							$qid=$row[0];
							$ii=0;
							$ij=1;
							//for($ii=0;$opArray[$ii]!=NULL;$i++)
							while($opArray[$ii]!=NULL)
							{	
								if($sOption=="optext".$ij)
								{	
									$sQueryOption="INSERT INTO quiz_online.option_table (op_id, que_id, op_option, op_correct_ans) 
														VALUES (NULL, '{$qid}', '{$opArray[$ii]}', 1)";																										
								}
								else
								{	
									
								$sQueryOption="INSERT INTO quiz_online.option_table (op_id, que_id, op_option, op_correct_ans) 
													VALUES (NULL, '{$qid}', '{$opArray[$ii]}', 0)";																										
								}
								$mysqli->query($sQueryOption);
								
								$ii++;							
								$ij++;
							}
														
																
						}
						if($sQueTypeValue=="Descriptive")			// Use to Store the Descriptive type question.
						{

							$sVerifyQuery="select test_result_type from test_detail where test_id={$iTestid}";
							$verifyTestType=$mysqli->query($sVerifyQuery);
							$Vrow=$verifyTestType->fetch_row();
							if($Vrow[0]==1)
							{
								//Test Result type Set 0 when Descriptive Type Questin added in the test, this not allow to create a test result.
								$sChangeQuery="update test_detail set test_result_type=1 where test_id={$iTestid}";
								$mysqli->query($sChangeQuery);
							}

						}
						if($sQueTypeValue=="Multiple")			// Use to store the Multiple choice question.
						{
							// Below Query is use to know the qustion id for which a multiple choice option  is store.
							$sQuery="select max(que_id) from  quiz_online.question_table ";							
							$result=$mysqli->query($sQuery);
							$row=$result->fetch_row();
							$qid=$row[0];
							$ii=0;
							$iii=0;
							$ij=1;
							//for($ii=0;$opArray[$ii]!=NULL;$i++)
							while($opArray[$ii]!=NULL)
							{	

								if($aMultipleOption[$iii]=="optext".$ij)
								{	
									$sQueryOption="INSERT INTO quiz_online.multi_option_table (multi_op_id, que_id, multi_option, multi_op_ans) 
														VALUES (NULL, '{$qid}', '{$opArray[$ii]}', 1)";	
									$iii++;																														
								}
								else
								{	
									
								$sQueryOption="INSERT INTO quiz_online.multi_option_table (multi_op_id, que_id, multi_option, multi_op_ans) 
														VALUES (NULL, '{$qid}', '{$opArray[$ii]}', 0)";																										
								}
								$mysqli->query($sQueryOption);
								
								$ii++;							
								$ij++;
							}
						}
						
						// Query to count the number of question available in the test.
						$sCountQueQuery = "select * from question_table where test_id = {$iTestid}";
						$iResult = $mysqli->query($sCountQueQuery); 
						if($iResult==True)
						{
							$iQue_Count = $iResult->num_rows;	
							$sInsertTotalQue="update test_detail set test_total_que={$iQue_Count} where test_id={$iTestid}";
							$mysqli->query($sInsertTotalQue);
						}

						
						header("Location: addQuestionHTML.php?id={$iTestid}&msg=1");
										
					}
					else
					{	
						header("Location: addQuestionHTML.php?id={$iTestid}&msg=0");															
					}
				}
			}
			else
			{
				header("Location: addQuestionHTML.php?id={$iTestid}&msg=-2");
										
			}
	?>		