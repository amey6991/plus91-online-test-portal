<?php
			/*
				SESSION Variable info.
				@lid: is use for login id
				@uid: is for user id
				@gid: is use for GroupId
				@id: is use for any other id such as group, test qustion
				@ut:  is use for the user type;
				@st:  use for status 
			*/
		session_start();		
		include ('classConnectQA.php');		

		$iQueId =  addslashes($_POST['hiddenQueId']);
		$iTestId =  addslashes($_POST['hiddenTestId']);		
		$sQueName = addslashes($_POST['queName']);
		$sQueName = htmlspecialchars($sQueName, ENT_QUOTES);
		$iQueMarks =  addslashes($_POST['queMarks']);
		if(isset($_POST['negMarks']))
		{
			$iNegMarks =   $_POST['negMarks'];			
		}
		else
		{
			$iNegMarks =   Null;
		}
		
		$iNegMarksOff = $_POST['negMarksOff'];	

		$sQueType =  addslashes($_POST['queType']);
		$sQueDescription =  addslashes($_POST['queDescription']);
		$sQueDescription = htmlspecialchars($sQueDescription, ENT_QUOTES);
		// Below Query is use to chek the type of question befor update.
		$sQueTypeQuery="select que_type from question_table where que_id={$iQueId}";
		$bQueTypeResult=$mysqli->query($sQueTypeQuery);
		if($bQueTypeResult == true)
		{
			$aQueTypeRow=$bQueTypeResult->fetch_row();
			$queType=$aQueTypeRow[0];
		}
		else
		{
			$queType="Invalide";	
		}		

		if(isset($_POST['op'])){
			$sOpName = $_POST['op'];
		}
		else
		{
			$sOpName = Null;	
		}

		$opIdArray = new SplFixedArray(7);
		$opArray = new SplFixedArray(7);	
			/*
				below loop is use to recived the option text and the radio button value.
			*/
		$bAnsFlag=1;				
		for($ii=0,$ij=1;$ii<7;$ii++,$ij++)
		{	if(isset($_POST['opid'.$ij]) || isset($_POST['optext'.$ij]))
			{
				$opIdArray[$ii]= addslashes($_POST['opid'.$ij]);			
				$opArray[$ii]= addslashes($_POST['optext'.$ij]);	
				
			}
			
		}	
		
		if($sQueType=="Multiple")		// Use to Store the Chekbox values in the array $aCheckOpp.
		{				
			$ii=0;
			$ij=1;
			$aCheckOpp=array();
			while($ij<=7)
			{
				if(isset($_POST['checkOpp'.$ij]))
				{
					$aCheckOpp[$ii]= addslashes($_POST['checkOpp'.$ij]);	
				}
				else
				{
					$aCheckOpp[$ii]=null;
				}									
				$ij++;
				$ii++;
			}
		}
		else
		{
			$aCheckOpp=array();
		}
		if ($mysqli->errno) 									
		{	
			header("location: editQuestion.php?id={$iTestId}&qid={$iQueId}&msg=-1");		
		}
		// Special Case : When Null value is selected as a answer.
		if($sQueType=="Objective")
		{			
			$iTextBoxNo=substr($sOpName, -1,1);			
			if($_POST['optext'.$iTextBoxNo]==Null)
			{
				header('location: editQuestion.php?id='.$iTestId.'&qid='.$iQueId.'&gid='.$iGid.'&msg=-3');// Code -3 : Is use to Display Msg that : Question Not Added, Becase Null Option is Choosen as a Answer.
				exit();
			}	
		}		
		// Update Question.
		if($iNegMarksOff== null || $iNegMarksOff==0)		// Neagative marks are not applicable.
		{
			$queUpdateQuery="update question_table set que_question='{$sQueName}',que_type='{$sQueType}',que_marks='{$iQueMarks}',que_ans_desc='{$sQueDescription}' where que_id='{$iQueId}' and test_id='{$iTestId}' ";
		}
		else
		{
			$queUpdateQuery="update question_table set que_question='{$sQueName}',que_type='{$sQueType}',que_marks='{$iQueMarks}',que_neg_marks='{$iNegMarks}',que_ans_desc='{$sQueDescription}' where que_id='{$iQueId}' and test_id='{$iTestId}' ";
		}														
		$queResult=$mysqli->query($queUpdateQuery);	
		if($queResult ==  false)
		{
			header('location: editQuestion.php?id='.$iTestId.'&qid='.$iQueId.'&gid='.$iGid.'&msg=0');// Code -3 : Is use to Display Msg that : Question Not Added, Becase Null Option is Choosen as a Answer.
			exit();
		}
		else
		{
			//
		}

		$iMsg=null;
		///////////////////////////////////
		
		if($sQueType==$queType)  // Question Type befor Question update if it is same.
		{
			$iMsg=QueUpdate1($opIdArray , $opArray , $sOpName , $aCheckOpp, $iQueId , $iTestId , $sOpName , $queType);		// This Function update Question only when Question type is not change.
		}
		else
		{
			$iMsg=QueUpdate2($opIdArray , $opArray , $sOpName , $aCheckOpp, $iQueId , $iTestId , $sOpName , $sQueType);		// This Function update Question when Question type is change.
		}

		header("location: editQuestion.php?id={$iTestId}&qid={$iQueId}&msg={$iMsg}");




/////////////////Function QueeUpdate1() Start updating Question whose question type not changes.
		function  QueUpdate1($opIdArray , $opArray , $sOpName , $aCheckOpp, $iQueId , $iTestId , $sOpName , $queType)
		{
			include ('classConnectQA.php');
			$ii=0;
			$ij=1;
//////////////////// Condition for Objective type Question Start.			
			if($queType=="Objective")
			{
				while($ii<7)
				{	
					if($opArray[$ii]!=NULL && $opIdArray[$ii] != null)
					{
			
						if($sOpName=='optext'.$ij)
						{
							$opUpdateQuery="update option_table set op_option='{$opArray[$ii]}',op_correct_ans=1 where que_id='{$iQueId}' and op_id='{$opIdArray[$ii]}'";											
						}
						else
						{
							$opUpdateQuery="update option_table set op_option='{$opArray[$ii]}',op_correct_ans=0 where que_id='{$iQueId}' and op_id='{$opIdArray[$ii]}'";											
						}	
						$queResult=$mysqli->query($opUpdateQuery);				
			
					}
					else
					{
			
						if($opArray[$ii]!=NULL && $opIdArray[$ii] == null)
						{
							
							if($sOpName=='optext'.$ij)
							{
								$sQueryOption="INSERT INTO quiz_online.option_table (op_id, que_id, op_option, op_correct_ans) 
														VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 1)";	
							}
							else
							{
								$sQueryOption="INSERT INTO quiz_online.option_table (op_id, que_id, op_option, op_correct_ans) 
														VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 0)";																
							}
							$queResult=$mysqli->query($sQueryOption);

						}
						else
						{
							
							if($opArray[$ii]==NULL && $opIdArray[$ii] != null)
							{							
								$opDelQuery="delete from option_table where op_id={$opIdArray[$ii]}";
								$opDelResult=$mysqli->query($opDelQuery);
							}
							else
							{								
								//return(0);
							}

						}
							

					}
					$ii++;
					$ij++;
				}				
				return(1);
			}
//////////////////// Condition for Objective type Question End.


			else
			{

	//////////////////// Condition for Multiple type Question Start.				
				if($queType=="Multiple")
				{
					while($ii<7)
					{	
						if($opArray[$ii]!=NULL && $opIdArray[$ii] != null)
						{
				
							if($aCheckOpp[$ii]=='optext'.$ij)
							{						
								$opUpdateQuery="update multi_option_table set multi_option='{$opArray[$ii]}',multi_op_ans=1 where que_id='{$iQueId}' and multi_op_id='{$opIdArray[$ii]}'";																																		
							}
							else
							{
								$opUpdateQuery="update multi_option_table set multi_option='{$opArray[$ii]}',multi_op_ans=0 where que_id='{$iQueId}' and multi_op_id='{$opIdArray[$ii]}'";																										
							}					
							$queResult=$mysqli->query($opUpdateQuery);

						}
						else
						{
				
							if($opArray[$ii]!=NULL && $opIdArray[$ii] == null)
							{							
								if($aCheckOpp[$ii]=='optext'.$ij)
								{
									$sQueryOption="INSERT INTO quiz_online.multi_option_table (multi_op_id, que_id, multi_option, multi_op_ans) 
														VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 1)";																				
								}
								else
								{
									$sQueryOption="INSERT INTO quiz_online.multi_option_table (multi_op_id, que_id, multi_option, multi_op_ans) 
															VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 0)";
								}
								$queResult=$mysqli->query($sQueryOption);

							}
							else
							{
								
								if($opArray[$ii]==NULL && $opIdArray[$ii] != null)
								{							
									$opDelQuery="delete from multi_option_table where multi_op_id='{$opIdArray[$ii]}'";
									$opDelResult=$mysqli->query($opDelQuery);	
								}
								else
								{
									//return(0);
								}

							}
							

						}							
						$ii++;
						$ij++;
					}
					return(1);
				}
	//////////////////// Condition for Multiple type Question Ends.	

				else
				{
	//////////////////// Condition Start for Descriptive type Question .	
					if($queType=="Descriptive")
					{
						$opDelQuery="delete from option_table where que_id={$iQueId}";
						$opDelResult=$mysqli->query($opDelQuery);
						
						$opDelQuery="delete from multi_option_table where que_id={$iQueId}";
						$opDelResult=$mysqli->query($opDelQuery);
						return(1);
					}
	//////////////////// Condition End for Descriptive type Question .	
					else
					{
						return(0);	
					}
					
				}

			}
		}
/////////////////Function QueeUpdate1() END updating Question whose question type not changes.




/////////////////Function QueeUpdate2() Start updating Question whose question type changes.
	 function QueUpdate2($opIdArray , $opArray , $sOpName , $aCheckOpp, $iQueId , $iTestId , $sOpName , $queType)
	 {
	 	include ('classConnectQA.php');
	 	$ii=0;
		$ij=1;		
		 if($queType=="Objective")	// if Question Become Objective after update the execute.
			{
				while($ii<7)
				{				
					if($opArray[$ii]!=NULL)
					{
						if($sOpName=='optext'.$ij)
						{
							$sQueryOption="INSERT INTO quiz_online.option_table (op_id, que_id, op_option, op_correct_ans) 
													VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 1)";	
						}
						else
						{
							$sQueryOption="INSERT INTO quiz_online.option_table (op_id, que_id, op_option, op_correct_ans) 
													VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 0)";																
						}
						$queResult=$mysqli->query($sQueryOption);	
						
					}
					else
					{									
						if($opArray[$ii]==NULL && ($opIdArray[$ii] != null || $opIdArray[$ii] == null))
						{							
							$opDelQuery="delete from option_table where op_id={$opIdArray[$ii]}";
							$opDelResult=$mysqli->query($opDelQuery);													
						}
												
					}					
					$ii++;
					$ij++;
				}
				$opDelQuery="delete from multi_option_table where que_id='{$iQueId}'";
				$opDelResult=$mysqli->query($opDelQuery);
				return(1);
			}
//////////////////// Condition for Objective type Question End.

			else
			{

	//////////////////// Condition for Multiple type Question Start.				
				if($queType=="Multiple")
				{
					while($ii<7)
					{	
						if($opArray[$ii]!=NULL)
						{
							if($aCheckOpp[$ii]=='optext'.$ij)
							{
								$sQueryOption="INSERT INTO quiz_online.multi_option_table (multi_op_id, que_id, multi_option, multi_op_ans) 
													VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 1)";																				
							}
							else
							{
								$sQueryOption="INSERT INTO quiz_online.multi_option_table (multi_op_id, que_id, multi_option, multi_op_ans) 
														VALUES (NULL, '{$iQueId}', '{$opArray[$ii]}', 0)";
							}
							$queResult=$mysqli->query($sQueryOption);		

						}
						else
						{	
							if($opArray[$ii]==NULL)
							{				
								
								$opDelQuery="delete from multi_option_table where multi_op_id='{$opIdArray[$ii]}'";
								$opDelResult=$mysqli->query($opDelQuery);	
							}
						}
						$ii++;
						$ij++;
					}							
					$opDelQuery="delete from option_table where que_id={$iQueId}";
					$opDelResult=$mysqli->query($opDelQuery);
					return(1);
				}
					
	//////////////////// Condition for Multiple type Question Ends.
				else
				{
	//////////////////// Condition Start for Descriptive type Question .	
					if($queType=="Descriptive")
					{
						$opDelQuery="delete from option_table where que_id={$iQueId}";
						$opDelResult=$mysqli->query($opDelQuery);
						
						$opDelQuery="delete from multi_option_table where que_id={$iQueId}";
						$opDelResult=$mysqli->query($opDelQuery);
						return(1);
					}
	//////////////////// Condition End for Descriptive type Question .	
					else
					{
						return(0);	
					}
					
				}

			}
		}
/////////////////Function QueeUpdate2() END updating Question whose question typechanges.




?>
