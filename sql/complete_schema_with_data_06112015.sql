/*
SQLyog Community v12.09 (64 bit)
MySQL - 5.6.17-log : Database - quiz_online
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`quiz_online` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `quiz_online`;

/*Table structure for table `allot_test` */

DROP TABLE IF EXISTS `allot_test`;

CREATE TABLE `allot_test` (
  `allot_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primark Key',
  `user_id` int(11) NOT NULL COMMENT 'To Refer the User details',
  `group_id` int(11) NOT NULL COMMENT 'To Refer Group details',
  `test_id` int(11) NOT NULL COMMENT 'To Refer Test Details',
  `allot_status` int(10) NOT NULL,
  PRIMARY KEY (`allot_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1 COMMENT='Allow to allot test individually';

/*Data for the table `allot_test` */

insert  into `allot_test`(`allot_id`,`user_id`,`group_id`,`test_id`,`allot_status`) values (1,2,1,1,1),(2,2,1,3,0),(3,2,1,2,0),(4,6,1,1,0),(5,6,1,3,1),(6,6,1,2,0),(7,6,1,8,1),(8,5,1,8,1),(9,2,1,8,1),(10,14,1,1,0),(11,13,1,1,0),(12,12,1,1,0),(13,11,1,1,0),(14,10,1,1,0),(15,9,1,1,0),(16,8,1,1,0),(17,7,1,1,0),(18,5,1,1,0),(19,14,1,3,0),(20,13,1,3,0),(21,12,1,3,0),(22,11,1,3,0),(23,10,1,3,0),(24,9,1,3,0),(25,8,1,3,0),(26,7,1,3,0),(27,5,1,3,0),(28,5,1,9,1),(29,5,1,2,0);

/*Table structure for table `apply_opportunity` */

DROP TABLE IF EXISTS `apply_opportunity`;

CREATE TABLE `apply_opportunity` (
  `ap_opp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `opp_id` int(11) NOT NULL COMMENT 'Work as a foreign key to identify the Opportunity.',
  `user_id` int(11) NOT NULL COMMENT 'Works as Foreign keey to identify the user',
  `ap_opp_doa` date NOT NULL COMMENT 'Date of Appling to an opportunity',
  `ap_opp_status` int(11) NOT NULL COMMENT 'Determine the user applid to the opportunity or not',
  PRIMARY KEY (`ap_opp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table use to maintain the record of a user whose appling through a opportun';

/*Data for the table `apply_opportunity` */

/*Table structure for table `des_marks_table` */

DROP TABLE IF EXISTS `des_marks_table`;

CREATE TABLE `des_marks_table` (
  `des_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `user_id` int(11) NOT NULL COMMENT 'To identify the User',
  `test_id` int(11) NOT NULL COMMENT 'To identify the test',
  `que_id` int(11) NOT NULL COMMENT 'To identify the que',
  `des_marks` varchar(11) NOT NULL COMMENT 'To store the marks score by the user for descriptive question',
  PRIMARY KEY (`des_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='Table to store the marks score by the user for descriptive question';

/*Data for the table `des_marks_table` */

insert  into `des_marks_table`(`des_id`,`user_id`,`test_id`,`que_id`,`des_marks`) values (1,5,3,0,'0');

/*Table structure for table `group_table` */

DROP TABLE IF EXISTS `group_table`;

CREATE TABLE `group_table` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'unique for group table',
  `group_name` varchar(100) DEFAULT NULL COMMENT 'group name given by admin',
  `group_desc` varchar(500) DEFAULT NULL COMMENT 'description about group',
  `group_doc` varchar(100) DEFAULT NULL COMMENT 'Date Of Creating Group ',
  `group_status` tinyint(1) DEFAULT NULL COMMENT 'Active:(1) or Inactive:(0) the Group',
  `group_extra` varchar(500) DEFAULT NULL COMMENT 'Extra field, future oriented',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=latin1 COMMENT='manage the group created by admin';

/*Data for the table `group_table` */

insert  into `group_table`(`group_id`,`group_name`,`group_desc`,`group_doc`,`group_status`,`group_extra`) values (1,'Plus91 Fresher Recruitment','Test available for the freshers. ','2013-05-15 10:22:02',1,NULL);

/*Table structure for table `login` */

DROP TABLE IF EXISTS `login`;

CREATE TABLE `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique to value for each  Login',
  `user_name` varchar(100) NOT NULL COMMENT 'user name to login',
  `password` varchar(100) NOT NULL COMMENT 'password to login',
  `type` tinyint(1) NOT NULL COMMENT '1 for user, 0 for admin and 2 for Tester.',
  PRIMARY KEY (`login_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='Table to store admin username and password. ';

/*Data for the table `login` */

insert  into `login`(`login_id`,`user_name`,`password`,`type`) values (1,'Admin','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',0),(2,'laxmikant.killekar@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(5,'csp567@gmail.com','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(6,'data@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(7,'m.nikhra@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(8,'kishan.gor@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(9,'snehal.bokey@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(10,'shraddha.wankhade@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(11,'meenu@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(12,'r-ajay.mandera@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(13,'dhanashree.kumkar@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(14,'ravisahani.plus91@gmail.com','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1),(16,'dhanashri.mude@plus91.in','louCdpTML8AHzhcSYb1hSjpztyCKTQQOy+3bb141LC0=',1),(17,'ajay.mandera@plus91.in','ptvZEhgJcoixASWP0bo6DmVQJQqEfd/8wyAoA+vZSfs=',1);

/*Table structure for table `login_system_info` */

DROP TABLE IF EXISTS `login_system_info`;

CREATE TABLE `login_system_info` (
  `logId` int(20) NOT NULL AUTO_INCREMENT COMMENT 'this field stores unique id for each row in this table',
  `user_id` int(20) NOT NULL COMMENT 'this field stores user id of user',
  `test_id` int(20) NOT NULL COMMENT 'this field stores test id of test that user selected',
  `sys_ip` varchar(100) NOT NULL COMMENT 'this field stores the ip address of user system',
  `sys_os` varchar(100) NOT NULL COMMENT 'this field stores operating system name of users system',
  `sys_browser` varchar(100) NOT NULL COMMENT 'this field stores browser name that user uses to acces our website',
  PRIMARY KEY (`logId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='this table stores information like IP , operating system , browser of user system.';

/*Data for the table `login_system_info` */

insert  into `login_system_info`(`logId`,`user_id`,`test_id`,`sys_ip`,`sys_os`,`sys_browser`) values (1,5,3,'192.168.1.110','Windows 7','Chrome'),(2,5,8,'192.168.1.104','Windows 7','Firefox'),(3,17,9,'182.70.48.43','Linux','Chrome');

/*Table structure for table `multi_option_table` */

DROP TABLE IF EXISTS `multi_option_table`;

CREATE TABLE `multi_option_table` (
  `multi_op_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'is a primary key',
  `que_id` int(11) NOT NULL COMMENT 'Question Id to maintain the relationship with que table.',
  `multi_option` varchar(1000) NOT NULL COMMENT 'Storing the multiple opption.',
  `multi_op_ans` varchar(100) NOT NULL COMMENT 'Use to know which answer is acceptable or not. Value: 1 is for answer acceptable and 0 depend on evaluator.',
  PRIMARY KEY (`multi_op_id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1 COMMENT='Use to Maintain the multiple choice question';

/*Data for the table `multi_option_table` */

insert  into `multi_option_table`(`multi_op_id`,`que_id`,`multi_option`,`multi_op_ans`) values (5,34,'Marie Curie','1'),(6,34,'Pierre Curie','1'),(7,34,'Alfred Nobel','0'),(8,34,'Albert Einstein','0');

/*Table structure for table `opportunity_table` */

DROP TABLE IF EXISTS `opportunity_table`;

CREATE TABLE `opportunity_table` (
  `opp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `opp_name` varchar(500) NOT NULL COMMENT 'Opportunity Name',
  `opp_code` varchar(100) NOT NULL COMMENT 'Opportunity Code',
  `opp_des` varchar(5000) NOT NULL COMMENT 'Opportunity Description',
  `opp_dateofopp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of Creating Opportunity',
  `opp_status` int(1) NOT NULL COMMENT 'Opportunity Status',
  PRIMARY KEY (`opp_id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1 COMMENT='Contain the Opportunity Details';

/*Data for the table `opportunity_table` */

insert  into `opportunity_table`(`opp_id`,`opp_name`,`opp_code`,`opp_des`,`opp_dateofopp`,`opp_status`) values (1,'Web Designer.','tpyutY1','Location(s) 	Pune\r\nEmployment Type 	Permanent\r\nJob Listed 	24-Jan-2013\r\nJob Description\r\n\r\nFluent in English\r\n\r\n0-3 years work experience\r\n\r\nShould have good knowledge of:\r\n\r\n1. HTML 5\r\n\r\n2. CSS 3\r\n\r\n3. Jquery\r\n\r\n4. Photoshop\r\n\r\n5. Wordpress\r\n\r\n6. Using Google Webmaster and Analytic Tools\r\n\r\n7. Domain and Server Management\r\n\r\nCandidates will be preferred with Knowledge in:\r\n\r\n1. PHP\r\n\r\n2. MySQL\r\n\r\n3. Making Presentation\r\n\r\n4. Social Media Tools','2013-05-23 23:08:18',1),(2,'CONTENT WRITER','yopptY2','Location(s) 	Pune\r\nEmployment Type 	Permanent\r\nJob Listed 	15-Jul-2012\r\nJob Description\r\nPlus91 Technologies, a Pune-based healthcare IT firm is looking for  qualified and reliable content writers to aid in developing content on diverse topics and specializations.\r\n \r\nJob Description:\r\nThe person needs to write Web Content and Articles on different topics. \r\nResponsibilities include gathering information, writing and editing material and coordination of publication by utilizing the electronic media and print. \r\nContent must be informative, error free, grammatically correct and well-constructed.\r\n \r\nQualities Required in Candidate:\r\nAble to Research on Internet for authentic information\r\nAbility to understand and learn about new topics to produce authentic, accurate, expert and original content on them.Ability to Write original articles on various subjects for Global audience\r\nExcellent knowledge of grammar and use of grammar and use of punctuation marks\r\nAn eye for detail\r\nGood time-management skills and ability to adhere to deadlines\r\n \r\nQualification:\r\nAny Graduate having excellent command in written English can apply\r\nOur remuneration will be commensurate with industry standards. Candidates with prior writing experience will have an added advantage.\r\n \r\nE-mail your cover letter, previous content work, writing samples, links to blogs and published articles if any, to careers@plus91.in apart from posting on this website\r\n\r\nBefore applying to this position please view our privacy policy.','2013-05-16 16:40:36',1),(3,'Mobile Application Developer - I','UPuRNo3','Location(s) 	Pune\r\nEmployment Type 	Permanent\r\nJob Listed 	14-Jul-2012\r\nJob Description\r\n\r\n Profile: The candidate will work as a lead in a small team creating a variety of iPad and iPhone Applications. The candidate will have to comply with the specified Project Management, Source Code Management, Collaboration and Documentation Rules.\r\n\r\nResponsibilities include developing Applications, Test Scripts, Regular Code Management, and Documentation.\r\n\r\nRequirements:\r\n\r\nBE, B.Sc. (Comp Science), MSc, MCA, MS\r\n\r\n1-3 years of development experience with strong project portfolio in Mobile Applications using ObjectiveC\r\n\r\nAt least 1 year experience of developing on iOS\r\n\r\nExperience with Mobile Software Design Principles\r\n\r\nDemonstrated interest in, knowledge of, and enthusiasm for Mobile technologies\r\n\r\nGood communication skills with Strong Presentation Skills \r\n\r\nPreferred Requirements:\r\n\r\nStrong in jQuery, HTML5, mongoDB and SQLLite\r\n\r\nWorking knowledge of either Windows Azure, Amazon Web Services or Rackspace Cloud\r\n\r\nExperience of programming using Git\r\n\r\nExperience of programming using Phone gap(Cordova) or Titanium\r\n\r\nPersonality Requirements:\r\n\r\nPassionate about coding, Open and Straight forward, Wants to work in a high charged start-up environment, loves challenges\r\n\r\nBefore applying to this position please view our privacy policy.','2013-05-23 22:54:02',1),(4,'SENIOR PHP DEVELOPER.','yntPNt4','Location(s) 	Pune\r\nEmployment Type 	Permanent\r\nJob Listed 	14-Jul-2014\r\nJob Description\r\n\r\n Profile: The candidate will work as a lead in a multi-faceted team supporting existing features and \r\n\r\nmodules as well as developing new ones. The candidate will have to comply with the specified \r\n\r\nProject Management, Source Code Management, Collaboration and Documentation Rules. \r\n\r\nResponsibilities include Hands on Coding as well as Project Management within a Project Team, \r\n\r\nmentoring trainee developers. \r\n\r\nRequirements: \r\n\r\nBE, B.Sc. (Comp Science), MSc, MCA, MS \r\n\r\n3+ years of development experience with strong project portfolio in PHP, AJAX, MySQL, High \r\n\r\nVolume Web Application Development \r\n\r\nUnderstanding of Cloud Based System Deployment Architectures \r\n\r\nExperience with Cloud based Software Design Principles \r\n\r\nDemonstrated interest in, knowledge of, and enthusiasm for Internet technologies \r\n\r\nGood communication skills with Strong Presentation Skills \r\n\r\nGood Leadership Skills \r\n\r\nExperience of programming using Git \r\n\r\nPreferred Requirements: \r\n\r\nStrong in jQuery, HTML5, mongoDB and MySQL \r\n\r\nWorking knowledge of either Windows Azure, Amazon Web Services or Rackspace Cloud \r\n\r\nPersonality Requirements: \r\n\r\nPassionate about coding, Open and Straight forward, Wants to work in a high charged start-up \r\n\r\nenvironment, loves challenges\r\n\r\nBefore applying to this position please view our privacy policy.','2015-11-03 12:45:19',1),(5,'PHP DEVELOPER.','PuTOOp5','Location(s) 	Pune\r\nEmployment Type 	Permanent\r\nJob Listed 	14-Jul-2012\r\nJob Description.\r\n\r\n Profile: The candidate will work in a multi-faceted team supporting existing features and modules \r\n\r\nas well as developing new ones. The candidate will have to comply with the specified Source \r\n\r\nCode Management, Collaboration and Documentation Rules. \r\n\r\nResponsibilities include developing PHP Classes and Scripts, Test Scripts, Regular Code \r\n\r\nManagement, Documentation. \r\n\r\nRequirements: \r\n\r\nBE, BCA, B.Sc. (Comp Science), MSc, MCA, MS \r\n\r\n0-2 years of development experience with strong project portfolio in PHP &amp; MySQL Web \r\n\r\nApplication Development \r\n\r\nDemonstrated interest in, knowledge of, and enthusiasm for Internet technologies \r\n\r\nGood communication skills \r\n\r\nPreferred Requirements: \r\n\r\nGood knowledge of English \r\n\r\nExperience of programming using Git \r\n\r\nStrong in Javascript, mongoDB and MySQL \r\n\r\nPersonality Requirements: \r\n\r\nAbility to work with a team, \r\n\r\nPassionate about coding, Wants to work in a high charged start-up environment, loves challenges\r\n\r\nBefore applying to this position please view our privacy policy.','2013-05-23 23:08:58',1),(6,'test 1','oirOit6','test 1','2013-05-23 23:06:23',0);

/*Table structure for table `opportunity_test` */

DROP TABLE IF EXISTS `opportunity_test`;

CREATE TABLE `opportunity_test` (
  `opp_test_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primark Key',
  `opp_id` int(11) NOT NULL COMMENT 'Use to get opportunity',
  `test_id` int(11) NOT NULL COMMENT 'Use to get test ',
  `opp_test_status` int(11) NOT NULL COMMENT 'Determine the status of opportunity test',
  PRIMARY KEY (`opp_test_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='opportunity_test table hold all that test which are added in the opportunity.';

/*Data for the table `opportunity_test` */

insert  into `opportunity_test`(`opp_test_id`,`opp_id`,`test_id`,`opp_test_status`) values (1,5,1,0),(2,4,1,1),(3,4,3,1),(4,4,2,0),(5,5,3,1),(6,5,8,1),(7,1,8,1);

/*Table structure for table `option_table` */

DROP TABLE IF EXISTS `option_table`;

CREATE TABLE `option_table` (
  `op_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique for each option',
  `que_id` int(10) DEFAULT NULL COMMENT 'To identify the question for the option.',
  `op_option` varchar(500) DEFAULT NULL COMMENT 'Contia option values, may be more than two for a same question.',
  `op_correct_ans` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`op_id`)
) ENGINE=MyISAM AUTO_INCREMENT=467 DEFAULT CHARSET=latin1 COMMENT='Manage no. of option for a question.';

/*Data for the table `option_table` */

insert  into `option_table`(`op_id`,`que_id`,`op_option`,`op_correct_ans`) values (1,1,'XYZ','1'),(2,1,'UVW','0'),(3,1,'VWX','0'),(4,1,'WXY','0'),(5,1,'None of these','0'),(6,4,'99','0'),(7,4,'100','0'),(8,4,'110','0'),(9,4,'120','1'),(10,4,'None of the above','0'),(11,5,'1/7','0'),(12,5,'2/7','0'),(13,5,'3/7','0'),(14,5,'1','0'),(15,5,'None of the above','1'),(16,6,'12 or 6','0'),(17,6,'8 or 4','0'),(18,6,'18','0'),(19,6,'18 or 15','0'),(20,6,'None of the above','1'),(62,25,'100','0'),(49,22,'2X = &X * Y','1'),(50,22,'2X + 7Y = 100','0'),(51,22,'2X * 7Y = 500','0'),(60,24,'None of these','0'),(59,24,'A12','0'),(58,24,'O18','0'),(65,26,'Chris Cairns( New zealand)','0'),(64,25,'120','0'),(63,25,'110','0'),(57,24,'N18','1'),(56,23,'WXY','0'),(55,23,'VWX','0'),(54,23,'UVW','0'),(53,23,'XYZ','1'),(52,22,'2x2 + 7Y = 50','0'),(61,25,'99','1'),(66,26,'Vivian Richards (West Indies)','0'),(67,26,'Sachin Tendulkar (India)','0'),(68,26,'Yuvraj Singh(India)','1'),(69,27,'12 or 6','1'),(70,27,'8 or 4','0'),(71,27,'18','0'),(72,27,'18 or 15','0'),(73,28,'ARP','1'),(74,28,'RARP','0'),(75,28,'ICMP','0'),(76,28,'RTP','0'),(77,29,'1/7','1'),(78,29,'2/7','0'),(79,29,'3/7','0'),(80,29,'1','0'),(81,30,'Moon','0'),(82,30,'Srihari Kota','1'),(83,30,'Bangalor','0'),(84,30,'Mysore','0'),(85,31,'London','0'),(86,31,'Moscow','0'),(87,31,'Copenhagen','1'),(88,31,'Stockholm','0'),(89,32,'January 15','0'),(90,32,'November 19','0'),(91,32,'June 5','1'),(92,32,'July 7','0'),(93,33,'Aarti Ramaswamy','0'),(94,33,'Koneru Humpy','0'),(95,33,'S. Meenakshi','0'),(96,33,'S. Vijayalakshmi','1'),(97,35,'Measuring its height','0'),(98,35,'Measuring its diameter','0'),(99,35,'Analyzing its sap','0'),(100,35,'Counting the anual growth rings of its stem','1'),(101,36,'Itanagar','0'),(102,36,'Agartala','0'),(103,36,'Shillong','0'),(104,36,'None of the above','1'),(105,37,'Kiran Bedi','1'),(106,37,'Smitha Patil','0'),(107,37,'Kiran Rao','0'),(108,37,'Kabir Bedi','0'),(109,38,'Chetak','1'),(110,38,'Chetan','0'),(111,38,'Ketan','0'),(112,38,'Chota Chetan','0'),(113,39,'Kuchipudi','0'),(114,39,'Kathak','0'),(115,39,'Manipuri','0'),(116,39,'Mohiniattam','1'),(117,40,'Atlantic Ocean','0'),(118,40,'Arctic Ocean','0'),(119,40,'Pacific Ocean','1'),(120,40,'Indian Ocean','0'),(121,41,'The light rays undergo total internal reflaction in the drop','0'),(122,41,'Fine drop of water in fog polarize the light','0'),(123,41,'The fine drop are opaque to the light','0'),(124,41,'The drops scatter most of the light','1'),(125,42,'Bihar','0'),(126,42,'Orissa','1'),(127,42,'Rajasthan','0'),(128,42,'Gujarat','0'),(129,43,'Henry David Thoreau','1'),(130,43,'David Ricardo','0'),(131,43,'Henry Kissinger','0'),(132,43,'Bestrand Russel','0'),(133,44,'Right to equality','1'),(134,44,'Right to Property','0'),(135,44,'Right to Freedom','0'),(136,44,'Right to constitutional remedies','0'),(137,45,'United states of America','1'),(138,45,'United Kingdom','0'),(139,45,'Switzerland','0'),(140,45,'Canada','0'),(141,46,'Uttaranchal','0'),(142,46,'Arunachal Pradesh','0'),(143,46,'Andhra Pradesh','0'),(144,46,'Jammu and Kashmir','0'),(145,46,' Himachal Pradesh','1'),(146,47,'Valentina Tereshkova','0'),(147,47,'Edword White','0'),(148,47,'Yuri Gagarin','1'),(149,47,'Alan Shapard','0'),(150,48,'21','0'),(151,48,'22','0'),(152,48,'23','0'),(153,48,'24','1'),(154,49,'9690','1'),(155,49,'9720','0'),(156,49,'9930','0'),(157,49,'9960','0'),(158,50,'x3 = y4','1'),(159,50,'x3 = y','0'),(160,50,'x = y4','0'),(161,50,'x20 = y15','0'),(162,51,'1/n','1'),(163,51,'1/(n+1)','0'),(164,51,'2(n-1)/n','0'),(165,51,'n/n+1','0'),(166,52,'45','1'),(167,52,'-45','0'),(168,52,'-54','0'),(169,52,'-55','0'),(170,53,'Rs. 4,475','1'),(171,53,'Rs. 4,500','0'),(172,53,'Rs. 4,575','0'),(173,53,'Rs. 4,975','0'),(174,54,'150','1'),(175,54,'120','0'),(176,54,'100','0'),(177,54,'90','0'),(178,55,'127','1'),(179,55,'133','0'),(180,55,'235','0'),(181,55,'305','0'),(182,56,'âˆš3','1'),(183,56,'3âˆš4','0'),(184,56,'4âˆš6','0'),(185,56,'6âˆš8','0'),(186,57,'17','1'),(187,57,'16','0'),(188,57,'1','0'),(189,57,'2','0'),(190,58,'4','1'),(191,58,'10','0'),(192,58,'12','0'),(193,58,'18','0'),(194,59,'1','1'),(195,59,'5','0'),(196,59,'9','0'),(197,59,'10','0'),(198,60,'8 days','1'),(199,60,'10 days','0'),(200,60,'12 days','0'),(201,60,'20 days','0'),(202,61,'5 minutes','1'),(203,61,'8 minutes','0'),(204,61,'10 minutes','0'),(205,61,'12 minutes','0'),(206,62,'Rs. 4,280','1'),(207,62,'Rs. 2,840','0'),(208,62,'Rs. 4,820','0'),(209,62,'Rs. 4,o28','0'),(210,63,'1:2','1'),(211,63,'âˆš2 :1','0'),(212,63,'1 : âˆš2','0'),(213,63,'2 :1','0'),(214,64,'( 16/3 ) Ï€cmÂ² ','1'),(215,64,'( 22/x ) Ï€cmÂ² ','0'),(216,64,'( 28/x ) Ï€cmÂ² ','0'),(217,64,'( 32/x ) Ï€cmÂ² ','0'),(218,65,'168 cmÂ³','1'),(219,65,'154 cmÂ³','0'),(220,65,'1078 cmÂ³','0'),(221,65,'800 cmÂ³','0'),(222,66,'4(4/13) cm','1'),(223,66,'4(8/13) cm','0'),(224,66,'5 cm','0'),(225,66,'7 cm','0'),(226,67,'3.5% decrease','1'),(227,67,'3.5% increase','0'),(228,67,'5% increase','0'),(229,67,'5% decrease','0'),(230,68,'Rs. 844.80','1'),(231,68,'Rs. 929.28','0'),(232,68,'Rs. 1,044.80','0'),(233,68,'Rs. 1.000','0'),(234,69,'8','1'),(235,69,'18','0'),(236,69,'20','0'),(237,69,'24','0'),(238,70,'14%','1'),(239,70,'10%','0'),(240,70,'12%','0'),(241,70,'15%','0'),(242,71,'1 : 3','1'),(243,71,'2 : 3','0'),(244,71,'3 : 2','0'),(245,71,'1 : 2','0'),(246,72,'10','1'),(247,72,'12','0'),(248,72,'15','0'),(249,72,'16','0'),(250,73,'1 : 2','1'),(251,73,'1 : 3','0'),(252,73,'2 : 3','0'),(253,73,'2 : 5','0'),(254,74,'2 : 3','1'),(255,74,'4 : 3','0'),(256,74,'5 : 2','0'),(257,74,'2 : 3','0'),(258,75,'Rs. 75','1'),(259,75,'Rs. 175','0'),(260,75,'Rs. 300','0'),(261,75,'Rs. 126','0'),(262,76,'38 years','1'),(263,76,'39 years','0'),(264,76,'40 years','0'),(265,76,'41 years','0'),(266,77,'A + 1','1'),(267,77,'A - 1','0'),(268,77,'A + 3','0'),(269,77,'A - 3','0'),(270,78,'48','1'),(271,78,'36','0'),(272,78,'24','0'),(273,78,'18','0'),(274,79,'m / n','1'),(275,79,'m + n','0'),(276,79,'mn','0'),(277,79,'m-n','0'),(278,80,'10%','1'),(279,80,'11%','0'),(280,80,'12%','0'),(281,80,'13%','0'),(282,81,'Rs. 700','1'),(283,81,'Rs. 750','0'),(284,81,'Rs. 800','0'),(285,81,'Rs. 900','0'),(286,82,'5/4','1'),(287,82,'4/5','0'),(288,82,'8/5','0'),(289,82,'5/8','0'),(290,83,'10','1'),(291,83,'11','0'),(292,83,'21','0'),(293,83,'10','0'),(294,84,'Rs. 360','1'),(295,84,'Rs. 370','0'),(296,84,'Rs. 380','0'),(297,84,'Rs. 390','0'),(298,85,'15','1'),(299,85,'20','0'),(300,85,'25','0'),(301,85,'32','0'),(302,86,'50','1'),(303,86,'70','0'),(304,86,'100','0'),(305,86,'150','0'),(306,87,'5.5','1'),(307,87,'5.2','0'),(308,87,'5','0'),(309,87,'2.5','0'),(310,88,'Rs. 12.50','1'),(311,88,'Rs. 13.00','0'),(312,88,'Rs. 13.50','0'),(313,88,'Rs. 14.00','0'),(314,89,'100 km/hr','1'),(315,89,'120 km/hr','0'),(316,89,'140 km/hr','0'),(317,89,'150 km/hr','0'),(318,90,'17 m','1'),(319,90,'20 m','0'),(320,90,'19 m','0'),(321,90,'18 m','0'),(322,91,'6 : 3 : 2','1'),(323,91,'2 : 3 : 4','0'),(324,91,'3 : 4 : 6','0'),(325,91,'3 : 4 : 2','0'),(326,92,'Nonee','1'),(327,92,'One','0'),(328,92,'Two','0'),(329,92,'Three','0'),(330,93,'MYCILEBF','1'),(331,93,'KTAILEBF','0'),(332,93,'MTAGJEBF','0'),(333,93,'KTAGJEBF','0'),(334,94,'None','1'),(335,94,'One','0'),(336,94,'Two','0'),(337,94,'Three','0'),(338,95,'HK','1'),(339,95,'BD','0'),(340,95,'FI','0'),(341,95,'MP','0'),(342,96,'Tomato','1'),(343,96,'Brinjal','0'),(344,96,'Radish','0'),(345,96,'Pumpkin','0'),(346,97,'None','1'),(347,97,'One','0'),(348,97,'Two','0'),(349,97,'Three','0'),(350,98,'Dog','0'),(351,98,'Horse','1'),(352,98,'Wolf','0'),(353,98,'Jackal','0'),(354,99,'#*%â†‘','1'),(355,99,'#$%â†‘','0'),(356,99,'#*@$','0'),(357,99,'#*Câ†‘','0'),(358,100,'pa','1'),(359,100,'na','0'),(360,100,'ta','0'),(361,100,'ja','0'),(362,101,'84','1'),(363,101,'120','0'),(364,101,'72','0'),(365,101,'108','0'),(366,102,'Oasis','1'),(367,102,'Strait','0'),(368,102,'Isthmus','0'),(369,102,'Bay','0'),(370,103,'Hot and dry','1'),(371,103,'Wet tropical ','0'),(372,103,'Mediterranean','0'),(373,103,'Cold temperate ','0'),(374,104,'Barometer','1'),(375,104,'Hygroscope ','0'),(376,104,'Pykometer','0'),(377,104,'Quartz clock','0'),(378,105,'Grreat Himalays ','1'),(379,105,'Indian Ocean','0'),(380,105,'Antartcica','0'),(381,105,'Arabian Sea','0'),(382,106,'Fold Mountains','1'),(383,106,'Residual Mountains','0'),(384,106,'Volcani Mountains','0'),(385,106,'None of the above','0'),(386,107,'1 degree','1'),(387,107,'5 degree','0'),(388,107,'10 degree','0'),(389,107,'15 degree','0'),(390,108,'Contours ','1'),(391,108,'Isobars ','0'),(392,108,'Meridians ','0'),(393,108,'Steppes','0'),(394,109,'Brahmaputra, Ganges and Godavari','1'),(395,109,'Cauveri, Krishna and Narmada','0'),(396,109,'Krishna, Godavari and Tapi','0'),(397,109,'Narmada, Ganges and Brahmaputra','0'),(398,110,'Conduction ','1'),(399,110,'Convection ','0'),(400,110,'Inversion ','0'),(401,110,'Radiation ','0'),(402,111,'59821','0'),(403,111,'59182','0'),(404,111,'52981','0'),(405,111,'59281','1'),(406,112,'BGQMUL','1'),(407,112,'CGRLTK','0'),(408,112,'CGRTLK','0'),(409,112,'BGQLUM','0'),(410,113,'CYCLE','0'),(411,113,'PENCIL','0'),(412,113,'YOKE','1'),(413,113,'DIAL','0'),(414,114,'FIST','0'),(415,114,'LARK','0'),(416,114,'HYPOCRISY','0'),(417,114,'PISTON','1'),(418,115,'Danny','1'),(419,115,'Johnny','0'),(420,115,'Kenny','0'),(421,115,'Bobby','0'),(422,116,'20, 30, 40, 50','1'),(423,116,'20, 25, 30, 35','0'),(424,116,'30, 35, 40, 45','0'),(425,116,'30, 40, 50, 60','0'),(426,117,'It rotate on its axis from east to west','1'),(427,117,'The day and night are as result of rotation.','0'),(428,117,'It rotates round the Sun, taking 365 days, 5 hours and 48 minutes and 45.51 seconds.','0'),(429,117,'None of the above','0'),(430,118,'Encouraging, enjoying','1'),(431,118,'Permitting , do not have','0'),(432,118,'Confining, entitled','0'),(433,118,'Promoting, benefit','0'),(434,119,'expenditure, capital','1'),(435,119,'sanctions, initiatives','0'),(436,119,'allotment, security','0'),(437,119,'disbursement, investment','0'),(438,120,'sweeping, regime','1'),(439,120,'transparent, hike','0'),(440,120,'drastically, net','0'),(441,120,'constitutional, revenue','0'),(442,121,'released, asset','1'),(443,121,'issued, Services','0'),(444,121,'drafted, clearing','0'),(445,121,'notified, transaction.','0'),(453,136,'yellow','0'),(452,136,'black','1'),(451,136,'green','0'),(450,136,'white','0'),(454,137,'FFFFFF','0'),(455,137,'FFFFFF','0'),(456,137,'FF FF','1'),(457,137,'FFFF','0'),(458,139,'OS','1'),(459,139,'Compiler','0'),(460,139,'never return value','0'),(461,139,'None of the above','0'),(462,140,'-128 to +127','1'),(463,140,'-128 to +128','0'),(464,140,'-127 to +128','0'),(465,140,'-128 to +128','0'),(466,140,'-127 to +127','0');

/*Table structure for table `question_attachment` */

DROP TABLE IF EXISTS `question_attachment`;

CREATE TABLE `question_attachment` (
  `attach_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `que_id` int(10) DEFAULT NULL COMMENT 'question id',
  `que_type` varchar(100) DEFAULT NULL COMMENT 'question type',
  `location` varchar(100) DEFAULT NULL COMMENT 'question type',
  `file_name` varchar(500) DEFAULT NULL COMMENT 'question type',
  `file_status` int(11) DEFAULT NULL COMMENT 'question type',
  PRIMARY KEY (`attach_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1 COMMENT='this table stores attache file address with any question';

/*Data for the table `question_attachment` */

insert  into `question_attachment`(`attach_id`,`que_id`,`que_type`,`location`,`file_name`,`file_status`) values (1,8,'Descriptive','questionAttachMent/','8_91b6bdd5b82e13737ffbef282713d7f9.png',0),(2,8,'Descriptive','questionAttachMent/','8_b5c55776131586980299520281f027c7.png',0),(3,8,'Descriptive','questionAttachMent/','8_9f838d6b344e9b6188d03eaaf4e7e5e1.png',1),(4,139,'Objective','questionAttachMent/','139_19f0ef207b935e4bc1ba89d7ce0e9caa.png',0),(5,139,'Objective','questionAttachMent/','139_6f659cc46e255fa085df2d46927b1f6b.png',0);

/*Table structure for table `question_table` */

DROP TABLE IF EXISTS `question_table`;

CREATE TABLE `question_table` (
  `que_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique question id for each question',
  `test_id` int(10) DEFAULT NULL COMMENT 'Test id to include question in the specified test.',
  `que_question` varchar(500) DEFAULT NULL COMMENT 'Question to be ask.',
  `que_type` varchar(100) DEFAULT NULL COMMENT 'Question either objective or Describtive ',
  `que_marks` varchar(100) DEFAULT NULL COMMENT 'marks per question',
  `que_neg_marks` varchar(100) DEFAULT NULL COMMENT 'Nagative mark for the question.',
  `que_ans_desc` varchar(500) DEFAULT NULL COMMENT 'Answer Description',
  `que_status` tinyint(1) DEFAULT NULL COMMENT 'active:(1) or inactive:(0) ',
  `que_extra` varchar(500) DEFAULT NULL COMMENT 'Extra feld, Future Oriented',
  PRIMARY KEY (`que_id`)
) ENGINE=MyISAM AUTO_INCREMENT=142 DEFAULT CHARSET=latin1 COMMENT='Managing all the question required for test.';

/*Data for the table `question_table` */

insert  into `question_table`(`que_id`,`test_id`,`que_question`,`que_type`,`que_marks`,`que_neg_marks`,`que_ans_desc`,`que_status`,`que_extra`) values (1,1,'Complete the series\r\nBCD, HIJ, OPQ, ......?Select Answer','Objective','4','2','',1,''),(2,1,'Look at this series :\r\nJ14, L16, _ , P20, R22\r\n\r\nWhat number should fill the blank?','Descriptive','4','','',1,''),(3,1,'Find the least number which when divided by 35, leaves reminder 25, when divided by 45, leaves reminder 35 and when divided by 55 leaves reminder 45.','Descriptive','4','','',1,''),(4,1,'How many result lies betwen 300 and 500 in which, 4 comes only one time','Objective','4','','',1,''),(5,1,'What is the probability of getting 53 Friday in a leap year ?','Objective','4','','',1,''),(6,1,'S1 & S2 are the two sets of parallel lines. The number of lines in S1 is greater than the number of lines in S2, They intersect at 12 points. The number of parallelogram that S1 and S2 may form is','Objective','4','','',1,''),(7,1,'Write a simple CSS file for a simple three column Website','Descriptive','4','','',1,''),(8,1,'Given the image of website template shown below. Design this website without using tables such that it works in IE6, IE7, IE8.','Descriptive','4','','',1,''),(9,1,'What are the advantages and need for having a website for a clinic or a hospital in these days and age ? ','Descriptive','4','','',1,''),(10,1,'What is the first thing you want to do as a plus91 employee ?','Descriptive','4','','',1,''),(11,1,'What are the Microsoft projects called in the USA ?','Descriptive','4','','',1,''),(12,1,'Which type of Doctors are Tech Savvy ?  Give reasons as to why they are. This is in term of specialty.','Descriptive','4','','',1,''),(13,1,'Explain how to increase interaction using the internet. Feel free to innovate and come up with your own answer.','Descriptive','4','','',1,''),(14,1,'What tag line do you think is ideal for Plus91 Technologies-An Indian Healthcare IT firm ?','Descriptive','4','','',1,''),(15,1,'What are the various Operating system you have heard of, list the name and company ?','Descriptive','4','','',1,''),(25,3,'How many number lies between 300 and 500 in which , 4 comes only one time','Objective','1','0','',1,''),(24,3,'Look at this series : J14,L16,___,P20,R22,........what number should fill the blank ?','Objective','1','0','',1,''),(22,3,'Which of the following represent valid constraints in linear programming ? ','Objective','1','0','',1,''),(23,3,'Complete the series BCD,HIJ,OPQ ....?','Objective','1','0','',1,''),(26,3,'Which of the following cricketers holds the world record of maximum number of sixes in T 20-20 match ?','Objective','1','0','',1,''),(27,3,'S1 and S2 are two sets of parallel lines . The number of lines in S1 is greater than the number of lines in S2 . They intersect at 12 points. The number of parallelograms that S1 and S2 may form is','Objective','1','0','',1,''),(28,3,'If you wanted to locate the hardware address of a local device , which protocol would you use ?','Objective','1','0','',1,''),(29,3,'What is the probability of getting 53 Fridays in a leap year?','Objective','1','0','',1,''),(30,3,'ISRO, Bangalore launched Chandrayan from one the following places','Objective','1','0','',1,''),(31,3,'What is capital of Denmark ','Objective','1','0','',1,''),(32,3,'Which day is world environment day ?','Objective','1','0','',1,''),(33,3,'Who is India\'s first Women Grand Master in Chess ?','Objective','1','0','',1,''),(34,3,'Who discovered Radium ?','Multiple','1','0','',1,''),(35,3,'The age of a tree can be determined by','Objective','1','0','',1,''),(36,3,'What is the capital of Mizoram ?','Objective','1','0','',1,''),(37,3,'Who was the first Indian women IPS officer ?','Objective','1','0','',1,''),(38,3,'What was the name of Rana Pratap\'s horse ?','Objective','1','0','',1,''),(39,3,'Which one of the following is essentially a solo dance ?','Objective','1','0','',1,''),(40,3,'The deepest oceanic trench Mariana is located in  ','Objective','1','0','',1,''),(41,3,'Although fog consist of fine drop of water , we cannot see clearly through it because','Objective','1','0','',1,''),(42,3,'Which state has the lower per capita income in India ?','Objective','1','0','',1,''),(43,3,'Gandhi\'s inspiration for Civil Disobedience come from the writings of','Objective','1','0','',1,''),(44,3,'Which of the following is not a fundamental Right.','Objective','1','0','',1,''),(45,3,'The fundamental Rights in our constitution are inspired by the Constitution of','Objective','1','0','',1,''),(46,3,'Where Naptha Jhakri Project is located','Objective','1','0','',1,''),(47,3,'The first person to enter into space was','Objective','1','0','',1,''),(48,3,'The number of perfect square number between 50 and 1000 is','Objective','1','0','',1,''),(49,3,'The largest 4-digit number exactly divisible by each of 12,15,18 and 27 is','Objective','1','0','',1,''),(50,3,'If x, y are two positive real number and x(1/3) = y(1/4), then which of the following relation is true ?','Objective','1','0','',1,''),(51,3,'When simplified , the sum\r\n1/2 + 1/6 +1/12 + 1/20 + 1/30 + .......+1/n(n+1)','Objective','1','0','',1,''),(52,3,' 1^2 - 2^2 + 3^2 - 4^2 +........-10^2 is equal to','Objective','1','0','',1,''),(53,3,'A man joined a service in the scale Rs. 3,325-50-4,275-75-5,660. His salary after 23 year will be','Objective','1','0','',1,''),(54,3,'A man has in all Rs. 640 in the denomination of one-rupee , five-rupee and ten-rupee notes. The number of each type of notes are equal . What is the total number of notes he has ?','Objective','1','0','',1,''),(55,3,'The greatest number , by which 1657 and 2037 are divided to give remainders 6 and 5 respectively , is','Objective','1','0','',1,''),(56,3,'The greatest one of âˆš3 , 3âˆš4 , 4âˆš6 and 6âˆš8 is','Objective','1','0','',1,''),(57,3,'If 17^200 is divided by 18 , the remainder is','Objective','1','0','',1,''),(58,3,'If for non zero x, xÂ² -4x-1 = 0 , the value of (xÂ² + 1/xÂ²) is','Objective','1','0','',1,''),(59,3,'1/(1+âˆš2) + (1 / (âˆš2 + âˆš3))  + ( 1/(âˆš3 + âˆš4) ) + .....+ ( 1/( âˆš99 + âˆš100 ) ) is equal to','Objective','1','0','',1,''),(60,3,'A and B together can complete a work in 8 days and B and C together in 12 days . All of the three together can complete the work in 6 days. In how much time will A and C together complete the work ?','Objective','1','0','',1,''),(61,3,'A tank can be filled by two pipes in 20 minutes and 30 minutes respectively . When the tank was empty , the two pipes ware opened . After some time , the first pipe was stopped and the tank filled in 18 minutes . After how much time of the start was the first pipe stopped ?','Objective','1','0','',1,''),(62,3,'A and B and C entered into a partnership. A invested Rs. 2,560 and B invested Rs 2,000. At the end of the year , they gained Rs. 1,105 out of which A got Rs 320 C\'s capital was ','Objective','1','0','',1,''),(63,3,'The ratio of the areas of the in-circle and the circum-circle of a square is','Objective','1','0','',1,''),(64,3,'The area of an equilateral triangle inscribed in a circle is 4âˆš3 cmÂ². The area of the circle is','Objective','1','0','',1,''),(65,3,'In a right circular cone , the radius of its base is 7 cm and its height 24 cm . A cross section is made through the mid point of the height parallel to the base. The volume of the upper portion is','Objective','1','0','',1,''),(66,3,'The base and altitude of a right angled triangle are 12 cm and 5 cm respectively . The perpendicular distance of its hypothesis from the opposite vertex is','Objective','1','0','',1,''),(67,3,'If the height of a cylinder is increased by 15% and the radius of its base is decreased by 10%, then by what percent will its curved surface area change ?','Objective','1','0','',1,''),(68,3,'A shopkeeper gives 12% additional discount after giving an initial discount of 20% on the marked price of a radio. If the sale price of the radio is Rs. 704, the marked price is ','Objective','1','0','',1,''),(69,3,'A shopkeeper after allowing a discount of 10% on the marked price makes a profit of 8%. How much percent above the cost price is the market price ?','Objective','1','0','',1,''),(70,3,' A shopkeeper gives a successive discounts on an article marked Rs. 450. The first discount is given is 10%. If the customer pays Rs. 344.25 for the article, the second discount given is ','Objective','1','0','',1,''),(71,3,'If a, b , c are three numbers such that a : b = 3 : 4 and b : c = 8 : 9,  then a : c is equal to  ','Objective','1','0','',1,''),(72,3,'In a regular polygon, the exterior and interior angles are in the ratios 1:4. The number of sides of polygon is ','Objective','1','0','',1,''),(73,3,'The ratio of the ages of a father and his son 10 year hence will be 5:3, while 10 year ago , it was 3:1. The ratio of the age of the son to that of the father today, is','Objective','1','0','',1,''),(74,3,'Vessels A and B contain mixture of milk and water in the ratio 4:5 and 5:1 respectively. In what ratio should quantities of mixture be taken fro A and B to form a mixture in which milk to water in the ratio 5:4 ?','Objective','1','0','',1,''),(75,3,'A bag contains three types of coins 1 rupee coins, 50 p-coins totaling 175 coins. If the total value of the coins of each kind be the same, the total amount in the bag is','Objective','1','0','',1,''),(76,3,'The average age of twenty four boys and their teacher is 15 years. When the teacher\'s  age is excluded, the average age decrease by 1 year. The age of the teacher is','Objective','1','0','',1,''),(77,3,'The average score of the class of boys and girls in an examination is A. The ratios of boys and girls in the class is 3:1. If the average score age of the boys is A+1, the average score of the girls is ','Objective','1','0','',1,''),(78,3,'The average of three numbers is 28. If the first number is half of the second and the third number is twice the second, then the number is ','Objective','1','0','',1,''),(79,3,'If the average of m is nÂ² and that of n number is mÂ² , then the average of (m + m) number is ','Objective','1','0','',1,''),(80,3,'Nita blends two varieties of tea-one costing Rs. 180 peer kg and another costing Rs. 200 kg in the ratio 5:3. if she sells the blended variety at Rs. 210 per kg then her grain is','Objective','1','0','',1,''),(81,3,'If a manufacturer gains 10%, wholesaler 15% and retailer 25%, then the production cost of an article, whose retail price is Rs. 1,265, is','Objective','1','0','',1,''),(82,3,'Partha earns 15% on an investment but losses of 10% on another investment. if the ratio of two investment is 3:5, the the combine loss percent is','Objective','1','0','',1,''),(83,3,'A tradesman, by means of a balance defrauds 10% in buying goods and also defrauds 10% in selling, his gain percent is','Objective','1','0','',1,''),(84,3,'The total cost of price of two watches id Rs. 840. One is sold at a profit of 16% and the other at the loss of 12%. There is no loss or gain the whole transaction. The cost price of the watch on which the shopkeeper gains, is','Objective','1','0','',1,''),(85,3,'If Nita\'s salary is 25% more than Papiya\'s salary, then the percentage by which Papiya\'s salary is less then Nita\'s salary is','Objective','1','0','',1,''),(86,3,'A candidate who scores 30% fails by 5 marks, while another candidate who scores 40% marks get 10 more than minimum pass marks. the minimum marks required to pass are','Objective','1','0','',1,''),(87,3,'An interval of 3 hour 40 min is wrongly estimate as 3 hours 45.5 minutes. the error percentage is','Objective','1','0','',1,''),(88,3,'The Government reduced the price of sugar by 10%. By this the consumer can buy 6.2 kg more sugar for Rs. 837. The reduced per kg of sugar is','Objective','1','0','',1,''),(89,3,'Two trains are moving on two parallel tracks but in opposite direction. A person sitting in the train moving at the speed of 80 km/hr passes the seconds. If the length of the second train is 1000 m ,its speed is','Objective','1','0','',1,''),(90,3,'In a one-kilometer race A, B and C are the three participants. A can give B a start of 50 m and C start of 69 m. The stat which B can allow is ','Objective','1','0','',1,''),(91,3,'A person invests money in three different schemes for 6 years, 10 years and 12 years at 10% , 12% and 15% simple interest respectively. At the completion of each scheme, he get the same interest. The ratio of his investment is','Objective','1','0','',1,''),(92,3,'How many such digits are there in the number 5236978 each  of which is as far away from the beginning of the number as when the digits are rearranged i ascending order within the number','Objective','1','0','',1,''),(93,3,'In a certain code COMPUTER is written as LNBVQSFU. How is BULKHEAD written in that code ?','Objective','1','0','',1,''),(94,3,'How many meaningful English words can be made with the letters IDE using each letter only once in some word ? ','Objective','1','0','',1,''),(95,3,'Three of the following four are a like in a certain way and so form a group. Which is the one that dose not belong to that group ? ','Objective','1','0','',1,''),(96,3,'Three of the following four are alike a certain way and so form a group. Which is the one that dose not belong to that group ?','Objective','1','0','',1,''),(97,3,'How many such pairs of letters are there in the words IMPORTANCE, each of which has as many letters between them in the words as they have between them in the English alphabet.','Objective','1','0','',1,''),(98,3,'Three of the following four are alike a certain way and so form group. Which is the one that does not belong to that group.','Objective','1','0','',1,''),(99,3,'In a certain code FIRE is written #%@$ and DEAL is written as Â©$*â†‘ .  How is FAIL written in that code ?','Objective','1','0','',1,''),(100,3,'In  a certain code language, \'comes again\' is written as \'ho na\', \'come over here\' is written as \'pa na ta\' and \'over and above\' is written as \'ki ta ja\'. How is \'here\' written in the code language ?','Objective','1','0','',1,''),(101,3,'Four of the following five are alike in a certain way and so form a group. Which is the one that does not belong to that group ?','Objective','1','0','',1,''),(102,3,'A narrow passage of water connecting two large bodies of water is known as a/an','Objective','1','0','',1,''),(103,3,'Lateritie soil develop under which of the following types of climate?','Objective','1','0','',1,''),(104,3,'Which of the following instrument is used for reading the changes that occurs in atmospheric humidity ?','Objective','1','0','',1,''),(105,3,'India\'s permanent research station Dakshin Gangotri is situated in the','Objective','1','0','',1,''),(106,3,'Himalayas are','Objective','1','0','',1,''),(107,3,'For a time difference of one hour the longitudinal distance is equal to ','Objective','1','0','',1,''),(108,3,'Imaginary lines drawn on a global map, from pile to pole and perpendicular to the Equator, are called ','Objective','1','0','',1,''),(109,3,'The rivers included in which of the following group flow into the Bay of Bengal?','Objective','1','0','',1,''),(110,3,'Atmospheric temperature increases at the higher altitudes due to','Objective','1','0','',1,''),(111,3,'In a certain code the following numbers are coded in a certain way by assigning signs:\r\n1 for Ã· , 2 for x,  3 for _ ,  4 for +, 5 for >, 6 for <, 7 for ^, 8 for v , 9 for *.\r\nWhich number can be decoded from the following ?\r\n>*xvÃ·','Objective','1','0','',1,''),(112,3,'The word \"UNITED\" is coded as \'SLGRCB\'. How should the word \'DISOWN\' be coded ?','Objective','1','0','',1,''),(113,3,'A word given in capital letters below is followed by four words as options A), B), C) and D). Out of these only one cannot be formed by using the letter of the given word. Find out that words.\r\nENCYLCLOPEDIA ','Objective','1','0','',1,''),(114,3,'A word id given in capital letters below. It is followed by four words as options A), B), C) and D). Out of these four words, three cannot be formed from the letters of the words in capital letters. Point out the word which can be formed from the letters of the given word in capital letters.\r\nPHILANTHROPIST ','Objective','1','0','',1,''),(115,3,'Six friends are sitting in a circle and playing cards. Kenny is to the left of Danny, Michael is in between Bobby and Johnny. Roger is in between Kenny and Bobby. Who is sitting to the right of Michale ?','Objective','1','0','',1,''),(116,3,'A, B, C, D are four friends. Average age of A and C is 35 years and that of B and D is 40 Years. Average age of B, C and D is 40 years. The sum of the ages of A and D is equal to that of B and C. Find out the ages ( in years ) of A, B, C and D','Objective','1','0','',1,''),(117,3,'Which of the following statement is on the motion of earth is TRUE ?\r\n','Objective','1','0','',1,''),(118,3,'\"__________the activities of moneylenders could have an adverse impact on those who ____________ access to bank credit.\"','Objective','1','0','Fill up the right words in the following sentence from the options :',1,''),(119,3,'\"The budget announced substantial ________ of ________ in critical section like education and healthcare.\"','Objective','1','0','Fill up the right words in the following sentence from the options :',1,''),(120,3,'The government decided not to make any _____ changes in the country\'s tax ___________.','Objective','1','0','Fill up the right words in the following sentence from the options :',1,''),(121,3,'\"The RBI has ________ a statement that the implementation of KYC norms should not lead to the denial of banking _______ to customers.\"','Objective','1','0','Fill up the right words in the following sentence from the options :',1,''),(122,2,'What is \n\ndifference between mysql_connect and mysql_pconnect ?','Descriptive','4','0','',1,''),(123,2,'How to store binary data in MYSQL ?','Descriptive','4','0','',1,''),(124,2,'What is difference between \n\nmysql_fetch_array() , mysql_fetch_row() and mysql_fetch_object() ?','Descriptive','4','0','',1,''),(125,2,'How can we find the number \n\nof rows in a result set using PHP ?','Descriptive','4','0','',1,''),(126,2,'What is the difference between the function unlike and unset \n\n?','Descriptive','4','0','',1,''),(127,2,'What\'s the difference \n\nbetween Locking and Lockless ( Optimistic and Pessimistic ) concurrency \n\nmodels ?','Descriptive','4','0','',1,''),(128,2,'What is the \n\ndifference between htmlentities( ) and htmlspecialchars( ) ?','Descriptive','4','0','',1,''),(129,2,'What is meant by this error \n\n?\r\n\'Warning : session_start( ) [ Function.session-start ]: Node  long \n\nexists in .......\'','Descriptive','4','0','',1,''),(130,2,'Name 2 \n\nadvantages of a Non-Relational Database System over a Relational \n\nDatabase System.','Descriptive','4','0','',1,''),(131,2,'What is \r\n\r\nthe output of the following PHP code Snippet ','Descriptive','4','0','',1,''),(132,2,'What is the output f the following PHP Code \r\n\r\nSnippet','Descriptive','4','0','',1,''),(133,2,'Write a PHP \n\nScript t retrieve values from a database on a remote database server and \n\nstore them in a local database server.','Descriptive','6','0','',1,''),(134,2,'Write a PHP script which will display all the files and \n\nfolder in a folder. The folders must be displayed above the files, he \n\nlisting of files must be in alphabetical order ( Additional points for \n\nbetter features will be granted on the solution )','Descriptive','10','0','',1,''),(135,2,'What is something substantive that you\'ve done \n\nto improve as a developer in your career ?','Descriptive','10','0','',1,''),(136,8,'sky : blue : : road : ?','Objective','1','0','',1,''),(137,8,'If RGB= FFFFFF then what is a B R ?','Objective','1','0','',1,''),(139,9,'Function main() return value to ?','Objective','1','0','',1,''),(140,9,'What is the range of short int','Objective','1','0','',1,''),(141,9,'Write a program to illustrate break.','Descriptive','1','0','',1,'');

/*Table structure for table `score_board_table` */

DROP TABLE IF EXISTS `score_board_table`;

CREATE TABLE `score_board_table` (
  `score_board_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'unque id for each score',
  `user_id` int(10) DEFAULT NULL COMMENT 'To identify the user',
  `test_id` int(10) DEFAULT NULL COMMENT 'To idenify the test',
  `sb_doa` datetime DEFAULT NULL COMMENT 'doa: Date Of Attempting test',
  `sb_time_taken` varchar(50) DEFAULT NULL COMMENT 'test start time.',
  `sb_in_time` datetime DEFAULT NULL COMMENT 'test end time',
  `sb_out_time` datetime DEFAULT NULL COMMENT 'test end time',
  `sb_attempt_que` int(10) DEFAULT NULL COMMENT 'no. of question attempt by the user for each test',
  `sb_correct_ans` int(10) DEFAULT NULL COMMENT 'no. of corect answer.',
  `sb_wrong_ans` int(10) DEFAULT NULL COMMENT 'no. of wrong answe.',
  `sb_marks_obt` varchar(100) DEFAULT NULL COMMENT 'marks obtain to the user for each test.',
  `sb_marks_out_of` varchar(100) DEFAULT NULL COMMENT 'out of marks i.e. sum of all question marks in a test',
  `sb_result_status` tinyint(1) NOT NULL,
  `sb_extra` varchar(500) DEFAULT NULL COMMENT 'extra field, future oriented.',
  PRIMARY KEY (`score_board_id`)
) ENGINE=MyISAM AUTO_INCREMENT=4 DEFAULT CHARSET=latin1 COMMENT='Record of all user attempted test.';

/*Data for the table `score_board_table` */

insert  into `score_board_table`(`score_board_id`,`user_id`,`test_id`,`sb_doa`,`sb_time_taken`,`sb_in_time`,`sb_out_time`,`sb_attempt_que`,`sb_correct_ans`,`sb_wrong_ans`,`sb_marks_obt`,`sb_marks_out_of`,`sb_result_status`,`sb_extra`) values (1,5,3,'2013-06-01 16:50:10','0:00:06','2013-06-01 16:50:04','2013-06-01 16:50:10',0,0,0,'0','100',0,NULL),(2,5,8,'2013-06-07 06:29:44','0:00:10','2013-06-07 06:29:34','2013-06-07 06:29:44',3,1,2,NULL,'3',0,NULL),(3,17,9,'2015-11-03 12:40:28','0:00:32','2015-11-03 12:39:56','2015-11-03 12:40:28',3,1,1,NULL,'3',0,NULL);

/*Table structure for table `share_result` */

DROP TABLE IF EXISTS `share_result`;

CREATE TABLE `share_result` (
  `share_id` int(10) NOT NULL AUTO_INCREMENT,
  `user_id` int(10) DEFAULT NULL,
  `test_id` int(10) DEFAULT NULL,
  `share_date` datetime DEFAULT NULL,
  `share_status` int(10) DEFAULT NULL,
  PRIMARY KEY (`share_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='this table stores record of shared result';

/*Data for the table `share_result` */

/*Table structure for table `stud_ans_table` */

DROP TABLE IF EXISTS `stud_ans_table`;

CREATE TABLE `stud_ans_table` (
  `user_ans_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id for every answer',
  `user_id` int(10) DEFAULT NULL COMMENT 'User id to identify the user',
  `test_id` int(10) DEFAULT NULL COMMENT 'test id to identify the test',
  `que_id` int(10) DEFAULT NULL COMMENT 'question id to identify the quetion',
  `user_ans_answer` varchar(5000) DEFAULT NULL COMMENT 'answer given by the user.',
  `op_correct_ans` varchar(500) NOT NULL,
  `attempt` int(20) NOT NULL COMMENT 'this field stores the attempt of exam',
  PRIMARY KEY (`user_ans_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1 COMMENT='Managing the student record of solve  test.';

/*Data for the table `stud_ans_table` */

insert  into `stud_ans_table`(`user_ans_id`,`user_id`,`test_id`,`que_id`,`user_ans_answer`,`op_correct_ans`,`attempt`) values (1,5,8,136,'450','',0),(2,5,8,137,'454','',0),(3,5,8,138,'9','',0),(4,5,8,138,'10','',0),(5,17,9,139,'459','',0),(6,17,9,140,'462','',0),(7,17,9,141,'break','',0);

/*Table structure for table `test_detail` */

DROP TABLE IF EXISTS `test_detail`;

CREATE TABLE `test_detail` (
  `test_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique test id for each test',
  `group_id` int(11) DEFAULT NULL COMMENT 'group id to add test in the available group',
  `test_name` varchar(100) DEFAULT NULL COMMENT 'test name given by the admin',
  `test_subject` varchar(100) DEFAULT NULL COMMENT 'test base on the subject',
  `test_desc` varchar(500) DEFAULT NULL COMMENT 'test description',
  `test_doa` date DEFAULT NULL COMMENT 'Date Of Adding Test ',
  `test_from` date DEFAULT NULL COMMENT 'Date of Test  Valid From  ',
  `test_to` date DEFAULT NULL COMMENT 'Date of Test  Valid Till  ',
  `test_duration` int(11) DEFAULT NULL COMMENT 'Time given to solve the test',
  `test_total_que` int(10) DEFAULT NULL COMMENT 'Total no. of question  in the test',
  `test_attempt_limit` int(10) DEFAULT NULL COMMENT 'No. of attempt given for a test to an individual user',
  `test_result_type` varchar(100) DEFAULT NULL COMMENT 'Display result instantly or pending .',
  `test_code` varchar(100) DEFAULT NULL COMMENT 'Code for each test, as a key to start the test. ',
  `test_status` tinyint(1) DEFAULT NULL COMMENT 'active:(1) or inactive (0) test',
  `test_neg_status` int(10) NOT NULL COMMENT 'Use To On or Off a Negative Marks in a test',
  `test_msg` varchar(500) NOT NULL COMMENT 'Message Fr User after completing the test',
  `test_scope` tinyint(1) NOT NULL COMMENT 'determine the test scope where as value 2 is use for public test and 1 is use for private test',
  `test_extra` varchar(500) DEFAULT NULL COMMENT 'extra field, future oriented',
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=latin1 COMMENT='Admin Managing the test detail here. ';

/*Data for the table `test_detail` */

insert  into `test_detail`(`test_id`,`group_id`,`test_name`,`test_subject`,`test_desc`,`test_doa`,`test_from`,`test_to`,`test_duration`,`test_total_que`,`test_attempt_limit`,`test_result_type`,`test_code`,`test_status`,`test_neg_status`,`test_msg`,`test_scope`,`test_extra`) values (1,1,'Plus91 Freshers','Common','Test For Freshers applying in plus91 2013','2013-05-10','2013-05-10','2015-11-30',60,15,1,'1','bFACD1',0,1,'Thank You for taking a test',2,NULL),(3,1,'Fresher&#039;s Aptitude','Aptitude','Round One Aptitude test, Question base aptitude and reasoning  ','2013-05-15','2013-05-15','2013-06-30',60,100,1,'1','eDABF2',1,0,'',2,NULL),(2,1,'Programmer Level 1','Computer','Programmer Level One : These test is  prepared for testing the technical skills and it is base on PHP.','2013-05-15','2013-05-15','2013-06-18',60,15,1,'1','faEBC1',0,1,'Thank You for submitting test, soon your result dispatch in your account.',1,NULL),(8,1,'Fresher&#039;s aptittude Two','Aptitude','Contain second round aptitude question&#039;s','2013-05-25','2013-05-25','2015-11-30',10,2,1,'1','DCfdd4',0,0,'Thank you! for taking second round aptitute test.',1,NULL),(9,1,'Technical Test&#039;s','Computer','Technical test base on c language. ','2013-05-25','2013-05-25','2015-12-01',10,3,1,'1','Bbbca9',1,1,'Thank you for taking &quot;Technical Test&#039;s&quot;, whish you a Best Luck !',2,NULL);

/*Table structure for table `user_details` */

DROP TABLE IF EXISTS `user_details`;

CREATE TABLE `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique to identify the user',
  `login_id` int(10) NOT NULL COMMENT 'use to identify username and password',
  `user_full_name` varchar(100) DEFAULT NULL COMMENT 'User Full  Name ',
  `user_email` varchar(100) DEFAULT NULL COMMENT 'User Email',
  `user_doreg` date DEFAULT NULL COMMENT 'Date of Registration (i.e. d-o-reg)',
  `user_contact` varchar(200) DEFAULT NULL COMMENT 'User Contact',
  `user_dob` date DEFAULT NULL COMMENT 'User Contact',
  `user_address` varchar(500) DEFAULT NULL COMMENT 'User Contact',
  `user_city` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_dist` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_state` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_country` varchar(100) DEFAULT NULL COMMENT 'User Contact',
  `user_status` tinyint(1) DEFAULT NULL COMMENT 'Active:(1) or Inactive:(0) User',
  `varification_code` varchar(300) DEFAULT NULL COMMENT 'This field stores unique code for each user and after varifying this through email user login activated',
  `user_about` varchar(1000) DEFAULT NULL COMMENT 'this field stores - something about user',
  `user_twt_link` varchar(300) DEFAULT NULL COMMENT 'this field stores - twiter profile link',
  `user_linkdin_link` varchar(300) DEFAULT NULL COMMENT 'this field stores - linkdin profile link of user',
  `user_git_link` varchar(300) DEFAULT NULL COMMENT 'this field stores - git hub link of user profile',
  `user_website` varchar(300) DEFAULT NULL COMMENT 'this field stores - own url of user',
  `user_extra` varchar(500) DEFAULT NULL COMMENT 'extra field',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM AUTO_INCREMENT=18 DEFAULT CHARSET=latin1 COMMENT='This table store User Details.';

/*Data for the table `user_details` */

insert  into `user_details`(`user_id`,`login_id`,`user_full_name`,`user_email`,`user_doreg`,`user_contact`,`user_dob`,`user_address`,`user_city`,`user_dist`,`user_state`,`user_country`,`user_status`,`varification_code`,`user_about`,`user_twt_link`,`user_linkdin_link`,`user_git_link`,`user_website`,`user_extra`) values (1,1,'Admin','updateadminemail@admin.com',NULL,'','0000-00-00','','','','Maharashtra','',1,NULL,'','','','','',NULL),(2,2,'laxmi','laxmikant.killekar@plus91.in','2013-05-11','9876543210','2013-05-11','','','','','',1,'e172e202cdc03888b18c56051e37b727','','','','','',NULL),(5,5,'Chetan Patil','csp567@gmail.com','2013-05-16','9970703657','0000-00-00','','','','Select State','Select Country',1,'ea2ee27a2ff32d09fccc3c8fac542f29','','','','','http://placementcell.vbirsm.org',NULL),(6,6,'Altaf','data@plus91.in','2013-05-16','7709326454',NULL,NULL,NULL,NULL,NULL,NULL,1,'36c12628e5fe655f53e46d5d3fb1add7',NULL,NULL,NULL,NULL,NULL,NULL),(7,7,'Mitu Nikhra','m.nikhra@plus91.in','2013-05-16','9372804891',NULL,NULL,NULL,NULL,NULL,NULL,1,'01411a37e72020cac3955306d7288190',NULL,NULL,NULL,NULL,NULL,NULL),(8,8,'Kishan Gor','kishan.gor@plus91.in','2013-05-16','9730659516',NULL,NULL,NULL,NULL,NULL,NULL,1,'31cd9422da613ea1852acb8aaa50a091',NULL,NULL,NULL,NULL,NULL,NULL),(9,9,'Snehal','snehal.bokey@plus91.in','2013-05-16','9561076459',NULL,NULL,NULL,NULL,NULL,NULL,1,'3684adad093722549d9f9277f2559284',NULL,NULL,NULL,NULL,NULL,NULL),(10,10,'Shraddha','shraddha.wankhade@plus91.in','2013-05-16','9960789386',NULL,NULL,NULL,NULL,NULL,NULL,1,'d048ef9cd6617726e0066db7789696b0',NULL,NULL,NULL,NULL,NULL,NULL),(11,11,'Meenu','meenu@plus91.in','2013-05-16','9272886521','0000-00-00','','','','','',1,'4e89462c64270382d1938a72d38876b8','','','','','',NULL),(12,12,'Ajay','r-ajay.mandera@plus91.in','2013-05-16','9999999999',NULL,NULL,NULL,NULL,NULL,NULL,1,'261c6bacc55916f42fbcef544c789e03',NULL,NULL,NULL,NULL,NULL,NULL),(13,13,'Dhanashree ','dhanashree.kumkar@plus91.in','2013-05-16','9999999999',NULL,NULL,NULL,NULL,NULL,NULL,1,'8bc4850745a92e27e176b591f11feea8',NULL,NULL,NULL,NULL,NULL,NULL),(14,14,'Ravi','ravisahani.plus91@gmail.com','2013-05-16','8862064649','0000-00-00','','','','','',1,'74e9fbaedbfd4225c9b66252807350b2',NULL,NULL,NULL,NULL,NULL,NULL),(16,16,'dhanashri','dhanashri.mude@plus91.in','2014-02-08','9405418035','0000-00-00','','','','','',0,'de564d49e16f4523626fe595d791ab8b',NULL,NULL,NULL,NULL,NULL,NULL),(17,17,'Ajay','ajay.mandera@plus91.in','2015-11-03','7875889186','0000-00-00','','','','','',1,'1ad7467bfe2c39815ece4e0032896b2c',NULL,NULL,NULL,NULL,NULL,NULL);

/*Table structure for table `user_resume` */

DROP TABLE IF EXISTS `user_resume`;

CREATE TABLE `user_resume` (
  `res_id` int(50) NOT NULL AUTO_INCREMENT COMMENT 'this field stores unique id for every resume location.',
  `user_id` int(50) NOT NULL COMMENT 'this field stores user id of user',
  `test_id` int(50) NOT NULL COMMENT 'This field stores the test id of the test for which he/she apply',
  `location` varchar(500) NOT NULL COMMENT 'this field stores the location and resume  file name',
  `filename` varchar(500) NOT NULL COMMENT 'this field Stores file name',
  `resume_status` int(11) NOT NULL COMMENT 'this field Stores file name',
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1 COMMENT='This table is used to store student resume';

/*Data for the table `user_resume` */

insert  into `user_resume`(`res_id`,`user_id`,`test_id`,`location`,`filename`,`resume_status`) values (1,2,0,'Resume/','2_857c0007bbbbc25d437f272f3ba7d0c7.docx',1),(2,5,0,'Resume/','5_9d151c48d2a8eedc0784a0ef3e78a124.doc',1),(3,6,0,'Resume/','6_5228681a48e6a9476c778514412467fd.docx',1),(4,17,0,'Resume/','17_3bf63f5e31980fd3bfdef8d91f974e94.docx',1);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
