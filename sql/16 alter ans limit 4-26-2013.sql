ALTER TABLE `stud_ans_table`
	CHANGE COLUMN `user_ans_answer` `user_ans_answer` VARCHAR(2000) NOT NULL DEFAULT '0' COMMENT 'answer given by the user.' AFTER `que_id`;
