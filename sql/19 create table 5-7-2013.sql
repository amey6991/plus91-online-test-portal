CREATE TABLE `share_result` (
	`share_id` INT(10) NULL AUTO_INCREMENT,
	`user_id` INT(10) NULL,
	`test_id` INT(10) NULL,
	`share_status` INT(10) NULL,
	PRIMARY KEY (`share_id`)
)
COMMENT='this table stores record of shared result'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;