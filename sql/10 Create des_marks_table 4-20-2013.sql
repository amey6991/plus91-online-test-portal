-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 20, 2013 at 10:14 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `des_marks_table`
--

CREATE TABLE IF NOT EXISTS `des_marks_table` (
  `des_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'primary key',
  `user_id` int(11) NOT NULL COMMENT 'To identify the User',
  `test_id` int(11) NOT NULL COMMENT 'To identify the test',
  `que_id` int(11) NOT NULL COMMENT 'To identify the que',
  `des_marks` int(11) NOT NULL COMMENT 'To store the marks score by the user for descriptive question',
  PRIMARY KEY (`des_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Table to store the marks score by the user for descriptive question' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
