

CREATE TABLE IF NOT EXISTS `allot_test` (
  `allot_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primark Key',
  `user_id` int(11) NOT NULL COMMENT 'To Refer the User details',
  `group_id` int(11) NOT NULL COMMENT 'To Refer Group details',
  `test_id` int(11) NOT NULL COMMENT 'To Refer Test Details',
  PRIMARY KEY (`allot_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Allow to allot test individually' AUTO_INCREMENT=1 ;