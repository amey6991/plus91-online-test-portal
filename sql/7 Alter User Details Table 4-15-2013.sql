ALTER TABLE `user_details`
	ADD COLUMN `user_dob` DATE NULL DEFAULT NULL COMMENT 'User Contact' AFTER `user_contact`,
	ADD COLUMN `user_address` VARCHAR(500) NULL DEFAULT NULL COMMENT 'User Contact' AFTER `user_dob`,
	ADD COLUMN `user_city` VARCHAR(100) NULL DEFAULT NULL COMMENT 'User Contact' AFTER `user_address`,
	ADD COLUMN `user_dist` VARCHAR(100) NULL DEFAULT NULL COMMENT 'User Contact' AFTER `user_city`,
	ADD COLUMN `user_state` VARCHAR(100) NULL DEFAULT NULL COMMENT 'User Contact' AFTER `user_dist`,
	ADD COLUMN `user_country` VARCHAR(100) NULL DEFAULT NULL COMMENT 'User Contact' AFTER `user_state`;
