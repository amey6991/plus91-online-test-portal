ALTER TABLE `user_details`
	ADD COLUMN `user_about` VARCHAR(1000) NULL DEFAULT NULL COMMENT 'this field stores - something about user' AFTER `varification_code`,
	ADD COLUMN `user_twt_link` VARCHAR(300) NULL DEFAULT NULL COMMENT 'this field stores - twiter profile link' AFTER `user_about`,
	ADD COLUMN `user_linkdin_link` VARCHAR(300) NULL DEFAULT NULL COMMENT 'this field stores - linkdin profile link of user' AFTER `user_twt_link`,
	ADD COLUMN `user_git_link` VARCHAR(300) NULL DEFAULT NULL COMMENT 'this field stores - git hub link of user profile' AFTER `user_linkdin_link`,
  	ADD COLUMN `user_website` VARCHAR(300) NULL DEFAULT NULL COMMENT 'this field stores - own url of user' AFTER `user_git_link`,
	ADD COLUMN `user_extra` VARCHAR(500) NULL DEFAULT NULL COMMENT 'extra field' AFTER `user_website`;
