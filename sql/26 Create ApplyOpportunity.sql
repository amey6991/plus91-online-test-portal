CREATE TABLE IF NOT EXISTS `apply_opportunity` (
  `ap_opp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `opp_id` int(11) NOT NULL COMMENT 'Work as a foreign key to identify the Opportunity.',
  `user_id` int(11) NOT NULL COMMENT 'Works as Foreign keey to identify the user',
  `ap_opp_doa` date NOT NULL COMMENT 'Date of Appling to an opportunity',
  `ap_opp_status` int(11) NOT NULL COMMENT 'Determine the user applid to the opportunity or not',
  PRIMARY KEY (`ap_opp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table use to maintain the record of a user whose appling through a opportun' AUTO_INCREMENT=1 ;