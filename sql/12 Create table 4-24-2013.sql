CREATE TABLE `login_system_info` (
	`logId` INT(20) NOT NULL AUTO_INCREMENT COMMENT 'this field stores unique id for each row in this table',
	`user_id` INT(20) NOT NULL COMMENT 'this field stores user id of user',
	`test_id` INT(20) NOT NULL COMMENT 'this field stores test id of test that user selected',
	`sys_ip` VARCHAR(100) NOT NULL COMMENT 'this field stores the ip address of user system',
	`sys_os` VARCHAR(100) NOT NULL COMMENT 'this field stores operating system name of users system',
	`sys_browser` VARCHAR(100) NOT NULL COMMENT 'this field stores browser name that user uses to acces our website',
	PRIMARY KEY (`logId`)
)
COMMENT='this table stores information like IP , operating system , browser of user system.'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;
