-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Apr 01, 2013 at 10:11 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `group`
--

CREATE TABLE IF NOT EXISTS `group` (
  `group_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'unique for group table',
  `group_name` varchar(100) DEFAULT NULL COMMENT 'group name given by admin',
  `group_desc` varchar(500) DEFAULT NULL COMMENT 'description about group',
  `group_doc` varchar(100) DEFAULT NULL COMMENT 'Date Of Creating Group ',
  `group_status` tinyint(1) DEFAULT NULL COMMENT 'Active:(1) or Inactive:(0) the Group',
  `group_extra` varchar(500) DEFAULT NULL COMMENT 'Extra field, future oriented',
  PRIMARY KEY (`group_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='manage the group created by admin' AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `login_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Unique to value for each  Login',
  `user_name` varchar(100) NOT NULL COMMENT 'user name to login',
  `password` varchar(100) NOT NULL COMMENT 'password to login',
  `type` tinyint(1) NOT NULL COMMENT '1 for user and 0 for admin',
  PRIMARY KEY (`login_id`),
  UNIQUE KEY `user_name` (`user_name`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Table to store admin username and password. ' AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `option_table`
--

CREATE TABLE IF NOT EXISTS `option_table` (
  `op_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique for each option',
  `que_id` int(10) DEFAULT NULL COMMENT 'To identify the question for the option.',
  `op_option` varchar(500) DEFAULT NULL COMMENT 'Contia option values, may be more than two for a same question.',
  `op_correct_ans` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`op_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Manage no. of option for a question.' AUTO_INCREMENT=81 ;

-- --------------------------------------------------------

--
-- Table structure for table `question_table`
--

CREATE TABLE IF NOT EXISTS `question_table` (
  `que_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique question id for each question',
  `test_id` int(10) DEFAULT NULL COMMENT 'Test id to include question in the specified test.',
  `que_question` varchar(500) DEFAULT NULL COMMENT 'Question to be ask.',
  `que_type` varchar(100) DEFAULT NULL COMMENT 'Question either objective or Describtive ',
  `que_marks` int(10) DEFAULT NULL COMMENT 'marks per question',
  `que_neg_marks` int(10) DEFAULT NULL COMMENT 'Nagative mark for the question.',
  `que_ans_desc` varchar(500) DEFAULT NULL COMMENT 'Answer Description',
  `que_status` tinyint(1) DEFAULT NULL COMMENT 'active:(1) or inactive:(0) ',
  `que_extra` varchar(500) DEFAULT NULL COMMENT 'Extra feld, Future Oriented',
  PRIMARY KEY (`que_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Managing all the question required for test.' AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `score_board_table`
--

CREATE TABLE IF NOT EXISTS `score_board_table` (
  `score_board_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'unque id for each score',
  `user_id` int(10) DEFAULT NULL COMMENT 'To identify the user',
  `test_id` int(10) DEFAULT NULL COMMENT 'To idenify the test',
  `sb_doa` datetime DEFAULT NULL COMMENT 'doa: Date Of Attempting test',
  `sb_start_time` datetime DEFAULT NULL COMMENT 'test start time.',
  `sb_end_time` datetime DEFAULT NULL COMMENT 'test end time',
  `sb_attempt_que` int(10) DEFAULT NULL COMMENT 'no. of question attempt by the user for each test',
  `sb_correct_ans` int(10) DEFAULT NULL COMMENT 'no. of corect answer.',
  `sb_wrong_ans` int(10) DEFAULT NULL COMMENT 'no. of wrong answe.',
  `sb_marks_obt` int(10) DEFAULT NULL COMMENT 'marks obtain to the user for each test.',
  `sb_marks_out_of` int(10) DEFAULT NULL COMMENT 'out of marks i.e. sum of all question marks in a test',
  `sb_result_status` tinyint(1) NOT NULL,
  `sb_extra` varchar(500) DEFAULT NULL COMMENT 'extra field, future oriented.',
  PRIMARY KEY (`score_board_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1 COMMENT='Record of all user attempted test.' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `stud_ans_table`
--

CREATE TABLE IF NOT EXISTS `stud_ans_table` (
  `user_ans_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique id for every answer',
  `user_id` int(10) DEFAULT NULL COMMENT 'User id to identify the user',
  `test_id` int(10) DEFAULT NULL COMMENT 'test id to identify the test',
  `que_id` int(10) DEFAULT NULL COMMENT 'question id to identify the quetion',
  `user_ans_answer` varchar(500) DEFAULT NULL COMMENT 'answer given by the user.',
  `op_correct_ans` varchar(500) NOT NULL,
  `attempt` int(20) NOT NULL COMMENT 'this field stores the attempt of exam',
  PRIMARY KEY (`user_ans_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Managing the student record of solve  test.' AUTO_INCREMENT=326 ;

-- --------------------------------------------------------

--
-- Table structure for table `test_detail`
--

CREATE TABLE IF NOT EXISTS `test_detail` (
  `test_id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Unique test id for each test',
  `group_id` int(11) DEFAULT NULL COMMENT 'group id to add test in the available group',
  `test_name` varchar(100) DEFAULT NULL COMMENT 'test name given by the admin',
  `test_subject` varchar(100) DEFAULT NULL COMMENT 'test base on the subject',
  `test_desc` varchar(500) DEFAULT NULL COMMENT 'test description',
  `test_doa` date DEFAULT NULL COMMENT 'Date Of Adding Test ',
  `test_from` date DEFAULT NULL COMMENT 'Date of Test  Valid From  ',
  `test_to` date DEFAULT NULL COMMENT 'Date of Test  Valid Till  ',
  `test_duration` int(11) DEFAULT NULL COMMENT 'Time given to solve the test',
  `test_total_que` int(10) DEFAULT NULL COMMENT 'Total no. of question  in the test',
  `test_attempt_limit` int(10) DEFAULT NULL COMMENT 'No. of attempt given for a test to an individual user',
  `test_result_type` varchar(100) DEFAULT NULL COMMENT 'Display result instantly or pending .',
  `test_code` varchar(100) DEFAULT NULL COMMENT 'Code for each test, as a key to start the test. ',
  `test_status` tinyint(1) DEFAULT NULL COMMENT 'active:(1) or inactive (0) test',
  `test_extra` varchar(500) DEFAULT NULL COMMENT 'extra field, future oriented',
  PRIMARY KEY (`test_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='Admin Managing the test detail here. ' AUTO_INCREMENT=6 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_details`
--

CREATE TABLE IF NOT EXISTS `user_details` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'unique to identify the user',
  `login_id` int(10) NOT NULL COMMENT 'use to identify username and password',
  `user_full_name` varchar(100) DEFAULT NULL COMMENT 'User Full  Name ',
  `user_email` varchar(100) DEFAULT NULL COMMENT 'User Email',
  `user_doreg` date DEFAULT NULL COMMENT 'Date of Registration (i.e. d-o-reg)',
  `user_contact` varchar(200) DEFAULT NULL COMMENT 'User Contact',
  `user_status` tinyint(1) DEFAULT NULL COMMENT 'Active:(1) or Inactive:(0) User',
  `user_extra` varchar(500) DEFAULT NULL COMMENT 'Extra field, future oriented',
  PRIMARY KEY (`user_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 COMMENT='This table store User Details.' AUTO_INCREMENT=5 ;

-- --------------------------------------------------------

--
-- Table structure for table `user_resume`
--

CREATE TABLE IF NOT EXISTS `user_resume` (
  `res_id` int(50) NOT NULL AUTO_INCREMENT COMMENT 'this field stores unique id for every resume location.',
  `user_id` int(50) NOT NULL COMMENT 'this field stores user id of user',
  `test_id` int(50) NOT NULL COMMENT 'This field stores the test id of the test for which he/she apply',
  `location` varchar(500) NOT NULL COMMENT 'this field stores the location and resume  file name',
  `filename` varchar(500) NOT NULL COMMENT 'this field Stores file name',
  PRIMARY KEY (`res_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This table is used to store student resume' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
