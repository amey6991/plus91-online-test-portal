CREATE TABLE IF NOT EXISTS `opportunity_test` (
  `opp_test_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primark Key',
  `opp_id` int(11) NOT NULL COMMENT 'Use to get opportunity',
  `test_id` int(11) NOT NULL COMMENT 'Use to get test ',
  `opp_test_status` int(11) NOT NULL COMMENT 'Determine the status of opportunity test',
  PRIMARY KEY (`opp_test_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='opportunity_test table hold all that test which are added in the opportunity.' AUTO_INCREMENT=1 ;