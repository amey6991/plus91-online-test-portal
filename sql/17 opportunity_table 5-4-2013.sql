-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 04, 2013 at 01:59 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `opportunity_table`
--

CREATE TABLE IF NOT EXISTS `opportunity_table` (
  `opp_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Primary key',
  `opp_name` varchar(500) NOT NULL COMMENT 'Opportunity Name',
  `opp_code` varchar(100) NOT NULL COMMENT 'Opportunity Code',
  `opp_des` varchar(5000) NOT NULL COMMENT 'Opportunity Description',
  `opp_status` int(1) NOT NULL COMMENT 'Opportunity Status',
  PRIMARY KEY (`opp_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Contain the Opportunity Details' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
