CREATE TABLE `question_attachment` (
	`attach_id` INT(10) NULL AUTO_INCREMENT COMMENT 'primary key',
	`que_id` INT(10) NULL COMMENT 'question id',
	`que_type` VARCHAR(100) NULL COMMENT 'question type',
	`location` VARCHAR(100) NULL COMMENT 'question type',
	`file_name` VARCHAR(500) NULL COMMENT 'question type',
	`file_status` INT NULL COMMENT 'question type',
	PRIMARY KEY (`attach_id`)
)
COMMENT='this table stores attache file address with any question'
COLLATE='latin1_swedish_ci'
ENGINE=InnoDB;
