ALTER TABLE `score_board_table`
	ADD COLUMN `sb_in_time` DATETIME NULL DEFAULT NULL COMMENT 'test end time' AFTER `sb_time_taken`,
	CHANGE COLUMN `sb_end_time` `sb_out_time` DATETIME NULL DEFAULT NULL COMMENT 'test end time' AFTER `sb_in_time`;
