-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 07, 2013 at 11:51 AM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `quiz_online`
--

-- --------------------------------------------------------

--
-- Table structure for table `multi_option_table`
--

CREATE TABLE IF NOT EXISTS `multi_option_table` (
  `multi_op_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'is a primary key',
  `que_id` int(11) NOT NULL COMMENT 'Question Id to maintain the relationship with que table.',
  `multi_option` varchar(1000) NOT NULL COMMENT 'Storing the multiple opption.',
  `multi_op_ans` varchar(100) NOT NULL COMMENT 'Use to know which answer is acceptable or not. Value: 1 is for answer acceptable and 0 depend on evaluator.',
  PRIMARY KEY (`multi_op_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Use to Maintain the multiple choice question' AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
