<?php
	include('classConnectQA.php');

    error_reporting(0);
    session_start();
    error_reporting(0);
    if(isset($_GET['qtp']))
    {
        $sQueType = $_GET['qtp'];
    }
    else
    {
        $sQueType = 0;
    }
	if(isset($_GET['quid']))
	{
		$iQueId = $_GET['quid'];
	}
	else
	{
		$iQueId = "";
	}
	$error = "";
	$msg = "";
	$fileElementName = 'fileToUpload';
	if(!empty($_FILES[$fileElementName]['error']))
	{
		switch($_FILES[$fileElementName]['error'])
		{

			case '1':
				$error = 'The uploaded file exceeds the upload_max_filesize ,You cant upload more than 2 MB.';
				break;
			case '2':
				$error = 'The uploaded file exceeds the MAX_FILE_SIZE . You cant upload more than 2 MB.';
				break;
			case '3':
				$error = 'The uploaded file was only partially uploaded';
				break;
			case '4':
				$error = 'No file was uploaded.';
				break;

			case '6':
				$error = 'Missing a temporary folder';
				break;
			case '7':
				$error = 'Failed to write file to disk';
				break;
			case '8':
				$error = 'File upload stopped by extension';
				break;
			case '999':
			default:
				$error = 'No error code avaiable';
		}
	}elseif(empty($_FILES['fileToUpload']['tmp_name']) || $_FILES['fileToUpload']['tmp_name'] == 'none')
	{
		$error = 'No file was uploaded..';
	}else 
	{

			$msg .= " File Name: " . $_FILES['fileToUpload']['name'] . ", ";
			$msg .= " File Size: " . @filesize($_FILES['fileToUpload']['tmp_name']);

			$sOrgFileName = $_FILES["fileToUpload"]["name"];
            $sFileContentType = $_FILES["fileToUpload"]["type"];
            $iFileSize = $_FILES["fileToUpload"]["size"];

            if($iFileSize >= 2097152)
            {
                 $error = "You cant upload file more than 2 MB.";
                //$sUnknownFileFormat = 249;
                echo "{";
                echo  "error: '" . $error . "',\n";
                echo  "msg: '" . $msg . "'\n";
                echo "}";
                
                exit();
            }

            $sGetFileExt = strtolower(substr($sOrgFileName, -3, 3));
                    /* This Finds file extension from file name */
            $sFileExt ="";

            if( $sGetFileExt == "jpg" )
            {
              $sFileExt = ".jpg";
            }
            if( $sGetFileExt == "jpeg" )
            {
              $sFileExt = ".jpeg";
            }
            if( $sGetFileExt == "JPEG" )
            {
              $sFileExt = ".JPEG";
            }
            if( $sGetFileExt == "gif" )
            {
              $sFileExt = ".gif";
            }
            if( $sGetFileExt == "png" )
            {
              $sFileExt = ".png";
            }
            if($sFileExt == ".jpg" || $sFileExt == ".png" || $sFileExt == ".jpeg" || $sFileExt == ".JPEG" || $sFileExt == ".gif")
            {
               
            }
            else
            {
                $error = "You can upload only .jpg , .jpeg , .png or .gif only";
                //$sUnknownFileFormat = 249;
                echo "{";
                echo  "error: '" . $error . "',\n";
                echo  "msg: '" . $msg . "'\n";
                echo "}";
                //header("location:profileedit.php?msg={$sUnknownFileFormat}");
                exit();
            }
             function getExtension($str) {
                     $i = strrpos($str,".");
                     if (!$i) { return ""; }
                     $l = strlen($str) - $i;
                     $ext = substr($str,$i+1,$l);
                     return $ext;
             }
            $iRandomDigit = uniqid($iQueId); //! This generate the unique number using uniqid function.
            $iTestId = 0;
            //$sQueType = "Descriptive";
            $sNewFileName = $iQueId."_".md5($sOrgFileName . $iRandomDigit).$sFileExt;
            $sLocation = "questionAttachMent/";
			$sResumeCheckAttach = "select count(que_id) from question_attachment where que_id = {$iQueId} AND file_status = 1";
            $iCheckAttach = $mysqli->query($sResumeCheckAttach);
            $iCheckRowResume = $iCheckAttach->fetch_row();
            if($iCheckRowResume[0] >=1)
            {
                $sUpdateResume = "UPDATE question_attachment SET file_status = 0 where que_id = $iQueId AND file_status = 1";
                $mysqli->query($sUpdateResume);

                //! Add resume location and name to database .
                $sInsertQuery = "INSERT INTO question_attachment (que_id, que_type, location, file_name ,file_status) 
                                VALUES ('$iQueId','$sQueType', '$sLocation' , '$sNewFileName' , 1)";
                $mysqli->query($sInsertQuery);

                /* this code return the compressed image */
                
                $image = $_FILES["fileToUpload"]["name"];
                $uploadedfile = $_FILES["fileToUpload"]['tmp_name'];
                if ($image) 
                {

                    $filename = stripslashes($_FILES["fileToUpload"]["name"]);

                    $extension = getExtension($filename);
                    $extension = strtolower($extension);
                    if($extension=="jpg" || $extension=="jpeg" )
                    {
                        $uploadedfile = $_FILES["fileToUpload"]['tmp_name'];
                        $src = imagecreatefromjpeg($uploadedfile);

                    }
                    else if($extension=="png")
                    {
                        $uploadedfile = $_FILES["fileToUpload"]['tmp_name'];
                        $src = imagecreatefrompng($uploadedfile);

                    }
                    else 
                    {
                        $src = imagecreatefromgif($uploadedfile);
                    }

                    list($width,$height)=getimagesize($uploadedfile);
                    
                    $newwidth=400;
                    $newheight=250;
                    $tmp=imagecreatetruecolor($newwidth,$newheight);

                    imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                    $filename = "questionAttachMent/". $sNewFileName;

                    imagejpeg($tmp,$filename,100);

                    imagedestroy($src);
                    imagedestroy($tmp);
                }
                 //! move uploaded file to the specified foder with new name
                //move_uploaded_file($_FILES['fileToUpload']["tmp_name"],"questionAttachMent/" . $sNewFileName);
            }
            else
            {
                //! Add resume location and name to database .
                $sInsertQuery = "INSERT INTO question_attachment (que_id, que_type, location, file_name ,file_status) 
                                VALUES ('$iQueId','$sQueType', '$sLocation' , '$sNewFileName' , 1)";
                $mysqli->query($sInsertQuery);

                /* this code return the compressed image */
                
                $image = $_FILES["fileToUpload"]["name"];
                $uploadedfile = $_FILES["fileToUpload"]['tmp_name'];
                if ($image) 
                {

                    $filename = stripslashes($_FILES["fileToUpload"]["name"]);

                    $extension = getExtension($filename);
                    $extension = strtolower($extension);
                    if($extension=="jpg" || $extension=="jpeg" )
                    {
                        $uploadedfile = $_FILES["fileToUpload"]['tmp_name'];
                        $src = imagecreatefromjpeg($uploadedfile);

                    }
                    else if($extension=="png")
                    {
                        $uploadedfile = $_FILES["fileToUpload"]['tmp_name'];
                        $src = imagecreatefrompng($uploadedfile);

                    }
                    else 
                    {
                        $src = imagecreatefromgif($uploadedfile);
                    }

                    list($width,$height)=getimagesize($uploadedfile);
                    
                    $newwidth=400;
                    $newheight=250;
                    $tmp=imagecreatetruecolor($newwidth,$newheight);

                    imagecopyresampled($tmp,$src,0,0,0,0,$newwidth,$newheight,$width,$height);

                    $filename = "questionAttachMent/". $sNewFileName;

                    imagejpeg($tmp,$filename,100);

                    imagedestroy($src);
                    imagedestroy($tmp);
                }
                 //! move uploaded file to the specified foder with new name
                //move_uploaded_file($_FILES['fileToUpload']["tmp_name"],"questionAttachMent/" . $sNewFileName);
            }
           
	
	}		
	echo "{";
	echo				"error: '" . $error . "',\n";
	echo				"msg: '" . $msg . "'\n";
	echo "}";
?>